﻿namespace DuckGame.MserDuck.ConfigEntries
{
    public class ArgumentErrorsConnectionEntry : ConfigEntry
    {
        /// <summary>
        /// The name of the ConfigEntry in the config.cfg file.
        /// </summary>
        public override string GetName()
        {
            return "ShowArgumentErrorsConnection";
        }

        /// <summary>
        /// The description comments of the config entry, to add above it. The commenting string (ConfigManager.commentString) is added on its own.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[] {"While entering commands, ArgumentError popups (if enabled) will have a line shown connecting to the corresponding argument."};
        }

        /// <summary>
        /// The default value of the ConfigEntry when first generated.
        /// </summary>
        /// <returns></returns>
        public override object GetDefaultValue()
        {
            return true;
        }

        /// <summary>
        /// Gets invoked after the value is read. If the key does not exist, this does not get executed.
        /// </summary>
        public override void AfterRead()
        {
            ChatManager.ArgumentErrorsConnection = bool.Parse(this.Value.ToString());
        }
    }
}
