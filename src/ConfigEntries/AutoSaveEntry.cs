﻿namespace DuckGame.MserDuck.ConfigEntries
{
    public class AutoSaveEntry : ConfigEntry
    {
        /// <summary>
        /// The name of the ConfigEntry in the config.cfg file.
        /// </summary>
        public override string GetName()
        {
            return "AutoSave";
        }

        /// <summary>
        /// The default value of the ConfigEntry when first generated.
        /// </summary>
        /// <returns></returns>
        public override object GetDefaultValue()
        {
            return false;
        }

        /// <summary>
        /// The category of this config entry.
        /// </summary>
        /// <returns></returns>
        public override string GetCategory()
        {
            return "General";
        }

        /// <summary>
        /// The description comments of the config entry, to add above it. The commenting string (ConfigManager.commentString) is added on its own.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[] {"When enabled, saves the config file once DuckGame is closing."};
        }

        /// <summary>
        /// Gets invoked after the value is read.
        /// </summary>
        public override void AfterRead()
        {
            ConfigManager.AutoSave = bool.Parse(this.Value.ToString());
        }
    }
}
