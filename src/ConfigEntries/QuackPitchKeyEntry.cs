﻿using System;

namespace DuckGame.MserDuck.ConfigEntries
{
    public class QuackPitchKeyEntry : ConfigEntry
    {
        /// <summary>
        /// The name of the ConfigEntry in the config.cfg file.
        /// </summary>
        public override string GetName()
        {
            return "QuackPitchKey";
        }

        /// <summary>
        /// The description comments of the config entry, to add above it. The commenting string (ConfigManager.commentString) is added on its own.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[]
            {
                "Which key should be held in order to pitch quacking using the mouse.",
                "Supports both trigger names (e.g. STRAFE, CHAT, etc) and key names (e.g. LeftCtrl, LeftAlt, ...)",
                "Specify \"always\" to skip the check, making the quack always pitch according to the mouse."
            };
        }

        /// <summary>
        /// The category of this config entry.
        /// </summary>
        /// <returns></returns>
        public override string GetCategory()
        {
            return "Gameplay";
        }

        /// <summary>
        /// The default value of the ConfigEntry when first generated.
        /// </summary>
        /// <returns></returns>
        public override object GetDefaultValue()
        {
            return "STRAFE";
        }

        /// <summary>
        /// Gets invoked after the value is read. If the key does not exist, this does not get executed.
        /// </summary>
        public override void AfterRead()
        {
            var name = this.Value.ToString();

            //Always?
            if (name.EqualsTo(true, "always"))
            {
                //Always!
                DuckBehaviourHandler.QuackKey = Keys.None;
                DuckBehaviourHandler.QuackTrigger = "ALWAYS";
                return;
            }

            //Check action names first
            var trigs = Triggers.toIndex;
            byte index;
            if (trigs.TryGetValue(name, out index))
            {
                //Is a trigger!
                DuckBehaviourHandler.QuackKey = Keys.None;
                DuckBehaviourHandler.QuackTrigger = name;
                return;
            }

            //Check key names
            Keys key;
            Enum.TryParse(name, true, out key);
            DuckBehaviourHandler.QuackKey = key;
            DuckBehaviourHandler.QuackTrigger = null;
        }
    }
}
