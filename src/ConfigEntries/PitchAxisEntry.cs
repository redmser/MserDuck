﻿namespace DuckGame.MserDuck.ConfigEntries
{
    public class PitchAxisEntry : ConfigEntry
    {
        /// <summary>
        /// The name of the ConfigEntry in the config.cfg file.
        /// </summary>
        public override string GetName()
        {
            return "PitchAxis";
        }

        /// <summary>
        /// The description comments of the config entry, to add above it. The commenting string (ConfigManager.commentString) is added on its own.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[]
            {
                "Changes the mouse axis for pitching instruments.",
                "0 = No mouse pitch",
                "1 = X-Axis (Left/Right)",
                "2 = Y-Axis (Up/Down)",
            };
        }

        /// <summary>
        /// The default value of the ConfigEntry when first generated.
        /// </summary>
        /// <returns></returns>
        public override object GetDefaultValue()
        {
            return 1;
        }

        /// <summary>
        /// The category of this config entry.
        /// </summary>
        /// <returns></returns>
        public override string GetCategory()
        {
            return "Gameplay";
        }

        /// <summary>
        /// Gets invoked after the value is read. If the key does not exist, this does not get executed.
        /// </summary>
        public override void AfterRead()
        {
            Instrument.PitchAxis = int.Parse(this.Value.ToString());
        }
    }
}
