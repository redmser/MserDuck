﻿using System;
using System.Collections.Generic;

namespace DuckGame.MserDuck.CommandHandlers
{
	public class VocaQuackCommand : CommandHandler
	{
        /// <summary>
        /// How loud should you have to SCREAAMM(!) before that lazy duck
        /// finally manages to quack.
        /// </summary>
        private static double _quackThreshold = 1000000;

		/// <summary>
		/// Used for smoothing, to prevent your beautiful legato voice from triggering
		/// an unholy barrage of rapid-fire quacks. 8 is a nice number.
		/// </summary>
		private const int HistorySize = 8;

		/// <summary>
		/// Should the duck ACTUALLY quack, or just open its mouth in astonishment
		/// at your heavenly vocalisations.
		/// </summary>
		private static bool _playQuack = true;

		/// <summary>
		/// How much milliseconds of data should the duck-NSA collect from your
		/// mic at once?
		/// </summary>
		private static readonly TimeSpan BufferSize = TimeSpan.FromMilliseconds(100);

		/// <summary>
		/// Shoo, stay away from this!
		/// </summary>
		private static bool _enabled = false; // <(O(oo)O)>

		/// <summary>
		/// Your microphone. It is all in here. You're not getting it back!
		/// </summary>
		private static Microsoft.Xna.Framework.Audio.Microphone _microphone = null;

		/// <summary>
		/// When a sound buffer is null, and you scream, do you even make a sound?
		/// </summary>
		private static byte[] _soundBuffer = null;

		/// <summary>
		/// 2nd print.
		/// </summary>
		private static bool[] _aRecentHistoryOfDucks = new bool[HistorySize];

		/// <summary>
		/// If you touch this, ducks might quack at your toaster.
		/// </summary>
		public static bool Enabled
		{
			get
			{
				return _enabled;
			}

			set
			{
				if (!value)
				{
					_microphone.Stop();
					_microphone = null;
					_soundBuffer = null;
				}
				_enabled = value;
			}
		}

		/// <summary>
		/// Called from the game's main loop. Processes the data from the mic.
		/// </summary>
		public static void Update()
		{
			if (!Enabled)
			{
				// go away.
				return;
			}

			Duck duk = MserGlobals.GetLocalDuck();
			if (duk == null || duk.dead)
			{
                //Out of luck, gone is duk.
                return;
			}

			if (_microphone == null)
			{
				_microphone = Microsoft.Xna.Framework.Audio.Microphone.Default;
				if (_microphone == null)
				{
                    //sad.
                    Enabled = false;
					ChatManager.Echo("No default microphone found.");
					ChatManager.Echo("You can talk all you want, but no quacks for you >:(");
					return;
				}
			}

			//If we ACTUALLY manage to get here (probs not) we got at least a non null mic.
			//Pray that it actually works.

			if (_soundBuffer == null)
			{
				_microphone.BufferDuration = BufferSize;
				_soundBuffer = new byte[_microphone.GetSampleSizeInBytes(BufferSize)];
				_microphone.Start();
			}

			int amountOfCowsProcessed = _microphone.GetData(_soundBuffer);

			short val;
			double volume = 0;

			for (int i = 0; i < amountOfCowsProcessed-1; i+=2)
			{
				val = (short)(_soundBuffer[i] | (_soundBuffer[i + 1] << 8));
				volume += val * val;
			}

			volume /= (double)(amountOfCowsProcessed / 2);

			int truthiness = (volume > _quackThreshold ? 1 : 0);

			for (int i = 0; i < HistorySize-1; ++i)
			{
				_aRecentHistoryOfDucks[i] = _aRecentHistoryOfDucks[i + 1];
				truthiness += (_aRecentHistoryOfDucks[i] ? 1 : 0);
			}
			_aRecentHistoryOfDucks[HistorySize-1] = (volume > _quackThreshold);


			if (truthiness >= HistorySize / 2)
			{
				if (duk.quack == 0 && _playQuack)
				{
					if (Network.isActive)
					{
						duk._netQuack.Play(1, duk.quackPitch / 255f);
					} else {
						// Why even call netquack again when network is active I have no idea.
						// The first one who explains this to me gets a free ConfettiAbsurdo.
						Hat h = duk.GetEquipment(typeof(Hat)) as Hat;
						if (h != null)
						{
							h.Quack(1f, duk.inputProfile.leftTrigger);
						}
						else
						{
							duk._netQuack.Play(1f, duk.inputProfile.leftTrigger);
						}
					}

				}

                //Open the mouth
				duk.quack = HistorySize / 2;
			}

		}

		public override ExecuteInfo Execute(Profile sender, List<string> args)
		{
            //Parse arguments...
            var setquack = false;
            if (args.Count == 0)
            {
                //Toggle enabled state
			    Enabled = !Enabled;
            }
            else if (args.Count == 1)
            {
                //Either toggle quack or set value
                if (args[0].EqualsTo(true, "quack"))
                {
                    _playQuack = !_playQuack;
                    setquack = true;
                }
                else
                {
                    //Set enabled state
                    switch (args[0])
                    {
                        case "on":
                        case "true":
                        case "1":
                        case "enable":
                            Enabled = true;
                            break;
                        case "off":
                        case "false":
                        case "0":
                        case "disable":
                            Enabled = false;
                            break;
                        case "toggle":
                            Enabled = !Enabled;
                            break;
                    }
                }
            }
            else
            {
                //Either set threshold or set quack
                if (args[0].EqualsTo(true, "threshold"))
                {
                    //Threshold
                    _quackThreshold = int.Parse(args[1]);
                    return new ExecuteInfo("Updated threshold value.");
                }
                else
                {
                    //Quack
                    switch (args[1])
                    {
                        case "on":
                        case "true":
                        case "1":
                        case "enable":
                            _playQuack = true;
                            break;
                        case "off":
                        case "false":
                        case "0":
                        case "disable":
                            _playQuack = false;
                            break;
                        case "toggle":
                            _playQuack = !_playQuack;
                            break;
                    }
                    setquack = true;
                }
            }

            if (setquack)
            {
                return new ExecuteInfo(_playQuack ? "Will play quacking sounds now." : "Won't play quacking sounds now.");
            }

            string result = (Enabled ? "A duck is you!" : "A duck is you no longer :C");
			return new ExecuteInfo(result, -1, Severity.Info);
		}

		public override string[] GetNames()
		{
			return new[] { "vocaquack", "vquack", "vq" };
		}

		public override string[] GetHelpMessage()
		{
			return new[] { "Makes your duck quack when you rock the mic!",
                           "Use threshold parameter to change when quacking should start and stop.",
                           "Use quack parameter to enable/disable sounds when opening mouth."};
		}

		public override bool IsLocalOnly() { return true; }

        //TODO: VocaQuackCommand - since threshold and quack are only really set once, would make more sense to have them as config entries

        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (index == 0)
            {
                return new AutoCompleteEntry[] { "on", "off", "toggle", "threshold", "quack" };
            }
            else if (index == 1)
            {
                //Conditional argument
                if (args[0].EqualsTo(true, "quack"))
                    return AutoCompleteEntry.Toggles;
            }
            return null;
        }

        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            //No args is also valid
            if (args.Count > 0)
            {
                //Only check threshold for now
                if (args[0].EqualsTo(true, "threshold"))
                {
                    //Show current value
                    yield return new ArgumentError(0, "Current threshold value: " + _quackThreshold, Severity.Info);

                    //Needs second parameter: new value
                    if (args.Count != 2)
                    {
                        yield return ArgumentError.InvalidArgumentCount(2, args.Count);
                    }
                    else
                    {
                        //Check if value specified is an int
                        int ass;
                        if (!int.TryParse(args[1], out ass) || ass <= 0)
                        {
                            yield return new ArgumentError(1, "Invalid threshold specified. Needs to be a positive integer value.");
                        }
                    }
                }
            }
        }

        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            if (args.Count == 1)
            {
                if (args[0].EqualsTo(true, "threshold"))
                    return new CommandArgument[] { "Action/threshold", "Value" };
                else if (args[0].EqualsTo(true, "quack"))
                    return new CommandArgument[] { "Action/quack", "?Value/on/off/toggle" };
            }
            return new CommandArgument[] { "?Action/on/off/toggle/threshold/quack", "?..." };
        }
    }
}
