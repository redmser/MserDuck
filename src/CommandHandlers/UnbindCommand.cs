﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class UnbindCommand : CommandHandler, IBind
    {
        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] { "unbind", "u", "ub" };
        }

        /// <summary>
        /// Returns a list containing the expected arguments, as displayed in the "usage" message for this command, as well as for use for naming or type checking.
        /// <para />The order of the CommandArguments in the list is obviously important.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            return new[] { new CommandArgument("Key") {DisplayText = "<key>/all"} };
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[]
            {
                "Unbinds the specified key or all commands currently bound.",
                "Use /bind to add a new binding. These binds do not interfere with default controls.",
                "Enter no parameter in order to get a quick summary of set binds."
            };
        }

        /// <summary>
        /// Whether this command should execute only for the client that entered it, or for every connected user as well.
        /// </summary>
        /// <returns></returns>
        public override bool IsLocalOnly()
        {
            return true;
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            if (args.Count != 1)
            {
                yield return ArgumentError.InvalidArgumentCount(1, args.Count);
            }
            else
            {
                //Check key
                var key = BindManager.GetKeyByString(args[0]);
                if (key == Keys.None)
                {
                    yield return new ArgumentError(0, "The specified key does not exist.");
                }
                else
                {
                    //Get bound info
                    var boundList = BindManager.GetBoundCommandsByKey(key);
                    if (boundList.Count == 0)
                    {
                        yield return new ArgumentError(0, "The specified key is not bound to any command.", Severity.Info, -1, -1);
                    }
                    else
                    {
                        yield return new ArgumentError(0, "The specified key is bound to: " + string.Join("\n", boundList).InsertEvery(130, "\n", true, 40), Severity.Info, -1, -1);
                    }
                }
            }
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player (or the local player, if IsLocalOnly returns true).
        /// <para />Return null to indicate a successful execute (command executed as expected), an informing ExecuteInfo about the command's actions,
        /// or an error ExecuteInfo to indicate failure (specified parameters caused exception).
        /// <para />Do not use ExecuteErrors for parameter validation (this is done by CheckArgumentsValid before execution), but only for anything that can only be determined on Execute!
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            try
            {
                if (args[0].ToLowerInvariant() == "all")
                {
                    //Unbind all keys
                    var amt = BindManager.UnbindAllCommands();
                    return new ExecuteInfo(amt == 0
                        ? "No commands are currently bound to key \"" + args[0] +  "\"."
                        : "Unbound " + amt + " bound command(s).");
                }
                else
                {
                    //Get key and unbind if possible
                    var key = BindManager.GetKeyByString(args[0]);
                    if (key == Keys.None)
                    {
                        return new ExecuteInfo("The specified key \"" + args[0] + "\" does not exist.", 0);
                    }
                    else
                    {
                        //Check if bound
                        var amt = BindManager.UnbindCommand(key);
                        if (amt == 0)
                        {
                            return new ExecuteInfo("No command is bound to key \"" + args[0] + "\".");
                        }
                        else
                        {
                            return new ExecuteInfo("Unbound " + Extensions.GetPlural(amt, "command") + "!");
                        }
                    }
                }
            }
            catch (Exception)
            {
                return new ExecuteInfo("Unexpected error occured trying to unbind a key (wow!).", -1);
            }

        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered. Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (index <= 0)
            {
                //Get list of keys of bound cmds together with part of the bind content
                return BindManager.Commands.Select(c => new AutoCompleteEntry(c.Key.ToString()))
                    .Concat(new [] {new AutoCompleteEntry("all")});
            }
            return null;
        }
    }
}
