﻿using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class RepeatCommand : CommandHandler
    {
        //TODO: /repeat, add arguments for amount + delay between (similar to timer)
        //IMP: /repeat hardcrashes when entered with a space afterwards!
        //  ... is it trying to repeat-execute itself? Maybe use IBind for it?

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[] {"Executes the last entered command."};
        }

        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] {"repeat", "r", "redo"};
        }

        /// <summary>
        /// Whether this command should execute only for the client that entered it, or for every connected user as well.
        /// </summary>
        /// <returns></returns>
        public override bool IsLocalOnly()
        {
            return true;
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            if (args == null || args.Count > 0)
            {
                yield return ArgumentError.ArgumentsNotUsed();
            }

            if (ChatManager.SentMessages.Count != RepeatCommand._localMsgCount)
            {
                //Update count and error state
                RepeatCommand._localMsgCount = ChatManager.SentMessages.Count;
                RepeatCommand._showError = !ChatManager.SentMessages.Any(cmd => cmd.StartsWith("/") &&
                    !(ChatManager.GetCommandByName(cmd) is RepeatCommand || ChatManager.GetCommandByName(cmd) is IBind));
            }

            if (RepeatCommand._showError)
            {
                //Return that error
                yield return new ArgumentError(-1, "No command to execute from history has been found.", Severity.Warning);
            }
        }

        private static int _localMsgCount = -1;
        private static bool _showError = false;

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player.
        /// <para />Return true to indicate success, false to indicate failure (will print usage).
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            foreach (var cmd in ChatManager.SentMessages)
            {
                //Has to be a command (to avoid trolling)
                if (!cmd.StartsWith("/"))
                    continue;

                //Has to be another command than the specified blacklist
                if (ChatManager.GetCommandByName(cmd) is RepeatCommand ||
                    ChatManager.GetCommandByName(cmd) is IBind)
                    continue;

                //Re-execute
                var fakeChatNM = new NMChatMessage((byte)DuckNetwork.core.localDuckIndex, cmd, DuckNetwork.core.chatIndex);
                Send.Message(fakeChatNM);
                ChatManager.HandleChatMessage(fakeChatNM);
                return null;
            }

            return new ExecuteInfo("No command to execute from history has been found.", Severity.Warning);
        }
    }
}
