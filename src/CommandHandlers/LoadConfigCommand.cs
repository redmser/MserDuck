﻿using System.Collections.Generic;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class LoadConfigCommand : CommandHandler
    {
        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] { "loadconfig", "loadcfg", "load", "cfg", "reload", "reloadconfig", "reloadcfg" };
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[]
            {
                "Reloads the config.cfg file in DuckGame's directory.",
                "More help inside the file itself. Check ducklog for any autoexec errors!"
            };
        }

        /// <summary>
        /// Whether this command should execute only for the client that entered it, or for every connected user as well.
        /// </summary>
        /// <returns></returns>
        public override bool IsLocalOnly()
        {
            return true;
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            if (args.Count > 0)
                yield return ArgumentError.ArgumentsNotUsed();
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player (or the local player, if IsLocalOnly returns true).
        /// <para />Return null to indicate a successful execute (command executed as expected), an informing ExecuteInfo about the command's actions,
        /// or an error ExecuteInfo to indicate failure (specified parameters caused exception).
        /// <para />Do not use ExecuteErrors for parameter validation (this is done by CheckArgumentsValid before execution), but only for anything that can only be determined on Execute!
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            //Load basic /config entries
            var error = ConfigManager.LoadConfigFile(false);

            //Load all binds
            ConfigManager.LoadAutoexecutes();

            //Clear any pre-set binds
            BindManager.UnbindAllCommands();

            //Execute only binds
            var num = ConfigManager.RunAutoexecutes(true);

            if (error == null)
            {
                return new ExecuteInfo("Reloaded config and " + num + " bind(s).");
            }
            else
            {
                return new ExecuteInfo(error, -1);
            }
        }
    }
}
