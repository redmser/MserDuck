﻿using System.Collections.Generic;

namespace DuckGame.MserDuck.CommandHandlers
{
    /// <summary>
    /// Abstract base class for chat commands. By inheriting this class, the subclass will be considered a command automatically.
    /// </summary>
    public abstract class CommandHandler
    {
        //HINT: Add any new class inheriting from CommandHandler to the CommandHandlers namespace in order for it to be recognized as a command.

        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public abstract string[] GetNames();

        /// <summary>
        /// Returns a list containing the expected arguments, as displayed in the "usage" message for this command, as well as for use for naming or type checking.
        /// <para />The order of the CommandArguments in the list is obviously important.
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            //TODO: CommandHandler - cache ExpectedArguments in ChatManager values
            return new CommandArgument[] {};
        }

        /// <summary>
        /// Returns a list containing the expected arguments, as displayed in the "usage" message for this command, as well as for use for naming or type checking.
        /// <para />The order of the CommandArguments in the list is obviously important.
        /// <para />Some commands may change their arguments depending on arguments specified beforehand. Use the overload accepting args if this behaviour is required.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CommandArgument> GetExpectedArguments()
        {
            return this.GetExpectedArguments(new List<string>());
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public virtual string[] GetHelpMessage()
        {
            return new[] { "" };
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player (or the local player, if IsLocalOnly returns true).
        /// <para />Return null to indicate a successful execute (command executed as expected), an informing ExecuteInfo about the command's actions,
        /// or an error ExecuteInfo to indicate failure (specified parameters caused exception).
        /// <para />Do not use ExecuteErrors for parameter validation (this is done by CheckArgumentsValid before execution), but only for anything that can only be determined on Execute!
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public abstract ExecuteInfo Execute(Profile sender, List<string> args);

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered, separated by spaces after the commandName.
        /// <para />Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public virtual IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            return null;
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public virtual IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            return null;
        }

        /// <summary>
        /// Whether this command should execute only for the client that entered it, or for every connected user as well.
        /// </summary>
        /// <returns></returns>
        public virtual bool IsLocalOnly()
        {
            return false;
        }

        /// <summary>
        /// Whether this command is not implemented yet, and as such should not be run.
        /// <para />Returning null as an <see cref="ExecuteInfo" /> will instead show a warning about the command not being implemented.
        /// <para />You may implement certain execute behaviour regardless.
        /// </summary>
        /// <returns></returns>
        public virtual bool IsNotImplemented()
        {
            return false;
        }
    }
}
