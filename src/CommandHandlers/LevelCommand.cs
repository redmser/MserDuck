﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class LevelCommand : CommandHandler
    {
        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] {"level", "lvl", "setlevel", "changelevel", "map", "setmap", "changemap"};
        }

        /// <summary>
        /// Returns a string containing the expected arguments, as displayed in the "usage" message for this command.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            return new CommandArgument[] {"Level/Path/Class"};
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[] {"Changes the level to the specified level",
                          "either by .lev file or a class inheriting from Level."};
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            //TODO: /level argerrors - detect when parameters are "unused" / help with params similarly to spawnex
            //TODO: /level argerrors - show current level name/class
            if (args.Count == 0)
            {
                yield return ArgumentError.InvalidArgumentCount();
            }
            else
            {
                if (args.First().EqualsToAny(true, "skip", "next"))
                    yield break;

                var tuple = LevelCommand.GetLevel(args);
                if (tuple.Item1 == null && tuple.Item2 != null)
                {
                    yield return new ArgumentError(0, tuple.Item2);
                }
            }
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player.
        /// <para />Return true to indicate success, false to indicate failure (will print usage).
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            if (args.First().EqualsToAny(true, "skip", "next"))
            {
                //Skip match
                ((DM)MserGlobals.GetField(typeof(GameLevel), "_mode").GetValue(Level.current)).SkipMatch();
                return null;
            }

            var tuple = LevelCommand.GetLevel(args);
            if (tuple.Item1 == null && tuple.Item2 != null)
            {
                return new ExecuteInfo(tuple.Item2, 0);
            }

            //Change level
            //TODO: /level execute - seems like properties for the level dont get set to make DG recognize it as a DM level, will have to find out what to specify where
            //  based on how the game changes level otherwise! That may fix more levels than just the regular .lev files, even!
            Level.current = tuple.Item1;
            return null;
        }

        private static Tuple<Level, string> GetLevel(List<string> level)
        {
            //Synonym for TeamSelect2
            var levname = level.First().ToLowerInvariant();
            if (levname == "lobby")
            {
                //HINT: REMINDER /level - Change lobby level once custom is in place.
                return new Tuple<Level, string>(new TeamSelect2(), null);
            }

            //Random level
            if (levname == "random" || levname == "procedural")
            {
                //NYI: /level random - force procedural level
            }

            //Testing rockthrow
            if (levname == "rockthrow" || levname == "intermission")
            {
                //HINT: REMINDER /level - Randomized intermission level once in place.
                return new Tuple<Level, string>((Level)Activator.CreateInstance(MserGlobals.GetType("RockIntro"), new object[] {null}), null);
            }

            var type = MserGlobals.GetType(level.First());

            if (type == null)
            {
                //Try .lev file
                var lev = new GameLevel("");
                var path = lev.ProcessLevelPath(level.First());
                if (path == null)
                {
                    //Retry with GUID
                    path = lev.ProcessLevelPath(Content.GetLevelID(level.First(), LevelLocation.Any));
                }

                if (path == null)
                    return new Tuple<Level, string>(null, "Could not load specified .lev file or unknown class.");

                lev = new GameLevel(path);
                return new Tuple<Level, string>(lev, null);
            }

            //Check if inheriting from level
            if (!type.IsSubclassOf(typeof(Level)))
                return new Tuple<Level, string>(null, "The specified class does not inherit from the type Level.");
            
            //Set level to instance of type
            try
            {
                Level lev;

                if (level.Count == 1)
                {
                    //Without args
                    lev = (Level)Activator.CreateInstance(type);
                }
                else
                {
                    //With args
                    var strlist = level.GetRange(1, level.Count - 1);
                    var pars = CommandHelpers.ParseStringToObjects(strlist).ToArray();
                    lev = (Level)Activator.CreateInstance(type, pars);
                }

                return new Tuple<Level, string>(lev, null);
            }
            catch
            {
                return new Tuple<Level, string>(null, "Couldn't create instance of type. It may require parameters.");
            }
        }

        /// <summary>
        /// Returns a list of strings that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own! Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">The value of the argument currently being entered by the user.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (index != 0)
                return null;

            //Level list
            return CacheManager.GetLevelAutoCompleteInfo().Concat(new AutoCompleteEntry[] {"Skip"});
        }
    }
}
