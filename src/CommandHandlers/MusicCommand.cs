﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework.Audio;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class MusicCommand : CommandHandler
    {
        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] { "music", "song" };
        }

        /// <summary>
        /// Returns a list containing the expected arguments, as displayed in the "usage" message for this command, as well as for use for naming or type checking.
        /// <para />The order of the CommandArguments in the list is obviously important.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            if (args.Count > 0 && args[0].EqualsToAny(true, "play", "playd"))
            {
                return new CommandArgument[] {"Action/play", "Name", "?Looping"};
            }
            else if (args.Count > 0 && args[0].EqualsToAny(true, "stop", "random", "pitch"))
            {
                return new CommandArgument[] {"Action/stop/random"};
            }

            return new CommandArgument[] {"Action/play/stop/random", "?..."};
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[]
            {
                "Plays the specified song, stops any music from playing or plays a random song.",
                "Specific songs are read from the Music folders in DuckGame and MserDuck.",
                "Stopping songs (currently) does not work in-game.",
            };
        }

        //TODO: /music - allow omitting play argument if detected as a file (how does autocomplete work then though?)
        //  Once something got figured out, try to implement for the /cape cmd too, or maybe /timer even

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            //Song playing
            if (MusicEx.GetSongState() != SoundState.Playing)
            {
                yield return new ArgumentError(0, "No song is currently playing.", Severity.Info);
            }
            else
            {
                var song = MusicEx.GetCurrentSong();
                yield return new ArgumentError(0, "Now playing: " + song, Severity.Info);
            }

            if (args.Count == 0)
            {
                yield return ArgumentError.InvalidArgumentCount();
            }

            if (args.Count < 2 && (args[0] == "play" || args[0] == "playd"))
            {
                yield return ArgumentError.InvalidArgumentCount(2, args.Count, true);
            }

            if (args.Count > 0 && (args[0] != "play" && args[0] != "playd" && args[0] != "pitch" && args[0] != "stop" && args[0] != "random"))
            {
                yield return new ArgumentError(0, "Unknown action specified. Known: play, playd, pitch, random, stop");
            }
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player (or the local player, if IsLocalOnly returns true).
        /// <para />Return null to indicate a successful execute (command executed as expected), an informing ExecuteInfo about the command's actions,
        /// or an error ExecuteInfo to indicate failure (specified parameters caused exception).
        /// <para />Do not use ExecuteErrors for parameter validation (this is done by CheckArgumentsValid before execution), but only for anything that can only be determined on Execute!
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            var track = "null";

            if (args.Count > 1)
            {
                //Get track
                track = args[1];

                if (track.EndsWith(".ogg"))
                {
                    //Remove the extension
                    track = track.Substring(0, track.Length - 4);
                }
            }

            switch (args[0])
            {
                case "play":
                case "playd": //Left in for legacy
                    var looping = true;

                    if (args.Count >= 3)
                    {
                        if (!bool.TryParse(args[2], out looping))
                            looping = true;
                    }

                    try
                    {
                        MusicEx.PlaySong(track, looping);
                    }
                    catch (Exception)
                    {
                        return new ExecuteInfo("The specified song \"" + track + "\" cannot be found!", 1);
                    }
                    break;
                case "stop":
                    MusicEx.Stop();
                    break;
                case "random":
                    if (!sender.localPlayer)
                    {
                        return null;
                    }

                    try
                    {
                        track = MusicEx.GetRandomTrack("InGame", GameMode.core._currentMusic);
                        
                        //Use this to sync it over the network correctly
                        ChatManager.EnterChatMessage("/music play " + track, true);
                    }
                    catch (Exception)
                    {
                        return new ExecuteInfo("Could not play random track \"" + track + "\", cannot be found!", -1);
                    }
                    break;
                case "pitch":
                    try
                    {
                        //I doubt this will ever work, but o wel
                        MusicEx.SetPitch(float.Parse(track));
                    }
                    catch (Exception e)
                    {
                        return new ExecuteInfo("Error settings song pitch: " + e.Message, -1);
                    }
                    break;
                default:
                    return new ExecuteInfo("Unknown action specified.");
            }

            return null;
        }

        /// <summary>
        /// Returns a list of strings that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own! Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered. Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (args.Count > 0 && (args[0] == "play" || args[0] == "playd"))
            {
                if (index == 1)
                {
                    //Show list of songs in base dir
                    return CacheManager.GetMusicAutoCompleteInfo();
                }
                else if (index == 2)
                {
                    //Looping bool
                    return AutoCompleteEntry.Booleans;
                }
            }

            return null;
        }
    }
}
