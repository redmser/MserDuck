﻿using System;
using System.Collections.Generic;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class SpawnExCommand : CommandHandler
    {
        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] { "spawnex", "createex", "sx", "sex", "spawnx", "createx", "cex", "cx" };
        }

        /// <summary>
        /// Returns a list containing the expected arguments, as displayed in the "usage" message for this command, as well as for use for naming or type checking.
        /// <para />The order of the CommandArguments in the list is obviously important.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            return new CommandArgument[] {"Thing Name", "Count", "?Parameters"};
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[] { "Spawns any amount of things with the specified constructor parameters.",
                           "Thing name must match the class name of an object type.",
                           "Note that location is not included and has to be added manually where needed."};
        }

        /// <summary>
        /// Whether this command should execute only for the client that entered it, or for every connected user as well.
        /// </summary>
        /// <returns></returns>
        public override bool IsLocalOnly()
        {
            return true;
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            if (args.Count < 2)
            {
                yield return ArgumentError.InvalidArgumentCount(2, args.Count, true);
            }
            else if (args.Count > 0)
            {
                //Check if valid thing
                var thingType = MserGlobals.GetType(args[0]);

                //Type has to inherit from thing
                if (thingType != null && !thingType.IsSubclassOf(typeof(Thing)))
                {
                    yield return new ArgumentError(0, "Type to spawn has to inherit from \"Thing\".");
                }

                if (thingType == null)
                {
                    yield return new ArgumentError(0, "Unknown type of thing to spawn specified!");
                }
                else
                {
                    //TODO: /spawnex argerrors - Check if valid constructor exists for the specified parameters (also do autocomplete titles for it)
                }
            }

            if (args.Count > 1)
            {
                //Check count
                int count;
                if (!int.TryParse(args[1], out count))
                {
                    yield return new ArgumentError(1, "The specified amount of things is invalid.");
                }
                else if (count <= 0)
                {
                    yield return new ArgumentError(1, "The specified amount of things has to be >0");
                }
            }
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player (or the local player, if IsLocalOnly returns true).
        /// <para />Return null to indicate a successful execute (command executed as expected), an informing ExecuteInfo about the command's actions,
        /// or an error ExecuteInfo to indicate failure (specified parameters caused exception).
        /// <para />Do not use ExecuteErrors for parameter validation (this is done by CheckArgumentsValid before execution), but only for anything that can only be determined on Execute!
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            //Get all params that aren't the type
            List<string> strList = args.GetRange(2, args.Count - 2);
            object[] pars = CommandHelpers.ParseStringToObjects(strList).ToArray();

            try
            {
                //Generic name
                Type thingType = MserGlobals.GetType(args[0]) /* ?? MserGlobals.GetType(args[0] + "MP") */;

                if (thingType != null)
                {
                    var count = int.Parse(args[1]);

                    for (int i = 0; i < count; i++)
                    {
                        var spawned = (Thing)Activator.CreateInstance(thingType, pars);
                        CommandHelpers.SpawnThing(spawned);
                    }
                    return null;
                }
                else
                {
                    return new ExecuteInfo("Can't spawn an unknown type.", 0);
                }
            }
            catch (Exception ex)
            {
                Program.LogLine("General spawning error: " + ex.Message + ex.StackTrace);
                return new ExecuteInfo("General spawning error. See ducklog!", -1);
            }
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered, separated by spaces after the commandName.
        /// <para />Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (index > 0)
            {
                return null;
            }
            else
            {
                //Only provide object list for now
                return CacheManager.GetTypeAutoCompleteInfo(false);
            }
        }
    }
}
