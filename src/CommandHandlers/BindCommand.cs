﻿using System.Collections.Generic;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class BindCommand : CommandHandler, IBind, IAutoRun
    {
        //IMP: /addbind - get rid of this addbind system and all of its traces (such as "list of bound commands on key")

        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] { "addbind", "ab" };
        }

        /// <summary>
        /// Returns a list containing the expected arguments, as displayed in the "usage" message for this command, as well as for use for naming or type checking.
        /// <para />The order of the CommandArguments in the list is obviously important.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            return new CommandArgument[] {"Key", "Command", "?Arguments"};
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[]
            {
                "This is no longer supported. Please use /bind with chains instead!"
                /*"Binds the specified command to the name of the specified key, keeping any other binds on that key.",
                "It is recommended to use /bind with /chain instead, to avoid race conditions!",
                "Use /unbind to remove binding. These binds do not interfer with default controls.",
                "Enter no command in order to get the currently bound commands for that key."*/
            };
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            var errorlist = new List<ArgumentError>();

            if (args.Count == 0)
            {
                errorlist.Add(ArgumentError.InvalidArgumentCount());
            }
            if (args.Count >= 1)
            {
                //Check key
                var key = BindManager.GetKeyByString(args[0]);
                if (key == Keys.None)
                {
                    errorlist.Add(new ArgumentError(0, "The specified key does not exist."));
                }
                else
                {
                    //Get bound info
                    var boundList = BindManager.GetBoundCommandsByKey(key);
                    if (boundList.Count == 0)
                    {
                        errorlist.Add(new ArgumentError(0, "The specified key is not bound to any command.", Severity.Info));
                    }
                    else
                    {
                        errorlist.Add(new ArgumentError(0, "The specified key is bound to: " + string.Join("\n", boundList).InsertEvery(130, "\n", true, 40), Severity.Info));
                    }
                }
            }
            if (args.Count > 1)
            {
                var cmd = ChatManager.GetCommandByName(args[1]);
                if (cmd == null)
                {
                    errorlist.Add(ArgumentError.CommandUnknown(1));
                }
                else
                {
                    if (cmd is IBind)
                    {
                        errorlist.Add(new ArgumentError(1, "The specified command may not be used in a bind."));
                    }

                    var errors = cmd.CheckArgumentsValid(args.GetRange(2, args.Count - 2));
                    foreach (var err in errors)
                    {
                        err.Index += 3;
                        errorlist.Add(err);
                    }
                }
            }

            return errorlist;
        }

        /// <summary>
        /// Whether this command should execute only for the client that entered it, or for every connected user as well.
        /// </summary>
        /// <returns></returns>
        public override bool IsLocalOnly()
        {
            return true;
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player (or the local player, if IsLocalOnly returns true).
        /// <para />Return null to indicate a successful execute (command executed as expected), an informing ExecuteInfo about the command's actions,
        /// or an error ExecuteInfo to indicate failure (specified parameters caused exception).
        /// <para />Do not use ExecuteErrors for parameter validation (this is done by CheckArgumentsValid before execution), but only for anything that can only be determined on Execute!
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            try
            {

                if (args.Count == 1)
                {
                    //Get key and check if bound
                    var key = BindManager.GetKeyByString(args[0]);
                    if (key == Keys.None)
                    {
                        return new ExecuteInfo("The specified key \"" + args[0] + "\" does not exist.", 0);
                    }
                    else
                    {
                        //Check if bound
                        var boundList = BindManager.GetBoundCommandsByKey(key);
                        if (boundList.Count == 0)
                        {
                            return new ExecuteInfo("The specified key \"" + args[0] + "\" is currently not bound.");
                        }
                        else
                        {
                            //Print list of bound commands of the key
                            return new ExecuteInfo("The specified key \"" + args[0] + "\" is bound to: "+string.Join("\n", boundList).InsertEvery(120, "\n", true, 40));
                        }
                    }
                }
                else
                {
                    //Bind command to specified key
                    var key = BindManager.GetKeyByString(args[0]);
                    if (key == Keys.None)
                    {
                        return new ExecuteInfo("The specified key \"" + args[0] + "\" does not exist.", 0);
                    }
                    else
                    {
                        //Get the command specified
                        var cmd = ChatManager.GetCommandByName(args[1]);

                        if (cmd == null)
                        {
                            return new ExecuteInfo("The specified command \"" + args[1] + "\" is unknown.", 1);
                        }
                        else if (cmd is IBind)
                        {
                            return new ExecuteInfo("You may not bind a bind or unbind command!", 1);
                        }
                        else
                        {
                            //Get remaining args, if any
                            var remainingArgs = new List<string>();

                            if (args.Count > 2)
                            {
                                remainingArgs.AddRange(args.GetRange(2, args.Count - 2));
                            }

                            //Bind!
                            BindManager.BindCommand(cmd, remainingArgs, key);

                            //Inform about amount of commands on key
                            if (BindManager.GetBoundCommandsByKey(key).Count > 1)
                            {
                                return new ExecuteInfo("NOTE: You have " + BindManager.GetBoundCommandsByKey(key).Count + " commands bound to the specified key.");
                            }
                            else
                            {
                                return new ExecuteInfo("Bound command to key " + key);
                            }
                        }
                    }
                }
            }
            catch
            {
                return new ExecuteInfo("Unable to bind command, unexpected exception occured!", -1);
            }
        }
    }
}
