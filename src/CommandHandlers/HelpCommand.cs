﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class HelpCommand : CommandHandler
    {
        /// <summary>
        /// Whether this command should execute only for the client that entered it, or for every connected user as well.
        /// </summary>
        /// <returns></returns>
        public override bool IsLocalOnly()
        {
            return true;
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            if (args.Count >= 1)
            {
                //Is a number??
                int page;
                if (!int.TryParse(args[0], out page))
                {
                    //Is a command maybe???
                    var cmd = ChatManager.GetCommandByName(args[0]);
                    if (cmd == null)
                    {
                        yield return ArgumentError.CommandUnknown(0);
                    }
                }
            }
            if (args.Count > 1)
                yield return ArgumentError.ArgumentsNotUsed(true, 1);
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player (or the local player, if IsLocalOnly returns true).
        /// <para />Return null to indicate a successful execute (command executed as expected), an informing ExecuteInfo about the command's actions,
        /// or an error ExecuteInfo to indicate failure (specified parameters caused exception).
        /// <para />Do not use ExecuteErrors for parameter validation (this is done by CheckArgumentsValid before execution), but only for anything that can only be determined on Execute!
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            //TODO: /help command - if not entering any args, inform about specifying pages (or at all just show page numbers!)
            //TODO: /help command - also show help for other stuff ...
            //  - Constructors of things or parameters needed by a method
            //  - Data type/default value of properties/fields
            //  - Modifier description + default value

            var cmds = ChatManager.Commands;
            var page = 1;

            if (args.Count >= 1 && !int.TryParse(args[0], out page))
            {
                //Get command from name
                CommandHandler cmd = ChatManager.GetCommandByName(args[0]);

                if (cmd == null)
                {
                    return new ExecuteInfo(string.Format("Unknown command \"{0}\" to display help for.", args[0]), 0);
                }

                //Get help for command
                return new ExecuteInfo(ChatManager.GetUsage(cmd) + "\n" + string.Join("\n", ChatManager.GetHelp(cmd)));
            }
            else
            {
                //Open help page
                if (page <= 0)
                    page = 1;

                var index = (page - 1) * ChatManager.Entries;

                if (index > cmds.Count - 1)
                    index = Math.Max(0, cmds.Count - 1 - ChatManager.Entries);

                List<CommandHandler> pageCmds;

                if (cmds.Count - index >= ChatManager.Entries)
                {
                    //Get whole page
                    pageCmds = cmds.GetRange(index, ChatManager.Entries);
                }
                else
                {
                    //Get partial page
                    pageCmds = cmds.GetRange(index, cmds.Count % ChatManager.Entries);
                }

                var chatlist = pageCmds.OrderBy(c => c.GetNames()[0]).Select(pageCmd => ChatManager.GetShortCommandText(pageCmd)).ToList();

                return new ExecuteInfo(string.Join("\n", chatlist));
            }
        }

        /// <summary>
        /// Returns a list containing the expected arguments, as displayed in the "usage" message for this command, as well as for use for naming or type checking.
        /// <para />The order of the CommandArguments in the list is obviously important.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            return new CommandArgument[] { "?Command/Command/Page" };
        }

        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new string[] { "help", "h", "?", "usage", "list", "ls" };
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new string[] { "Returns a list of all commands or help for the specified command." };
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered. Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (index <= 0)
            {
                //Simply show all commands
                return ChatManager.Commands.Select(c => new AutoCompleteEntry(c.GetNames()[0]));
            }

            return null;
        }
    }
}
