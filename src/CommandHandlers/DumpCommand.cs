﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class DumpCommand : CommandHandler
    {
        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] {"dump"};
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[]
            {
                "Dumps a list of all command names, item names, modifier names",
                "Output is a dump.txt in the duckgame executable directory."
            };
        }

        /// <summary>
        /// Whether this command should execute only for the client that entered it, or for every connected user as well.
        /// </summary>
        /// <returns></returns>
        public override bool IsLocalOnly()
        {
            return true;
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            if (args.Count > 0)
                yield return ArgumentError.ArgumentsNotUsed();
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player.
        /// <para />Return true to indicate success, false to indicate failure (will print usage).
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            //TODO: /dump - more info to be shown...
            //  - Instead of items only, show all types
            //      - If possible, include custom constructors for items (or all types??)
            //  - Config entries with names and default values
            //  - Modifier descriptions

            var lines = new List<string>();
            var ccnt = 0;
            var icnt = 0;
            var mcnt = 0;
            var scnt = 0;

            lines.Add("Dump from " + DateTime.Now + "\r\n");

            lines.Add("----------COMMANDS----------");
            foreach (CommandHandler cmd in ChatManager.Commands)
            {
                lines.Add(string.Format("Name: {0}", string.Join("/", cmd.GetNames())));
                var exargs = cmd.GetExpectedArguments().ToList();
                if (exargs.Any())
                    lines.Add(string.Format("Parameters: {0}", string.Join(" ", exargs)));
                lines.Add(string.Format("Help: {0}", string.Join("\r\n", cmd.GetHelpMessage())));
                lines.Add("");
                ccnt++;
            }

            lines.Add("----------ITEMS----------");
            foreach (Type item in Editor.GetSubclasses(typeof(Thing)).OrderBy(x => x.Assembly.GetName().Name))
            {
                lines.Add(string.Format("{0}: {1} (inherits from {2})", item.Assembly.GetName().Name, item.Name, item.BaseType.Name));
                icnt++;
            }

            lines.Add("");
            lines.Add("----------MODIFIERS----------");
            var specials = new[]
            {
                new UnlockData {id = "DILLY", name = "(UNUSED) Start with Dueling Pistol"},
                new UnlockData {id = "COOLBOOK", name = "(UNUSED) Start with Good Book" }
            };
            foreach (var mod in Unlocks.GetUnlocks(UnlockType.Modifier)
                .Concat(ModifierManager.Modifiers.Select(mod => new UnlockData { name = mod.GetName() }))
                .Concat(specials).OrderBy(x => x.shortName))
            {
                if (string.IsNullOrWhiteSpace(mod.id))
                {
                    lines.Add(string.Format("MserDuck: {0}", mod.shortName));
                }
                else
                {
                    lines.Add(string.Format("DuckGame: {0} ({1})", mod.shortName, mod.id));
                }
                mcnt++;
            }

            lines.Add("");
            lines.Add("----------MATCH SETTINGS----------");
            lines.Add("(Default Values get overridden by config.cfg!)");
            //TODO: /dump - can default match setting values be gotten by creating new instances/keeping original default values before overwriting?
            foreach (var sett in TeamSelect2.matchSettings.Concat(TeamSelect2.onlineSettings).OrderBy(x => x.id))
            {
                lines.Add(string.Format("{0} - Default: {1}", sett.id, sett.defaultValue.ToString()));
                scnt++;
            }

            File.WriteAllLines("dump.txt", lines);

            return new ExecuteInfo(string.Format("Dumped {0} commands, {1} items, {3} match settings and {2} modifiers to dump.txt", ccnt, icnt, mcnt, scnt));
        }
    }
}
