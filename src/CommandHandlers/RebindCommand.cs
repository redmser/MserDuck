﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class RebindCommand : BindCommand
    {
        //TODO: /bind commands - Add ability to bind with modifier keys: chain key names like A+B+C+D wheras last key in list should be the one to be pressed and
        //  the rest have to be held in order to execute the bind

        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] { "bind", "b" };
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[]
            {
                "Binds the specified command to the name of the specified key (replacing other binds on this key).",
                "Use /unbind to remove binding. These binds do not interfer with default controls.",
                "Enter no command in order to get the currently bound commands for that key."
            };
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player (or the local player, if IsLocalOnly returns true).
        /// <para />Return null to indicate a successful execute (command executed as expected), an informing ExecuteInfo about the command's actions,
        /// or an error ExecuteInfo to indicate failure (specified parameters caused exception).
        /// <para />Do not use ExecuteErrors for parameter validation (this is done by CheckArgumentsValid before execution), but only for anything that can only be determined on Execute!
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            try
            {

                if (args.Count == 1)
                {
                    //Same implementation
                    return base.Execute(sender, args);
                }
                else
                {
                    //Bind command to specified key
                    var key = BindManager.GetKeyByString(args[0]);
                    if (key == Keys.None)
                    {
                        return new ExecuteInfo("The specified key \"" + args[0] + "\" does not exist.", 0);
                    }
                    else
                    {
                        //First unbind
                        BindManager.UnbindCommand(key);

                        //Same implementation
                        return base.Execute(sender, args);
                    }
                }
            }
            catch
            {
                return new ExecuteInfo("Unable to rebind command, unexpected exception occured!", -1);
            }
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered. Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (index <= 0)
            {
                //Keys
                return Enum.GetNames(typeof(Keys)).Select(c => new AutoCompleteEntry(c));
            }

            if (index == 1)
            {
                //Commands
                return ChatManager.Commands.Where(c => !(c is IBind)).Select(c => new AutoCompleteEntry(c.GetNames()[0]));
            }
            
            //Show autocomplete for args[1]
            var cmd = ChatManager.GetCommandByName(args[1]);
            if (cmd == null)
                return null;
            return cmd.GetAutoCompleteEntries(index - 2, args.GetRange(2, index - 1));
        }
    }
}
