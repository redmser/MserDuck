﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class SetCommand : CommandHandler
    {
        //TODO: /set - allow getting and setting STATIC fields and properties (add additional autocomplete icon for this)

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            var errors = new List<ArgumentError>();
            try
            {
                if (args.Count < 2)
                {
                    errors.Add(ArgumentError.InvalidArgumentCount(2, args.Count, true));
                }
                if (args.Count >= 1)
                {
                    //Check if part of info list
                    var tuple = CommandHelpers.HandleObjectListByName(args[0], MserGlobals.GetLocalDuck());
                    if (tuple != null)
                    {
                        if (tuple.Item1.Count == 0 || tuple.Item1.All(t => t == null))
                        {
                            errors.Add(new ArgumentError(0, "No thing of this type were found in the level.", Severity.Warning));
                        }

                        //Pick type to do checks on based off stuff
                        var type = tuple.Item2;
                        if (args.Count >= 2 && tuple.Item1.All(t => t != null))
                        {
                            //Get memberinfo
                            var nfo = CacheManager.GetSetAutoCompleteInfo(type);
                            if (nfo == null)
                                yield break;

                            var info = nfo.ToList();
                            if (!info.Any(i => i.Name.Equals(args[1], StringComparison.InvariantCultureIgnoreCase)))
                            {
                                errors.Add(new ArgumentError(1, string.Format("The specified field or property is not a part of \"{0}\".", type.Name)));
                            }
                            else
                            {
                                var member = info.First(i => i.Name.Equals(args[1], StringComparison.InvariantCultureIgnoreCase));

                                //Check if it can be written to
                                var canWrite = (member is FieldInfo && !((FieldInfo)member).IsInitOnly) || (member is PropertyInfo && ((PropertyInfo)member).CanWrite);
                                if (args.Count > 2 && !canWrite)
                                {
                                    errors.Add(new ArgumentError(1, "The specified field or property cannot be written to."));
                                }

                                //Check datatype
                                if (args.Count > 2)
                                {
                                    var expect = member is FieldInfo ? ((FieldInfo)member).FieldType : ((PropertyInfo)member).PropertyType;
                                    var result = CommandHelpers.ParseStringToObjects(new[] { args[2] }, MserGlobals.GetLocalDuck())[0].GetType();

                                    if (expect != result && !Extensions.CanConvert(expect, result))
                                    {
                                        errors.Add(new ArgumentError(2, string.Format("The specified value has to be of type \"{0}\" (is \"{1}\").", expect.Name, result.Name)));
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        errors.Add(new ArgumentError(0, "The specified type of thing is unknown."));
                    }
                }
                if (args.Count > 3)
                {
                    errors.Add(ArgumentError.ArgumentsNotUsed(true, 3));
                }
            }
            catch (Exception ex)
            {
                Program.LogLine("Error in /set argerrors: " + ex.Message + ex.StackTrace);
            }
            foreach (var e in errors)
            {
                yield return e;
            }
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player (or the local player, if IsLocalOnly returns true).
        /// <para />Return null to indicate a successful execute (command executed as expected), an informing ExecuteInfo about the command's actions,
        /// or an error ExecuteInfo to indicate failure (specified parameters caused exception).
        /// <para />Do not use ExecuteErrors for parameter validation (this is done by CheckArgumentsValid before execution), but only for anything that can only be determined on Execute!
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            try
            {
                args = args.Crop().ToList();

                object param = null;
                string objName = args[0];

                if (args.Count >= 3)
                {
                    param = CommandHelpers.ParseStringToObjects(new[] { args[2] })[0];
                }

                var tuple = CommandHelpers.HandleObjectListByName(objName, sender.duck);

                if (tuple == null || tuple.Item1 == null || tuple.Item2 == null)
                {
                    return new ExecuteInfo("An invalid or unknown type was specified.", 0);
                }

                var objects = tuple.Item1;
                var ty = tuple.Item2;

                if (objects.Count == 0)
                {
                    return new ExecuteInfo("No things of this type found in the level.", Severity.Warning);
                }

                var exec = new List<string>();

                foreach (var th in objects)
                {
                    if (th == null) continue;

                    var myField = MserGlobals.GetField(ty, args[1]);
                    if (myField == null)
                    {
                        var myProp = MserGlobals.GetProperty(ty, args[1]);
                        if (myProp == null)
                        {
                            return new ExecuteInfo("No such property or field.", 1);
                        }
                        else
                        {
                            if (param == null)
                            {
                                //Get myProp
                                exec.Add(string.Format("#{0}: {1}", (objects.IndexOf(th) + 1), Extensions.ToNeatString(myProp.GetValue(th, null))));
                            }
                            else
                            {
                                //Set myProp
                                myProp.SetValue(th, param, null);
                            }
                        }
                    }
                    else
                    {
                        if (param == null)
                        {
                            //Get myField
                            exec.Add(string.Format("#{0}: {1}", (objects.IndexOf(th) + 1), Extensions.ToNeatString(myField.GetValue(th))));
                        }
                        else
                        {
                            //Set myField
                            myField.SetValue(th, param);
                        }
                    }
                }

                //TODO: /get command - mention how many entries were culled by Math.Min
                return exec.Count > 0 ? new ExecuteInfo(string.Join("\n", exec.GetRange(0, Math.Min(exec.Count, ChatManager.Entries)))) : null;
            }
            catch (Exception ex)
            {
                Program.LogLine("Error in SetCommand: " + ex.Message + ex.StackTrace);
                return new ExecuteInfo("Unexpected error getting or setting the specified field or property.", -1);
            }
        }

        /// <summary>
        /// Returns a list containing the expected arguments, as displayed in the "usage" message for this command, as well as for use for naming or type checking.
        /// <para />The order of the CommandArguments in the list is obviously important.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            return new[] {new CommandArgument("Thing") {DisplayText = "[all/near]<thing>[!]" }, "!Field/Property", "?Value"};
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[] { "Sets the specified field/property to the value.",
                                  "Add ! to exclude sender from all/near calls.",
                                  "By not specifying a value, it prints the value instead." };
        }

        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] { "set", "get" };
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered. Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (index <= 0)
            {
                //Handled the same way
                return new CallCommand().GetAutoCompleteEntries(index, args);
            }
            else if (index == 1)
            {
                try
                {
                    //Get properties and fields
                    var stuf = CommandHelpers.HandleObjectListByName(args[0], MserGlobals.GetLocalDuck());
                    if (stuf == null)
                        return null;

                    var type = stuf.Item2;
                    var info = CacheManager.GetSetAutoCompleteInfo(type);
                    if (info == null)
                        return null;

                    return info.DistinctBy(i => i.Name).Select(i => new AutoCompleteEntry(i.Name, drawDelegate: CommandHelpers.GetInfoIconDrawDelegate(i)));
                }
                catch
                {
                    //broken *10
                }
            }

            return null;
        }
    }
}
