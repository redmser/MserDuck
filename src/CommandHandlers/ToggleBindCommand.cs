﻿using System.Collections.Generic;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class ToggleBindCommand : RebindCommand
    {
        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns/>
        public override string[] GetNames()
        {
            return new[] {"bindtoggle", "toggle", "togglebind"};
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para/>
        /// Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns/>
        public override string[] GetHelpMessage()
        {
            return new[]
            {
                "Pressing the bound key executes two or more commands, cycling every time the key is pressed.",
                "Commands are separated by a comma."
            };
        }

        /// <summary>
        /// Whether this command should execute only for the client that entered it, or for every connected user as well.
        /// </summary>
        /// <returns></returns>
        public override bool IsLocalOnly()
        {
            return true;
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player.
        /// <para/>
        /// Return true to indicate success, false to indicate failure (will print usage).
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param><param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns/>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            return null;

            //NYI: ToggleBind Execute
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered. Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            //NYI: ToggleBind autocomplete: should restart bind arguments after commas
            return base.GetAutoCompleteEntries(index, args);
        }

        /// <summary>
        /// Whether this command is not implemented yet, and as such should not be run.
        /// <para />Returning null as an <see cref="ExecuteInfo" /> will instead show a warning about the command not being implemented.
        /// <para />You may implement certain execute behaviour regardless.
        /// </summary>
        /// <returns></returns>
        public override bool IsNotImplemented()
        {
            return true;
        }
    }
}
