﻿using System.Collections.Generic;
using System.Reflection;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class GodCommand : CommandHandler
    {
        /// <summary>
        /// Duck._killed field. Whether the duck has been killed and can be killed again as a result of such.
        /// </summary>
        public static FieldInfo _killed = MserGlobals.GetField(typeof(Duck), "_killed");

        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] {"god", "godmode"};
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[] { "When god mode is enabled, makes yourself unkillable."};
        }

        /// <summary>
        /// Returns a string containing the expected arguments, as displayed in the "usage" message for this command.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            return new CommandArgument[] {"?Value/on/off/toggle"};
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            if (args.Count > 1)
                yield return ArgumentError.ArgumentsNotUsed(true, 1);
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player.
        /// <para />Return true to indicate success, false to indicate failure (will print usage).
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            bool newvalue;
            var arg = args.Count > 0 ? args[0] : "";

            switch (arg.ToLowerInvariant())
            {
                case "on":
                case "true":
                case "1":
                case "enable":
                    newvalue = true;
                    break;
                case "off":
                case "false":
                case "0":
                case "disable":
                    newvalue = false;
                    break;
                default:
                    newvalue = !ModifierManager.GodDucks.Contains(sender.duck);
                    break;
            }

            if (newvalue && !ModifierManager.GodDucks.Contains(sender.duck))
                ModifierManager.GodDucks.Add(sender.duck);
            else if (!newvalue && ModifierManager.GodDucks.Contains(sender.duck))
            {
                ModifierManager.GodDucks.Remove(sender.duck);
                GodCommand._killed.SetValue(sender.duck, false);
            }

            if (!sender.localPlayer)
                return null;

            var state = newvalue ? "Enabled" : "Disabled";
            return new ExecuteInfo(state + " god mode.");
        }

        /// <summary>
        /// Returns a list of strings that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own! Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered. Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (index <= 0)
            {
                return AutoCompleteEntry.Toggles;
            }

            return null;
        }
    }
}
