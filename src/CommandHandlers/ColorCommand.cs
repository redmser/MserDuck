﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class ColorCommand : CommandHandler
    {
        //TODO: /color - color picking, maybe using DebugUI's (unfinished) one?

        /// <summary>
        /// A list of color names to use for /color.
        /// </summary>
        public static Dictionary<string, Vec3> ColorNames = new Dictionary<string, Vec3>
        {
            //HINT: Add any custom /color names here!
            { "black",   new Vec3(20 ,20 ,20 ) },
            { "ballz",   new Vec3(47 ,72 ,78 ) },
            { "frogs",   new Vec3(163,206,39 ) },
            { "dinos",   new Vec3(68 ,137,26 ) },
            { "uglies",  new Vec3(164,100,34 ) },
            { "pyro",    new Vec3(56 ,56 ,56 ) },
            { "swack",   new Vec3(49 ,162,242) },
            { "sentry",  new Vec3(120,20 ,20 ) },
            { "vs",      new Vec3(104,33 ,122) },
            { "cobalt",  new Vec3(36 ,105,148) },
            { "wktk",    new Vec3(85 ,255,170) },
            { "fire",    new Vec3(240,87 ,0  ) },
            { "key",     new Vec3(247,224,90 ) },
            { "pumpkins",new Vec3(235,137,49 ) },
            //TODO: /color - add a system for colors that can change over time, possibly using keyframes of sort (separate per R G B values?)
          //{ "ayla",   new Vec3(0  ,0  ,0  ) },
        };

        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] { "color", "colour", "col", "c" };
        }

        /// <summary>
        /// Returns a list containing the expected arguments, as displayed in the "usage" message for this command, as well as for use for naming or type checking.
        /// <para />The order of the CommandArguments in the list is obviously important.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            return new[] { new CommandArgument("Color", true) {DisplayText = "[Color (Name/Hex/RGB)]" } };
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[] { "Changes the color of your duck.",
                           "RGB values must be between 0 and 255.",
                           "Leave out any arguments to print the currently selected color."};
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            if (args.Count > 0)
            {
                var res = CommandHelpers.GetColorFromString(args);

                if (res == null)
                {
                    //TODO: /color ArgumentErrors - be more precise by checking input (similar to name? RGB or Hex wrongly formatted?)
                    yield return new ArgumentError(-1, "Invalid color specified.");
                }
            }
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player (or the local player, if IsLocalOnly returns true).
        /// <para />Return null to indicate a successful execute (command executed as expected), an informing ExecuteInfo about the command's actions,
        /// or an error ExecuteInfo to indicate failure (specified parameters caused exception).
        /// <para />Do not use ExecuteErrors for parameter validation (this is done by CheckArgumentsValid before execution), but only for anything that can only be determined on Execute!
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            try
            {
                if (args.Count > 0)
                {
                    var res = CommandHelpers.SetColorFromString(args, sender);

                    if (res)
                    {
                        return null;
                    }
                    else
                    {
                        return new ExecuteInfo(string.Format("Invalid color \"{0}\" specified.", string.Join(" ", args)), 0);
                    }
                }
                else
                {
                    //Print current color
                    if (!sender.localPlayer)
                        return null;

                    var color = sender.persona.colorUsable;
                    var colorname = MserGlobals.GetNameOfColor(color, ReturnValueFormat.Visual);

                    return new ExecuteInfo("Currently selected color: " + colorname);
                }
            }
            catch
            {
                return new ExecuteInfo("An unexpected exception occured changing the color.", 0);
            }
        }

        /// <summary>
        /// Returns a list of strings that represents the auto complete info for the specified argument of the command.
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args"></param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (args.Any())
            {
                //Check for valido
                int ass;
                if (((index == 0 && args[0].StartsWith("#")) || (index >= 0 && int.TryParse(args[0], out ass))) && CommandHelpers.GetColorFromString(args) != null)
                {
                    //Is a RGB/Hex value
                    return new[] {CacheManager.GetColorAutoCompleteEntry(args)};
                }

                //Return list of all color names
                return CacheManager.GetColorAutoCompleteInfo();
            }

            return null;
        }
    }
}
