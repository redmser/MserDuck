﻿using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class AliasCommand : CommandHandler, IAutoRun
    {
        //TODO: /alias command - condition for IBind that the alias isnt being executed! this should make /run also not applicable for the IBind implementation!
        //TODO: /alias command - how to remove an alias?

        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] {"alias", "a", "macro"};
        }

        /// <summary>
        /// Returns a list containing the expected arguments, as displayed in the "usage" message for this command, as well as for use for naming or type checking.
        /// <para />The order of the CommandArguments in the list is obviously important.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            //TODO: /alias arguments - display parameters + names and optional here (instead of autocomplete entries)
            return new CommandArgument[] {"Name", "?Command", "?Arguments"};
        }

        /// <summary>
        /// Whether this command should execute only for the client that entered it, or for every connected user as well.
        /// </summary>
        /// <returns></returns>
        public override bool IsLocalOnly()
        {
            return true;
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[]
            {
                "Defines an alias, executing the specified command whenever the alias is executed.",
                "Omit command and arguments in order to execute the specified alias, otherwise (re-)defines it.",
                "To force execution, use /run instead.",
                "Use %p% inside of the command arguments to define a parameter \"p\" which has to be specified on execution.",
                "Define an optional parameter \"o\" using %o=value%."
            };
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player (or the local player, if IsLocalOnly returns true).
        /// <para />Return null to indicate a successful execute (command executed as expected), an informing ExecuteInfo about the command's actions,
        /// or an error ExecuteInfo to indicate failure (specified parameters caused exception).
        /// <para />Do not use ExecuteErrors for parameter validation (this is done by CheckArgumentsValid before execution), but only for anything that can only be determined on Execute!
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            //Handle alias name
            var name = args[0];

            //Check if whatever follows is a definition or parameters
            var exist = AliasManager.GetAlias(name);
            if (args.Count == 1)
            {
                //Check if it can execute
                if (exist == null)
                    return new ExecuteInfo("Could not find the specified alias.", 0);

                //Has to be execution, go for it
                AliasManager.ExecuteAlias(name);
                return null;
            }

            if (exist != null && AliasManager.CountParams(exist) > 0 && !IsCommandName(args[1]))
            {
                //Is parameters if alias exists, contains % in it and first param is not a command's name!
                AliasManager.ExecuteAlias(name, args.GetRange(1, args.Count - 1));
                return null;
            }

            //Has to be a definition
            return AddAlias(name, string.Join(" ", args.GetRange(1, args.Count - 1)));
        }

        protected virtual ExecuteInfo AddAlias(string name, string commandline)
        {
            AliasManager.AddAlias(name, commandline);
            return new ExecuteInfo("Successfully defined alias!");
        }

        protected virtual bool IsCommandName(string name)
        {
            return ChatManager.GetCommandByName(name) != null;
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            var cropped = args.Crop().ToList();

            if (!args.Any() || string.IsNullOrWhiteSpace(args[0]))
            {
                yield return ArgumentError.InvalidArgumentCount();
            }
            else
            {
                var name = args[0];

                //I am so sorry :(
                float ass;
                if (name.EqualsTo(true, "delay") || float.TryParse(name, out ass))
                    yield return new ArgumentError(0, "Invalid alias name, please select another.");

                var alias = AliasManager.GetAlias(name);
                if (alias == null)
                {
                    //New definition, can't go too wrong here
                    foreach (var ae in NewDef(args))
                    {
                        yield return ae;
                    }
                }
                else
                {
                    //Check if new definition
                    var allparas = AliasManager.CountParams(alias);
                    if (allparas > 0 && args.Count > 1 && !IsCommandName(args[1]))
                    {
                        //Has parameters
                        var neededparas = AliasManager.CountParams(alias, false);
                        var specd = cropped.Count - 1;

                        if (specd < neededparas)
                        {
                            //No paras even though needed
                            yield return new ArgumentError(specd, string.Format("No value for parameter \"{0}\" specified.",
                                AliasManager.GetParameterName(alias, specd + 1)));
                        }
                        else if (specd > allparas)
                        {
                            //Too many paras!
                            yield return new ArgumentError(specd, string.Format("Specified {0}, but the alias only accepts {1}.",
                                Extensions.GetPlural(specd, "parameter"), allparas));
                        }
                    }
                    else
                    {
                        //New definition
                        foreach (var ae in NewDef(args))
                        {
                            yield return ae;
                        }
                    }
                }
            }
        }

        protected virtual IEnumerable<ArgumentError> NewDef(List<string> args)
        {
            var name = args[0];
            var alias = AliasManager.GetAlias(name);
            var cmdline = string.Join(" ", args);

            if (args.Count > 1)
            {
                var cmd = ChatManager.GetCommandByName(args[1]);
                if (cmd == null)
                {
                    //What command??
                    yield return ArgumentError.CommandUnknown(1);
                }
                else if (args.Count > 2)
                {
                    //Cloned argumenterrors - but only if not a parameter
                    var aes = cmd.CheckArgumentsValid(args.GetRange(2, args.Count - 2));
                    if (aes != null)
                    {
                        foreach (var ae in aes)
                        {
                            ae.Index += 2;
                            if (args[ae.Index].Contains("%"))
                                continue;

                            yield return ae;
                        }
                    }
                }
            }

            //General info
            var parcnt = AliasManager.CountParams(cmdline);

            var redefinfo = "";
            if (alias != null)
            {
                redefinfo = "\nOld definition: " + alias.ToString(true, false);
            }

            if (parcnt > 0)
            {
                yield return new ArgumentError(0, string.Format("Defining new alias with {0}.{1}", Extensions.GetPlural(parcnt, "parameter"), redefinfo), Severity.Info);
            }
            else
            {
                yield return new ArgumentError(0, string.Format("Defining new parameterless alias.{0}", redefinfo), Severity.Info);
            }

            //TODO: /alias validation - warn user if an optional parameter is not the last parameter in the list (meaning, another required parameter follows it)
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered, separated by spaces after the commandName.
        /// <para />Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            //BUG: /alias - recursed ACEs did not always show up (example: /alias name set all...)

            if (index == 0)
            {
                //Return all alias names
                var aces = GetAliasList();
                foreach (var ace in aces)
                {
                    yield return ace;
                }
            }
            else if (args.Count > 1)
            {
                var name = args[0];
                var alias = AliasManager.GetAlias(name);
                var cmd = ChatManager.GetCommandByName(args[1]);
                if (alias == null && cmd != null)
                {
                    //New definition, can't go too wrong here
                    var aces = cmd.GetAutoCompleteEntries(index, args.GetRange(2, args.Count - 2));
                    if (aces == null)
                        yield break;
                    foreach (var ace in aces)
                    {
                        yield return ace;
                    }
                }
                else if (alias != null)
                {
                    //Check if new definition
                    var allparas = AliasManager.CountParams(alias);
                    if (allparas > 0 && !IsCommandName(args[1]))
                    {
                        //Has parameters
                        var specd = args.Count - 1;

                        if (specd < allparas)
                        {
                            //Name and whether optional or not
                            var optional = AliasManager.IsOptionalParameter(alias, specd) ? "[Optional] " : "";
                            var display = string.Format("{0}{1}", optional, AliasManager.GetParameterName(alias, specd));
                            yield return new AutoCompleteEntry(display, null, false, null, false);
                        }
                    }
                    else if (cmd != null)
                    {
                        //New definition
                        var aces = cmd.GetAutoCompleteEntries(index, args.GetRange(2, args.Count - 2));
                        if (aces == null)
                            yield break;
                        foreach (var ace in aces)
                        {
                            yield return ace;
                        }
                    }
                }
            }
        }

        protected virtual IEnumerable<AutoCompleteEntry> GetAliasList()
        {
            return new[] { new AutoCompleteEntry("<New Alias Name>", null, true, null, false) }
                    .Concat(AliasManager.Aliases.Select(a => new AutoCompleteEntry(a.Name)));
        }
    }
}
