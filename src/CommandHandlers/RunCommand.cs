﻿using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class RunCommand : AliasCommand
    {
        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] {"run"};
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[]
            {
                "Runs the specified alias.",
                "This can also be done using /alias, but this forces execution,",
                "meaning this command will never try to re-define the alias."
            };
        }

        /// <summary>
        /// Returns a list containing the expected arguments, as displayed in the "usage" message for this command, as well as for use for naming or type checking.
        /// <para />The order of the CommandArguments in the list is obviously important.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            return new CommandArgument[] { "Name", "?Parameters" };
        }

        protected override ExecuteInfo AddAlias(string name, string commandline)
        {
            return new ExecuteInfo("Could not find the specified alias.", 0);
        }

        protected override IEnumerable<ArgumentError> NewDef(List<string> args)
        {
            yield return new ArgumentError(0, "Could not find the specified alias.");
        }

        protected override IEnumerable<AutoCompleteEntry> GetAliasList()
        {
            return AliasManager.Aliases.Select(a => new AutoCompleteEntry(a.Name));
        }

        protected override bool IsCommandName(string name)
        {
            return false;
        }
    }
}
