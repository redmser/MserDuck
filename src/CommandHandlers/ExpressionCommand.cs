﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class ExpressionCommand : CommandHandler
    {
        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] {"execute", "exec", "expression", "exp", "e"};
        }

        /// <summary>
        /// Returns a string containing the expected arguments, as displayed in the "usage" message for this command.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            if (args.Count > 0 && args[0].EqualsToAny(true, "echo", "call", "echolocal", "calllocal"))
            {
                return new CommandArgument[] { "Action/echo/call/echolocal/calllocal", "Expression" };
            }
            else if (args.Count > 0 && args[0].EqualsToAny(true, "yes", "no", "reset", "yesall", "noall"))
            {
                return new CommandArgument[] { "Action/reset/yes/no/yesall/noall" };
            }

            return new CommandArgument[] { "Action/echo/call/echolocal/calllocal/reset/yes/no/yesall/noall", "?..." };
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[]
            {
                "Call executes the specified code.",
                "Echo acts as a return statement for the specified code.",
                "Calllocal/Echolocal only executes for yourself. Reset to clear all saved expressions.",
                "Code is interpreted as it would be in C#, in a static method.",
                "Use yes/no to accept/decline incoming expresssions - append all to do so for all."
            };
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            if (args.Count == 0)
            {
                yield return ArgumentError.InvalidArgumentCount(1, 0, true);
            }
            else
            {
                if (args.Count > 1 && (args[0] == "y" || args[0] == "yall" || args[0] == "yes" || args[0] == "yesall" ||
                    args[0] == "n" || args[0] == "nall" || args[0] == "no" || args[0] == "noall" || args[0] == "reset"))
                {
                    yield return ArgumentError.ArgumentsNotUsed(true, 1);
                }
                else if (args[0] != "y" && args[0] != "yall" && args[0] != "yes" && args[0] != "yesall" &&
                         args[0] != "n" && args[0] != "nall" && args[0] != "no" && args[0] != "noall" &&
                         args[0] != "reset" && args[0] != "call" && args[0] != "echo" && args[0] != "calllocal" && args[0] != "echolocal" &&
                         args[0] != "e" && args[0] != "c" && args[0] != "el" && args[0] != "cl")
                {
                    yield return new ArgumentError(0, "Unknown action specified. Supported: yes, no, yesall, noall, call(local), echo(local), reset");
                }
            }
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player.
        /// <para />Return true to indicate success, false to indicate failure (will print usage).
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            try
            {
                //expression reset - remove all saved snippets
                if (args[0] == "reset" && sender.localPlayer)
                {
                    //Reset the snippet list
                    if (File.Exists(CodeSnippet.SnippetFile))
                        File.Delete(CodeSnippet.SnippetFile);

                    if (CacheManager.KnownCodeSnippets != null)
                        CacheManager.KnownCodeSnippets.Clear();

                    CodeSnippet._pendingSnippet = null;
                    return new ExecuteInfo("Reset all code snippet states!");
                }

                //expression y/n - accept or decline latest pending snippet
                if (sender.localPlayer &&
                    (args[0] == "y" || args[0] == "yes" || args[0] == "yall" || args[0] == "yesall" ||
                    args[0] == "n" || args[0] == "no" || args[0] == "nall" || args[0] == "noall"))
                {
                    if (CodeSnippet._pendingSnippet == null)
                    {
                        return new ExecuteInfo("No code snippet to confirm!");
                    }

                    var action = "";

                    switch (args[0])
                    {
                        case "y":
                        case "yes":
                            CodeSnippet._pendingSnippet.Permitted = true;
                            //Check if in level that snippet got initially sent at!
                            if (Level.current == CodeSnippet._pendingSnippet.ReceivedLevel)
                                CodeSnippet.ExecuteCodeSnippet(CodeSnippet._pendingSnippet);
                            action = "Accepted";
                            break;
                        case "n":
                        case "no":
                            CodeSnippet._pendingSnippet.Permitted = false;
                            action = "Rejected";
                            break;
                        case "yall":
                        case "yesall":
                            var cnt = CodeSnippet._snippetQueue.Count;
                            do
                            {
                                CodeSnippet._pendingSnippet.Permitted = true;
                                //Check if in level that snippet got initially sent at!
                                if (Level.current == CodeSnippet._pendingSnippet.ReceivedLevel)
                                    CodeSnippet.ExecuteCodeSnippet(CodeSnippet._pendingSnippet);
                                CacheManager.KnownCodeSnippets.Add(CodeSnippet._pendingSnippet);
                                CodeSnippet._pendingSnippet = null;
                                CodeSnippet.MoveThroughQueue();
                            } while (CodeSnippet._snippetQueue.Any());
                            CodeSnippet.SaveSnippets();
                            return new ExecuteInfo(string.Format("Accepted {0} code snippet(s)!", cnt));
                        case "nall":
                        case "noall":
                            var cnt2 = CodeSnippet._snippetQueue.Count;
                            do
                            {
                                CodeSnippet._pendingSnippet.Permitted = false;
                                CacheManager.KnownCodeSnippets.Add(CodeSnippet._pendingSnippet);
                                CodeSnippet._pendingSnippet = null;
                                CodeSnippet.MoveThroughQueue();
                            } while (CodeSnippet._snippetQueue.Any());
                            CodeSnippet.SaveSnippets();
                            return new ExecuteInfo(string.Format("Rejected {0} code snippet(s)!", cnt2));
                    }

                    //For simple yes/no calls
                    CodeSnippet.LoadSnippetsIfNeeded();
                    CacheManager.KnownCodeSnippets.Add(CodeSnippet._pendingSnippet);
                    CodeSnippet._pendingSnippet = null;
                    CodeSnippet.SaveSnippets();
                    CodeSnippet.MoveThroughQueue();
                    return new ExecuteInfo(action + " code snippet!");
                }

                //Basic command execution
                var codeargs = args.GetRange(1, args.Count - 1);
                var local = false;
                string code;

                switch (args[0])
                {
                    //TODO: /execute - replace "return null" with a "return new NoValue()" and replace the hidden value check for
                    //  CodeSnippet's ExecuteCodeSnippet check and similar. not really backwards-compatible though
                    case "e":
                    case "echo":
                        code = "return " + string.Join(" ", codeargs);
                        break;
                    case "el":
                    case "echolocal":
                        local = true;
                        goto case "e";
                    case "c":
                    case "call":
                        code = string.Join(" ", codeargs);
                        code += "; return null";
                        break;
                    case "cl":
                    case "calllocal":
                        local = true;
                        goto case "c";
                    default:
                        return new ExecuteInfo("Unknown action specified.", 0, Severity.Usage);
                }

                //Return if to be run local and sender isn't local duck
                if (local && !sender.localPlayer)
                    return null;

                //Get snippet if existing
                //BUG: /execute - bundling same commands into one accept does not seem to work correctly
                var snip = CodeSnippet.GetSnippetFromCode(code);

                //Try caching
                if (snip == null)
                {
                    //Create new snippet
                    snip = new CodeSnippet(code, sender, Level.current);
                }

                //THIS CHECK SAVES LIVES!!!
                if (!CodeSnippet.ValidCodeSnippet(sender, snip))
                    return null;

                CodeSnippet.ExecuteCodeSnippet(snip);

                if (sender.localPlayer)
                {
                    //Save now since it is known a new snippet got added
                    CodeSnippet.SaveSnippets();
                }

                return null;
            }
            catch (Exception ex)
            {
                Program.LogLine("Error in ExecuteCommand: " + ex.Message + ex.StackTrace);
                return new ExecuteInfo("Unexpected exception during execute command.", -1);
            }
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered. Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (index <= 0)
            {
                //Keywords
                var acceptwords = new List<AutoCompleteEntry>();

                //Add accepting keywords based on context (should this be done though?? will not show on /bind for example!)
                if (CodeSnippet._pendingSnippet != null)
                    acceptwords = new List<AutoCompleteEntry> { "yes", "no", "yesall", "noall" };

                return new AutoCompleteEntry[]
                {
                    "echo", "echolocal", "call", "calllocal", "reset"
                }.Concat(acceptwords);
            }

            //TODO: /execute - Incredibly simple code autocomplete (using reflection perhaps?) - compare with DebugUI's code editor
            return null;
        }
    }
}
