﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using DuckGame.MserDuck.CommandHandlers;
using DuckGame.MserDuck.ModifierHandlers;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Collection of DuckGame and MserDuck-related events. All methods inside get called, unless otherwise stated.
    /// <para />Callers may be injected methods, so be sure not to modify the signature.
    /// </summary>
    public static class MserEvents
    {
        /// <summary>
        /// Called before any other mod loaded. This should not be modified, as it only sets up anything application-dependant, nothing mod-related!
        /// </summary>
        public static void OnPreInitialize()
        {
            //Only notify about MserDuck launching. Do not use this for initializing,
            //to avoid this mod relying on code of mods that aren't loaded yet.
            MonoMain.loadMessage = "Loading MserDuck";
            Program.LogLine(String.Format("-- MserDuck initialized on {1} at {0} --", DateTime.Now.ToLongTimeString(), DateTime.Now.ToShortDateString()));

            //DuckGame window changes
            Program.main.Window.Title = "MserDuck";
            Program.main.Window.AllowUserResizing = true;
            Program.main.Window.ClientSizeChanged += MserEvents.OnWindowResize;
        }

        /// <summary>
        /// Called when loading MserDuck. This can be used to initialize any custom data, but be aware that Level.current is null.
        /// <para />If you need anything to execute later on, either use a Thing's Initialize method, or use the Update method with very limited checks!
        /// </summary>
        public static void OnInitialize()
        {
            var prof = MserDebug.StartProfiling("MserDuckInitialize");

            try
            {
                // Initializers!
                // HINT: Anything you want to happen at the launch of the game should be run here.
                // Note that the game is still loading, so Level.current is null and no duck is spawned yet.
                // If you need anything to execute as soon as a certain state is reached, simply toss it into the Update loop
                // or check if there is a method that gets executed then.
                // Be sure to add profiling info!

                try
                {
                    MusicEx.SearchSongDir(MserGlobals.GetContentDir() + "Music");
                    prof.StepProfiling("SearchSongDir");
                }
                catch (Exception e)
                {
                    Program.LogLine("Error initializing custom song directory! " + e.Message + e.StackTrace);
                }

                //Replaced with better systems
                //MserGlobals.InitializeCustomLevels();
                //prof.StepProfiling("InitializeCustomLevels");

                try
                {
                    Network.Initialize();
                    prof.StepProfiling("NetworkInit");
                }
                catch (Exception e)
                {
                    Program.LogLine("Error initializing netmessages! " + e.Message + e.StackTrace);
                }

                try
                {
                    ModifierManager.CreateModifierMenu();
                    prof.StepProfiling("CreateModifierMenu");
                }
                catch (Exception e)
                {
                    Program.LogLine("Error initializing mserduck options! " + e.Message + e.StackTrace);
                }

                try
                {
                    KeyLayoutManager.Initialize();
                    prof.StepProfiling("KeyLayoutManagerInit");
                }
                catch (Exception e)
                {
                    Program.LogLine("Error initializing custom keyboard layouts! " + e.Message + e.StackTrace);
                }

                try
                {
                    CodeSnippet.Initialize();
                    prof.StepProfiling("CodeSnippetInit");
                }
                catch (Exception e)
                {
                    Program.LogLine("Error initializing code snippets! " + e.Message + e.StackTrace);
                }

                try
                {
                    WeaponFakker.Initialize();
                    prof.StepProfiling("WeaponFakkerInit");
                }
                catch (Exception e)
                {
                    Program.LogLine("Error initializing fak weapons! " + e.Message + e.StackTrace);
                }

                //Broken and unused anyway
                //DeathCrateHat.Init();
                //prof.StepProfiling("DeathCrateHatInit");

                try
                {
                    PInvoke.UnblockDll(MserDuck.Injector.InjectorPath); //This has to be called before method injection
                    prof.StepProfiling("UnblockDll");
                }
                catch (Exception e)
                {
                    Program.LogLine("Error unblocking method injector! " + e.Message + e.StackTrace);
                }

                try
                {
                    // Inject MserDuck code
                    MserDuck.PerformMethodInjection();
                    prof.StepProfiling("Injections");
                }
                catch (Exception e)
                {
                    Program.LogLine("Fatal error: method injection failed! Check the following info for more details: " + e.Message + e.StackTrace);
                }

                // Load misc config
                ConfigManager.LoadConfigFile(true);
                prof.StepProfiling("LoadConfigFile");
                ConfigManager.LoadAutoexecutes();
                prof.StepProfiling("LoadAutoexecs");

                // Clear ducklog
                if (DuckLogManager.RemoveLog == DuckLogManager.RemoveLogMode.OnStart ||
                    DuckLogManager.RemoveLog == DuckLogManager.RemoveLogMode.FileSize)
                {
                    DuckLogManager.RemoveDuckLog();
                }
            }
            catch (Exception e)
            {
                Program.LogLine("ERROR DURING INITIALIZE! " + e.Message + e.StackTrace);
            }
            finally
            {
                //Check version number
                if (MserDuck.CheckVersion)
                {
                    var update = new Thread(GitHelper.UpdateCommitNumber);
                    update.Start();
                }
                else
                {
                    Program.LogLine("Loading of MserDuck sure has finished. Enjoy your stay! :)");
                }
            }

            prof.EndProfiling();
        }

        /// <summary>
        /// Called every tick. Do not rely on another thing's Update to be called before/after this, as the order is not reliable.
        /// </summary>
        public static void OnTick()
        {
            try
            {
                // HINT: Add any function call you want to run once per frame here!
                // Be sure to add profiling info!
                var prof = MserDebug.StartProfiling("MserDuckUpdate");

                BindManager.Update();
                MserDebug.StepProfiling(prof, "BindManager");

                LobbyHandler.Update();
                MserDebug.StepProfiling(prof, "LobbyHandler");

                CodeSnippet.Update();
                MserDebug.StepProfiling(prof, "CodeSnippet");

                ModifierManager.Update();
                MserDebug.StepProfiling(prof, "ModifierManager");

                MenuBackgroundHandler.Update();
                MserDebug.StepProfiling(prof, "MenuBackgroundHandler");

                WeaponSubstitutionHandler.Update();
                MserDebug.StepProfiling(prof, "WeaponSubstitutionHandler");

                LevelChangeDetector.Update();
                MserDebug.StepProfiling(prof, "LevelChangeDetector");

                CapeHandler.Update();
                MserDebug.StepProfiling(prof, "CapeHandler");

                ChainCommand.Update();
                MserDebug.StepProfiling(prof, "ChainCommand");

                DuckBehaviourHandler.Update();
                MserDebug.StepProfiling(prof, "DuckBehaviourHandler");

                TimerEntry.Update();
                MserDebug.StepProfiling(prof, "TimerEntry");

				VocaQuackCommand.Update();
				MserDebug.StepProfiling(prof, "VocaQuack");

                MserDebug.EndProfiling(prof);

                //Should be last to track any entity changes!
                DebugUI.Update();
            }
            catch (Exception e)
            {
                if (_onTickExceptionShown)
                    return;

                _onTickExceptionShown = true;
                Program.LogLine("ERROR DURING UPDATE (suppressing further errors, there may be more): " + e.Message + e.StackTrace);
            }
        }

        /// <summary>
        /// Whether a "OnTick" exception has already been printed to the ducklog once.
        /// </summary>
        private static bool _onTickExceptionShown = false;

        /// <summary>
        /// Executed just as the game is quitting. Useful for clean-up and autosaves.
        /// </summary>
        public static void OnGameExit()
        {
            try
            {
                //Autosave if enabled
                if (ConfigManager.AutoSave)
                {
                    Program.LogLine(String.Format("-- DuckGame is closing at {0} - performing config autosave... --", DateTime.Now.ToLongTimeString()));
                    ConfigManager.SaveConfigFile();
                }
                else
                {
                    Program.LogLine(String.Format("-- DuckGame is closing at {0} --", DateTime.Now.ToLongTimeString()));
                }

                //Remove ducklog
                if (DuckLogManager.RemoveLog == DuckLogManager.RemoveLogMode.OnQuit)
                {
                    DuckLogManager.RemoveDuckLog();
                }

                //Save DebugUI geometry
                DebugUI.SaveGeometry();

                //TODO: MserDuck - autosave snippets when quitting instead
                //  Investigate whether saving is a huge slowdown
            }
            catch (Exception e)
            {
                Program.LogLine("ERROR EXITING MSERDUCK: " + e.Message + e.StackTrace);
            }
        }

        /// <summary>
        /// Called when the amount of ducks in the current server increased. Not called for the client joining!
        /// </summary>
        public static void OnJoin()
        {

        }

        /// <summary>
        /// Called every time a duck gets killed.
        /// <para />Original call to this method is irrelevant.
        /// </summary>
        /// <param name="d">Duck that was killed.</param>
        /// <param name="type">What the duck was killed by.</param>
        public static void OnKill(Duck d, DestroyType type)
        {
            //Spawn a glitchgrenade
            ModifierManager.GetModifier<GlitchGrenadeOnDeathModifier>().OnKill(d);
        }

        /// <summary>
        /// Level being drawn. Called for every Level, be sure to keep checks fast and simple.
        /// </summary>
        public static void OnLevelDraw()
        {
            //Before Level draw

            //Original Level call (override)
            if (Level.core.currentLevel != null)
            {
                Level.core.currentLevel.DoDraw();
            }

            //After Level draw
            SunBomb.DrawSun();
        }

        private static readonly List<Profile> _previousWinners = new List<Profile>();

        /// <summary>
        /// Run everytime the <code>Level.current</code> property changes. This happens the moment the level transition occurs.
        /// </summary>
        public static void OnLevelEnded()
        {
            //Reset capes
            CapeHandler.editedCapes.Clear();

            //Populate list
            MserEvents._previousWinners.Clear();
            MserEvents._previousWinners.AddRange(GameMode.lastWinners);
        }

        /// <summary>
        /// Run everytime a new round in a level starts. This happens the moment all ducks can move, and does not include non-deathmatch levels such as the intermissions.
        /// <para />This guarantees a <code>GameState.Playing</code>.
        /// </summary>
        public static void OnRoundStarted()
        {
            //Enable binds again, in case they got messed up
            BindManager.BindsEnabled = true;

            //Run autobinds for all clients
            try
            {
                for (var i = 0; i < AutoRunCommand.ExecuteList.Count; i++)
                {
                    var name = AutoRunCommand.ExecuteList[i];
                    var alias = AliasManager.GetAlias(name);
                    var paracnt = AliasManager.CountParams(alias);
                    var paras = new List<string>();
                    while (paras.Count < paracnt)
                    {
                        i++;
                        paras.Add(AutoRunCommand.ExecuteList[i]);
                    }
                    AliasManager.ExecuteAlias(name, paras);
                }
            }
            catch (Exception e)
            {
                Program.LogLine("Unable to execute autorun alias: " + e.Message + e.StackTrace);
            }

            //Cars on start
            if (ModifierManager.GetModifier<StartWithCarModifier>().Enabled)
            {
                StartWithCarModifier.SpawnCars();
            }

            /////////////////
            // Server only //
            /////////////////

            if (Network.isActive && !Network.isServer)
                return;

            //Purple Block for Winner
            if (ModifierManager.GetModifier<PurpleBlockForWinnersModifier>().Enabled)
            {
                ModifierManager.GetModifier<PurpleBlockForWinnersModifier>().OnLevelChanged();

                foreach (var winner in MserEvents._previousWinners)
                {
                    ModifierManager.GetModifier<PurpleBlockForWinnersModifier>().GrantSaveBlock(winner);
                }
            }

            //Spawn boomerang if fell off
            if (Boomerang.NumToReturn > 0 && Rando.Int(0, 4) == 2)
            {
                Boomerang.NumToReturn--;

                //Spawn a boomerang flying in
                var ducks = Level.current.things[typeof(Duck)].Cast<Duck>().ToList();
                var duck = ducks[Rando.Int(0, ducks.Count - 1)];
                var side = Rando.Int(0, 1);
                var border = side == 0 ? Level.current.camera.left : Level.current.camera.right;
                var boom = new Boomerang(border, duck.y - 24);
                boom.hSpeed = side == 0 ? 10 : -10;
                boom.vSpeed = -0.5f;
                boom.launch();
                boom.willReturn = Rando.Int(0, 4) == 2;
                Level.Add(boom);
            }
        }

        /// <summary>
        /// Called when the game crashes, overriding the old crash handler.
        /// </summary>
        /// <param name="e"></param>
        public static void HandleGameCrash(Exception e)
        {
            try
            {
                //Log the error
                var error = MonoMain.hadInfiniteLoop ? MonoMain.infiniteLoopDetails : MonoMain.GetExceptionString(e);
                Program.LogLine(error);

                //I believe this sends a WM_CLOSE, not sure why it is needed though
                PInvoke.SendMessage(Program.main.Window.Handle, 16u, IntPtr.Zero, IntPtr.Zero);
                Environment.Exit(1);
            }
            catch (Exception)
            {
                var file = new StreamWriter("ducklog.txt", true);
                file.WriteLine("Failed to write exception to log.\n");
                file.Close();
            }
        }

        /// <summary>
        /// Executed to draw graphics on the Layer.Console. Only shown if Network.isActive!
        /// </summary>
        public static void OnConsoleDraw()
        {
            DebugUI.Draw();
            ModifierManager.DrawDescriptions();
        }

        /// <summary>
        /// Called whenever DuckGame's window is resized.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void OnWindowResize(object sender, EventArgs e)
        {
            //Currently not in place due to black screens and crap
            //MserDuck._newSize = new Vec2(Program.main.Window.ClientBounds.Width, Program.main.Window.ClientBounds.Height);
        }

        /// <summary>
        /// Called whenever a new Event is logged and added to the Events list.
        /// </summary>
        /// <param name="e">The event that has been logged.</param>
        public static void OnLogEvent(LoggedEvent e)
        {
            try
            {
                var name = e.Type;
                if (name == "RoundStartEvent")
                {
                    MserEvents.OnRoundStarted();
                }
                else if (name == "DisarmEvent" && ModifierManager.GetModifier<SuperDisarmModifier>().Enabled)
                {
                    //TODO: MserEvents - move all kinds of disarm handling to its own function
                    SuperDisarmModifier.DoSuperDisarm(e.Victim.duck);
                }
            }
            catch (Exception ex)
            {
                Program.LogLine("Error handling logged event " + e.Type + ": " + ex.Message + ex.StackTrace);
            }
        }
    }
}
