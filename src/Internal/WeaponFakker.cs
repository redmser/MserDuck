﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Helper class for turning weapons into fak weapons.
    /// </summary>
    public static class WeaponFakker
    {
        /// <summary>
        /// The namespace in which fak items are in.
        /// </summary>
        public const string FakNamespace = "DuckGame.MserDuck.Fakes";

        /// <summary>
        /// Converts the specified PhysicsObject to its fak version, if one exists and it isn't already a fak.
        /// <para />This will remove and readd it from the level, trying to replace it seamlessly!
        /// </summary>
        /// <param name="obj"></param>
        public static bool ConvertToFak(PhysicsObject obj)
        {
            //Program.LogLine("Enter ToFak");
            //Has to have a fak version to convert to
            if (!HasFakVersion(obj))
                return false;
            //Program.LogLine("TF: has a FAK");

            //Has a fak version, create it!
            var fak = (PhysicsObject)Activator.CreateInstance(WeaponFakker.GetFakVersion(obj), 0, 0);
            if (fak.GetType() == obj.GetType())
                return false;

            //Program.LogLine("TF: Converting " + obj.GetType().Name + " to FAK " + fak.GetType().Name);
            WeaponSubstitutionHandler.SubstituteItem(obj, fak, true, false);
            return true;
        }

        /// <summary>
        /// Converts the specified PhysicsObject to its normal version if it currently is fak.
        /// <para />This will remove and readd it from the level, trying to replace it seamlessly!
        /// </summary>
        /// <param name="obj"></param>
        public static bool ConvertFromFak(PhysicsObject obj)
        {
            //Should not already be a fak
            //Program.LogLine("Enter FromFak");
            if (!IsFak(obj))
                return false;
            //Program.LogLine("FF: is a FAK");

            //Try to see if a regular version exists
            var t = GetAttributeType(obj.GetType());
            if (t == null)
                return false;
            //Program.LogLine("FF: has attr type");

            //Has a regular version, create it!
            var normalo = (PhysicsObject)Activator.CreateInstance(t, 0, 0);
            if (normalo.GetType() == obj.GetType())
                return false;

            //Program.LogLine("FF: Converting FAK " + obj.GetType().Name + " to " + normalo.GetType().Name);
            WeaponSubstitutionHandler.SubstituteItem(obj, normalo, true, false);
            return true;
        }

        /// <summary>
        /// Toggles the state of the specified PhysicsObject between fak and regular, depending on what it currently is.
        /// </summary>
        /// <param name="obj"></param>
        public static bool ToggleFakState(PhysicsObject obj)
        {
            //All required checks are inside, these will not trigger twice!
            return ConvertFromFak(obj) || WeaponFakker.ConvertToFak(obj);
        }

        /// <summary>
        /// Whether the specified type is a fak item.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool IsFak(Type obj)
        {
            return obj.Namespace == FakNamespace;
        }

        /// <summary>
        /// Whether the specified PhysicsObject is a fak.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool IsFak(PhysicsObject obj)
        {
            return IsFak(obj.GetType());
        }

        /// <summary>
        /// Whether the specified PhysicsObject (regular version) has a fak variant.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool HasFakVersion(PhysicsObject obj)
        {
            return HasFakVersion(obj.GetType());
        }

        /// <summary>
        /// Whether the specified PhysicsObject (regular version) has a fak variant.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool HasFakVersion(Type obj)
        {
            //Fak weapon has a fak version (obviously)
            if (IsFak(obj))
                return true;

            return GetFakVersion(obj) != null;
        }

        /// <summary>
        /// Returns the fak version of this item, or null if none exists.
        /// </summary>
        /// <param name="regularVersion"></param>
        /// <returns></returns>
        public static Type GetFakVersion(Type regularVersion)
        {
            if (IsFak(regularVersion))
                return regularVersion;

            foreach (var fak in WeaponFakker.FakTypes)
            {
                var attr = WeaponFakker.GetAttributeType(fak);
                if (attr == regularVersion)
                    return fak;
            }

            return null;
        }

        /// <summary>
        /// Returns the fak version of this item, or null if none exists.
        /// </summary>
        /// <param name="regularVersion"></param>
        /// <returns></returns>
        private static Type GetFakVersion(PhysicsObject regularVersion)
        {
            return GetFakVersion(regularVersion.GetType());
        }

        /// <summary>
        /// Returns the regular version of this fak item. May return null if none is specified.
        /// </summary>
        /// <param name="fakVersion"></param>
        /// <returns></returns>
        public static Type GetRegularVersion(Type fakVersion)
        {
            if (!IsFak(fakVersion))
                return fakVersion;

            return GetAttributeType(fakVersion);
        }

        /// <summary>
        /// Returns the regular version of this fak item. May return null if none is specified.
        /// </summary>
        /// <param name="fakVersion"></param>
        /// <returns></returns>
        public static Type GetRegularVersion(PhysicsObject fakVersion)
        {
            return GetRegularVersion(fakVersion.GetType());
        }

        private static Type GetAttributeType(Type fakVersion)
        {
            var bag = ContentProperties.GetBag(fakVersion);
            object obj;
            try
            {
                obj = bag.Get("fakSubstitute");
            }
            catch (PropertyNotFoundException)
            {
                return null;
            }

            //Try type first
            if ((obj as Type) != null)
                return (Type)obj;

            //Try string to type next
            var t = MserGlobals.GetType(obj.ToString());
            return t;
        }

        private static readonly FieldInfo _bagDictionary = MserGlobals.GetField(typeof(PropertyBag), "_dictionary");

        internal static void Initialize()
        {
            //Add can't-spawn property to all faks
            foreach (var fak in FakTypes)
            {
                var bags = (IPropertyBag)ContentProperties.GetBag(fak);
                if (!bags.Contains("canSpawn"))
                {
                    //Not specified? Add it! - Since the property bag is supposedly read-only, use reflection to get it.
                    var dict = (Dictionary<string, object>)_bagDictionary.GetValue(bags);
                    dict.Add("canSpawn", false);
                }
                else if (bags.Get<bool>("canSpawn"))
                {
                    //Can spawn? Set it!
                    bags.Set("canSpawn", false);
                }
            }
        }

        /// <summary>
        /// A list of all fak weapon types.
        /// </summary>
        public static readonly IEnumerable<Type> FakTypes = typeof(MserDuck).Assembly.GetTypes().Where(t => t.IsClass && t.Namespace == FakNamespace);
    }
}
