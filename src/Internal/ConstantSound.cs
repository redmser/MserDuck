﻿using Microsoft.Xna.Framework.Audio;

namespace DuckGame.MserDuck
{

    public class ConstantSound : IAutoUpdate
    {

        public ConstantSound(string sound, float startVolume = 0f, float startPitch = 0f, string multiSound = null)
        {
            AutoUpdatables.Add(this);
            if (startVolume > 0f)
            {
                this._effect = SFX.Play(sound, startVolume * SFX.volume, startPitch, 0f, true);
                return;
            }
            if (multiSound != null)
            {
                this._effect = SFX.GetMultiSound(sound, multiSound);
                return;
            }
            this._effect = SFX.Get(sound, startVolume * SFX.volume, startPitch, 0f, true);
        }

        ~ConstantSound()
        {
            this._lerpSpeed = 0f;
            this._lerpVolume = 0f;
        }

        public void Kill()
        {
            this._killSound = true;
        }

        public void Update()
        {
            if (this._effect == null || (this._startLevel != null && Level.current != this._startLevel))
            {
                return;
            }
            if (this._killSound)
            {
                this._effect.Stop();
                return;
            }
            if (this._effect.Volume > 0.01f && this._effect.State != SoundState.Playing)
            {
                this._effect.Play();
                this._startLevel = Level.current;
            }
            else if (this._effect.Volume < 0.01f && this._effect.State == SoundState.Playing)
            {
                this._effect.Stop();
            }
            this._effect.Volume = Maths.LerpTowards(this._effect.Volume, this._lerpVolume * SFX.volume, this._lerpSpeed);
        }

        public float lerpSpeed
        {

            get
            {
                return this._lerpSpeed;
            }

            set
            {
                this._lerpSpeed = value;
            }
        }

        public float lerpVolume
        {

            get
            {
                return this._lerpVolume;
            }

            set
            {
                this._lerpVolume = value;
            }
        }

        public float pitch
        {

            get
            {
                return this._effect.Pitch;
            }

            set
            {
                this._effect.Pitch = value;
            }
        }

        public float volume
        {

            get
            {
                return this._effect.Volume;
            }

            set
            {
                this._effect.Volume = value;
                this._lerpVolume = value;
            }
        }

        private Sound _effect;

        private bool _killSound;

        private float _lerpSpeed = 0.1f;

        private float _lerpVolume;

        private Level _startLevel;
    }
}
