﻿namespace DuckGame.MserDuck
{
    /// <summary>
    /// Is a command that interacts with the binds system. Required to, for example, forbid using such a command as an argument of another.
    /// </summary>
    public interface IBind
    {

    }
}
