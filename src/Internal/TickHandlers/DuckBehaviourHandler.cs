﻿using System.Reflection;

namespace DuckGame.MserDuck
{
    public static class DuckBehaviourHandler
    {
        private static readonly MethodInfo _getVote = MserGlobals.GetMethod("Vote", "GetVote");
        private static readonly FieldInfo _leftStick = MserGlobals.GetField("RegisteredVote", "leftStick");
        private static readonly FieldInfo _rightStick = MserGlobals.GetField("RegisteredVote", "rightStick");
        private static FieldBinding _ISAAC;

        /// <summary>
        /// Trigger name to pitch quack. Set using <see cref="ConfigEntries.QuackPitchKeyEntry"/> in config.
        /// </summary>
        public static string QuackTrigger;

        /// <summary>
        /// Key value to pitch quack. Set using <see cref="ConfigEntries.QuackPitchKeyEntry"/> in config.
        /// </summary>
        public static Keys QuackKey;

        /// <summary>
        /// Whether to skip strafe-controlled pitching.
        /// </summary>
        public static bool SkipPitch;

        public static void Update()
        {
            if (MserGlobals.GetGameState() == GameState.RockThrow)
            {
                //Remove all guns
                var guns = Level.current.things[typeof(Gun)];
                foreach (Gun gun in guns)
                {
                    Level.Remove(gun);
                }
            }

            //Quack pitching
            if (SkipPitch || Level.current == null)
                return;

            var duck = MserGlobals.GetLocalDuck();
            if (duck == null)
                return;

            var pitching = false;
            if (DuckBehaviourHandler.QuackTrigger == null)
            {
                //Use key
                if (DuckBehaviourHandler.QuackKey != Keys.None)
                {
                    pitching = Keyboard.Down(DuckBehaviourHandler.QuackKey);
                }
            }
            else
            {
                //Always?
                if (QuackTrigger.EqualsTo(false, "ALWAYS"))
                {
                    pitching = true;
                }
                else
                {
                    //Use trigger
                    pitching = duck.inputProfile.Down(DuckBehaviourHandler.QuackTrigger);
                }
            }

            if (pitching)
            {
                //Do pitchy
                if (_ISAAC == null || _ISAAC.thing != duck)
                {
                    _ISAAC = new FieldBinding(duck, "quackPitch");
                    duck._netQuack.pitchBinding = _ISAAC;
                }
                duck.manualQuackPitch = true;
                duck.quackPitch = (byte)Maths.Clamp(Instrument.GetMousePitch(true) * 255, 0, 255);
            }
            else
            {
                //No pitchy
                duck.manualQuackPitch = false;
                duck.quackPitch = 0;
            }

            return;

            //TODO: DuckBehaviourHandler - Sync vote sign angle and slightly limit the amount
            var vote = _getVote.Invoke(null, new object[] { duck.profile });
            if (vote != null)
            {
                DuckBehaviourHandler._leftStick.SetValue(vote, new Vec2(Mouse.x - Layer.HUD.camera.width / 2, Mouse.y - Layer.HUD.camera.height / 2));
                if (Mouse.left == InputState.Down || Mouse.right == InputState.Down)
                    DuckBehaviourHandler._rightStick.SetValue(vote, new Vec2(Mouse.x - Layer.HUD.camera.width / 2, Mouse.y - Layer.HUD.camera.height / 2));
            }
        }
    }
}
