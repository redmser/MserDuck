﻿using System.Reflection;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Handler for replacing the main menu's background sprites.
    /// </summary>
    public static class MenuBackgroundHandler
    {
        private static int _updatedMenu = 0;

        private static MserText _text;

        public static void UpdateCommitNumber()
        {
            string str;
            Color col;

            if (MserDuck.CommitNumber < 0)
            {
                if (MserDuck.CheckVersion)
                {
                    if (MserDuck.CommitNumber == -1)
                    {
                        str = "...";
                        col = Color.Cyan;
                    }
                    else
                    {
                        str = "ERROR";
                        col = Color.DarkRed;
                    }
                }
                else
                {
                    str = null;
                    col = Color.Transparent;
                }
            }
            else
            {
                str = "v" + MserDuck.CommitNumber;
                col = Color.White;
            }

            if (str != null && MserGlobals.GetGameState() == GameState.TitleScreen)
            {
                if (_text != null)
                    Level.Remove(_text);
                _text = new MserText(28, 33, str, col);
                Level.Add(_text);
            }
        }

        public static void Update()
        {
            // Final check (_pressStartBlink) is there so this is called AFTER the .Initialize, because doing so before would not update anything (obviously).
            // If you know of a better way to do this without arbitrary delays, let me know! :)
            if (MenuBackgroundHandler._updatedMenu < 2 && MserGlobals.GetGameState() == GameState.TitleScreen &&
                (float)(MserGlobals.GetField("TitleScreen", "_pressStartBlink")).GetValue(Level.current) > 0f)
            {
                //Change logo
                var title = MserGlobals.GetField("TitleScreen", "_title").GetValue(Level.current);
                if (title != null)
                {
                    MserGlobals.GetProperty(typeof(Thing), "graphic").SetValue(title, new Sprite(Thing.GetPath<MserDuck>("mserDuckTitle")), null);
                    MenuBackgroundHandler._updatedMenu = 2;

                    //Add a version number too
                    UpdateCommitNumber();
                }

                //Change background
                if (MenuBackgroundHandler._updatedMenu == 0)
                {
                    FieldInfo spacebg = MserGlobals.GetField("TitleScreen", "_space");
                    Level.Remove((SpaceBackgroundMenu)spacebg.GetValue(Level.current));
                    var newbg = new MserMenuBackground(-999f, -999f, true, 0.6f);
                    newbg.update = false;

                    spacebg.SetValue(Level.current, newbg);
                    Level.Add(newbg);

                    FieldInfo metalbg = MserGlobals.GetField("TitleScreen", "_background");
                    metalbg.SetValue(Level.current, new Sprite(Thing.GetPath<MserDuck>("mserBackground"), 0f, 0f));

                    MenuBackgroundHandler._updatedMenu = 1;
                }
            }
            else if (MenuBackgroundHandler._updatedMenu > 0 && MserGlobals.GetGameState() != GameState.TitleScreen)
            {
                // In case the user would return to the menu, update it once needed.
                MenuBackgroundHandler._updatedMenu = 0;
            }
        }
    }
}
