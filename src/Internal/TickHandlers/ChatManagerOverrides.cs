﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Receives sent chat messages and decides how they should be handled.
    /// <para />The overridden DuckNetwork code for updating and displaying chat is found in the other partial part <code>ChatManagerOverrides.cs</code>.
    /// </summary>
    public static partial class ChatManager
    {
        private static readonly FieldInfo _coreField = MserGlobals.GetField("ConnectionStatusUI", "_core");
        private static readonly FieldInfo _barsField = MserGlobals.GetField("ConnectionStatusUICore", "bars");
        private static readonly FieldInfo _profileField = MserGlobals.GetField("ConnectionStatusBar", "profile");
        private static readonly FieldInfo _positionField = MserGlobals.GetField("ConnectionStatusBar", "position");
        private static readonly FieldInfo _debarField = MserGlobals.GetField("ConnectionStatusUI", "_bar");

        private static readonly FieldInfo _pauseOpen = MserGlobals.GetField(typeof(DuckNetwork), "_pauseOpen");
        private static readonly FieldInfo _quit = MserGlobals.GetField(typeof(DuckNetwork), "_quit");
        private static readonly FieldInfo _menuOpenProfile = MserGlobals.GetField(typeof(DuckNetwork), "_menuOpenProfile");
        private static readonly FieldInfo _menuClosed = MserGlobals.GetField(typeof(DuckNetwork), "_menuClosed");
        private static readonly FieldInfo _settingsBeforeOpen = MserGlobals.GetField(typeof(DuckNetwork), "_settingsBeforeOpen");
        private static readonly MethodInfo _closeAllCorners = MserGlobals.GetMethod("HUD", "CloseAllCorners");
        private static readonly MethodInfo _openMenu = MserGlobals.GetMethod(typeof(DuckNetwork), "OpenMenu");
        private static readonly MethodInfo _showConnectionStatusUI = MserGlobals.GetMethod("ConnectionStatusUI", "Show");
        private static readonly MethodInfo _hideConnectionStatusUI = MserGlobals.GetMethod("ConnectionStatusUI", "Hide");

        /// <summary>
        /// Overridden base function.
        /// </summary>
        public static void Draw()
        {
            //Network-only draws on Layer.Console
            MserEvents.OnConsoleDraw();

            var region = MserDebug.StartProfiling("ChatDraw");

            try
            {
                if (DuckNetwork.core.localDuckIndex == -1)
                {
                    return;
                }

                Vec2 size = new Vec2(Layer.Console.width, Layer.Console.height);
                float yDraw = 0f;
                int numDraw = ChatManager.Entries;
                FontManager.Chat.scale = new Vec2(2f);
                Vec2 messagePos = Vec2.Zero;
                Color boxColor = Color.White;
                Profile p = null;
                Color textColor = Color.Black;

                if (DuckNetwork.core.enteringText && !DuckNetwork.core.stopEnteringText)
                {
                    messagePos = new Vec2(14f, yDraw + (size.y - 32f));
                    p = DuckNetwork.profiles[DuckNetwork.core.localDuckIndex];
                    if (p.persona != null)
                    {
                        boxColor = p.persona.colorUsable;
                    }
                    textColor = ChatManager.GetChatTextColor(boxColor);

                    DuckNetwork.core.cursorFlash++;
                    if (DuckNetwork.core.cursorFlash > 30)
                    {
                        DuckNetwork.core.cursorFlash = 0;
                    }
                    bool flash = DuckNetwork.core.cursorFlash >= 15;
                    string fullString = p.name + ": " + DuckNetwork.core.currentEnterText;
                    if (flash)
                    {
                        fullString += "|"; //HINT: REMINDER ChatManager - Insert blinky line at caret position instead
                    }
                    Graphics.DrawRect(messagePos + new Vec2(-1f, -1f), messagePos + new Vec2(FontManager.Chat.GetWidth(fullString, false) + 4f, 20f) + new Vec2(1f, 1f), Color.Black * 0.6f, 0.7f, false, 1f);

                    Graphics.DrawRect(messagePos, messagePos + new Vec2(FontManager.Chat.GetWidth(fullString, false) + 4f, 20f), boxColor * 0.6f, 0.8f, true, 1f);
                    FontManager.Chat.Draw(fullString, messagePos + new Vec2(2f, 2f), textColor, 1f, false);
                    yDraw -= 22f;
                }

                MserDebug.StepProfiling(region, "EnteringChat");

                //The sinning begins
                ChatManager.AutoCompleteCount = 0;

                //Autocomplete box
                if (DuckNetwork.core.enteringText && !DuckNetwork.core.stopEnteringText &&
                    DuckNetwork.core.currentEnterText.StartsWith("/"))
                {
                    var proflen = FontManager.Chat.GetWidth(p.name + ": ");
                    var backColor = boxColor;
                    var boxHeight = 0f;
                    //Already starting command body?
                    if (!DuckNetwork.core.currentEnterText.Contains(" "))
                    {
                        //TODO: ChatManager - mark (icon + textcolor) or hide commands which you dont have permission to execute (-> config entry whether hide or marking)

                        //Get commands fitting currentEnterText
                        var displist = CacheManager.GetCommandAutoCompleteList();

                        //This is a sin
                        ChatManager.AutoCompleteCount = displist.Count;
                        if (ChatManager.AutoCompleteCount > 0)
                        {
                            //Show box of the first bunch
                            var num = Math.Min(ChatManager.AutoCompleteCount, ChatManager.Entries);
                            var dispmin = displist.GetRange(ChatManager.AutoCompleteTopIndex, num);
                            var dispstr = string.Join("\n", dispmin);

                            Graphics.DrawRect(messagePos + new Vec2(-1f + proflen, -1f - 20f * num),
                                              messagePos + new Vec2(FontManager.Chat.GetWidth(dispstr) + 4f + proflen, 0f) +
                                              new Vec2(1f, 1f), Color.Black * 0.75f, 0.92f, false, 1f);
                            Graphics.DrawRect(messagePos + new Vec2(proflen, -20f * num),
                                              messagePos + new Vec2(FontManager.Chat.GetWidth(dispstr) + 4f + proflen, 0f),
                                              backColor * 0.8f, 0.95f, true, 1f);

                            boxHeight = 20f * num + 5;

                            for (var i = 0; i < dispmin.Count; i++)
                            {
                                var ln = dispmin[i];
                                var color = ChatManager.GetAdjustedColor(textColor);
                                //Smart highlight color
                                if (i == AutoCompleteSelected - ChatManager.AutoCompleteTopIndex)
                                    color = Color.Blue.Difference(backColor) > 60 ? Color.Blue : Color.Yellow;
                                FontManager.Chat.Draw(ln, messagePos + new Vec2(2f + proflen, 2f - 20f * num + 20f * i),
                                                      color, 1f, false);
                            }
                        }

                        MserDebug.StepProfiling(region, "AutoCompleteSimple");
                    }
                    else
                    {
                        //Contains arguments, dont do commands, go for args now - Show detailed info
                        var split = ChatManager.GetSplitChat();
                        var cmd = ChatManager.GetCommandByName(split[0]);
                        if (cmd != null)
                        {
                            //Get argument + index
                            var args = split.GetRange(1, split.Count - 1);
                            var index = split.Count - 2;
                            var list = cmd.GetAutoCompleteEntries(index, args);
                            var showexpected = false;
                            var expected = cmd.GetExpectedArguments(args).ToList();

                            if (list == null || !list.Any())
                            {
                                //No autocomplete data
                                showexpected = true;
                            }
                            else
                            {
                                //Show detailed info box based on what it startswith
                                var filtered = CacheManager.GetArgumentAutoCompleteList().ToList();

                                //This is a sin2
                                ChatManager.AutoCompleteCount = filtered.Count;

                                if (filtered.Any())
                                {
                                    //Draw box for first bunch
                                    var argname = "";
                                    if (expected.Count > index)
                                        argname = expected.ElementAt(index).Name;

                                    var num = Math.Min(ChatManager.AutoCompleteCount, ChatManager.Entries);
                                    var dispmin = filtered.GetRange(ChatManager.AutoCompleteTopIndex, num);
                                    var dispstr = string.Join("\n", dispmin.Select(e => e.Display).Concat(new[] { argname }));
                                    var arglen = proflen + FontManager.Chat.GetWidth(DuckNetwork.core.currentEnterText
                                                                                                .Substring(0, DuckNetwork.core.currentEnterText.LastIndexOf(" ") + 1));

                                    Graphics.DrawRect(messagePos + new Vec2(-1f + arglen, -1f - 20f * (num + 1) - 5f),
                                                      messagePos + new Vec2(FontManager.Chat.GetWidth(dispstr) + 4f + arglen, -5f) +
                                                      new Vec2(1f, 1f), Color.Black * 0.75f, 0.92f, false, 1f);
                                    Graphics.DrawRect(messagePos + new Vec2(arglen, -20f * (num + 1) - 5f),
                                                      messagePos + new Vec2(FontManager.Chat.GetWidth(dispstr) + 4f + arglen, -5f),
                                                      backColor * 0.8f, 0.95f, true, 1f);

                                    boxHeight = 20f * (num + 1) + 5f;
                                    for (var i = 0; i < dispmin.Count; i++)
                                    {
                                        var ln = dispmin[i];
                                        var color = ln.Autocompletes ? ChatManager.GetAdjustedColor(textColor) : ChatManager.GetAdjustedColor(textColor).MergeWith(Color.Gray, 0.5f);
                                        //Smart highlight color
                                        if (i == AutoCompleteSelected - ChatManager.AutoCompleteTopIndex)
                                        {
                                            color = Color.Blue.Difference(backColor) > 60 ? Color.Blue : Color.Yellow;
                                            if (!ln.Autocompletes)
                                                color = color.MergeWith(Color.Gray, 0.5f);
                                        }
                                        FontManager.Chat.Draw(ln.Display, messagePos + new Vec2(2f + arglen, 2f - 20f * num + 20f * i - 5f),
                                                              color, 1f, false);

                                        if (ln.DrawDelegate != null)
                                        {
                                            //Call the draw function here
                                            var startpos = messagePos + new Vec2(-20f + arglen, 0f - 20f * num + 20f * i - 5f);
                                            ln.DrawDelegate(new Rectangle(startpos.x, startpos.y, 20f, 20f));
                                        }
                                    }

                                    //Draw argument's name at top
                                    if (!string.IsNullOrWhiteSpace(argname))
                                    {
                                        //TODO: ChatManager - render argument name centered of the box to differenciate from AC entries, maybe line below too?
                                        FontManager.Chat.Draw(argname, messagePos + new Vec2(2f + arglen, 2f - 20f * (num + 1) - 5f), textColor, 1f);
                                    }
                                }
                                else
                                {
                                    //No fitting autocomplete data
                                    showexpected = true;
                                }
                            }

                            if (showexpected)
                            {
                                //Show generic expected args
                                var exstr = "";
                                if (expected.Any())
                                {
                                    exstr = string.Join(" ", expected);
                                }
                                var dispstr = split[0] + " " + exstr;

                                //Show box for args
                                Graphics.DrawRect(messagePos + new Vec2(-1f + proflen, -26f),
                                                  messagePos + new Vec2(FontManager.Chat.GetWidth(dispstr) + 4f + proflen, -5f) +
                                                  new Vec2(1f, 1f), Color.Black * 0.75f, 0.92f, false, 1f);
                                Graphics.DrawRect(messagePos + new Vec2(proflen, -25f),
                                                  messagePos + new Vec2(FontManager.Chat.GetWidth(dispstr) + 4f + proflen, -5f),
                                                  backColor * 0.8f, 0.95f, true, 1f);
                                FontManager.Chat.Draw(dispstr, messagePos + new Vec2(2f + proflen, -23f),
                                                      ChatManager.GetAdjustedColor(textColor), 1f, false);

                                boxHeight = 25f;
                            }

                            MserDebug.StepProfiling(region, "AutoCompleteAdvanced");

                            //Show the argumenterrors
                            var errorlist = CacheManager.GetArgumentErrorsList().ToList();
                            MserDebug.PrintEntry("AE count", errorlist.Count);
                            var yoff = boxHeight;
                            for (var i = 0; i < errorlist.Count; i++)
                            {
                                try
                                {
                                    //TODO: ChatManager - option to tint letters of chat that are errory
                                    // Similarly, highlight the currently entering argument in the expected args string if no autocompletes are there

                                    //To anyone who tries to fix a büg in here sooner or later (which will probably be me):
                                    //I am so sorry for this bit of code. Unlike whatever is above or below, THIS is insanely horrible.
                                    var err = errorlist[i];
                                    var charStart = err.StartChar;
                                    var charEnd = err.EndChar;
                                    var firstspace = err.Index + 1;
                                    var lastspace = err.Index + 2;
                                    if (err.Index < 0)
                                    {
                                        firstspace = 1;
                                        lastspace = 0;
                                    }
                                    var globalStart = DuckNetwork.core.currentEnterText.NthIndexOf(" ", firstspace) + 1;
                                    var globalEnd = DuckNetwork.core.currentEnterText.NthIndexOf(" ", lastspace) - 1;
                                    if (globalEnd < 0 || globalEnd >= DuckNetwork.core.currentEnterText.Length)
                                    {
                                        globalEnd = DuckNetwork.core.currentEnterText.Length - 1;
                                    }
                                    if (globalEnd < globalStart)
                                    {
                                        //Resolve no-args issues
                                        globalEnd = globalStart;
                                        globalStart--;
                                    }
                                    if (charStart < 0)
                                    {
                                        //No offsetty
                                        charStart = 0;
                                    }
                                    if (charEnd < 0 || charEnd <= charStart || charEnd > globalEnd)
                                    {
                                        //No spaghetti
                                        charEnd = globalEnd - globalStart;
                                    }

                                    if (err.Index == -69)
                                    {
                                        //Special case, whoops
                                        globalStart = 0;
                                        charStart = 0;
                                        charEnd = DuckNetwork.core.currentEnterText.NthIndexOf(" ", 1) - 1;
                                    }

                                    var globalWidth = proflen + FontManager.Chat.GetWidth(DuckNetwork.core.currentEnterText.Substring(0, globalStart)) + 7;
                                    var globalWidthCharOffset = globalWidth + FontManager.Chat.GetWidth(DuckNetwork.core.currentEnterText.Substring(globalStart, charStart));
                                    var argWidth = FontManager.Chat.GetWidth(DuckNetwork.core.currentEnterText.Substring(globalStart + charStart, charEnd - charStart));
                                    var linepos = messagePos + new Vec2(globalWidthCharOffset, 20);
                                    //TODO: ChatManger - make it fit until border of screen
                                    //  This code would work similarly to the line-wrapping code to be implemented for chat
                                    var textsplit = err.Message.Split('\n').Truncate(140, true);
                                    var text = string.Join("\n", textsplit);
                                    var textwidth = FontManager.Chat.GetWidth(text);
                                    var textheight = FontManager.Chat.GetHeight(text);
                                    yoff += 5f + textheight;
                                    var boxpos = messagePos + new Vec2(err.Index < 0 ? globalWidthCharOffset : globalWidth, -yoff);
                                    Color bgcolor;
                                    Color fgcolor;

                                    //Icon
                                    ChatManager.Symbols.position = boxpos + new Vec2(2);
                                    switch (err.Severity)
                                    {
                                        case Severity.Usage:
                                        case Severity.Error:
                                            ChatManager.Symbols.frame = 0;
                                            bgcolor = new Color(ChatManager.ErrorColor / 255);
                                            break;
                                        case Severity.Warning:
                                            ChatManager.Symbols.frame = 1;
                                            bgcolor = new Color(ChatManager.WarningColor / 255);
                                            break;
                                        default:
                                            ChatManager.Symbols.frame = 2;
                                            bgcolor = new Color(ChatManager.InfoColor / 255);
                                            break;
                                    }

                                    if (ChatManager.ArgumentErrorsPopups)
                                    {
                                        ChatManager.Symbols.Draw();

                                        //Background box
                                        fgcolor = ChatManager.GetChatTextColor(bgcolor);
                                        Graphics.DrawRect(boxpos - new Vec2(1), boxpos + new Vec2(textwidth + 25, 3 + textheight), Color.Black * 0.75f, 0.9f);
                                        Graphics.DrawRect(boxpos, boxpos + new Vec2(textwidth + 24, 2 + textheight), bgcolor * 0.75f, 0.95f);
                                        //Text
                                        FontManager.Chat.Draw(text, boxpos + new Vec2(20, 2), fgcolor, 0.97f);
                                        if (ChatManager.ArgumentErrorsConnection)
                                        {
                                            //Connecting line
                                            Extensions.DrawDoubleDottedLine(new Vec2(8 + linepos.x, 3 + textheight + boxpos.y), linepos + new Vec2(8, -20), bgcolor * 0.5f, fgcolor * 0.5f, 2, 2, 0.99f);
                                        }
                                    }

                                    //Underlining argument
                                    if (ChatManager.ArgumentErrorsUnderline)
                                    {
                                        ChatManager._underline.position = linepos;
                                        ChatManager._underline.color = bgcolor;
                                        var linelen = 0;
                                        do
                                        {
                                            ChatManager._underline.Draw();
                                            ChatManager._underline.x += 6;
                                            linelen += 6;
                                        } while (linelen < argWidth - 2);
                                    }
                                }
                                catch (Exception e)
                                {
                                    MserDebug.PrintEntry("AE FAILED", e.Message + e.StackTrace);
                                }
                            }

                            MserDebug.StepProfiling(region, "ArgumentErrors");

                            //Move chat history above the argumenterrors (adding boxHeight to avoid it being added twice, shit)
                            yDraw -= yoff - boxHeight;
                        }
                    }

                    //Move chat history above the autocomplete box
                    yDraw -= boxHeight;
                }

                // Regular chat history display

                //TODO: ChatManager - highlight profile name with permission state somehow (same goes for while you're entering a message yourself!!!)

                float hatDepth = 1f;
                foreach (ChatMessage message in DuckNetwork.core.chatMessages)
                {
                    string fullString2 = message.who.name + ": " + message.text;
                    float leftOffset = 20f;
                    FontManager.Chat.scale = new Vec2(2f * message.scale);
                    float stringWidth = FontManager.Chat.GetWidth(fullString2, false) + leftOffset;
                    Vec2 messagePos2 = new Vec2(-((15f + stringWidth) * (1f - message.slide)) + 14f, yDraw + (size.y - 32f));
                    Graphics.DrawRect(messagePos2 + new Vec2(-1f, -1f), messagePos2 + new Vec2(stringWidth + 4f, 20f) + new Vec2(1f, 1f), Color.Black * 0.6f * message.alpha, 0.7f, false, 1f);
                    float extraScale = 0.3f + (float)message.text.Length * 0.007f;
                    if (extraScale > 0.5f)
                    {
                        extraScale = 0.5f;
                    }
                    if (message.slide > 0.8f)
                    {
                        message.scale = Lerp.FloatSmooth(message.scale, 1f, 0.1f, 1.1f);
                    }
                    else if (message.slide > 0.5f)
                    {
                        message.scale = Lerp.FloatSmooth(message.scale, 1f + extraScale, 0.1f, 1.1f);
                    }
                    message.slide = Lerp.FloatSmooth(message.slide, 1f, 0.1f, 1.1f);
                    Color boxColor2 = Color.White;
                    if (message.who.persona != null)
                    {
                        boxColor2 = message.who.persona.colorUsable;
                    }
                    //Smart text color
                    Color textColor2 = ChatManager.GetChatTextColor(boxColor2);
                    if (message.who.persona != null)
                    {
                        SpriteMap hat = message.who.persona.defaultHead;
                        if (message.who.team != null && message.who.team.hasHat && (message.who.connection != DuckNetwork.localConnection || !message.who.team.locked))
                        {
                            hat = message.who.team.hat;
                        }
                        hat.CenterOrigin();
                        hat.depth = hatDepth;
                        hat.alpha = message.alpha;
                        hat.scale = new Vec2(2f, 2f);
                        Graphics.Draw(hat, messagePos2.x, messagePos2.y);
                        hat.scale = new Vec2(1f, 1f);
                        hat.alpha = 1f;
                        boxColor2 *= 0.85f;
                        boxColor2.a = 255;
                    }
                    Graphics.DrawRect(messagePos2, messagePos2 + new Vec2(stringWidth + 4f, 20f), boxColor2 * 0.75f * message.alpha, 0.6f, true, 1f);
                    FontManager.Chat.Draw(fullString2, messagePos2 + new Vec2(2f + leftOffset, 2f), textColor2 * message.alpha, 0.9f, false);
                    yDraw -= 26f;
                    hatDepth -= 0.01f;
                    if (numDraw == 0)
                    {
                        break;
                    }
                    numDraw--;
                }

                MserDebug.StepProfiling(region, "ChatHistory");

                //Reset afterwards
                FontManager.Chat.scale = new Vec2(2f);
            }
            finally
            {
                MserDebug.EndProfiling(region);
            }
        }

        /// <summary>
        /// Called when the scoreboard should be drawn.
        /// </summary>
        public static void DrawScoreboard()
        {
            var core = ChatManager._coreField.GetValue(null);
            var bars = (IList)ChatManager._barsField.GetValue(core);
            var debar = (Sprite)ChatManager._debarField.GetValue(null);

            int numElements = bars.Count;
            float heightPerElement = 14f;
            Vec2 drawPos = new Vec2(30f, Layer.HUD.height / 2f - (float)numElements * heightPerElement / 2f);
            int i = 0;
            foreach (var bar in bars)
            {
                var thisprofile = (Profile)ChatManager._profileField.GetValue(bar);
                var thisposition = (float)ChatManager._positionField.GetValue(bar);

                if (thisprofile.connection != null && thisprofile.connection.status != ConnectionStatus.Disconnected)
                {
                    if (thisposition > 0.01f)
                    {
                        Vec2 meDraw = new Vec2(drawPos.x, drawPos.y + (float)(i * 14));
                        meDraw.x -= Layer.HUD.width * (1f - thisposition);
                        debar.depth = 0.8f;
                        Graphics.Draw(debar, meDraw.x, meDraw.y);
                        FontManager.BiosSmall.depth = 0.9f;
                        bool local = false;
                        int transferProgress;
                        int transferSize;
                        if (thisprofile.connection == DuckNetwork.localConnection)
                        {
                            transferProgress = DuckNetwork.core.levelTransferProgress;
                            transferSize = DuckNetwork.core.levelTransferSize;
                            local = true;
                        }
                        else
                        {
                            transferProgress = thisprofile.connection.dataTransferProgress;
                            transferSize = thisprofile.connection.dataTransferSize;
                        }
                        if (transferProgress != transferSize)
                        {
                            FontManager.BiosSmall.scale = new Vec2(0.5f, 0.5f);
                            if (local)
                            {
                                FontManager.BiosSmall.Draw(string.Concat(new string[]
                                {
                                    "@ONLINENEUTRAL@|DGYELLOW|DOWNLOADING   ",
                                    transferProgress.ToString(),
                                    "\\",
                                    transferSize.ToString(),
                                    "B"
                                }), new Vec2(meDraw.x + 3f, meDraw.y + 3f), Color.White, 0.9f, null, false);
                            }
                            else
                            {
                                FontManager.BiosSmall.Draw(string.Concat(new string[]
                                {
                                    "@ONLINENEUTRAL@|DGYELLOW|SENDING CUSTOM ",
                                    transferProgress.ToString(),
                                    "\\",
                                    transferSize.ToString(),
                                    "B"
                                }), new Vec2(meDraw.x + 3f, meDraw.y + 3f), Color.White, 0.9f, null, false);
                            }
                            float progress = (float)transferProgress / (float)transferSize;
                            int barHeight = 3;
                            int barLeftOffset = 11;
                            int barYOffset = 7;
                            int barWidth = 90;
                            Graphics.DrawRect(meDraw + new Vec2((float)barLeftOffset, (float)barYOffset), meDraw + new Vec2((float)(barLeftOffset + barWidth), (float)(barYOffset + barHeight)), Color.White, 0.9f, false, 0.5f);
                            Graphics.DrawRect(meDraw + new Vec2((float)barLeftOffset, (float)barYOffset), meDraw + new Vec2((float)barLeftOffset + (float)barWidth * progress, (float)(barYOffset + barHeight)), new Color(163, 206, 39), 0.87f, true, 1f);
                            Graphics.DrawRect(meDraw + new Vec2((float)barLeftOffset, (float)barYOffset), meDraw + new Vec2((float)(barLeftOffset + barWidth), (float)(barYOffset + barHeight)), new Color(192, 32, 45), 0.84f, true, 1f);
                        }
                        else if (thisprofile.connection.loadingStatus != DuckNetwork.levelIndex)
                        {
                            FontManager.BiosSmall.Draw("@ONLINENEUTRAL@|DGYELLOW|SENDING...", new Vec2(meDraw.x + 3f, meDraw.y + 3f), Color.White, 0.9f, null, false);
                        }
                        else
                        {
                            FontManager.BiosSmall.Draw("@ONLINEGOOD@|DGGREEN|READY!", new Vec2(meDraw.x + 3f, meDraw.y + 3f), Color.White, 0.9f, null, false);
                        }
                        FontManager.BiosSmall.scale = new Vec2(1f, 1f);
                        string name = thisprofile.name;
                        if (name.Length > 11)
                        {
                            name = name.Substring(0, 11) + ".";
                        }
                        var dukcolor = thisprofile.persona.colorUsable;
                        if (dukcolor.Difference(new Color(15, 21, 27)) < 20)
                            dukcolor = ChatManager.GetAdjustedColor(dukcolor);
                        var text = string.Format("{0}/{1}", name, Extensions.GetPlural(thisprofile.team.score, "pt", null, false));
                        FontManager.BiosSmall.Draw(text, new Vec2(meDraw.x + (float)debar.width - FontManager.BiosSmall.GetWidth(name, false, null) - 90f, meDraw.y + 3f), dukcolor, 0.9f, null, false);
                        int pingval = (int)System.Math.Round((double)(thisprofile.connection.manager.ping * 1000f));
                        string ping = pingval.ToString();
                        ping += "|WHITE|MS";
                        if (pingval < 150)
                        {
                            ping = "|DGGREEN|" + ping + "@SIGNALGOOD@";
                        }
                        else if (pingval < 250)
                        {
                            ping = "|DGYELLOW|" + ping + "@SIGNALNORMAL@";
                        }
                        else if (thisprofile.connection.status == ConnectionStatus.Connected)
                        {
                            ping = "|DGRED|" + ping + "@SIGNALBAD@";
                        }
                        else
                        {
                            ping = "|DGRED|" + ping + "@SIGNALDEAD@";
                        }
                        FontManager.BiosSmall.Draw(ping, new Vec2(meDraw.x + (float)debar.width - 3f - FontManager.BiosSmall.GetWidth(ping, false, null), meDraw.y + 3f), Color.White, 0.9f, null, false);
                    }
                    i++;
                }
            }
        }

        /// <summary>
        /// Overriden base function.
        /// </summary>
        public static void Update()
        {
            if (MonoMain.pauseMenu == null && (bool)ChatManager._pauseOpen.GetValue(null))
            {
                ChatManager._closeAllCorners.Invoke(null, null);
                ChatManager._pauseOpen.SetValue(null, false);
                if (Network.isServer)
                {
                    TeamSelect2.UpdateModifierStatus();
                }
            }
            /*if (MonoMain.pauseMenu == null && (bool)ChatManager.willOpenSettingsInfo.GetValue(null))
            {
                ChatManager.DoMatchSettingsInfoOpen.Invoke(null, null);
                ChatManager.willOpenSettingsInfo.SetValue(null, false);
            }*/
            if (DuckNetwork.core.status == DuckNetStatus.Disconnected || DuckNetwork.core.status == DuckNetStatus.Disconnecting)
            {
                ((MenuBoolean)ChatManager._quit.GetValue(null)).value = false;
                return;
            }
            if (((MenuBoolean)ChatManager._quit.GetValue(null)).value)
            {
                if ((Profile)ChatManager._menuOpenProfile.GetValue(null) != null && ((Profile)ChatManager._menuOpenProfile.GetValue(null)).slotType == SlotType.Local)
                {
                    DuckNetwork.Kick((Profile)ChatManager._menuOpenProfile.GetValue(null));
                }
                else
                {
                    if (Steam.lobby != null)
                    {
                        UIMatchmakingBox.nonPreferredServers.Add(Steam.lobby.id);
                    }
                    Level.current = new DisconnectFromGame();
                }
                ((MenuBoolean)ChatManager._quit.GetValue(null)).value = false;
            }
            if (((MenuBoolean)ChatManager._menuClosed.GetValue(null)).value && Network.isServer)
            {
                string newSettings = TeamSelect2.GetMatchSettingString();
                if (newSettings != (string)ChatManager._settingsBeforeOpen.GetValue(null))
                {
                    TeamSelect2.SendMatchSettings(null, false);
                    Send.Message(new NMMatchSettingsChanged());
                }
                ((MenuBoolean)ChatManager._quit.GetValue(null)).value = false;
            }
            if (Keyboard.Pressed(ScoreboardKey))
            {
                if (!ChatManager._showingScoreboard)
                {
                    ChatManager._showConnectionStatusUI.Invoke(null, null);
                    ChatManager._showingScoreboard = true;
                }
            }
            else if (!Keyboard.Down(ScoreboardKey))
            {
                if (ChatManager._showingScoreboard)
                {
                    ChatManager._hideConnectionStatusUI.Invoke(null, null);
                    ChatManager._showingScoreboard = false;
                }
            }
            if (Network.isClient)
            {
                if (DuckNetwork.status == DuckNetStatus.Connected)
                {
                    DuckNetwork.core.attemptTimeout = 10f;
                }
                if (DuckNetwork.core.attemptTimeout > 0f)
                {
                    DuckNetwork.core.attemptTimeout -= Maths.IncFrameTimer();
                }
                else if (DuckNetwork.status != DuckNetStatus.Connected)
                {
                    DuckNetwork.RaiseError(new DuckNetErrorInfo
                    {
                        error = DuckNetError.ConnectionTimeout, message = "|RED|Connection timeout."
                    });
                }
            }
            if (MonoMain.pauseMenu != null)
            {
                DuckNetwork.core.enteringText = false;
                DuckNetwork.core.stopEnteringText = false;
            }
            bool skipPause = false;
            if (Network.isActive && MonoMain.pauseMenu == null)
            {
                System.Collections.Generic.List<ChatMessage> remove = new System.Collections.Generic.List<ChatMessage>();
                foreach (ChatMessage i in DuckNetwork.core.chatMessages)
                {
                    i.timeout -= 0.016f;
                    if (i.timeout < 0f)
                    {
                        i.alpha -= 0.01f;
                    }
                    if (i.alpha < 0f)
                    {
                        remove.Add(i);
                    }
                }
                foreach (ChatMessage j in remove)
                {
                    DuckNetwork.core.chatMessages.Remove(j);
                }
                if (DuckNetwork.core.stopEnteringText)
                {
                    DuckNetwork.core.enteringText = false;
                    DuckNetwork.core.stopEnteringText = false;
                }
                //if (!DevConsole.core.open)
                {
                    //Custom Input
                    ChatManager.UpdateInput();

                    bool entering = DuckNetwork.core.enteringText;
                    DuckNetwork.core.enteringText = false;
                    bool pressed = Input.Pressed("CHAT", "Any");
                    DuckNetwork.core.enteringText = entering;
                    if (pressed)
                    {
                        if (!DuckNetwork.core.enteringText)
                        {
                            DuckNetwork.core.enteringText = true;
                            DuckNetwork.core.currentEnterText = "";
                            Keyboard.keyString = "";
                        }
                        else if (ChatManager.ChatMode != ChatManager.ChatModeSetting.EnterOnly || Keyboard.Pressed(Keys.Enter, false))
                        {
                            ChatManager.SendMessage();
                        }
                    }
                    else if (DuckNetwork.core.enteringText && Keyboard.Pressed(Keys.Escape, false))
                    {
                        ChatManager._previousIndex = -1;
                        ChatManager.AutoCompleteSelected = -1;
                        ChatManager.AutoCompleteCount = 0;
                        DuckNetwork.core.stopEnteringText = true;
                        skipPause = true;
                    }
                    else if (ChatManager.ChatMode != ChatManager.ChatModeSetting.Default && DuckNetwork.core.enteringText && Keyboard.Pressed(Keys.Enter, false))
                    {
                        ChatManager.SendMessage();
                    }
                }
                if (DuckNetwork.core.enteringText)
                {
                    //Old truncating code
                    /*if (Keyboard.keyString.Length > 60)
                    {
                        Keyboard.keyString = Keyboard.keyString.Substring(0, 60);
                    }*/

                    DuckNetwork.core.currentEnterText = Keyboard.keyString;

                    if (DuckNetwork.core.currentEnterText != ChatManager._lastChatText)
                    {
                        //Update lists
                        ChatManager._lastChatText = DuckNetwork.core.currentEnterText;

                        CacheManager.UpdateCommandAutoCompleteList();
                        CacheManager.UpdateArgumentAutoCompleteList();
                        CacheManager.UpdateArgumentErrorsList();
                    }
                }

                //After input
                if (ChatManager.AutoCompleteCount > 0)
                {
                    ChatManager.AutoCompleteSelected = Maths.Clamp(ChatManager.AutoCompleteSelected, 0, ChatManager.AutoCompleteCount - 1);
                }
                else
                {
                    ChatManager.AutoCompleteSelected = -1;
                }
            }
            bool openedMenu = false;
            foreach (Profile p in DuckNetwork.profiles)
            {
                if (p.connection != null)
                {
                    if (MonoMain.pauseMenu == null && (DuckNetwork.duckNetUIGroup == null || !DuckNetwork.duckNetUIGroup.open) && p.connection == DuckNetwork.localConnection && p.inputProfile.Pressed("START", false) && !skipPause && !openedMenu && !(Level.current is RockScoreboard) && (Level.current is TeamSelect2 || Level.current is GameLevel || Level.current is RockScoreboard))
                    {
                        ChatManager._openMenu.Invoke(null, new object[] {p});
                        openedMenu = true;
                    }
                    if (p.connection.status == ConnectionStatus.Connected && p.networkStatus != DuckNetStatus.Disconnecting && p.networkStatus != DuckNetStatus.Disconnected && p.networkStatus != DuckNetStatus.Failure)
                    {
                        p.currentStatusTimeout -= Maths.IncFrameTimer();
                        if (p.networkStatus == DuckNetStatus.NeedsNotificationWhenReadyForData)
                        {
                            if (p.currentStatusTimeout <= 0f)
                            {
                                p.currentStatusTimeout = 2f;
                                p.currentStatusTries++;
                            }
                            if (p.currentStatusTries > 10)
                            {
                                if (p.isHost)
                                {
                                    DuckNetwork.RaiseError(new DuckNetErrorInfo
                                    {
                                        error = DuckNetError.ConnectionTimeout, message = "|RED|Took too long to receive level data."
                                    });
                                }
                                p.connection.Disconnect();
                            }
                            if (Network.isServer || DuckNetwork.localConnection.loadingStatus == DuckNetwork.levelIndex)
                            {
                                if (p.connection != DuckNetwork.localConnection)
                                {
                                    Send.Message(new NMLevelDataReady(DuckNetwork.levelIndex), p.connection);
                                }
                                p.networkStatus = DuckNetStatus.WaitingForLoadingToBeFinished;
                            }
                        }
                        if (p.networkStatus == DuckNetStatus.WaitingForLoadingToBeFinished)
                        {
                            if (p.connection.loadingStatus == DuckNetwork.levelIndex)
                            {
                                p.networkStatus = DuckNetStatus.Connected;
                            }
                            else
                            {
                                if (p.currentStatusTimeout <= 0f)
                                {
                                    if (p.connection != DuckNetwork.localConnection)
                                    {
                                        Send.Message(new NMAwaitingLevelReady(DuckNetwork.levelIndex), p.connection);
                                        //DevConsole.Log(DCSection.DuckNet, "|DGYELLOW|Requesting level data from " + p.name, -1);
                                    }
                                    else
                                    {
                                        //DevConsole.Log(DCSection.DuckNet, "|DGYELLOW|Still waiting for level data...", -1);
                                    }
                                    p.currentStatusTimeout = 7f;
                                    p.currentStatusTries++;
                                }
                                if (p.currentStatusTries > 3)
                                {
                                    if (p.isHost)
                                    {
                                        DuckNetwork.RaiseError(new DuckNetErrorInfo
                                        {
                                            error = DuckNetError.ConnectionTimeout, message = "|RED|Took too long to connect with " + p.name + "."
                                        });
                                    }
                                    p.connection.Disconnect();
                                }
                            }
                        }
                        if (p.connection.wantsGhostData == (int)DuckNetwork.levelIndex && Level.current.initialized)
                        {
                            GhostManager.context.RefreshGhosts(null);
                            Level.current.SendLevelData(p.connection);
                            GhostManager.context.SendAllGhostData(false, NetMessagePriority.ReliableOrdered, p.connection);
                            Send.Message(new NMEndOfGhostData(DuckNetwork.levelIndex), p.connection);
                            Send.Message(new NMLevelDataReady(DuckNetwork.levelIndex), p.connection);
                            /*DevConsole.Log(DCSection.DuckNet, string.Concat(new object[]
                            {
                                "|LIME|",
                                p.connection.identifier,
                                " LOADED LEVEL (",
                                DuckNetwork.levelIndex,
                                ")"
                            }), -1);*/
                            p.connection.wantsGhostData = -1;
                        }
                    }
                }
            }
            if (DuckNetwork.error != null && DuckNetwork.core.status != DuckNetStatus.Disconnecting)
            {
                DuckNetwork.core.status = DuckNetStatus.Disconnecting;
                Network.Disconnect();
            }
        }
    }
}
