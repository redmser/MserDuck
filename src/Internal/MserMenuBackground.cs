﻿namespace DuckGame.MserDuck
{
    public class MserMenuBackground : SpaceBackgroundMenu
    {
        public MserMenuBackground(float xpos, float ypos, bool moving, float speedMult) : base(xpos, ypos, moving, speedMult)
        {
            base.graphic = new SpriteMap("backgroundIcons", 16, 16, false)
            {
                frame = 1
            };
            this.center = new Vec2(8f, 8f);
            this._collisionSize = new Vec2(16f, 16f);
            this._collisionOffset = new Vec2(-8f, -8f);
            base.depth = 0.9f;
            base.layer = Layer.Foreground;
            this._visibleInGame = false;
            this._speedMult = speedMult;
            this._moving = moving;
        }

        public override void Initialize()
        {
            Level.current.backgroundColor = new Color(0, 0, 0);
            this._parallax = new ParallaxBackground(base.GetPath("mserSpace"), 0f, 0f, 3);
            float speed = 5.5f * this._speedMult;
            this._parallax.AddZone(20, 0.93f, speed, this._moving, true);
            this._parallax.AddZone(21, 0.93f, speed, this._moving, true);
            this._parallax.AddZone(22, 0.87f, speed, this._moving, true);
            this._parallax.AddZone(23, 0.84f, speed, this._moving, true);
            this._parallax.AddZone(24, 0.81f, speed, this._moving, true);
            this._parallax.AddZone(25, 0.81f, speed, this._moving, true);
            this._parallax.AddZone(26, 0.81f, speed, this._moving, true);
            this._parallax.AddZone(27, 0.78f, speed, this._moving, true);
            this._parallax.AddZone(28, 0.78f, speed, this._moving, true);
            this._parallax.AddZone(29, 0.78f, speed, this._moving, true);
            this._extraYOffset = 16f;
            this._parallax.FUCKINGYOFFSET = 8f;
            Level.Add(this._parallax);
        }

        private bool _moving;

        private float _speedMult;
    }
}