﻿using System;
using System.Runtime.InteropServices;
using Trinet.Core.IO.Ntfs;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Commonly used PInvoke calls (hah, you need to marshal them!!!) and general WindowsAPI wizardry.
    /// </summary>
    public static class PInvoke
    {
        public const int GWL_STYLE = -16;
        public const uint WS_MINIMIZE = 0x20000000;
        public const uint WS_MAXIMIZE = 0x1000000;
        public const uint WS_CAPTION = 0xC00000;
        public const uint WS_SYSMENU = 0x80000;
        public const uint WS_THICKFRAME = 0x40000;

        /// <summary>
        /// Bitmask for a borderless window.
        /// </summary>
        public const uint WS_BORDERLESS = ~(PInvoke.WS_CAPTION | PInvoke.WS_THICKFRAME /* | WS_MINIMIZE | WS_MAXIMIZE */ | PInvoke.WS_SYSMENU);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, uint dwNewLong);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        /// <summary>
        /// Equivilent to pressing the "Unblock" checkbox/button on the properties dialog of a DLL file.
        /// <para />A way to check whether this is needed is by getting the "Zone.Identifier" stream and verifying whether it exists.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static void UnblockDll(string path)
        {
            try
            {
                FileSystem.GetAlternateDataStream(path, "Zone.Identifier").Delete();
            }
            catch
            {
                //Suppressing exception since this may not work well on LINUX
            }
        }
    }
}
