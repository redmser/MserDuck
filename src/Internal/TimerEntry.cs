using System.Collections.Generic;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// A running timer created by /timer.
    /// </summary>
    public class TimerEntry
    {
        public string Name;

        public BoundCommand Command;

        public int Amounts;

        public int Interval;

        private int _tick;

        /// <summary>
        /// Returns whether it should be removed.
        /// </summary>
        /// <returns></returns>
        public bool DoUpdate()
        {
            this._tick++;

            //Should tick?
            if (this._tick >= this.Interval)
            {
                //TICKED!
                this._tick = 0;

                if (this.Amounts > 0)
                {
                    //Execute
                    this.Amounts--;
                }
                //DebugUI.PrintEntry("Timer " + this.Name, "Executed!");
                ChatManager.EnterChatMessage(this.Command, true, true);
                if (this.Amounts == 0)
                {
                    //Remove at 0 amounts
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// List of running timers.
        /// </summary>
        public static readonly List<TimerEntry> Timers = new List<TimerEntry>();

        /// <summary>
        /// Updates all timers running.
        /// </summary>
        public static void Update()
        {
            if (!Network.isActive)
                return;

            var removes = new List<TimerEntry>();
            foreach (var timer in TimerEntry.Timers)
            {
                if (timer.DoUpdate())
                {
                    removes.Add(timer);
                }
            }
            foreach (var remove in removes)
            {
                TimerEntry.Timers.Remove(remove);
            }
        }
    }
}