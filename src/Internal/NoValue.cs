﻿namespace DuckGame.MserDuck
{
    /// <summary>
    /// A struct representing no value. Used when null has a special meaning, and has to be differenciated from.
    /// </summary>
    public struct NoValue
    {

    }
}
