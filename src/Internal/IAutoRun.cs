﻿namespace DuckGame.MserDuck
{
    /// <summary>
    /// A command with this interface will be autorun when loaded using /loadcfg.
    /// </summary>
    public interface IAutoRun
    {

    }
}
