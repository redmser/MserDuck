﻿using System;
using System.Collections.Generic;
using System.IO;
using IniParser;
using IniParser.Model;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// File that represents the meta data info of a skin sprite. Reads contents when instanciated.
    /// </summary>
    public class SkinInfo
    {
        /// <summary>
        /// Creates a new SkinInfo object, reading the contents from the file (specified by the path) into the different fields once created.
        /// <para />The file has to exist when loading, otherwise an exception is thrown!
        /// </summary>
        /// <param name="path"></param>
        public SkinInfo(string path)
        {
            //TODO: SkinInfo - allow changing the offsets for held items, equipment, arms position etc

            try
            {
                if (!File.Exists(path))
                    throw new FileNotFoundException("Couldn't find SkinInfo file " + path);

                if (SkinInfo._parser == null)
                    SkinInfo.SetupParser();

                this._data = SkinInfo._parser.ReadFile(path);

                if (this._data == null)
                    throw new ArgumentNullException("path", "The specified ini data could not be read.");

                //Size
                var sizestr = this._data.Global["size"];
                if (sizestr != null)
                {
                    //May want to check formatting idk
                    var sizestuf = sizestr.Split('x');
                    if (sizestuf.Length == 2)
                        this.Size = new Vec2(float.Parse(sizestuf[0]), float.Parse(sizestuf[1]));
                }

                //Center
                var center = this._data.Global["center"];
                if (center != null)
                {
                    //May want to check formatting idk
                    var centerstuf = center.Split('x');
                    if (centerstuf.Length == 2)
                        this.Center = new Vec2(float.Parse(centerstuf[0]), float.Parse(centerstuf[1]));
                }

                //Recolor
                var recol = this._data.Global["recolor"];
                if (recol != null)
                    this.Recolor = bool.Parse(recol);

                //AnimProps
                var animdef = this._data.Global["animdefault"];
                if (animdef != null)
                    this.AnimDefault = animdef;

                var animspeed = this._data.Global["animspeed"];
                if (animspeed != null)
                    this.AnimSpeed = FloatOrRange(animspeed);

                //AnimList
                var failcnt = 0;
                for (int i = 1; true; i++)
                {
                    var key = this._data.Global["anim" + i];
                    if (key == null)
                    {
                        if (failcnt >= 3)
                            break;

                        failcnt++;
                        continue;
                    }

                    this.AnimList.Add(new SkinAnimation(key));
                }
            }
            catch (Exception e)
            {
                Program.LogLine("Failed creating SkinInfo: " + e.Message + e.StackTrace);
            }
        }

        /// <summary>
        /// Parses the specified input string as either a float number, or if a - is included, as a random range.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static float FloatOrRange(string input)
        {
            if (input.Contains("-"))
                return MserGlobals.ParseRandom(input);
            return float.Parse(input);
        }

        private IniData _data;
        private static FileIniDataParser _parser;

        /// <summary>
        /// SpriteMap's size.
        /// </summary>
        public Vec2 Size;

        /// <summary>
        /// Whether to recolor the sprite.
        /// </summary>
        public Nullable<bool> Recolor;

        /// <summary>
        /// Default animation speed multiplier.
        /// </summary>
        public Nullable<float> AnimSpeed;

        /// <summary>
        /// Default animation. Currently unused.
        /// </summary>
        public string AnimDefault;

        /// <summary>
        /// List of animations.
        /// </summary>
        public readonly List<SkinAnimation> AnimList = new List<SkinAnimation>();

        /// <summary>
        /// Center of the sprite.
        /// </summary>
        public Nullable<Vec2> Center;

        private static void SetupParser()
        {
            _parser = new FileIniDataParser();
            _parser.Parser.Configuration.CommentString = ConfigManager.commentString;
            _parser.Parser.Configuration.CaseInsensitive = true;
            _parser.Parser.Configuration.SkipInvalidLines = true;
        }
    }
}
