﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using DuckGame.MserDuck.CommandHandlers;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Similar to MserGlobals, except only relevant for specific commands.
    /// <para />This class is used to make MserGlobals less cluttered of generic, one-time use functions.
    /// </summary>
    internal static class CommandHelpers
    {
        /// <summary>
        /// Tries to parse a list of strings to a list of objects with specified formatting (see Call/Set usage help for more info).
        /// </summary>
        /// <param name="strs">List of strings to try parsing.</param>
        /// <param name="sender">Sender of the command, in case it is needed for parsing "near..." calls.</param>
        /// <returns></returns>
        internal static List<object> ParseStringToObjects(IEnumerable<string> strs, Duck sender = null)
        {
            //TODO: CommandHelpers parsing: for multiple objects to be influences (e.g. /set all...), calculate a new value per object
            // This is needed for example for random values. There should ALSO be a correct "this" which returns its own instance (IS THIS NEEDED IS THE BETTER QUESTION)
            if (sender == null)
                sender = MserGlobals.GetLocalDuck();

            var region = MserDebug.StartProfiling("ParseStringToObjects");

            var objList = new List<object>();
            var strlist = strs.ToList();
            foreach (var str in strlist)
            {
                bool boolRes;
                int intRes;
                float floatRes;
                Vec2 vec2Res;
                Vec3 vec3Res;
                Vec4 vec4Res;
                Color colorRes;
                float randRes;

                //Null :)
                if (str.EqualsTo(true, "null"))
                {
                    objList.Add(null);
                }
                //Common properties
                else if (str.EqualsToAny(true, "currentlevel", "thislevel", "level"))
                {
                    objList.Add(Level.current);
                }
                else if (str.EqualsToAny(true, "mypersona", "thispersona", "persona"))
                {
                    objList.Add(sender.persona);
                }
                else if (str.EqualsToAny(true, "myprofile", "thisprofile", "profile"))
                {
                    objList.Add(sender.profile);
                }
                else if (str.EqualsToAny(true, "myduck", "me", "this", "thisduck"))
                {
                    objList.Add(sender);
                }
                else if (str.EqualsToAny(true, "thiscolor", "mycolor"))
                {
                    objList.Add(sender.persona.colorUsable);
                }
                //Common readonly properties
                else if (str.EqualsTo(true, "Vec3.Zero"))
                {
                    objList.Add(Vec3.Zero);
                }
                else if (str.EqualsTo(true, "Vec3.One"))
                {
                    objList.Add(Vec3.One);
                }
                else if (str.EqualsTo(true, "Vec2.Zero"))
                {
                    objList.Add(Vec2.Zero);
                }
                else if (str.EqualsTo(true, "Vec2.One"))
                {
                    objList.Add(Vec2.One);
                }
                //Common parsing
                else if (bool.TryParse(str, out boolRes))
                {
                    objList.Add(boolRes);
                }
                else if (str.Contains(".") && float.TryParse(str, out floatRes))
                {
                    objList.Add(floatRes);
                }
                else if (int.TryParse(str, out intRes))
                {
                    objList.Add(intRes);
                }
                //Custom parsing
                //TODO: CommandHelpers value parsing: if number (float or int) ends with a + or -, then add/subtract specified number to old value (pass old as parameter)
                else if (MserGlobals.TryParseRandom(str, out randRes))
                {
                    objList.Add(randRes);
                }
                else if (MserGlobals.TryParseVec4(str, out vec4Res))
                {
                    objList.Add(vec4Res);
                }
                else if (MserGlobals.TryParseVec3(str, out vec3Res))
                {
                    objList.Add(vec3Res);
                }
                else if (MserGlobals.TryParseVec2(str, out vec2Res))
                {
                    objList.Add(vec2Res);
                }
                else if (MserGlobals.TryParseColor(str, out colorRes))
                {
                    objList.Add(colorRes);
                }
                else
                {
                    //Try HandleObjectListByName as a last resort
                    var tuple = CommandHelpers.HandleObjectListByName(str, sender, false);
                    if (tuple != null && tuple.Item1.Count > 0)
                    {
                        objList.AddRange(tuple.Item1);
                    }
                    else
                    {
                        //Add string directly
                        objList.Add(str);
                    }
                }
            }
            MserDebug.StepProfiling(region, "Loops x" + strlist.Count);
            return objList;
        }

        /// <summary>
        /// Returns a list of keywords usable as special object names in /call and /set
        /// </summary>
        internal static readonly IEnumerable<string> ObjectListNames = new[] {"held", "shoes", "armor", "helmet", "hat", "everyone", "level"};

        /// <summary>
        /// Returns a tuple containing a list of objects fitting the specified objName (for formatting info, see usage help of Call/Set commands).
        /// </summary>
        /// <param name="objName">String that should be parsed specifying what object is wanted.</param>
        /// <param name="sender">Duck that sent this message. Used for default ("this") and for position checks.</param>
        /// <param name="returnsSender">Usually, anything unknown is the sender's duck. Set to false to return null here.</param>
        /// <returns></returns>
        internal static Tuple<List<object>, Type> HandleObjectListByName(string objName, Duck sender, bool returnsSender = true)
        {
            Type ty;
            List<object> objects = new List<object>();
            var lowerName = objName.ToLowerInvariant();

            var region = MserDebug.StartProfiling("HandleObjectListByName");

            switch (lowerName)
            {
                case ("hold"):
                case ("held"):
                case ("holdobject"):
                    objects.Add(sender.holdObject);
                    ty = typeof(Holdable);
                    break;
                case ("boots"):
                case ("shoes"):
                    objects.Add(sender._equipment.FirstOrDefault(x => x is Boots));
                    ty = typeof(Boots);
                    break;
                case ("armor"):
                case ("chestplate"):
                    objects.Add(sender._equipment.FirstOrDefault(x => x is ChestPlate));
                    ty = typeof(ChestPlate);
                    break;
                case ("helmet"):
                    objects.Add(sender._equipment.FirstOrDefault(x => x is Helmet));
                    ty = typeof(Helmet);
                    break;
                case ("hat"):
                    objects.Add(sender.hat);
                    ty = typeof(Hat);
                    break;
                case ("everyone"):
                case ("all"):
                    objects.AddRange(Level.current.things[typeof(Duck)]);
                    ty = typeof(Duck);
                    break;
                case ("level"):
                    objects.Add(Level.current);
                    ty = typeof(Level);
                    break;
                default:
                    var exclude = false;

                    //Get exclusion flag
                    if (objName.EndsWith("!"))
                    {
                        objName = objName.Substring(0, objName.Length - 1);
                        exclude = true;
                    }

                    //Get range of things
                    if (objName.StartsWith("all"))
                    {
                        //All in level
                        objName = objName.Substring(3);
                        ty = MserGlobals.GetType(objName);
                        if (!MserGlobals.IsThing(ty))
                        {
                            return null;
                        }

                        objects.AddRange(Level.current.things[ty]);

                        if (exclude && objects.Contains(sender))
                        {
                            //Exclude sender
                            objects.Remove(sender);
                        }
                    }
                    else if (objName.StartsWith("near"))
                    {
                        //All near to sender
                        objName = objName.Substring(4);
                        ty = MserGlobals.GetType(objName);
                        if (!MserGlobals.IsThing(ty))
                        {
                            return null;
                        }

                        objects.AddRange(Level.CheckCircleAll<Thing>(sender.position, 32));

                        var remlist = new List<Thing>();
                        foreach (Thing th in objects)
                        {
                            //Remove if wrong type
                            if (th.GetType() != ty && !th.GetType().IsSubclassOf(ty))
                            {
                                remlist.Add(th);
                            }
                        }
                        foreach (var rem in remlist)
                        {
                            objects.Remove(rem);
                        }

                        if (exclude && objects.Contains(sender))
                        {
                            //Exclude sender
                            objects.Remove(sender);
                        }
                    }
                    else if (!exclude)
                    {
                        if (!returnsSender)
                            return null;

                        //Default: Sender (type is default - Duck)
                        objects.Add(sender);
                        ty = typeof(Duck);
                    }
                    else
                    {
                        return null;
                    }
                    break;
            }

            Type newty;
            if (objects.Count == 0 || !objects.AllEqual(e => e.GetType()))
            {
                newty = ty;
            }
            else
            {
                newty = objects.First().GetType();
            }

            MserDebug.EndProfiling(region);

            return new Tuple<List<object>, Type>(objects, newty);
        }

        /// <summary>
        /// Returns the AutoCompleteEntryDrawDelegate to draw an icon to show next to a /call or /set info entry.
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        internal static AutoCompleteEntryDrawDelegate GetInfoIconDrawDelegate(MemberInfo info)
        {
            return rect =>
            {
                var icons = new List<Sprite>();
                int frame;
                bool priv;
                var redonly = false;

                if (info is PropertyInfo)
                {
                    var cast = ((PropertyInfo)info);
                    frame = 5;
                    redonly = !cast.CanWrite;
                    priv = cast.CanWrite && cast.GetSetMethod(true).IsPrivate;
                }
                else if (info is FieldInfo)
                {
                    var cast = ((FieldInfo)info);
                    frame = 8;
                    redonly = cast.IsInitOnly;
                    priv = cast.IsPrivate;
                }
                else
                {
                    var cast = ((MethodInfo)info);
                    frame = 3;
                    priv = cast.IsPrivate;
                }

                IconManager.TinyIcons.frame = frame;
                var clone = IconManager.TinyIcons.Clone();
                clone.scale = new Vec2(2);
                icons.Add(clone);
                if (priv)
                {
                    //Add lock icon
                    icons.Add(new Sprite("tinyLock") { center = new Vec2(1), scale = new Vec2(2) });
                }
                if (redonly)
                {
                    //Add readonly icon
                    icons.Add(new Sprite("connectionX") { center = new Vec2(1), scale = new Vec2(2) });
                }

                var x = rect.x;
                foreach (var ico in icons)
                {
                    ico.x = x + 2;
                    ico.y = rect.y + 2;
                    ico.Draw();
                    x -= 20;
                }
            };
        }

        /// <summary>
        /// Returns the AutoCompleteEntryDrawDelegate to draw an icon to show the enable state of a modifier.
        /// </summary>
        /// <returns></returns>
        internal static AutoCompleteEntryDrawDelegate GetModifierIconDrawDelegate(bool enabled)
        {
            return rect =>
            {
                IconManager.TinyIcons.frame = (enabled ? 1 : 0);
                var ico = IconManager.TinyIcons.Clone();
                ico.scale = new Vec2(2);
                ico.x = rect.x + 2;
                ico.y = rect.y + 2;
                ico.Draw();
            };
        }

        /// <summary>
        /// Characters valid in a hexadecimal string.
        /// </summary>
        internal static readonly List<char> _hexchars = new List<char> { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

        /// <summary>
        /// Reformats the string to a hexadecimal string (readable by HexToVec3). Returns null if format is unknown.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        internal static string ReformatHex(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
                return null;

            str = str.ToUpperInvariant();

            //Append hash
            if ((str.Length == 6 || str.Length == 3) && str[0] != '#')
            {
                str = '#' + str;
            }

            //Duplicate the single digits
            if (str.Length == 4)
            {
                str = string.Format("#{0}{0}{1}{1}{2}{2}", str[1], str[2], str[3]);
            }

            //Check if valid hex
            if (str[0] == '#' && str.Length == 7)
            {
                for (int i = 1; i < 7; i++)
                {
                    if (!CommandHelpers._hexchars.Contains(str[i]))
                    {
                        return null;
                    }
                }
                return str;
            }

            return null;
        }

        /// <summary>
        /// Checks whether the specified string is a valid hexadecimal color.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        internal static bool IsValidHex(string str)
        {
            return CommandHelpers.ReformatHex(str) != null;
        }

        /// <summary>
        /// Converts the specified hex string to a Vec3. Be sure to verify the hex first (IsValidHex).
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        internal static Vec3 HexToVec3(string hex)
        {
            hex = hex.ToUpperInvariant();

            //Append hash
            if (hex.Length == 6 && hex[0] != '#')
            {
                hex = '#' + hex;
            }

            return new Vec3(Int32.Parse(String.Format("{0}{1}", hex[1], hex[2]), NumberStyles.HexNumber),
                            Int32.Parse(String.Format("{0}{1}", hex[3], hex[4]), NumberStyles.HexNumber),
                            Int32.Parse(String.Format("{0}{1}", hex[5], hex[6]), NumberStyles.HexNumber));
        }

        /// <summary>
        /// Returns the specified Microsoft(tm) color as a Vec3, or null if the color does not exist.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal static Vec3? ColorByMicrosoftName(string name)
        {
            var prop = typeof(Microsoft.Xna.Framework.Color).GetProperty(name, BindingFlags.Public | BindingFlags.Static | BindingFlags.IgnoreCase);
            if (prop == null)
            {
                return null;
            }
            else
            {
                Microsoft.Xna.Framework.Color color = (Microsoft.Xna.Framework.Color)prop.GetValue(null, null);
                return new Vec3(color.R, color.G, color.B);
            }
        }

        /// <summary>
        /// Returns the color from the specified color part list as a Vec3 from 0 to 255
        /// </summary>
        /// <param name="autoColor"></param>
        /// <returns></returns>
        internal static Vec3? GetColorFromString(List<string> autoColor)
        {
            try
            {
                if (autoColor == null)
                    return null;

                //Remove whitespacers
                autoColor = autoColor.Where(x => !String.IsNullOrWhiteSpace(x)).ToList();

                if (autoColor.Count == 3)
                {
                    //Cap values (do not allow 0 because, for unknown reasons, 0|0|0 does not change your color!)
                    var newColor = new Vec3();
                    newColor.x = Math.Max(1, Convert.ToSingle(autoColor[0]));
                    newColor.x = Math.Min(255, Convert.ToSingle(autoColor[0]));
                    newColor.y = Math.Max(1, Convert.ToSingle(autoColor[1]));
                    newColor.y = Math.Min(255, Convert.ToSingle(autoColor[1]));
                    newColor.z = Math.Max(1, Convert.ToSingle(autoColor[2]));
                    newColor.z = Math.Min(255, Convert.ToSingle(autoColor[2]));

                    return newColor;
                }

                if (autoColor.Count == 1)
                {
                    Vec3 dictColor;

                    if (ColorCommand.ColorNames.TryGetValue(autoColor[0].ToLowerInvariant(), out dictColor))
                    {
                        //Name exists!
                        return dictColor;
                    }

                    if (CommandHelpers.ColorByMicrosoftName(autoColor[0]) != null)
                    {
                        //Color exists!
                        return CommandHelpers.ColorByMicrosoftName(autoColor[0]).Value;
                    }

                    //Assume Hex as a final resort (check for format #RRGGBB)
                    var hexstr = autoColor[0];

                    if (CommandHelpers.IsValidHex(hexstr))
                        return CommandHelpers.HexToVec3(CommandHelpers.ReformatHex(hexstr));
                }
            }
            catch
            {
                //Doesn't matter if the color didn't work out, the command will handle it.
            }

            return null;
        }

        /// <summary>
        /// Colors the specified DuckPersona using the color arguments provided.
        /// <para />Returns whether the color change was successful.
        /// </summary>
        /// <param name="autoColor"></param>
        /// <param name="profile"></param>
        /// <returns></returns>
        internal static bool SetColorFromString(List<string> autoColor, Profile profile = null)
        {
            if (profile == null)
                profile = MserGlobals.GetLocalDuck().profile;
            var persona = profile.persona;

            var color = CommandHelpers.GetColorFromString(autoColor);

            if (color == null)
                return false;

            persona.color = color.Value;
            persona.Recreate();

            Skin skin;
            if (Skin.SelectedSkins.TryGetValue(profile, out skin))
            {
                skin.Apply(profile);
            }

            return true;
        }

        /// <summary>
        /// Returns a list of Microsoft(tm) color names.
        /// </summary>
        /// <returns></returns>
        internal static IEnumerable<string> GetListOfMicrosoftColors()
        {
            return typeof(Microsoft.Xna.Framework.Color).GetProperties(BindingFlags.Public | BindingFlags.Static).Select(prop => prop.Name);
        }

        internal static Thing CreateSpecialThing(string name, Vec2 spawnPoint, Profile sender)
        {
            switch (name)
            {
                case "exchanger":
                {
                    Swapper sw = new Swapper(spawnPoint.x, spawnPoint.y);
                    return sw;
                }
                case "basketball":
                {
                    BasketballMP mp = new BasketballMP(spawnPoint.x, spawnPoint.y);
                    return mp;
                }
                case "firecracker":
                case "firecrackers":
                {
                    FireCrackersMP mp = new FireCrackersMP(spawnPoint.x, spawnPoint.y);
                    return mp;
                }
                case "dc":
                {
                    DeathCrate mp = new DeathCrate(spawnPoint.x, spawnPoint.y);
                    return mp;
                }
                case "brit":
                {
                    Britttannnia br = new Britttannnia(spawnPoint.x, spawnPoint.y);
                    return br;
                }
                case "guitar":
                {
                    Instrument instr = new Instrument(spawnPoint.x, spawnPoint.y, 0);
                    return instr;
                }
                case "flute":
                {
                    Instrument instr = new Instrument(spawnPoint.x, spawnPoint.y, 4);
                    return instr;
                }
                case "violin":
                {
                    Instrument instr = new Instrument(spawnPoint.x, spawnPoint.y, 3);
                    return instr;
                }
                case "bass":
                {
                    Instrument instr = new Instrument(spawnPoint.x, spawnPoint.y, 1);
                    return instr;
                }
                case "overdrive":
                {
                    Instrument instr = new Instrument(spawnPoint.x, spawnPoint.y, 2);
                    return instr;
                }
                case "trombone":
                {
                    Instrument instr = new Instrument(spawnPoint.x, spawnPoint.y, -2);
                    return instr;
                }
                case "saxaphone":
                {
                    Instrument instr = new Instrument(spawnPoint.x, spawnPoint.y, -3);
                    return instr;
                }
                case "acidtrumpet":
                {
                    Instrument instr = new Instrument(spawnPoint.x, spawnPoint.y, 5);
                    return instr;
                }
                case "scififlute":
                {
                    Instrument instr = new Instrument(spawnPoint.x, spawnPoint.y, 6);
                    return instr;
                }
                case "anyinstrument":
                case "instrument":
                {
                    Instrument instr = new Instrument(spawnPoint.x, spawnPoint.y, -1);
                    return instr;
                }
                case "ragdoll":
                {
                    NetworkRagdoll rag = new NetworkRagdoll(spawnPoint.x, spawnPoint.y, 0, 0, Vec2.Zero, sender.persona.color);
                    return rag;
                }
                case "car":
                {
                    CarController car = new CarController(spawnPoint.x, spawnPoint.y);
                    return car;
                }
                case "lectern":
                {
                    BookStand stand = new BookStand(spawnPoint.x, spawnPoint.y);
                    return stand;
                }
                case "keyboard":
                {
                    Piano piano = new Piano(spawnPoint.x, spawnPoint.y);
                    return piano;
                }
            }
            return null;
        }

        /// <summary>
        /// List of special things for being shown in the autocomplete list.
        /// </summary>
        internal static readonly List<string> _specialThings = new List<string>()
        {
            "Guitar", "Flute", "Violin", "Bass", "Overdrive", "Ragdoll", "AcidTrumpet", "ScifiFlute", "Car", "Exchanger", "Lectern", "Keyboard"
        };

        /// <summary>
        /// Returns an AutoCompleteEntryDrawDelegate for drawing the thing with the specified name.
        /// </summary>
        /// <param name="thing"></param>
        /// <returns></returns>
        internal static AutoCompleteEntryDrawDelegate GetThingDrawDelegate(string thing)
        {
            return null;

            /*
            rect =>
                  {
                      var sprite = GetDrawSprite(e);
                      if (sprite != null)
                      {
                          var topleft = sprite.Item2 - new Vec2(8);
                          Graphics.Draw(sprite.Item1, rect.x + 2, rect.y + 10, new Rectangle(topleft.x, topleft.y, topleft.x + 16, topleft.y + 16));
                      }
                  }
            */
        }

        /// <summary>
        /// Returns the sprite that can be drawn for a AutoCompleteEntryDrawDelegate of a thing with the specified name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static Tuple<Sprite, Vec2> GetDrawSprite(string name)
        {
            try
            {
                var thing = CommandHelpers.CreateSpecialThing(name.ToLowerInvariant(),
                                                              Vec2.Zero, MserGlobals.GetLocalDuck().profile);

                if (thing == null)
                    thing = (Thing)Activator.CreateInstance(MserGlobals.GetType(name), 0, 0);

                if (thing != null && thing.graphic != null)
                {
                    //Show preview image
                    var sprite = thing.graphic;
                    //var sprite = thing.GetEditorImage(16, 16);
                    sprite.CenterOrigin();
                    return new Tuple<Sprite, Vec2>(sprite, thing.center);
                }

            }
            catch
            { }

            return null;
        }

        /// <summary>
        /// Spawns the specified thing under certain circumstances limited to /spawn(ex).
        /// </summary>
        /// <param name="thing"></param>
        internal static void SpawnThing(Thing thing)
        {
            if (MserGlobals.GetGameState() == GameState.RockThrow && (thing is Gun || thing is CarController || thing is CarBody))
                return;

            Level.Add(thing);
        }
    }
}
