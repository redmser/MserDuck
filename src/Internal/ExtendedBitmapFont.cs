﻿using System;
using System.Linq;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Reimplemented, extended version of the BitmapFont. It allows for more characters to be displayed and includes more helper functions.
    /// </summary>
    public class ExtendedBitmapFont : Transform
    {
        private const int kTilesPerRow = 16;

        private static System.Collections.Generic.Dictionary<string, System.Collections.Generic.List<Rectangle>> widthMap = new System.Collections.Generic.Dictionary<string, System.Collections.Generic.List<Rectangle>>();

        private Sprite _texture;

        private static bool _mapInitialized = false;

        private static char[] _characters = new char[]
        {
            ' ',
            '!',
            '"',
            '#',
            '$',
            '%',
            '&',
            '\'',
            '(',
            ')',
            '*',
            '+',
            ',',
            '-',
            '.',
            '/',
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            ':',
            ';',
            '>',
            '=',
            '<',
            '?',
            '@',
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
            'V',
            'W',
            'X',
            'Y',
            'Z',
            '[',
            '\\',
            ']',
            '^',
            '_',
            '`',
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
            'l',
            'm',
            'n',
            'o',
            'p',
            'q',
            'r',
            's',
            't',
            'u',
            'v',
            'w',
            'x',
            'y',
            'z',
            '{',
            '|',
            '}',
            '~',
            '´',
            'ß',
            '°',
            'Ç',
            'ç',
            '«',
            '»',
            'º',
            'ª',
            '§'
        };

        private static int[] _characterMap = new int[255];

        private InputProfile _inputProfile;

        private int _maxWidth;

        private System.Collections.Generic.List<Rectangle> _widths;

        private int _charHeight;

        private int _firstYPixel;

        private int _letterIndex;

        private bool _drawingOutline;

        public int _highlightStart = -1;

        public int _highlightEnd = -1;

        public float height
        {
            get
            {
                return (float)this._texture.height * base.scale.y;
            }
        }

        public InputProfile inputProfile
        {
            get
            {
                return this._inputProfile;
            }
            set
            {
                this._inputProfile = value;
            }
        }

        public int maxWidth
        {
            get
            {
                return this._maxWidth;
            }
            set
            {
                this._maxWidth = value;
            }
        }

        public int maxRows
        {
            get;
            set;
        }

        public int characterHeight
        {
            get
            {
                return this._charHeight;
            }
        }

        public ExtendedBitmapFont(string image)
        {
            this._texture = new Sprite(image, 0f, 0f);
            if (!ExtendedBitmapFont.widthMap.TryGetValue(image, out this._widths))
            {
                this._widths = new System.Collections.Generic.List<Rectangle>();
                Color[] data = this._texture.texture.GetData();
                bool done = false;
                int leftPixel = -1;
                for (int ypixel = 1; ypixel < this._texture.height; ypixel += this._charHeight + 1)
                {
                    for (int xpixel = 0; xpixel < this._texture.width; xpixel++)
                    {
                        if (data[xpixel + ypixel * this._texture.width].r == 0 && data[xpixel + ypixel * this._texture.width].g == 0 && data[xpixel + ypixel * this._texture.width].b == 0 && data[xpixel + ypixel * this._texture.width].a == 0)
                        {
                            if (leftPixel == -1)
                            {
                                leftPixel = xpixel;
                            }
                        }
                        else if (leftPixel != -1)
                        {
                            if (this._charHeight == 0)
                            {
                                this._firstYPixel = ypixel;
                                xpixel--;
                                for (int yheight = ypixel + 1; yheight < this._texture.height; yheight++)
                                {
                                    if (data[xpixel + yheight * this._texture.width].r != 0 || data[xpixel + yheight * this._texture.width].g != 0 || data[xpixel + yheight * this._texture.width].b != 0 || data[xpixel + yheight * this._texture.width].a != 0)
                                    {
                                        this._charHeight = yheight - ypixel;
                                        break;
                                    }
                                }
                                xpixel++;
                            }
                            this._widths.Add(new Rectangle((float)leftPixel, (float)ypixel, (float)(xpixel - leftPixel), (float)this._charHeight));
                            leftPixel = -1;
                        }
                    }
                    if (done)
                    {
                        break;
                    }
                }
            }
            ExtendedBitmapFont.widthMap[image] = this._widths;
            if (this._widths.Count > 0)
            {
                this._charHeight = (int)this._widths[0].height;
            }
            if (!ExtendedBitmapFont._mapInitialized)
            {
                for (int i = 0; i < 255; i++)
                {
                    char c = (char)i;
                    ExtendedBitmapFont._characterMap[i] = 91;
                    for (int iChar = 0; iChar < ExtendedBitmapFont._characters.Length; iChar++)
                    {
                        if (ExtendedBitmapFont._characters[iChar] == c)
                        {
                            ExtendedBitmapFont._characterMap[i] = iChar;
                            break;
                        }
                    }
                }
                ExtendedBitmapFont._mapInitialized = true;
            }
        }

        public Sprite ParseSprite(string text, InputProfile input)
        {
            this._letterIndex++;
            string trigger = "";
            while (this._letterIndex != text.Length && text[this._letterIndex] != ' ' && text[this._letterIndex] != '@')
            {
                trigger += text[this._letterIndex];
                this._letterIndex++;
            }
            Sprite spr = null;
            if (input != null)
            {
                spr = input.GetTriggerImage(trigger);
            }
            if (spr == null)
            {
                spr = Input.GetTriggerSprite(trigger);
            }
            return spr;
        }

        public Color ParseColor(string text)
        {
            this._letterIndex++;
            string read = "";
            while (this._letterIndex != text.Length && text[this._letterIndex] != ' ' && text[this._letterIndex] != '|')
            {
                read += text[this._letterIndex];
                this._letterIndex++;
            }
            return ParseColorInternal(read);
        }

        public static Color ParseColorInternal(string color)
        {
            Color color1 = Color.Transparent;
            if (color == "RED")
                color1 = Color.Red;
            else if (color == "WHITE")
                color1 = Color.White;
            else if (color == "BLACK")
                color1 = Color.Black;
            else if (color == "DARKNESS")
                color1 = new Color(10, 10, 10);
            else if (color == "BLUE")
                color1 = Color.Blue;
            else if (color == "DGBLUE")
                color1 = new Color(49, 162, 242);
            else if (color == "DGRED")
                color1 = new Color(192, 32, 45);
            else if (color == "DGGREEN")
                color1 = new Color(163, 206, 39);
            else if (color == "DGYELLOW")
                color1 = new Color(247, 224, 90);
            else if (color == "DGORANGE")
                color1 = new Color(235, 136, 49);
            else if (color == "ORANGE")
                color1 = new Color(235, 137, 51);
            else if (color == "MENUORANGE")
                color1 = new Color(235, 137, 51);
            else if (color == "YELLOW")
                color1 = new Color(247, 224, 90);
            else if (color == "GREEN")
                color1 = Color.LimeGreen;
            else if (color == "LIME")
                color1 = Color.LimeGreen;
            else if (color == "GRAY")
                color1 = new Color(70, 70, 70);
            else if (color == "LIGHTGRAY")
                color1 = new Color(96, 119, 124);
            else if (color == "BLUEGRAY")
                color1 = new Color(47, 73, 79);
            else if (color == "PINK")
                color1 = new Color(246, 88, 191);
            else if (color == "PURPLE" || color == "DGPURPLE")
                color1 = new Color(115, 48, 242);
            return color1;
        }

        public InputProfile GetInputProfile(InputProfile input)
        {
            if (input == null)
            {
                input = ((this._inputProfile != null) ? this._inputProfile : InputProfile.FirstProfileWithDevice);
            }
            return input;
        }

        public float GetWidth(string text, bool thinButtons = false)
        {
            float wide = 0f;
            float widest = 0f;
            this._letterIndex = 0;
            while (this._letterIndex < text.Length)
            {
                bool processedSpecialCharacter = false;
                if (text[this._letterIndex] == '@')
                {
                    int iPos = this._letterIndex;
                    Sprite spr = this.ParseSprite(text, null);
                    if (spr != null)
                    {
                        wide += (thinButtons ? 6f : ((float)spr.width * spr.scale.x + 1f));
                        processedSpecialCharacter = true;
                    }
                    else
                    {
                        this._letterIndex = iPos;
                    }
                }
                else if (text[this._letterIndex] == '|')
                {
                    int iPos2 = this._letterIndex;
                    Color c = this.ParseColor(text);
                    if (c != new Color(0, 0, 0, 0))
                    {
                        processedSpecialCharacter = true;
                    }
                    else
                    {
                        this._letterIndex = iPos2;
                    }
                }
                else if (text[this._letterIndex] == '\n')
                {
                    if (wide > widest)
                    {
                        widest = wide;
                    }
                    wide = 0f;
                }
                if (!processedSpecialCharacter)
                {
                    byte charVal = (byte)Maths.Clamp((int)text[this._letterIndex], 0, 254);
                    int charIndex = ExtendedBitmapFont._characterMap[(int)charVal];
                    wide += (this._widths[charIndex].width - 1f) * base.scale.x;
                }
                this._letterIndex++;
            }
            if (wide > widest)
            {
                widest = wide;
            }
            return widest;
        }

        public int GetCharacterIndex(string text, float xPosition, float yPosition, int maxRows = 2147483647, bool thinButtons = false)
        {
            float wide = 0f;
            float high = 0f;
            int numHigh = 0;
            float widest = 0f;
            this._letterIndex = 0;
            while (this._letterIndex < text.Length)
            {
                if ((wide >= xPosition && yPosition < high + (float)this._charHeight * base.scale.y) || numHigh >= maxRows)
                {
                    return this._letterIndex - 1;
                }
                bool processedSpecialCharacter = false;
                if (text[this._letterIndex] == '@')
                {
                    int iPos = this._letterIndex;
                    Sprite spr = this.ParseSprite(text, null);
                    if (spr != null)
                    {
                        wide += (thinButtons ? 6f : ((float)spr.width * spr.scale.x + 1f));
                        processedSpecialCharacter = true;
                    }
                    else
                    {
                        this._letterIndex = iPos;
                    }
                }
                else if (text[this._letterIndex] == '|')
                {
                    int iPos2 = this._letterIndex;
                    Color c = this.ParseColor(text);
                    if (c != new Color(0, 0, 0, 0))
                    {
                        processedSpecialCharacter = true;
                    }
                    else
                    {
                        this._letterIndex = iPos2;
                    }
                }
                else if (text[this._letterIndex] == '\n')
                {
                    if (wide > widest)
                    {
                        widest = wide;
                    }
                    wide = 0f;
                    numHigh++;
                    high += (float)this._charHeight * base.scale.y;
                    processedSpecialCharacter = true;
                    if (numHigh >= maxRows)
                    {
                        return this._letterIndex;
                    }
                }
                if (!processedSpecialCharacter)
                {
                    bool skipped = false;
                    if (this.maxWidth > 0)
                    {
                        if (text[this._letterIndex] == ' ' || text[this._letterIndex] == '|' || text[this._letterIndex] == '@')
                        {
                            int idxxx = this._letterIndex + 1;
                            float additionalWidth = 0f;
                            while (idxxx < text.Count<char>() && text[idxxx] != ' ' && text[idxxx] != '|' && text[idxxx] != '@')
                            {
                                byte charVal = (byte)Maths.Clamp((int)text[idxxx], 0, 254);
                                int cIndex = ExtendedBitmapFont._characterMap[(int)charVal];
                                additionalWidth += (this._widths[cIndex].width - 1f) * base.scale.x;
                                idxxx++;
                            }
                            if (wide + additionalWidth > (float)this.maxWidth)
                            {
                                numHigh++;
                                high += (float)this._charHeight * base.scale.y;
                                wide = 0f;
                                skipped = true;
                                if (numHigh >= maxRows)
                                {
                                    return this._letterIndex;
                                }
                            }
                        }
                        else
                        {
                            byte charVal2 = (byte)Maths.Clamp((int)text[this._letterIndex], 0, 254);
                            int cIndex2 = ExtendedBitmapFont._characterMap[(int)charVal2];
                            if (wide + this._widths[cIndex2].width * base.scale.x > (float)this.maxWidth)
                            {
                                numHigh++;
                                high += (float)this._charHeight * base.scale.y;
                                wide = 0f;
                                if (numHigh >= maxRows)
                                {
                                    return this._letterIndex;
                                }
                            }
                        }
                    }
                    if (!skipped)
                    {
                        byte charVal3 = (byte)Maths.Clamp((int)text[this._letterIndex], 0, 254);
                        int charIndex = ExtendedBitmapFont._characterMap[(int)charVal3];
                        wide += (this._widths[charIndex].width - 1f) * base.scale.x;
                    }
                }
                if (high > yPosition)
                {
                    return this._letterIndex;
                }
                this._letterIndex++;
            }
            return this._letterIndex;
        }

        public Vec2 GetCharacterPosition(string text, int index, bool thinButtons = false)
        {
            float wide = 0f;
            float high = 0f;
            float widest = 0f;
            this._letterIndex = 0;
            while (this._letterIndex < text.Length)
            {
                if (this._letterIndex >= index)
                {
                    return new Vec2(wide, high);
                }
                bool processedSpecialCharacter = false;
                if (text[this._letterIndex] == '@')
                {
                    int iPos = this._letterIndex;
                    Sprite spr = this.ParseSprite(text, null);
                    if (spr != null)
                    {
                        wide += (thinButtons ? 6f : ((float)spr.width * spr.scale.x + 1f));
                        processedSpecialCharacter = true;
                    }
                    else
                    {
                        this._letterIndex = iPos;
                    }
                }
                else if (text[this._letterIndex] == '|')
                {
                    int iPos2 = this._letterIndex;
                    Color c = this.ParseColor(text);
                    if (c != new Color(0, 0, 0, 0))
                    {
                        processedSpecialCharacter = true;
                    }
                    else
                    {
                        this._letterIndex = iPos2;
                    }
                }
                else if (text[this._letterIndex] == '\n')
                {
                    if (wide > widest)
                    {
                        widest = wide;
                    }
                    wide = 0f;
                    high += (float)this._charHeight * base.scale.y;
                    processedSpecialCharacter = true;
                }
                if (!processedSpecialCharacter)
                {
                    bool skipped = false;
                    if (this.maxWidth > 0)
                    {
                        if (text[this._letterIndex] == ' ' || text[this._letterIndex] == '|' || text[this._letterIndex] == '@')
                        {
                            int idxxx = this._letterIndex + 1;
                            float additionalWidth = 0f;
                            while (idxxx < text.Count<char>() && text[idxxx] != ' ' && text[idxxx] != '|' && text[idxxx] != '@')
                            {
                                byte charVal = (byte)Maths.Clamp((int)text[idxxx], 0, 254);
                                int cIndex = ExtendedBitmapFont._characterMap[(int)charVal];
                                additionalWidth += (this._widths[cIndex].width - 1f) * base.scale.x;
                                idxxx++;
                            }
                            if (wide + additionalWidth > (float)this.maxWidth)
                            {
                                high += (float)this._charHeight * base.scale.y;
                                wide = 0f;
                                skipped = true;
                            }
                        }
                        else
                        {
                            byte charVal2 = (byte)Maths.Clamp((int)text[this._letterIndex], 0, 254);
                            int cIndex2 = ExtendedBitmapFont._characterMap[(int)charVal2];
                            if (wide + this._widths[cIndex2].width * base.scale.x > (float)this.maxWidth)
                            {
                                high += (float)this._charHeight * base.scale.y;
                                wide = 0f;
                            }
                        }
                    }
                    if (!skipped)
                    {
                        byte charVal3 = (byte)Maths.Clamp((int)text[this._letterIndex], 0, 254);
                        int charIndex = ExtendedBitmapFont._characterMap[(int)charVal3];
                        wide += (this._widths[charIndex].width - 1f) * base.scale.x;
                    }
                }
                this._letterIndex++;
            }
            return new Vec2(wide, high);
        }

        public void DrawOutline(string text, Vec2 pos, Color c, Color outline, Depth deep = default(Depth), float outlineThickness = 1f)
        {
            this._drawingOutline = true;
            this.Draw(text, pos + new Vec2(-outlineThickness, 0f), outline, deep + 2, true);
            this.Draw(text, pos + new Vec2(outlineThickness, 0f), outline, deep + 2, true);
            this.Draw(text, pos + new Vec2(0f, -outlineThickness), outline, deep + 2, true);
            this.Draw(text, pos + new Vec2(0f, outlineThickness), outline, deep + 2, true);
            this.Draw(text, pos + new Vec2(-outlineThickness, -outlineThickness), outline, deep + 2, true);
            this.Draw(text, pos + new Vec2(outlineThickness, -outlineThickness), outline, deep + 2, true);
            this.Draw(text, pos + new Vec2(-outlineThickness, outlineThickness), outline, deep + 2, true);
            this.Draw(text, pos + new Vec2(outlineThickness, outlineThickness), outline, deep + 2, true);
            this._drawingOutline = false;
            this.Draw(text, pos, c, deep + 5, false);
        }

        public void Draw(string text, Vec2 pos, Color c, Depth deep = default(Depth), bool colorSymbols = false)
        {
            this.Draw(text, pos.x, pos.y, c, deep, colorSymbols);
        }

        public void Draw(string text, float xpos, float ypos, Color c, Depth deep = default(Depth), bool colorSymbols = false)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return;
            }

            //Strip \r
            text = text.Replace("\r", string.Empty);

            Color highlight = new Color((int)(255 - c.r), (int)(255 - c.g), (int)(255 - c.b));
            float yOff = 0f;
            float xOff = 0f;
            int curRow = 0;
            this._letterIndex = 0;
            while (this._letterIndex < text.Length)
            {
                bool processedSpecialCharacter = false;
                if (text[this._letterIndex] == '@')
                {
                    int iPos = this._letterIndex;
                    Sprite spr = this.ParseSprite(text, null);
                    if (spr != null)
                    {
                        float al = spr.alpha;
                        spr.alpha = base.alpha * c.ToVector4().w;
                        if (spr != null)
                        {
                            float yCenter = (float)(this._texture.height / 2 - spr.height / 2);
                            if (colorSymbols)
                            {
                                spr.color = c;
                            }
                            Graphics.Draw(spr, xpos + xOff, ypos + yOff + yCenter, deep);
                            xOff += (float)(spr.width + 1);
                            spr.color = Color.White;
                        }
                        spr.alpha = al;
                        processedSpecialCharacter = true;
                    }
                    else
                    {
                        this._letterIndex = iPos;
                    }
                }
                else if (text[this._letterIndex] == '|')
                {
                    int iPos2 = this._letterIndex;
                    Color col = this.ParseColor(text);
                    if (col != new Color(0, 0, 0, 0))
                    {
                        if (!this._drawingOutline)
                        {
                            float al2 = c.ToVector4().w;
                            c = col;
                            c *= al2;
                        }
                        processedSpecialCharacter = true;
                    }
                    else
                    {
                        this._letterIndex = iPos2;
                    }
                }
                if (!processedSpecialCharacter)
                {
                    bool skippedLine = false;
                    if (this.maxWidth > 0)
                    {
                        if (text[this._letterIndex] == ' ' || text[this._letterIndex] == '|' || text[this._letterIndex] == '@')
                        {
                            int index = this._letterIndex + 1;
                            char sp = ' ';
                            float additionalWidth = this._widths[ExtendedBitmapFont._characterMap[(int)((byte)sp)]].width;
                            while (index < text.Count<char>() && text[index] != ' ' && text[index] != '|' && text[index] != '@')
                            {
                                byte charVal = (byte)Maths.Clamp((int)text[index], 0, 254);
                                int cIndex = ExtendedBitmapFont._characterMap[(int)charVal];
                                additionalWidth += (this._widths[cIndex].width - 1f) * base.scale.x;
                                index++;
                            }
                            if (xOff + additionalWidth > (float)this.maxWidth)
                            {
                                yOff += (float)this._charHeight * base.scale.y;
                                xOff = 0f;
                                curRow++;
                                skippedLine = true;
                            }
                        }
                        else
                        {
                            byte charVal2 = (byte)Maths.Clamp((int)text[this._letterIndex], 0, 254);
                            int cIndex2 = ExtendedBitmapFont._characterMap[(int)charVal2];
                            if (xOff + this._widths[cIndex2].width * base.scale.x > (float)this.maxWidth)
                            {
                                yOff += (float)this._charHeight * base.scale.y;
                                xOff = 0f;
                                curRow++;
                            }
                        }
                    }
                    if (this.maxRows != 0 && curRow >= this.maxRows)
                    {
                        return;
                    }
                    if (!skippedLine)
                    {
                        if (text[this._letterIndex] == '\n')
                        {
                            yOff += (float)this._charHeight * base.scale.y;
                            xOff = 0f;
                            curRow++;
                        }
                        else
                        {
                            byte charVal3 = (byte)Maths.Clamp((int)text[this._letterIndex], 0, 254);
                            int charIndex = ExtendedBitmapFont._characterMap[(int)charVal3];
                            Rectangle dat = this._widths[charIndex];
                            Graphics.ResetDepthBias();
                            this._texture.scale = base.scale;
                            if (this._highlightStart != -1 && this._highlightStart != this._highlightEnd && ((this._highlightStart < this._highlightEnd && this._letterIndex >= this._highlightStart && this._letterIndex < this._highlightEnd) || (this._letterIndex < this._highlightStart && this._letterIndex >= this._highlightEnd)))
                            {
                                Graphics.DrawRect(new Vec2(xpos + xOff, ypos + yOff), new Vec2(xpos + xOff, ypos + yOff) + new Vec2(dat.width * base.scale.x, (float)this._charHeight * base.scale.y), c, deep - 5, true, 1f);
                                this._texture.color = highlight;
                            }
                            else
                            {
                                this._texture.color = c;
                            }
                            this._texture.alpha = base.alpha;
                            Graphics.Draw(this._texture, xpos + xOff, ypos + yOff, dat, deep);
                            xOff += (dat.width - 1f) * base.scale.x;
                        }
                    }
                }
                this._letterIndex++;
            }
        }

        /// <summary>
        /// Returns the height of a line of text.
        /// </summary>
        /// <returns></returns>
        public float GetHeight()
        {
            return this._charHeight * this.scale.y;
        }

        /// <summary>
        /// Returns the height of the specified text.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public float GetHeight(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return 0;

            if (!text.Contains('\n'))
                return this.GetHeight();

            return (text.Length - text.Replace("\n", "").Length + 1) * this.GetHeight();
        }

        /// <summary>
        /// Fits the specified text to a certain pixel width, removing any characters that extend beyond it.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="width"></param>
        /// <param name="addDots"></param>
        /// <returns></returns>
        public string FitTextToWidth(string text, float width, bool addDots = true)
        {
            if (width <= 0 || text.Length == 0)
                return string.Empty;

            //Get longest line
            text = text.Split(new[] {"\r\n", "\n"}, StringSplitOptions.RemoveEmptyEntries).OrderByDescending(s => s.Length).First();

            var i = 0;
            var len = addDots ? this.GetWidth("...") : 0f;
            do
            {
                len += this.GetWidth(text[i].ToString());
                i++;

                if (i >= text.Length)
                {
                    i += 1;
                    break;
                }
            } while (len < width);
            return text.Truncate(i - 1, addDots);
        }
    }
}
