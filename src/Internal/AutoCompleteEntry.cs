﻿using System;
using System.Collections.Generic;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// An entry in the auto complete list.
    /// <para />You do not have to use this class if you don't need to specify extra options:
    /// <para />Use <code>new AutoCompleteEntry[] {"strings"}</code> to have the default constructor invoked, converting the strings to the entry's display and value.
    /// </summary>
    public class AutoCompleteEntry
    {
        private readonly string _display;

        /// <summary>
        /// Text to show in the autocomplete list.
        /// </summary>
        public string Display
        {
            get { return this._display; }
        }

        private readonly string _value;

        /// <summary>
        /// What to insert when autocompleted.
        /// </summary>
        public string Value
        {
            get
            {
                if (this._value == null)
                    return this.Display;
                return this._value;
            }
        }

        /// <summary>
        /// Whether this entry can be autocompleted to. Use false to display info or help texts (entries have different color).
        /// </summary>
        public bool Autocompletes
        {
            get { return this._autocompletes; }
        }

        private readonly bool _autocompletes;

        private readonly bool _sortByValue;

        /// <summary>
        /// Whether it should be sorted in the list by value or display.
        /// </summary>
        public bool SortByValue
        {
            get { return this._sortByValue; }
        }

        private readonly AutoCompleteEntryDrawDelegate _drawDelegate;

        /// <summary>
        /// A function that should define a draw call for this entry. It is recommended to only draw inside of the specified drawArea, or to the left of it.
        /// <para />Default size is 20x20.
        /// </summary>
        public AutoCompleteEntryDrawDelegate DrawDelegate
        {
            get { return this._drawDelegate; }
        }

        /// <summary>
        /// Creates a new auto complete entry.
        /// <para />You do not have to use this class if you don't need the extra options. Use new AutoCompleteEntry[] {"strings"} to have the default constructor invoked.
        /// </summary>
        /// <param name="display">Text to show in the autocomplete list.</param>
        /// <param name="value">What to insert when autocompleted.</param>
        /// <param name="sortByValue">Whether it should be sorted in the list by value or display.</param>
        /// <param name="drawDelegate"> A function that should define a draw call for this entry. It is recommended to only draw inside of the specified drawArea, or to the left of it.
        /// <para />Default size is 20x20.</param>
        /// <param name="autocompletes">Whether this entry can be autocompleted to. Use false to display info or help texts (entries have different color).</param>
        public AutoCompleteEntry(string display, string value = null, bool sortByValue = false, AutoCompleteEntryDrawDelegate drawDelegate = null, bool autocompletes = true)
        {
            this._display = display;
            this._value = value;
            this._sortByValue = sortByValue;
            this._drawDelegate = drawDelegate;
            this._autocompletes = autocompletes;
        }

        /// <summary>
        /// Creates an auto complete entry from a string.
        /// </summary>
        /// <param name="str"></param>
        public static implicit operator AutoCompleteEntry(string str)
        {
            return new AutoCompleteEntry(str);
        }

        private static readonly IEqualityComparer<AutoCompleteEntry> _defaultDisplayComparer = new AutoCompleteEntryComparer(true, false);

        /// <summary>
        /// A comparer used for checking if two AutoCompleteEntries have the same display strings (case insensitive).
        /// </summary>
        public static IEqualityComparer<AutoCompleteEntry> DefaultDisplayComparer
        {
            get { return AutoCompleteEntry._defaultDisplayComparer; }
        }

        private static readonly IEqualityComparer<AutoCompleteEntry> _defaultValueComparer = new AutoCompleteEntryComparer(true, true);

        /// <summary>
        /// A comparer used for checking if two AutoCompleteEntries have the same value strings (case insensitive).
        /// </summary>
        public static IEqualityComparer<AutoCompleteEntry> DefaultValueComparer
        {
            get { return AutoCompleteEntry._defaultValueComparer; }
        }

        private static readonly IEnumerable<AutoCompleteEntry> _booleans = new AutoCompleteEntry[] {"true", "false"};

        /// <summary>
        /// Autocomplete entries for the boolean values "true" and "false".
        /// </summary>
        public static IEnumerable<AutoCompleteEntry> Booleans
        {
            get { return _booleans; }
        }

        private static readonly IEnumerable<AutoCompleteEntry> _toggles = new AutoCompleteEntry[] { "on", "off", "toggle" };

        /// <summary>
        /// Autocomplete entries for the toggle values "on", "off" and "toggle".
        /// <para />Note that the commands usually also support "true/false", "1/0" and "enable/disable".
        /// </summary>
        public static IEnumerable<AutoCompleteEntry> Toggles
        {
            get { return _toggles; }
        }

        private bool _insertSpace = true;

        /// <summary>
        /// Whether autocompleting this entry adds a space after it.
        /// </summary>
        public bool InsertSpace
        {
            get { return this._insertSpace; }
            set { this._insertSpace = value; }
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            return string.Format("{{{0} {1}}}", this.Display, this.Value);
        }

        private class AutoCompleteEntryComparer : IEqualityComparer<AutoCompleteEntry>
        {
            private bool _ignoreCase;
            private bool _compareValues;

            public AutoCompleteEntryComparer(bool ignoreCase, bool compareValues)
            {
                this._ignoreCase = ignoreCase;
                this._compareValues = compareValues;
            }

            /// <summary>
            /// Determines whether the specified objects are equal.
            /// </summary>
            /// <returns>
            /// true if the specified objects are equal; otherwise, false.
            /// </returns>
            public bool Equals(AutoCompleteEntry x, AutoCompleteEntry y)
            {
                if (this._compareValues)
                {
                    return x.Value.Equals(y.Value,
                        this._ignoreCase
                            ? StringComparison.InvariantCultureIgnoreCase
                            : StringComparison.InvariantCulture);
                }

                return x.Display.Equals(y.Display,
                    this._ignoreCase
                        ? StringComparison.InvariantCultureIgnoreCase
                        : StringComparison.InvariantCulture);
            }

            /// <summary>
            /// Returns a hash code for the specified object.
            /// </summary>
            /// <returns>
            /// A hash code for the specified object.
            /// </returns>
            /// <param name="obj">The <see cref="T:System.Object"/> for which a hash code is to be returned.</param><exception cref="T:System.ArgumentNullException">The type of <paramref name="obj"/> is a reference type and <paramref name="obj"/> is null.</exception>
            public int GetHashCode(AutoCompleteEntry obj)
            {
                return obj.GetHashCode();
            }
        }
    }

    /// <summary>
    /// A function that should define a draw call for this entry. It is recommended to only draw inside of the specified <paramref name="drawArea"/>, or to the left of it.
    /// <para />Default size is 20x20.
    /// </summary>
    /// <param name="drawArea">The area to draw inside of.</param>
    public delegate void AutoCompleteEntryDrawDelegate(Rectangle drawArea);
}
