﻿using System;
using System.Collections.Generic;
using System.Linq;
using DuckGame.MserDuck.CommandHandlers;

namespace DuckGame.MserDuck
{
    public class PermissionRoleDefault : PermissionRole
    {
        /// <summary>
        /// Creates a new PermissionRoleDefault.
        /// </summary>
        public PermissionRoleDefault() : base("Default", Color.CornflowerBlue)
        {
            var icon = IconManager.GetIconFromSet(IconManager.TinyIcons, (int)IconManager.TinyIconsFrames.CurvedBlueArrow, default(Vec2), new Vec2(2), 0.9f);
            this._icon = icon;

            this.Permissions.AddRange(_defaultPermissions);
            //IMP: PermissionRoleDefault - doesnt work???
            this.Permissions.AddRange(_defaultCommands.Select(c => new CommandPermission((CommandHandler)Activator.CreateInstance(c))));
        }

        /// <summary>
        /// An icon to be associated with this role. Size should be 16x16.
        /// <para />This should not change over time or through user input.
        /// </summary>
        public override Sprite Icon
        {
            get { return this._icon; }
        }

        private readonly Sprite _icon;

        /// <summary>
        /// Whether a user of this role has the specified permission (specified by permission name).
        /// </summary>
        /// <param name="permission"></param>
        /// <returns></returns>
        public override bool HasPermission(Permission permission)
        {
            var cp = permission as CommandPermission;
            if (cp != null)
            {
                //Check command permission
                return _defaultCommands.Contains(cp.Command.GetType());
            }
            else
            {
                //Check other permission
                return _defaultPermissions.Any(p => p.GetName() == permission.GetName());
            }
        }

        /// <summary>
        /// List of default permissions.
        /// </summary>
        private static readonly List<Permission> _defaultPermissions = new List<Permission>
        {
            new DebugUIPermission(DebugUIPermissions.DebugModeOpen),
            new DebugUIPermission(DebugUIPermissions.DebugModeHoverInfo)
        }; 

        /// <summary>
        /// List of default commands that may be executed by a DefaultRole user.
        /// </summary>
        private static readonly List<Type> _defaultCommands = new List<Type>
        {
            //These commands do not influence the global game state in any way
            typeof(AliasCommand),
            typeof(AutoRunCommand),
            typeof(BindCommand),
            typeof(DebugCommand),
            typeof(DumpCommand),
            typeof(FindCommand),
            typeof(GameCommand),
            typeof(GitCommand),
            typeof(HelpCommand),
            typeof(HoldBindCommand),
            typeof(LoadConfigCommand),
            typeof(OpenDirCommand),
            typeof(RebindCommand),
            typeof(RepeatCommand),
            typeof(RunCommand),
            typeof(SaveConfigCommand),
            typeof(TimerCommand),
            typeof(ToggleBindCommand),
            typeof(UnbindCommand),

            //These commands are purely cosmetic and can be removed if undesirable
            typeof(CapeCommand),
            typeof(ColorCommand),
            typeof(SkinCommand),
            typeof(VocaQuackCommand)
        };
    }
}
