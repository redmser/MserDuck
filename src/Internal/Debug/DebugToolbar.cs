﻿using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck
{
    public class DebugToolbar : DebugBox
    {
        private readonly DebugListBox _listBox = new DebugListBox(56, Color.Black, 0f, 8);

        private readonly List<DebugBoxButton> _toolbarButtons = new List<DebugBoxButton>
        {
            //HINT: Add any DebugBoxes here that should show on the right toolbar.
            //  They will have to be added to the DebugUI._debugBoxes list as well unless
            //  the handling code for the button is changed.

            new DebugBoxButton(IconManager.ButtonIcons.CloneMap(), 19, () =>
            {
                var box = DebugUI._debugBoxes.OfType<DebugHelpBox>().FirstOrDefault();
                if (box != null)
                    box.Visible = !box.Visible;
            }, "Help"),
            new DebugBoxButton(IconManager.ButtonIcons.CloneMap(), 12, () =>
            {
                var box = DebugUI._debugBoxes.OfType<DebugErrorLog>().FirstOrDefault();
                if (box != null)
                    box.Visible = !box.Visible;
            }, "Error Log"),
            new DebugBoxButton(IconManager.ButtonIcons.CloneMap(), 4, () =>
            {
                var box = DebugUI._debugBoxes.OfType<DebugPrintBox>().FirstOrDefault();
                if (box != null)
                    box.Visible = !box.Visible;
            }, "Debug Values"),
            new DebugBoxButton(IconManager.ButtonIcons.CloneMap(), 5, () =>
            {
                var box = DebugUI._debugBoxes.OfType<DebugAssetExplorer>().FirstOrDefault();
                if (box != null)
                    box.Visible = !box.Visible;
            }, "Asset Explorer"),
            new DebugBoxButton(IconManager.ButtonIcons.CloneMap(), 6, () =>
            {
                var box = DebugUI._debugBoxes.OfType<DebugThingList>().FirstOrDefault();
                if (box != null)
                    box.Visible = !box.Visible;
            }, "Thing List"),
            new DebugBoxButton(IconManager.ButtonIcons.CloneMap(), 13, () =>
            {
                var box = DebugUI._debugBoxes.OfType<DebugProfilerBox>().FirstOrDefault();
                if (box != null)
                    box.Visible = !box.Visible;
            }, "Profiler"),
            new DebugBoxButton(IconManager.ButtonIcons.CloneMap(), 18, () =>
            {
                var box = DebugUI._debugBoxes.OfType<DebugCacheManager>().FirstOrDefault();
                if (box != null)
                    box.Visible = !box.Visible;
            }, "Cache Manager"),
            new DebugBoxButton(IconManager.ButtonIcons.CloneMap(), 20, () =>
            {
                var box = DebugUI._debugBoxes.OfType<DebugColorPicker>().FirstOrDefault();
                if (box != null)
                    box.Visible = !box.Visible;
            }, "Color Picker"),
        };

        /// <summary>
        /// Whether the location and size of this debugbox, as well as whether it is folded, should be stored on quit and reloaded as a default when created.
        /// <para />This only has any effect of debugboxes which get created together with the DebugUI.
        /// </summary>
        public override bool StoreGeometry
        {
            get { return false; }
        }

        public DebugToolbar()
        {
            this._hasClose = false;
            this._hasFold = false;
            this.Moveable = false;
            this.Resizable = false;

            foreach (var button in this._toolbarButtons)
            {
                button.IconMap.scale = new Vec2(2f);
                var dle = new DebugListEntry("", null, null, 0, button);
                dle.Height = button.TooltipText == "Help" ? 64 : 48;
                dle.Alignment = UIAlign.Top;
                dle.DisplayOrder = "BTI";
                this._listBox.AddEntry(dle);
            }
        }

        /// <summary>
        /// Returns whether any button was pressed
        /// </summary>
        /// <returns></returns>
        public override object DoUpdate()
        {
            this._listBox.position = new Vec2(Layer.Console.width - 48, 24);

            var clicked = (int)this._listBox.DoUpdate();
            if (clicked == -2)
            {
                base.DoUpdate();
                return true;
            }

            return base.DoUpdate();
        }

        public override void DoDraw()
        {
            if (!this.Visible)
                return;

            this._listBox.DoDraw();
        }
    }
}
