﻿namespace DuckGame.MserDuck
{
    public class DebugAssetExplorer : DebugBox
    {
        public DebugAssetExplorer(int index) : base(index)
        {
            this.InvisOnClose = true;
            this._visible = false;
            this.Title = "Asset Explorer";
        }

        //NYI: DebugAssetExplorer - Display list of different icons in DuckGame: buttonIcons, tinyIcons, Input._triggerImageMap
        //  PLUS other content of MserDuck and DuckGame, such as sprites, sounds, or shader previews
        //  Toss those into different categories separated using tabs. Clicking a button below the preview should copy a
        //  string to the clipboard which can be inserted straight into code (so Thing.GetPath<MserDuck>(...) for custom assets!)
    }
}
