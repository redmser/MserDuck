﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// A simple tabbed user control.
    /// </summary>
    public class DebugTabControl : DebugUIControl
    {
        public Vec2 Size;

        /// <summary>
        /// Where the tabs should be placed. Only supports UIAlign.Top and UIAlign.Bottom for now!
        /// </summary>
        public UIAlign Alignment;

        /// <summary>
        /// List of tab pages to display.
        /// </summary>
        public List<DebugTabPage> TabPages;

        /// <summary>
        /// Height of the tabs.
        /// </summary>
        public int TabHeight = 30;

        /// <summary>
        /// Currently active tab page.
        /// </summary>
        public DebugTabPage ActivePage
        { get { return this.TabPages[this.ActiveIndex]; } }

        /// <summary>
        /// Currently active tab page index.
        /// </summary>
        public int ActiveIndex = 0;

        /// <summary>
        /// Creates a simple tabbed user control.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="size"></param>
        /// <param name="alignment">Where the tabs should be placed. Only supports UIAlign.Top and UIAlign.Bottom for now!</param>
        /// <param name="tabs">List of tab pages to display.</param>
        public DebugTabControl(Vec2 position, Vec2 size, UIAlign alignment = UIAlign.Top, params DebugTabPage[] tabs)
        {
            this.position = position;
            this.Size = size;
            this.Alignment = alignment;
            if (tabs != null && tabs.Any())
                this.TabPages = tabs.ToList();
        }

        /// <summary>
        /// Called every tick to handle update logic. Return value can be used for telling if something happened in the update frame.
        /// </summary>
        /// <returns></returns>
        public override object DoUpdate()
        {
            //Update controls
            var pageoffset = this.position;
            foreach (var control in ActivePage.Controls)
            {
                var pos = control.position;
                control.position = control.position + pageoffset;
                //control.DoUpdate();
                control.position = pos;
            }

            return null;
        }

        /// <summary>
        /// Called every frame while the DebugUI and the user control is visible.
        /// </summary>
        public override void DoDraw()
        {
            if (!this.Visible)
                return;

            //Finally, draw contents
            var pageoffset = this.position;
            foreach (var control in ActivePage.Controls)
            {
                var pos = control.position;
                control.position = control.position + pageoffset;
                control.DoDraw();
                control.position = pos;
            }
        }
    }
}
