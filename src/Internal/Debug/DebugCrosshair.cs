﻿namespace DuckGame.MserDuck
{
    /// <summary>
    /// A crosshair placed onto things when using the DebugThingBox's eye button.
    /// </summary>
    public class DebugCrosshair : Thing
    {
        private Sprite sprite = new Sprite(Thing.GetPath<MserDuck>("snipe")) { center = new Vec2(32), depth = 0.99f };

        private SinWave _sine = new SinWave(0.025f);

        private Thing Thing;

        public DebugCrosshair(Thing th) : base(0, 0)
        {
            this.Thing = th;
        }

        public override void Draw()
        {
            if (this.sprite.alpha > 0)
            {
                var color = Color.Cyan.MergeWith(Color.Blue, _sine.normalized * 0.5f);
                Graphics.DrawRect(this.Thing.rectangle, color * this.sprite.alpha, 0.99f);
                this.sprite.angleDegrees += 5f;
                this.sprite.scale = new Vec2(_sine.normalized * 0.5f + 0.75f) * this.Thing.scale;
                this.sprite.color = color;
                this.sprite.Draw();
                this.sprite.alpha -= 0.01f;
            }
        }

        public override void Update()
        {
            this.sprite.position = this.Thing.position;
            if (this.sprite.alpha <= 0)
            {
                Level.Remove(this);
            }

            base.Update();
        }
    }
}
