﻿using System;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Values defining the geometry of a DebugBox (position, size, and whether it is folded).
    /// <para />Also includes the GetType().Name for identification and whether it is visible.
    /// </summary>
    [Serializable]
    public struct DebugBoxGeometry
    {
        /// <summary>
        /// Position of the DebugBox.
        /// </summary>
        public readonly Vec2 Position;

        /// <summary>
        /// Size of the DebugBox.
        /// </summary>
        public readonly Vec2 Size;

        /// <summary>
        /// Whether the DebugBox is folded.
        /// </summary>
        public readonly bool Folded;

        /// <summary>
        /// Type of the saved DebugBox, for identification. Use .GetType().Name for setting me.
        /// </summary>
        public readonly string BoxType;

        /// <summary>
        /// Whether the DebugBox is visible.
        /// </summary>
        public readonly bool Visible;

        /// <summary>
        /// Creates a new DebugBoxGeometry, used for serializing the DebugBox data.
        /// </summary>
        /// <param name="position">Position of the DebugBox.</param>
        /// <param name="size">Size of the DebugBox.</param>
        /// <param name="folded">Whether the DebugBox is folded.</param>
        /// <param name="visible">Whether the DebugBox is visible.</param>
        /// <param name="boxtype">Type of the saved DebugBox, for identification. Use .GetType().Name for setting me.</param>
        public DebugBoxGeometry(Vec2 position, Vec2 size, bool folded, bool visible, string boxtype)
        {
            this.Position = position;
            this.Size = size;
            this.Folded = folded;
            this.BoxType = boxtype;
            this.Visible = visible;
        }

        /// <summary>
        /// Indicates whether this instance and a specified object are equal.
        /// </summary>
        /// <returns>
        /// true if <paramref name="obj"/> and this instance are the same type and represent the same value; otherwise, false.
        /// </returns>
        /// <param name="obj">Another object to compare to. </param>
        public override bool Equals(object obj)
        {
            var geo = obj as DebugBoxGeometry?;
            if (geo == null)
                return false;

            return geo.Value.Position == this.Position && geo.Value.Size == this.Size && geo.Value.Folded == this.Folded &&
                geo.Value.Visible == this.Visible && geo.Value.BoxType == this.BoxType;
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that is the hash code for this instance.
        /// </returns>
        public override int GetHashCode()
        {
            return this.Position.GetHashCode() + this.Size.GetHashCode() + this.Folded.GetHashCode() + this.BoxType.GetHashCode() + this.Visible.GetHashCode();
        }

        /// <summary>
        /// Returns the fully qualified type name of this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> containing a fully qualified type name.
        /// </returns>
        public override string ToString()
        {
            return string.Format("{{{0}: pos {1};{2} size {3};{4}}}", this.BoxType, this.Position.x, this.Position.y, this.Size.x, this.Size.y);
        }
    }
}