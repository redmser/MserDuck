﻿namespace DuckGame.MserDuck
{
    public class DebugThingList : DebugBox
    {
        private DebugListBox _listBox = new DebugListBox(300f, default(Color), 0, -1);

        public DebugThingList(int index) : base(index)
        {
            this.InvisOnClose = true;
            this._visible = false;
            this.Title = "Thing List";
        }

        /// <summary>
        /// Returns whether any button was pressed
        /// </summary>
        /// <returns></returns>
        public override object DoUpdate()
        {
            if (!this.Folded)
            {
                var res = this._listBox.DoUpdate();
            }

            return base.DoUpdate();
        }

        public override void DoDraw()
        {
            if (!this.Folded)
            {
                this._listBox.DoDraw();
            }

            base.DoDraw();
        }

        //NYI: DebugThingList - List of things inside of the level, with sprite, location, click to open thingbox, button to crosshair/eye, etc. categorize by base class
        //  Allow to redirect a reference (e.g. heldobject of duck) by clicking a button in the SetValueBox, opening a ThingList browser showing only supported types in it
        //  or drag&drop. Also have a tab for a list of EVERY thing, similar to above, except clicking will spawn a new one at the place you click on next.
        //  May have this functionality in the planned level editor mode instead, not sure.
        //  Categorization by Level Editor Categories may be a thing??
    }
}
