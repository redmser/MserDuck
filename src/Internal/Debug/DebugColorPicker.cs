﻿namespace DuckGame.MserDuck
{
    public class DebugColorPicker : DebugBox
    {
        private DebugBoxButton _screenPick;

        public DebugColorPicker(int index) : base(index)
        {
            this.InvisOnClose = true;
            this._visible = false;
            this.Title = "Color Picker";

            this._screenPick = new DebugBoxButton(IconManager.ButtonIcons, 21, this.PickScreenColor, "Pick a color from the screen.");
            this._buttons.Add(this._screenPick);
        }

        private void PickScreenColor()
        {
            var map = IconManager.ButtonIcons.CloneMap();
            map.frame = 21;
            map.center = new Vec2(4, 20);
            map.depth = 1f;
            map.scale = new Vec2(2);
            DebugUI.Cursor = map;

            //NYI: DebugColorPicker - Hide the DebugUI windows (not like pressing F9!), show color preview (old and new) and click to pick
        }

        /// <summary>
        /// Called every frame while the DebugUI and the user control is visible.
        /// </summary>
        public override void DoDraw()
        {
            base.DoDraw();

            if (!Visible || Folded)
                return;
        }

        //NYI: DebugColorPicker - RGB HSVL sliders with live updating colors (see GIMP)
    }
}
