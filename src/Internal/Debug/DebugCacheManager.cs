﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck
{
    public class DebugCacheManager : DebugBox
    {
        private readonly DebugListBox _listBox;

        public DebugCacheManager(int index) : base(index)
        {
            this.InvisOnClose = true;
            this._visible = false;
            this.Title = "Cache Manager";
            this.Size = new Vec2(275, 225);
            this.MinimumSize = this.Size;
            this._listBox = new DebugListBox(0, default(Color), 0);
        }

        //NYI: DebugCacheManager - provides a list of caches, how big they are (entry count and size in memory)
        //  and options to clear selected or all, as well as viewing them or dumping them to a file.
        //  Should include snippets.dat and geometry.dat

        /// <summary>
        /// Returns whether any button was pressed
        /// </summary>
        /// <returns></returns>
        public override object DoUpdate()
        {
            if (!this.Visible)
                return false;

            //Updatey
            if (_dirty)
            {
                _dirty = false;
                this._listBox.Entries = _sizes.Select(s => new DebugListEntry(string.Format("{0}: {1}",
                    s.Key, Extensions.GetPlural(s.Value, "entry", "entries")), s.Key) {Tag = s.Value}).OrderByDescending(e => (int)e.Tag).ToList();
            }
            this._listBox.position = this.position + new Vec2(8, 32);
            this._listBox.Size = this.Size - new Vec2(16, 40);
            var clicked = this._listBox.DoUpdate();

            return (int)clicked != -1 || (bool)base.DoUpdate();
        }

        /// <summary>
        /// Called every frame while the DebugUI and the user control is visible.
        /// </summary>
        public override void DoDraw()
        {
            base.DoDraw();

            if (this.Folded || !this.Visible)
                return;

            this._listBox.DoDraw();
            FontManager.Chat.Draw(string.Format("Total: {0} entries", _sum), this.x + 8, this.y + this.Size.y - 16, Color.LightGray, 1);
        }

        private static bool _dirty = false;
        private static int _sum;
        private static Dictionary<string, int> _sizes; 

        public static void ReloadCacheSizes()
        {
            _sizes = CacheManager.CachedMonitoredFields().ToDictionary(f => f.Name, f => CacheManager.CountEntries(f));
            _sum = DebugCacheManager._sizes.Sum(s => s.Value);
            
            _dirty = true;
        }
    }
}
