﻿using System;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// A button in a debug box.
    /// </summary>
    public class DebugBoxButton : DebugUIControl
    {
        public Color BackColor = Color.White;

        public Sprite Icon;

        public SpriteMap IconMap;

        public int Frame;

        public Action OnClick;

        public DebugListEntry Entry;

        public Action DoDrawAction;

        /// <summary>
        /// Whether the background should be white, so it can be tinted by setting the backcolor.
        /// </summary>
        private bool _tintButton = false;

        /// <summary>
        /// Whether the background should be white, so it can be tinted by setting the backcolor.
        /// </summary>
        public bool TintButton
        {
            get { return this._tintButton; }
            set
            {
                if (this.TintButton == value) return;
                this._tintButton = value;

                _backSprite = value ? DebugBoxButton._buttonTintBG : DebugBoxButton._buttonBG;
            }
        }

        private Sprite _backSprite = _buttonBG;

        private static Sprite _buttonBG = new Sprite(Thing.GetPath<MserDuck>("button")) {depth = 0.93f};
        private static Sprite _buttonTintBG = new Sprite(Thing.GetPath<MserDuck>("buttonTint")) {depth = 0.93f};

        /// <summary>
        /// If set, hovering shows a tooltip text.
        /// </summary>
        public string TooltipText;

        private Vec2 _size
        {
            get
            {
                if (this.Icon == null)
                {
                    if (this.IconMap == null)
                        return new Vec2(24, 24);
                    var clone = this.IconMap.Clone();
                    return new Vec2(clone.width * this.IconMap.xscale, clone.height * this.IconMap.yscale);
                }
                return new Vec2(this.Icon.width * this.Icon.xscale, this.Icon.height * this.Icon.yscale);
            }
        }

        /// <summary>
        /// If set, positions the tooltip here instead of at the mouse (relative to the button position!).
        /// </summary>
        public Vec2 TooltipPosition;

        /// <summary>
        /// Creates a new button with a specific Sprite.
        /// </summary>
        /// <param name="icon"></param>
        /// <param name="onClick"></param>
        /// <param name="tooltip"></param>
        /// <param name="tint">Whether the background should be white, so it can be tinted by setting the backcolor.</param>
        public DebugBoxButton(Sprite icon, Action onClick, string tooltip = null, bool tint = false)
        {
            this.position = new Vec2(-32);
            this.Icon = icon;
            this.OnClick = onClick;
            this.TooltipText = tooltip;
            this.TintButton = tint;

            if (this.Icon != null)
                this.Icon.depth = 0.97f;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iconmap"></param>
        /// <param name="frame"></param>
        /// <param name="onClick"></param>
        /// <param name="tooltip"></param>
        /// <param name="tint">Whether the background should be white, so it can be tinted by setting the backcolor.</param>
        public DebugBoxButton(SpriteMap iconmap, int frame, Action onClick, string tooltip = null, bool tint = false)
        {
            this.position = new Vec2(-32);
            this.IconMap = iconmap;
            this.Frame = frame;
            this.OnClick = onClick;
            this.TooltipText = tooltip;
            this.TintButton = tint;

            if (this.IconMap != null)
                this.IconMap.depth = 0.97f;
        }

        /// <summary>
        /// Returns whether button was pressed.
        /// </summary>
        /// <returns></returns>
        public override object DoUpdate()
        {
            if (!this.Visible)
                return false;

            if (DebugUI._mousePos.IsInRange(this.position, this.position + this._size))
            {
                if (!string.IsNullOrWhiteSpace(this.TooltipText))
                {
                    //Show tooltip!
                    if (this.TooltipPosition == default(Vec2))
                    {
                        DebugUI.MouseToolTip(this.TooltipText, 0f);
                    }
                    else
                    {
                        DebugUI.ShowToolTip(this.TooltipText, this.position + this.TooltipPosition, 0f);
                    }
                }

                if (Mouse.left == InputState.Pressed)
                {
                    //CLICKED
                    if (this.OnClick != null)
                        this.OnClick.Invoke();
                    SFX.Play("tinyTick");
                    return true;
                }
            }
            return false;
        }

        public override void DoDraw()
        {
            if (this.x < -24 || this.y < -24)
                return;

            //TODO: DebugBoxButton icons to be centered?
            if (this.Icon != null)
            {
                this.Icon.position = this.position;
            }
            if (this.IconMap != null)
            {
                this.IconMap.position = this.position;
            }

            //Graphics.DrawRect(this.position, this.position + this._size, Color.DarkGray, 0.98f);
            _backSprite.position = this.position;
            _backSprite.scale = this._size / 24f;
            _backSprite.color = this.BackColor;
            _backSprite.Draw();

            if (this.Icon != null)
            {
                this.Icon.Draw();
            }
            else if (this.IconMap != null)
            {
                this.IconMap.frame = this.Frame;
                this.IconMap.Draw();
            }

            if (this.DoDrawAction != null)
                this.DoDrawAction();
        }
    }
}
