﻿using System.Collections.Generic;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// A tab page containing user controls for a DebugTabControl.
    /// </summary>
    public class DebugTabPage
    {
        /// <summary>
        /// The list of user controls to display inside of the DebugTabControl's page.
        /// </summary>
        public IEnumerable<DebugUIControl> Controls;

        /// <summary>
        /// The title for labelling the tab page.
        /// </summary>
        public string Title;

        /// <summary>
        /// Creates a tab page containing user controls for a DebugTabControl.
        /// </summary>
        /// <param name="title">The title for labelling the tab page.</param>
        /// <param name="controls">The list of user controls to display inside of the DebugTabControl's page.</param>
        public DebugTabPage(string title, params DebugUIControl[] controls)
        {
            this.Title = title;
            this.Controls = controls;
        }
    }
}
