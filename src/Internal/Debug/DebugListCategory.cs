using System;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// A class representing a category to be shown in a DebugCategorizedListBox.
    /// </summary>
    public class DebugListCategory : IEquatable<DebugListCategory>
    {
        /// <summary>
        /// Display name of the category. Entries are sorted into them based on this.
        /// </summary>
        public readonly string Name;

        /// <summary>
        /// Priority of the category. Higher means it will come earlier when sorting, lower means later.
        /// </summary>
        public int Priority;

        /// <summary>
        /// Whether the category is currently showing its children.
        /// </summary>
        public bool ShowingChildren = true;

        /// <summary>
        /// Whether the user can expand or collapse the category by clicking onto it.
        /// </summary>
        public bool ClickToCollapse = true;

        /// <summary>
        /// Corresponding list entry.
        /// </summary>
        public DebugListEntry ListEntry;

        /// <summary>
        /// Creates a new category with the specified name and the specified priority.
        /// </summary>
        /// <param name="name">Display name of the category. Entries are sorted into them based on this.</param>
        /// <param name="priority">Priority of the category. Higher means it will come earlier when sorting, lower means later.</param>
        public DebugListCategory(string name, int priority = 0)
        {
            this.Name = name;
            this.Priority = priority;
        }

        /// <summary>
        /// Creates a new category with the specified name and default priority.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static implicit operator DebugListCategory(string name)
        {
            return new DebugListCategory(name);
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        /// <param name="other">An object to compare with this object.</param>
        public bool Equals(DebugListCategory other)
        {
            if ((object)other == null)
                return false;

            return this.Name == other.Name;
        }

        public override bool Equals(object obj)
        {
            var item = obj as DebugListCategory;

            if ((object)item == null)
            {
                return false;
            }

            return this.Equals(item);
        }

        public static bool operator ==(DebugListCategory obj1, DebugListCategory obj2)
        {
            if (ReferenceEquals(obj1, obj2))
                return true;

            if (((object)obj1 == null) || ((object)obj2 == null))
                return false;

            return obj1.Name == obj2.Name;
        }

        public static bool operator !=(DebugListCategory obj1, DebugListCategory obj2)
        {
            return !(obj1 == obj2);
        }

        /// <summary>
        /// Serves as a hash function for a particular type. 
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}