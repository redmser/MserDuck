﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace DuckGame.MserDuck
{
    //NYI: DebugUI - allow live code editing from duck game without relaunch
    //  Have a debug box which allows entering code (hopefully with some form of syntax highlight and auto completes from reflection!)
    //  which will be executed live once it doesn't have errors by using /execute's compilation and a method in the place in the code (hardcoded)
    //  that decides what should be executed. For example DebugUI.ExecuteLive("code"); would then run the live-compiled snippet "code".
    //  How to handle for example "this" and calling base methods will be another challenge however.

    //NYI: DebugUI - more user controls
    //  May require a slider or up/down control for the profiler to set the avg time span.
    //  Most likely also need a reimplemented SplitContainer (resizable splitter for changing position of stuff inside, like for the profiler)

    //NYI: DebugUI - network debugger, similar to profiler, checking how much in/out/lag stuff etc!
    //  Some of this can be found in the crash log segment, so check what else you can get! NetMessages are already gotten from another kind of network debugger

    //NYI: DebugUI - apply the permissions system to DebugUI - only allow to use non-destructive functions if no permission for it
    //  ALSO include a permissions editor somewhere (either in DebugUI or in MserDuck Options) to easily and visually edit the permissions (look at discord sorta)

    /// <summary>
    /// Press F9 to toggle. Debugging UI in-game for easy member access and logging.
    /// </summary>
    public static class DebugUI
    {
        /// <summary>
        /// File containing the geometry data for all open windows.
        /// </summary>
        public const string GeometryFile = "geometry.dat";

        /// <summary>
        /// Some sure dark color!
        /// </summary>
        public static Color DarkColor
        {
            get { return new Color(14, 15, 17); }
        }

        /// <summary>
        /// Surely a neat color?
        /// </summary>
        public static Color SpringColor
        {
            get { return new Color(62, 158, 110); }
        }

        /// <summary>
        /// Whether the debug UI is enabled.
        /// </summary>
        private static bool _enabled;

        /// <summary>
        /// Key to toggle the debug UI. Set using <see cref="ConfigEntries.DebugUIEntry"/> in config.
        /// </summary>
        public static Keys ToggleKey = Keys.F9;

        /// <summary>
        /// Rate at which any live-updating values should be refreshed.
        /// </summary>
        private const int UpdateRate = 15;

        /// <summary>
        /// Current timer tick of update.
        /// </summary>
        private static int _updateTick;

        /// <summary>
        /// Sprite of the default mouse cursor.
        /// </summary>
        private static readonly SpriteMap _defaultCursor = new SpriteMap("cursors", 16, 16) {depth = 1f, scale = new Vec2(2) };

        /// <summary>
        /// Resize cursor sprites.
        /// </summary>
        private static readonly SpriteMap _resizeCursor = new SpriteMap(Thing.GetPath<MserDuck>("resize"), 32, 32) { depth = 1f };

        /// <summary>
        /// Currently shown mouse cursor.
        /// <para />Set to null to use default.
        /// </summary>
        public static SpriteMap Cursor
        {
            get
            {
                if (DebugUI._cursor == null)
                {
                    switch (DebugUI._resizeHandle)
                    {
                        case ResizeMode.None:
                            return DebugUI._defaultCursor;
                        case ResizeMode.TopLeft:
                            DebugUI._resizeCursor.angleDegrees = 0f;
                            DebugUI._resizeCursor.frame = 0;
                            DebugUI._resizeCursor.center = Vec2.Zero;
                            break;
                        case ResizeMode.Top:
                            DebugUI._resizeCursor.angleDegrees = 0f;
                            DebugUI._resizeCursor.frame = 1;
                            DebugUI._resizeCursor.center = new Vec2(16, 0);
                            break;
                        case ResizeMode.TopRight:
                            DebugUI._resizeCursor.angleDegrees = 90f;
                            DebugUI._resizeCursor.frame = 0;
                            DebugUI._resizeCursor.center = Vec2.Zero;
                            break;
                        case ResizeMode.Right:
                            DebugUI._resizeCursor.angleDegrees = 90f;
                            DebugUI._resizeCursor.frame = 1;
                            DebugUI._resizeCursor.center = new Vec2(16, 0);
                            break;
                        case ResizeMode.BottomRight:
                            DebugUI._resizeCursor.angleDegrees = 180f;
                            DebugUI._resizeCursor.frame = 0;
                            DebugUI._resizeCursor.center = Vec2.Zero;
                            break;
                        case ResizeMode.Bottom:
                            DebugUI._resizeCursor.angleDegrees = 180f;
                            DebugUI._resizeCursor.frame = 1;
                            DebugUI._resizeCursor.center = new Vec2(16, 0);
                            break;
                        case ResizeMode.BottomLeft:
                            DebugUI._resizeCursor.angleDegrees = 270f;
                            DebugUI._resizeCursor.frame = 0;
                            DebugUI._resizeCursor.center = Vec2.Zero;
                            break;
                        case ResizeMode.Left:
                            DebugUI._resizeCursor.angleDegrees = 270f;
                            DebugUI._resizeCursor.frame = 1;
                            DebugUI._resizeCursor.center = new Vec2(16, 0);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    //End of switch
                    return DebugUI._resizeCursor;
                }
                //Custom cursor
                return DebugUI._cursor;
            }
            set { _cursor = value; }
        }

        private static SpriteMap _cursor;

        /// <summary>
        /// Sprite of the crosshair - appears when rightclicking.
        /// </summary>
        private static readonly Sprite _crosshair = new Sprite("challenge/reticule") {depth = 0.38f};

        /// <summary>
        /// List of things the mouse is hovering over.
        /// </summary>
        private static List<Thing> _hoverThings;

        /// <summary>
        /// List of saved things the mouse was hovering over.
        /// </summary>
        private static List<Thing> _savedThings;

        /// <summary>
        /// Position of save.
        /// </summary>
        private static Vec2 _savedPos;

        /// <summary>
        /// BottomRight of save.
        /// </summary>
        private static Vec2 _endPos;

        /// <summary>
        /// Position at which the mouse is rendered at.
        /// </summary>
        public static Vec2 _mousePos;

        /// <summary>
        /// Index of which saved entry is hovered.
        /// </summary>
        private static int _hoveredSave = -1;

        /// <summary>
        /// Current resize mode.
        /// </summary>
        public static ResizeMode _resizeHandle = ResizeMode.None;

        /// <summary>
        /// Whether the DebugUI is opened for the first time.
        /// </summary>
        private static bool _firstLoad = true;

        /// <summary>
        /// List of debug boxes. Fill this list's constructor with any windows you want to be open by default, or ones that can't be fully closed.
        /// </summary>
        public static readonly List<DebugBox> _debugBoxes = new List<DebugBox>
        {
            new DebugPrintBox(0),
            new DebugAssetExplorer(1),
            new DebugThingList(2),
            new DebugErrorLog(3),
            new DebugProfilerBox(4),
            new DebugCacheManager(5),
            new DebugHelpBox(6),
            new DebugColorPicker(7),
            new DebugToolbar()
        }; 

        public static void Draw()
        {
            if (!DebugUI._enabled)
                return;

            var region = MserDebug.StartProfiling("DebugUIDraw");

            if (Mouse.available)
            {
                //Draw cursor at mouse position, if enabled
                Cursor.Draw();
            }

            //Show useful info at mouse
            DebugUI.ShadowedText(string.Format("{0};{1} ({2};{3})", DebugUI._mousePos.x, DebugUI._mousePos.y, Mouse.x, Mouse.y), DebugUI._mousePos.x + 16, DebugUI._mousePos.y, 0.1f, Color.PaleTurquoise);

            var thinglist = DebugUI._savedThings == null ? _hoverThings : DebugUI._savedThings;
            var drawpos = DebugUI._savedThings == null ? DebugUI._mousePos + new Vec2(4, 20) : DebugUI._savedPos;

            //Needs permission to show hoverinfo
            if (thinglist != null && PermissionManager.HasPermission("DebugModeHoverInfo"))
            {
                //Show list of things + info
                var textlist = thinglist.Select(t => t.GetType().Name).ToList();
                textlist.Insert(0, Extensions.GetPlural(thinglist.Count, "thing") + ":");

                //Render the text
                for (var i = 0; i < textlist.Count; i++)
                {
                    var color = i == 0 ? Color.Gainsboro : (i == DebugUI._hoveredSave + 1 ? Color.Cyan : Color.White);
                    DebugUI.ShadowedText(textlist[i], drawpos.x, drawpos.y + FontManager.Chat.GetHeight() * i, 0.4f, color);
                }

                var text = string.Join("\n", textlist);

                //Background opacity depending on le petit ye
                _endPos = new Vec2(drawpos.x + FontManager.Chat.GetWidth(text) + 4, drawpos.y + FontManager.Chat.GetHeight(text));
                Graphics.DrawRect(drawpos, _endPos, DarkColor * (DebugUI._savedThings == null ? 0.3f : 0.7f), 0.3f);

                if (DebugUI._savedThings == null && thinglist.Any())
                {
                    //Neat separator
                    Graphics.DrawRect(new Vec2(drawpos.x + 12, drawpos.y + 19), new Vec2(drawpos.x + FontManager.Chat.GetWidth(text) + 4, drawpos.y + 22), Color.White);
                }
                else
                {
                    //Neat crosshair
                    _crosshair.position = _savedPos - new Vec2(8);
                    _crosshair.Draw();
                }
            }
            MserDebug.StepProfiling(region, "ThingList");

            if (_tooltipText != null)
            {
                //Render tooltiptext
                var orig = DebugUI._mousePos - new Vec2(0, 32);
                if (DebugUI._tooltipPosition != default(Vec2))
                    orig = DebugUI._tooltipPosition;
                var w = new Vec2(FontManager.Chat.GetWidth(_tooltipText), FontManager.Chat.GetHeight(DebugUI._tooltipText));
                var p2 = orig + w;
                var p1 = orig;
                if (p2.x + 4 >= Layer.Console.width)
                {
                    p1.x = Layer.Console.width - w.x - 4;
                    p2.x = Layer.Console.width - 4;
                }
                else if (p1.x < 4)
                {
                    p1.x = 4;
                    p2.x = p1.x + w.x;
                }
                if (p2.y + 4 >= Layer.Console.height)
                {
                    p1.y = Layer.Console.height - 36;
                    p2.y = Layer.Console.height - 4;
                }
                else if (p1.y < 4)
                {
                    p1.y = 4;
                    p2.y = 36;
                }
                DebugUI.ShadowedText(_tooltipText, p1.x, p1.y, 0.995f, SpringColor * DebugUI._tooltipAlpha);
                Graphics.DrawRect(p1, p2, DarkColor * (DebugUI._tooltipAlpha - 0.5f), 0.985f);
            }
            MserDebug.StepProfiling(region, "Tooltip");

            //BOXES
            DebugBox profiler = null;
            foreach (var box in DebugUI._debugBoxes)
            {
                //Should be drawn last, to avoid early profiling data display
                if (box is DebugProfilerBox)
                {
                    profiler = box;
                    continue;
                }

                box.DoDraw();
                MserDebug.StepProfiling(region, "DebugBox " + box.Title);
            }

            MserDebug.EndProfiling(region);

            //Draw profiler NOW
            if (profiler != null)
                profiler.DoDraw();
        }

        public static void Update()
        {
            //Check for permission
            if (!PermissionManager.HasPermission("DebugModeOpen"))
            {
                //Disable if needed
                DebugUI._enabled = false;
                return;
            }

            //Toggle UI
            if (Keyboard.Pressed(DebugUI.ToggleKey))
            {
                if (!DebugUI._enabled)
                {
                    //Open sound
                    SFX.Play("hitBox");
                }
                DebugUI._enabled = !DebugUI._enabled;

                //First load
                if (_firstLoad)
                {
                    _firstLoad = false;
                    FirstLoad();
                }
            }

            if (!DebugUI._enabled)
                return;

            var region = MserDebug.StartProfiling("DebugUIUpdate");

            //Update coords
            DebugUI._mousePos = Mouse.available ? Mouse.positionConsole : Vec2.Zero;
            Cursor.position = _mousePos;

            //Hover info
            _hoverThings = Level.CheckPointAll<Thing>(Mouse.xScreen, Mouse.yScreen).ToList();

            //THING BOXES INFOS
            DebugUI._updateTick++;
            if (DebugUI._updateTick >= DebugUI.UpdateRate)
            {
                DebugUI._updateTick = 0;
                foreach (var box in DebugUI._debugBoxes.OfType<DebugThingBox>())
                {
                    box.ReloadInfo();
                }
                if (DebugUI._debugBoxes.OfType<DebugCacheManager>().Any(b => b.Visible))
                    DebugCacheManager.ReloadCacheSizes();
            }
            MserDebug.StepProfiling(region, "ThingBoxInfo");

            //ANY BOXES
            var anyPressed = false;
            foreach (var box in DebugUI._debugBoxes.Reverse<DebugBox>())
            {
                var res = (bool)box.DoUpdate();
                if (res)
                    anyPressed = true;
                MserDebug.StepProfiling(region, "DebugBox " + box.Title);
            }

            if (!anyPressed && _hoverThings != null && _hoverThings.Any() && DebugUI._savedThings == null && Mouse.right == InputState.Pressed && PermissionManager.HasPermission("DebugModeHoverInfo"))
            {
                //Hovering over items, no debugbox buttons were pressed, no menu currently open, and rightclicked
                _savedThings = _hoverThings;
                _savedPos = _mousePos;
                SFX.Play("openClick");
            }
            else if (DebugUI._savedThings != null)
            {
                //Saved menu is open
                if (DebugUI._mousePos.x < DebugUI._savedPos.x || DebugUI._mousePos.y < DebugUI._savedPos.y ||
                    DebugUI._mousePos.x > DebugUI._endPos.x   || DebugUI._mousePos.y > DebugUI._endPos.y)
                {
                    //Clicked outside
                    if (Mouse.left == InputState.Pressed)
                    {
                        //LMB, close menu
                        DebugUI._savedThings = null;
                        DebugUI._savedPos = Vec2.Zero;
                    }
                    else if (Mouse.right == InputState.Pressed)
                    {
                        //RMB, open new menu
                        _savedThings = _hoverThings;
                        _savedPos = _mousePos;
                        SFX.Play("openClick");
                    }

                    //Reset selected entry
                    DebugUI._hoveredSave = -1;
                }
                else
                {
                    //Mouse is inside, change selection
                    if (DebugUI._mousePos.y > DebugUI._savedPos.y + 22)
                    {
                        var num = (DebugUI._mousePos.y - DebugUI._savedPos.y) / FontManager.Chat.GetHeight() - 1;
                        DebugUI._hoveredSave = (int)Maths.Clamp(num, 0, DebugUI._savedThings.Count - 1);
                    }

                    //Clicked inside
                    if ((Mouse.left == InputState.Pressed || Mouse.right == InputState.Pressed) && DebugUI._hoveredSave > -1)
                    {
                        //Add the thing's box if menu entry is clicked
                        AddDebugBox(new DebugThingBox(DebugUI._savedThings[DebugUI._hoveredSave], DebugUI._debugBoxes.Count));

                        DebugUI._savedThings = null;
                        DebugUI._savedPos = Vec2.Zero;
                        DebugUI._hoveredSave = -1;
                    }
                }
            }
            MserDebug.StepProfiling(region, "ThingList");

            //FREEZE CAM
            if (Mouse.middle == InputState.Pressed && DebugUI._levelCam == null)
            {
                //set cam
                var camera = Level.current.camera;
                _levelCam = new Tuple<Level, Camera>(Level.current, camera);

                Level.current.camera = new Camera(camera.x, camera.y, camera.width, camera.height);
            }
            else if (Mouse.middle == InputState.Pressed && DebugUI._levelCam != null)
            {
                //reset cam
                if (Level.current == DebugUI._levelCam.Item1)
                {
                    Level.current.camera = DebugUI._levelCam.Item2;
                }
                DebugUI._levelCam = null;
            }

            //TOOLTIP
            if (_tooltipText != null)
            {
                DebugUI._tooltipTime -= Maths.IncFrameTimer();
                if (DebugUI._tooltipTime <= 0)
                {
                    DebugUI._tooltipAlpha -= 0.1f;
                    if (DebugUI._tooltipAlpha <= 0)
                    {
                        DebugUI._tooltipText = null;
                    }
                }
            }
            MserDebug.EndProfiling(region);
        }

        private static Tuple<Level, Camera> _levelCam;

        /// <summary>
        /// Run on first load of this DebugUI session.
        /// </summary>
        private static void FirstLoad()
        {
            LoadGeometry();
        }

        /// <summary>
        /// Loads the geometry of all open DebugBoxes.
        /// </summary>
        public static void LoadGeometry()
        {
            if (!File.Exists(DebugUI.GeometryFile) || Extensions.FileEmpty(DebugUI.GeometryFile))
                return;

            List<DebugBoxGeometry> geolist;
            using (Stream stream = File.Open(DebugUI.GeometryFile, FileMode.Open, FileAccess.Read))
            {
                var bformatter = new BinaryFormatter();
                geolist = (List<DebugBoxGeometry>)bformatter.Deserialize(stream);
            }

            //Program.LogLine("Loading geometry: " + Extensions.ToNeatString(geolist));

            foreach (var box in DebugUI._debugBoxes.Where(b => b.StoreGeometry))
            {
                var geo = geolist.FirstOrDefault(g => g.BoxType == box.GetType().Name);
                if (!geo.Equals(default(DebugBoxGeometry)))
                {
                    box.ApplyGeometry(geo);
                }
            }
        }

        /// <summary>
        /// Saves the geometry of all open DebugBoxes.
        /// </summary>
        public static void SaveGeometry()
        {
            var geolist = DebugUI._debugBoxes.Where(b => b.StoreGeometry).Select(b => b.GetGeometry()).ToList();

            using (Stream stream = File.Open(DebugUI.GeometryFile, FileMode.Create, FileAccess.Write))
            {
                var bformatter = new BinaryFormatter();
                bformatter.Serialize(stream, geolist);
            }

            //Program.LogLine("Saving geometry: " + Extensions.ToNeatString(geolist));
        }

        /// <summary>
        /// Adds the specified debug box to the debug UI.
        /// </summary>
        /// <param name="box"></param>
        public static void AddDebugBox(DebugBox box)
        {
            _debugBoxes.Add(box);

            if (DebugUI._enabled)
                SFX.Play("consoleSelect");
        }

        /// <summary>
        /// Removes the specified debug box from the debug UI. Unchecked for contains or null.
        /// </summary>
        /// <param name="box"></param>
        public static void RemoveDebugBox(DebugBox box)
        {
            _debugBoxes.Remove(box);

            if (DebugUI._enabled)
                SFX.Play("consoleCancel");
        }

        /// <summary>
        /// Renders text with a shadow effect.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="depth"></param>
        /// <param name="color">Default: <code>Color.White</code></param>
        public static void ShadowedText(string text, float x, float y, Depth depth, Color color = default(Color))
        {
            if (color == default(Color))
                color = Color.White;

            FontManager.Chat.Draw(text, x + 2, y + 2, Color.Black * 0.8f, depth.value - 0.015f);
            FontManager.Chat.Draw(text, x, y, color, depth);
        }

        private static string _tooltipText = null;
        private static float _tooltipTime = 0f;
        private static float _tooltipAlpha = 1f;
        private static Vec2 _tooltipPosition;

        /// <summary>
        /// Shows a text message that follows the mouse.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="time"></param>
        public static void MouseToolTip(string text, float time = 5f)
        {
            ShowToolTip(text, default(Vec2), time);
        }

        /// <summary>
        /// Shows a tooltip message at the specified position.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="pos"></param>
        /// <param name="time"></param>
        public static void ShowToolTip(string text, Vec2 pos, float time = 5f)
        {
            _tooltipText = text;
            _tooltipTime = time;
            _tooltipAlpha = 1f;
            _tooltipPosition = pos;
        }
    }

    /// <summary>
    /// Describes the current resize cursor to be used, or how the debugboxes are to be resized.
    /// </summary>
    public enum ResizeMode
    {
        None, TopLeft, Top, TopRight, Right, BottomRight, Bottom, BottomLeft, Left
    }
}
