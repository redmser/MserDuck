﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace DuckGame.MserDuck
{
    public class DebugProfilerBox : DebugBox
    {

        /// <summary>
        /// Time in ms when bar color should be red due to high execution time.
        /// </summary>
        private const int RedAmount = 8;

        private int BarWidth
        {
            get { return (int)this.Size.x - LeftColWidth - LabelWidth - BarMargin; }
        }
        private const int LeftColWidth = 210;

        /// <summary>
        /// Conversion factor from 100 nanoseconds (timer ticks) to milliseconds.
        /// </summary>
        private const int TicksToMillisecond = 10000;
        private const int LabelWidth = 200;
        private const int BarMargin = 150;

        private DebugListBox _methodList;

        private DebugListEntry _selectedMethod;

        private DebugScrollBar _scrollGraph;

        private bool _hasCutoff;

        private DebugBoxButton _profilingButton;

        private MserDebug.ProfilingRegion SelectedRegion
        {
            get
            {
                if (this._selectedMethod == null)
                    return null;
                return MserDebug.ProfilingData[this._selectedMethod.Name];
            }
        }

        public DebugProfilerBox(int index) : base(index)
        {
            //TODO: Profiler - button to write data to a log file, for easier managing (sort data by exec time)
            //TODO: Profiler - May need to inject into major methods to see if they cause any slowdowns.
            this.InvisOnClose = true;
            this._visible = false;
            this.MinimumSize = new Vec2(DebugProfilerBox.LeftColWidth + DebugProfilerBox.LabelWidth + DebugProfilerBox.BarMargin, 400);
            this.Size = this.MinimumSize + new Vec2(200, 125);
            this.Title = "Profiler";

            this._methodList = new DebugListBox(LeftColWidth, Color.Transparent, 0);
            this._methodList.Entries = MserDebug.ProfilingData.Select(d => this.NewListEntryFromName(d.Key)).ToList();
            this._scrollGraph = new DebugScrollBar
            {
                Direction = ScrollOrientation.VerticalScroll,
                RatioOffset = -2
            };
            this._profilingButton = new DebugBoxButton(IconManager.ButtonIcons, 0, this.ToggleProfiling,
                "Enables or disables profiling.\nUse \"/savecfg\" to save this setting.", true);
            this._profilingButton.TooltipPosition = new Vec2(0, -40);
            this.SetProfiling(MserDebug.Profiling);
        }

        private DebugListEntry NewListEntryFromName(string name)
        {
            return new DebugListEntry(name, name, null, 0,
                   new DebugBoxButton(IconManager.ButtonIcons, 16, () => this.ToggleMethod(name),
                       "Enables or disables profiling for this method."));
        }

        private void ToggleMethod(string name)
        {
            var entry = this._methodList.GetEntryByName(name);
            if (entry == null)
                return;

            var data = MserDebug.ProfilingData[name];
            data.Updating = !data.Updating;
            entry.Buttons.Single().Frame = data.Updating ? 16 : 17;
        }

        private void ToggleProfiling()
        {
            MserDebug.Profiling = !MserDebug.Profiling;
            SetProfiling(MserDebug.Profiling);
        }

        private void SetProfiling(bool profiling)
        {
            this._profilingButton.Frame = profiling ? 14 : 9;
            this._profilingButton.BackColor = profiling ? Color.ForestGreen : Color.Firebrick;
        }

        /// <summary>
        /// Called every frame while the DebugUI and the user control is visible.
        /// </summary>
        public override void DoDraw()
        {
            if (!this.Visible)
                return;

            base.DoDraw();

            if (this.Folded)
                return;

            this._profilingButton.DoDraw();

            if (!MserDebug.Profiling)
            {
                //Not profiling warning
                FontManager.Chat.Draw("Profiling is disabled!", this.x + 32, this.y + this.Size.y - 20, Color.Red, 0.9f);
            }
            else
            {
                //Some info there
                FontManager.Chat.Draw(string.Format("Profiling {0}.", Extensions.GetPlural(this._methodList.Entries.Count, "method"))
                    , this.x + 32, this.y + this.Size.y - 20, Color.LightSeaGreen, 0.9f);
            }

            //Generic user controls - including labels
            FontManager.Chat.Draw("Method:", this.x + 12, this.y + 30, Color.LightGray, 0.9f);
            FontManager.Chat.Draw(string.Format("Performance{0}:", this._selectedMethod == null ? string.Empty : string.Format(" ({0})", this._selectedMethod.Text)),
                this.x + 20 + LeftColWidth, this.y + 30, Color.LightGray, 0.9f);
            this._methodList.DoDraw();

            if (MserDebug.Profiling && this._selectedMethod != null)
            {
                //Draw perf graph
                this.DrawGraph();
            }
            else
            {
                //Notify about no selection
                FontManager.Chat.Draw("No method selected for profiling.", this.x + 12 + LeftColWidth, this.y + 48, Color.DarkGray, 0.9f);
            }
        }

        /// <summary>
        /// Returns whether any button was pressed
        /// </summary>
        /// <returns></returns>
        public override object DoUpdate()
        {
            //Update list coords
            this._methodList.position = this.position + new Vec2(2, 50);
            this._methodList.Size = new Vec2(LeftColWidth, this.Size.y - 62);

            //Update list entries
            foreach (var entry in this._methodList.Entries)
            {
                if (entry.Color == Color.Cyan)
                    continue;

                var time = (float)MserDebug.ProfilingData[entry.Name].TotalTime / TicksToMillisecond;
                entry.Color = GetExecTimeColor(time, true);
                entry.Text = string.Format("{0} - {1:N2}ms", entry.Name, time);
            }

            //Update selection
            var clicked = (int)this._methodList.DoUpdate();
            if (clicked > -1)
            {
                if (this._selectedMethod != null)
                {
                    //Deselect old
                    this._selectedMethod.Color = Color.White;
                }

                this._selectedMethod = this._methodList.Entries[clicked];
                this._selectedMethod.Color = Color.Cyan;

                //Update scrollbar pos
                this._scrollGraph.Value = 0;
            }

            var scrollup = false;
            if (this._selectedMethod != null && this._hasCutoff)
            {
                //Update scrollbar
                this._scrollGraph.MaximumValue = this.SelectedRegion.Events.Count;
                if (!this._scrollGraph.ScrollerFilled)
                {
                    this._scrollGraph.position = new Vec2(this.x + this.Size.x - DebugScrollBar.DefaultSize, this.y + 32);
                    this._scrollGraph.Size = new Vec2(DebugScrollBar.DefaultSize, this.Size.y - 60);
                    scrollup = (bool)this._scrollGraph.DoUpdate();
                }
            }

            this._profilingButton.position = new Vec2(this.x + 2, this.y + this.Size.y - 26);
            var button = (bool)this._profilingButton.DoUpdate();

            //Do not resize when scrolling
            if (scrollup)
                return true;

            var baseup = (bool)base.DoUpdate();
            return clicked != -1 || baseup || button;
        }

        private void DrawGraph()
        {
            var startx = LeftColWidth + this.x;

            //Step labels and bars
            this._hasCutoff = false;
            var pdat = this.SelectedRegion;
            var ycurr = 50;
            var max = this.Size.y - 50;
            var maxamt = 0f;
            if (pdat.Events.Any())
                maxamt = pdat.Events.Max(e => e.Time);
            var maxtime = maxamt / TicksToMillisecond;
            foreach (var step in pdat.Events)
            {
                //Colors
                var time = (float)step.Time / TicksToMillisecond;
                var barcol = GetExecTimeColor(time, false);
                var textcol = GetExecTimeColor(time, true);

                //Step Name
                FontManager.Chat.Draw(FontManager.Chat.FitTextToWidth(step.Name, LabelWidth - 5, false), startx + 12, this.y + ycurr + 6, textcol, 0.95f);

                //TODO: Profiler - option to disable proportional bars (wheras bars would instead be like 1ms = 5px or so) ;
                //  or option to have it proportional to RedAmount or higher

                //TODO: Profiler - highlight peaks, keep them up for a bit to see spikes (needs to save function name too, so it follows in case it moves, but if it
                //  stops existing, keep it at same place!!

                //TODO: Profiler - make graph list scrollable! (implement scrollbar logic, similar to listbox, PLUS scrolling if mouse is on the entries using the UpdateScroll method of the scrollbar)
                
                //TODO: Profiler - Display more info, so you got your usual ms | frames (with 60fps) | calls in timespan | avg execution time in timespan...
                //  Have a slider or up/down control to modify timespan live.

                //Bar
                var barstart = new Vec2(startx + LabelWidth + 10, this.y + ycurr);
                var barsize = new Vec2(Math.Max(1, time / maxtime * (this.BarWidth - 10)), 24);
                Graphics.DrawRect(barstart, barstart + barsize, barcol, 0.9f);

                //Step Time
                FontManager.Chat.Draw(time.ToString("N3") + "ms", startx + LabelWidth + barsize.x + 20, this.y + ycurr + 6, Color.Gray, 0.95f);

                ycurr += 32;

                //Simply cut off for now
                if (ycurr >= max)
                {
                    this._hasCutoff = true;
                    break;
                }

                //TODO: Profiler - display sum of ms at bottom
            }

            //Draw the scrollbar
            if (this._hasCutoff && !this._scrollGraph.ScrollerFilled)
                this._scrollGraph.DoDraw();
        }

        public void UpdateList(string name)
        {
            //Add new list entry
            this._methodList.Entries.Add(this.NewListEntryFromName(name));

            //Sort list
            this._methodList.Entries.Sort(DebugListEntry.NameComparer);
        }

        private static Color GetExecTimeColor(float time, bool isText)
        {
            return isText ? Color.CornflowerBlue.MergeWith(Color.Salmon, time / DebugProfilerBox.RedAmount) 
                          : Color.DodgerBlue.MergeWith(Color.Crimson,    time / DebugProfilerBox.RedAmount);
        }
    }
}
