﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// A scrollable listbox for debug windows.
    /// </summary>
    public class DebugListBox : DebugUIControl
    {
        /// <summary>
        /// List entries displayed by the list.
        /// </summary>
        public List<DebugListEntry> Entries = new List<DebugListEntry>();

        private int _maxLines = -1;

        /// <summary>
        /// Max amount of lines to show in list. Set to -1 to limit to Config Entry setting.
        /// </summary>
        public int MaxLines
        {
            get
            {
                if (this._maxLines < 0)
                    return ChatManager.Entries;
                return _maxLines;
            }
            set { this._maxLines = value; }
        }

        /// <summary>
        /// Topmost entry index.
        /// </summary>
        public int ScrollPos = 0;

        public Color BackColor = DebugUI.DarkColor;

        public float Width = 200f;

        public float Height
        {
            get
            {
                var ypos = 0f;
                for (int i = 0; i < this.MaxLines; i++)
                {
                    var j = i + this.ScrollPos;

                    if (j < 0 || j > this.Entries.Count - 1)
                        break;
                    ypos += this.Entries[j].Height;
                }
                return ypos;
            }
            set
            {
                var ysize = 0f;
                for (int i = 0; i < this.Entries.Count; i++)
                {
                    var j = i + this.ScrollPos;

                    if (j < 0 || j > this.Entries.Count - 1)
                    {
                        this.MaxLines = i;
                        return;
                    }

                    ysize += this.Entries[j].Height;

                    if (ysize >= value)
                    {
                        this.MaxLines = i;
                        return;
                    }
                }

                //Simple calculation
                //this.MaxLines = (int)(value / 24);
            }
        }

        public Vec2 Size
        {
            get
            {
                return new Vec2(this.Width, this.Height);
            }
            set
            {
                this.Width = value.x;
                this.Height = value.y;
            }
        }

        /// <summary>
        /// Returns the first entry with the longest Text.
        /// </summary>
        /// <returns></returns>
        private DebugListEntry GetLongestEntry()
        {
            if (!this.Entries.Any())
                return null;

            return this.Entries.OrderByDescending(e => e.Text.Length).First();
        }

        /// <summary>
        /// Gets the rectangle of the specified entry, as shown on screen space.
        /// <para />This overload allows you to specify both the entry and index (since both are needed) for a performance boost.
        /// </summary>
        /// <returns></returns>
        public Rectangle GetEntryRectangle(DebugListEntry entry, int index)
        {
            //Entry is not visible (offscreen)
            if (index < this.ScrollPos)
                return default(Rectangle);

            //Entry is not visible (hidden)
            if (!entry.Visible)
                return default(Rectangle);

            //Get rect
            var ypos = this.y;
            var max = this.MaxLines + this.ScrollPos;
            for (var i = this.ScrollPos; i < max; i++)
            {
                //Scrollpos is too far, no more entries
                if (i > this.Entries.Count - 1)
                    return default(Rectangle);

                //Scrollpos started too low, increase
                if (i < 0)
                    continue;

                //Get current entry
                var curr = this.Entries[i];
                if (!curr.Visible)
                {
                    //Ignore invisible
                    max++;
                    continue;
                }

                //If entry is found, return the rect early!
                if (curr == entry)
                    break;

                //Add the entry's height
                ypos += curr.Height;
            }

            return new Rectangle(this.x, ypos, this.Width, entry.Height);
        }

        /// <summary>
        /// Adds a new DebugListEntry to the bottom of the list.
        /// </summary>
        /// <param name="line"></param>
        public void AddEntry(DebugListEntry line)
        {
            this.InsertEntry(this.Entries.Count, line);
        }

        /// <summary>
        /// Adds multiple entries to the bottom of the list.
        /// </summary>
        /// <param name="entries"></param>
        public virtual void AddEntries(IEnumerable<DebugListEntry> entries)
        {
            foreach (var entry in entries)
            {
                this.AddEntry(entry);
            }
        }

        /// <summary>
        /// Adds a new DebugListEntry at the specified index.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="line"></param>
        public virtual void InsertEntry(int index, DebugListEntry line)
        {
            this.Entries.Insert(index, line);
        }

        /// <summary>
        /// Removes the specified DebugListEntry.
        /// </summary>
        /// <param name="line"></param>
        public void RemoveEntry(DebugListEntry line)
        {
            this.RemoveEntryAt(this.Entries.IndexOf(line));
        }

        /// <summary>
        /// Removes the DebugListEntry with the specified name property.
        /// </summary>
        /// <param name="name"></param>
        public void RemoveEntry(string name)
        {
            this.RemoveEntry(GetEntryByName(name));
        }

        /// <summary>
        /// Removes the DebugListEntry at the specified index.
        /// </summary>
        /// <param name="index"></param>
        public virtual void RemoveEntryAt(int index)
        {
            this.Entries.RemoveAt(index);
        }

        /// <summary>
        /// Removes all DebugListEntries.
        /// </summary>
        public virtual void ClearEntries()
        {
            this.Entries.Clear();
        }

        /// <summary>
        /// Returns the first entry with the specified name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public DebugListEntry GetEntryByName(string name)
        {
            return this.Entries.FirstOrDefault(e => e.Name == name);
        }

        public DebugListBox()
        {

        }

        public DebugListBox(params DebugListEntry[] entries)
        {
            this.Entries.AddRange(entries);
        }

        public DebugListBox(float width, Color backcolor, float alpha, int maxlines = -1) : this(width, backcolor, alpha, maxlines, new DebugListEntry[] {})
        {

        }

        public DebugListBox(float width, Color backcolor, float alpha, int maxlines = -1, params DebugListEntry[] entries) : this(entries)
        {
            this.Width = width;
            this.BackColor = backcolor;
            this.alpha = alpha;
            this.MaxLines = maxlines;
        }

        public override void DoDraw()
        {
            var ypos = this.y;
            var max = this.MaxLines;
            for (var eInd = 0; eInd < max; eInd++)
            {
                var ePos = eInd + this.ScrollPos;

                if (ePos < 0 || ePos > this.Entries.Count - 1)
                    break;

                var entry = this.Entries[ePos];
                if (!entry.Visible)
                {
                    max++;
                    continue;
                }

                this.DrawEntry(entry, new Vec2(this.x, ypos));
                ypos += entry.Height;
            }

            //BG BOX
            if (this.alpha > 0)
                Graphics.DrawRect(this.position, this.position + new Vec2(this.Width, ypos - this.y), this.BackColor * this.alpha, 0.983f);
        }

        /// <summary>
        /// Draws the specified listentry.
        /// </summary>
        /// <param name="entry">Entry data to draw.</param>
        /// <param name="pos">Position of the entry.</param>
        public void DrawEntry(DebugListEntry entry, Vec2 pos)
        {
            DrawEntry(entry, pos, this.Width);
        }

        /// <summary>
        /// Draws the specified listentry.
        /// </summary>
        /// <param name="entry">Entry data to draw.</param>
        /// <param name="pos">Position of the entry.</param>
        /// <param name="width">Width of the list entry.</param>
        public virtual void DrawEntry(DebugListEntry entry, Vec2 pos, float width)
        {
            if (entry == null)
                return;

            var leftmargin = 0f;
            var rightmargin = 0f;
            var rightaligned = false;
            var ypos = pos.y;

            //Increase height of multiline entries
            switch (entry.Alignment)
            {
                case UIAlign.Center:
                    ypos += (entry.Height - 24) / 2f;
                    break;
                 case UIAlign.Bottom:
                    ypos += entry.Height - 24;
                    break;
                case UIAlign.Top:
                    //No more offsetting
                    break;
                default:
                    throw new ArgumentOutOfRangeException("entry", "Invalid Alignment value specified. Only Center, Bottom and Top are supported!");
            }

            //Set up margins and positions
            var xpos = pos.x;
            float diff;
            foreach (var ch in entry.DisplayOrder)
            {
                switch (ch)
                {
                    case 'B':
                        if (entry.Buttons.Any())
                        {
                            diff = entry.Buttons.Count * 26;
                            if (rightaligned)
                                rightmargin += diff;
                            else
                                leftmargin += diff;

                            foreach (var button in entry.Buttons)
                            {
                                button.position = new Vec2(xpos, ypos);
                                if (rightaligned)
                                    xpos -= 26;
                                else
                                    xpos += 26;

                                button.DoDraw();
                            }
                        }
                        break;
                    case 'I':
                        if (entry.Icons.Any())
                        {
                            diff = entry.Icons.Count * 26;
                            if (rightaligned)
                                rightmargin += diff;
                            else
                                leftmargin += diff;

                            foreach (var icon in entry.Icons)
                            {
                                icon.position = new Vec2(xpos, ypos);
                                icon.depth = 0.9998f;
                                if (rightaligned)
                                    xpos -= 26;
                                else
                                    xpos += 26;

                                icon.Draw();
                            }
                        }
                        break;
                    case 'T':
                        rightaligned = true;
                        xpos = pos.x + width - 24;
                        break;
                    default:
                        throw new FormatException(string.Format("Unknown DisplayOrder char \"{0}\".", ch));
                }
            }

            //Render textbox
            var trunc = entry.Text;
            if (entry.Truncuated)
                trunc = FontManager.Chat.FitTextToWidth(entry.Text, width - leftmargin - rightmargin);
            FontManager.Chat.Draw(trunc, new Vec2(pos.x + leftmargin, ypos), entry.Color, 0.99f);
        }

        public void ScrollUp()
        {
            Scroll(-DebugScrollBar.ScrollDelta);
        }

        public void ScrollDown()
        {
            Scroll(DebugScrollBar.ScrollDelta);
        }

        private void Scroll(int amount)
        {
            //TODO: DebugListBox - If clipping text is a possibility, have smoother scrolling that way too.
            var visibles = 0;
            var reached = 0;
            do
            {
                var i = this.ScrollPos + reached;

                if (i < 0 || i > this.Entries.Count - this.MaxLines)
                    break;

                if (this.Entries[i].Visible)
                {
                    visibles++;
                }
                reached += Math.Sign(amount);
            } while (visibles < Math.Abs(amount));

            if (this.Entries.Count > this.MaxLines)
                this.ScrollPos = Maths.Clamp(this.ScrollPos + reached, 0, this.Entries.Count - this.MaxLines);
        }

        /// <summary>
        /// Returns index of clicked entry in list.
        /// <para />Returns -1 if none is clicked, or -2 if a button was clicked instead.
        /// </summary>
        /// <returns></returns>
        public override object DoUpdate()
        {
            //TODO: DebugListBox should have draggable scrollbar (rendered too) -> determinable position!

            if (!this.Visible)
                return -1;

            //Scrolled
            if (Math.Abs(Mouse.scroll) > MserGlobals.Epsilon)
            {
                //Mouse on listbox
                if (DebugUI._mousePos.IsInRange(this.position, this.position + this.Size))
                {
                    //Direction
                    if (Mouse.scroll > 0)
                    {
                        ScrollDown();
                    }
                    else
                    {
                        ScrollUp();
                    }
                }
            }

            //Update de buttons
            var buttonclick = false;
            var bcnt = 0;
            foreach (var entry in this.Entries.Where(e => e.Visible))
            {
                foreach (var button in entry.Buttons)
                {
                    bcnt++;
                    button.Entry = entry;

                    var didclick = (bool)button.DoUpdate();
                    if (didclick && !buttonclick)
                        buttonclick = true;
                }
            }

            //Priority #1, since buttons are on top of the entries
            if (buttonclick)
                return -2;

            //Check for entry clicks next
            if (Mouse.left == InputState.Pressed && DebugUI._mousePos.IsInRange(this.position, this.position + this.Size))
            {
                foreach (var entry in this.Entries)
                {
                    var i = this.Entries.IndexOf(entry);
                    var rect = this.GetEntryRectangle(entry, i);
                    if (DebugUI._mousePos.y >= rect.y && DebugUI._mousePos.y < rect.y + rect.height)
                    {
                        //Clicked on entry!
                        return i;
                    }
                }
            }

            return -1;
        }
    }
}
