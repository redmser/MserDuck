﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// A debug box, showing info about the specified thing.
    /// </summary>
    public class DebugThingBox : DebugBox
    {
        private bool _validThing = true;

        private readonly object Object;

        private Thing Thing
        {
            get { return this.Object as Thing; }
        }

        /// <summary>
        /// Whether to fix sorting for entries starting with an underscore. Set using <see cref="ConfigEntries.IgnoreUnderscoreEntry"/> in config.
        /// </summary>
        //TODO: DebugThingBox - has more use than just in this context!
        public static bool IgnoreUnderscore = true;

        private List<DebugThingInfo> _infos;

        private DebugCategorizedListBox _listBox;

        private DebugBoxButton _sniperbutton;

        public DebugThingBox(object thing, int index) : base(index)
        {
            this.Object = thing;
            this._listBox = new DebugCategorizedListBox(400f, GetCategory, DebugUI.DarkColor, 0.1f, -1);
            this._listBox.TemplateCategory = new DebugListEntry(null) {Truncuated = false, Height = 32};
            this._listBox.SortFunction = e => IgnoreUnderscore && e.Name[0] == '_' ? e.Name.Substring(1) : e.Name;

            //Sniper button
            _sniperbutton = new DebugBoxButton(IconManager.ButtonIcons.CloneMap(), 7, this.ShowThing);
            this._buttons.Add(_sniperbutton);

            if (this.Object == null)
                return;

            //TODO: DebugThingBox - regarding last .Where condition, why does for example the duck have those entries with generic values and '__'
            //  -> Create function to sort out "invalid" property/field/method info as such, needed in /set and /call too!
            this._infos = DebugThingBox.GetTypeInfo(this.Object.GetType())
                .Where(x => !x.Name.StartsWith("set_") && !x.Name.StartsWith("get_") && !x.Name.Contains("__"))
                .OrderBy(x => x.Name)
                .Select(i => new DebugThingInfo(i, this.Object)).ToList();

            var entries = this._infos.Select(SetupEntry).ToList();
            this._listBox.AddEntries(entries);
            this.Size = this._listBox.Size + new Vec2(0, 26);
        }

        /// <summary>
        /// Highlights this thing.
        /// </summary>
        private void ShowThing()
        {
            if (this.Thing == null || this.Thing.level != Level.current || /*!this.Thing.visible ||*/
                Math.Abs(this.Thing.rectangle.width) < 0.001f || Math.Abs(this.Thing.rectangle.height) < 0.001f)
            {
                //Error! Wouldn't be able to show thing
                SFX.Play("scanFail", 1, Rando.Float(-0.3f, 0.3f));
            }
            else
            {
                //Show fancy highlight
                SFX.Play("newLevelBing", 1, Rando.Float(-0.3f, 0.3f));
                Level.Add(new DebugCrosshair(this.Thing));
            }
        }

        public override void DoDraw()
        {
            if (!this.Visible)
                return;

            string header;
            if (!this._validThing)
            {
                header = string.Format("{0} (REMOVED)", this.Object.GetType().Name);
            }
            else
            {
                if (this.Thing == null)
                {
                    header = this.Object.GetType().Name;
                }
                else
                {
                    header = string.Format("{0} ({1};{2})", this.Thing.GetType().Name, this.Thing.x.ToString("N2"), this.Thing.y.ToString("N2"));
                }
            }
            this.Title = header;

            if (!this.Folded)
            {
                this._listBox.DoDraw();
            }

            base.DoDraw();
        }

        public override object DoUpdate()
        {
            if (this._validThing && (this.Thing == null || this.Thing.removeFromLevel || this.Thing.level == null || this.Thing.level != Level.current))
            {
                //Different level, rip thing! May want to mark that in other ways too
                this._validThing = false;
                this._buttons.Remove(this._sniperbutton);
            }

            foreach (var info in this._infos)
            {
                info.UpdateLocked();
            }

            if (!this.Folded)
            {
                this._listBox.position = new Vec2(this.x, this.y + 26);
                this._listBox.Size = this.Size - new Vec2(0, 26);
                var res = (int)this._listBox.DoUpdate();

                if (res >= 0)
                {
                    //Handle clicked entry
                    SFX.Play("tinyTick");
                    var entry = this._listBox.Entries[res];
                    if (!(entry.Tag is DebugListCategory))
                    {
                        if (!PermissionManager.HasPermission("DebugModeEditMembers"))
                        {
                            //Not permitted
                            DebugUI.MouseToolTip("You do not have permissions to edit values!", 3f);
                        }
                        else
                        {
                            //Edit/Execute selected
                            var tInfo = (DebugThingInfo)entry.Tag;
                            if (tInfo.Info is PropertyInfo || tInfo.Info is FieldInfo)
                            {
                                //Field/property editor
                                //DebugUI.AddDebugBox(new DebugSetValueBox(this, tInfo, DebugUI._debugBoxes.Count + 4));
                                DebugUI.MouseToolTip("Sorry, but this feature is currently not implemented!", 4f);
                            }
                            else
                            {
                                //TODO: DebugThingBox - Open method execution helper (for specifying parameters, if needed) - check if static, and if returns anything display that
                                DebugUI.MouseToolTip("Sorry, but this feature is currently not implemented!", 4f);
                            }
                        }
                    }

                    //Handle base update anyway
                    base.DoUpdate();
                    return true;
                }
            }

            return base.DoUpdate();
        }

        private static DebugListEntry SetupEntry(DebugThingInfo tInfo)
        {
            var info = tInfo.Info;
            var entry = new DebugListEntry(null, info.Name);
            string type;
            Color color;
            int frame;
            bool priv;
            var redonly = false;

            if (info is PropertyInfo)
            {
                var cast = ((PropertyInfo)info);
                type = cast.PropertyType.Name;
                color = Color.CornflowerBlue;
                frame = 5;
                redonly = !cast.CanWrite;
                priv = cast.CanWrite && cast.GetSetMethod(true).IsPrivate;
            }
            else if (info is FieldInfo)
            {
                var cast = ((FieldInfo)info);
                type = cast.FieldType.Name;
                color = Color.MediumSpringGreen;
                frame = 8;
                redonly = cast.IsInitOnly;
                priv = cast.IsPrivate;
            }
            else
            {
                var cast = ((MethodInfo)info);
                type = cast.ReturnType.Name;
                color = Color.Violet;
                frame = 3;
                priv = cast.IsPrivate;
            }

            tInfo.Category = GetCategory(tInfo);
            tInfo.Type = type; //TODO: DebugThingBox - correct display of types if they are, for instance, generics (or arrays?)
            entry.Color = color;

            IconManager.TinyIcons.frame = frame;
            var clone = IconManager.TinyIcons.Clone();
            clone.scale = new Vec2(3);
            entry.Icons.Add(clone);
            if (priv)
            {
                //Add lock icon
                entry.Icons.Add(new Sprite("tinyLock") { center = new Vec2(1), scale = new Vec2(3)});
            }
            if (redonly)
            {
                //Add readonly icon
                entry.Icons.Add(new Sprite("connectionX") { center = new Vec2(1), scale = new Vec2(2)});
            }
            else if (!(info is MethodInfo))
            {
                //Can write? Add a lock button to non-methods
                var butt = new DebugBoxButton(IconManager.ButtonIcons, 8, null, string.Format("Freeze value of {0}.", info is PropertyInfo ? "property" : "field"), true);
                butt.OnClick = () => ToggleLock(butt, tInfo);
                butt.BackColor = DebugUI.DarkColor;
                entry.Buttons.Add(butt);
            }

            entry.Tag = tInfo;
            entry.TextFunction = () => string.Format("({0}) {1} = {2}", tInfo.Type, tInfo.Info.Name, tInfo.CachedValue);

            return entry;
        }

        private static void ToggleLock(DebugBoxButton button, DebugThingInfo info)
        {
            if (!PermissionManager.HasPermission("DebugModeEditMembers"))
            {
                info.Locked = false;
                DebugUI.MouseToolTip("You do not have permissions to freeze values!", 3f);
                return;
            }

            info.Locked = !info.Locked;
            if (info.Locked)
            {
                info.LockedValue = info.GetCurrentValue();
                button.BackColor = Color.Cyan;
            }
            else
            {
                info.LockedValue = null;
                button.BackColor = DebugUI.DarkColor;
            }
        }

        public void ReloadInfo()
        {
            foreach (var info in this._infos)
            {
                info.UpdateValue();
            }
        }

        private static IEnumerable<MemberInfo> GetTypeInfo(Type type)
        {
            //TODO: DebugThingBox - should static members be gotten as well? probably put into a separate category, and has to be handled differently with the DebugThingInfo stuff
            foreach (var prop in type.GetProperties(MserGlobals.InstanceFlags))
            {
                yield return prop;
            }

            foreach (var field in type.GetFields(MserGlobals.InstanceFlags))
            {
                yield return field;
            }

            foreach (var method in type.GetMethods(MserGlobals.InstanceFlags))
            {
                yield return method;
            }
        }

        private static DebugListCategory GetCategory(DebugListEntry entry)
        {
            if (entry.Tag is DebugListCategory)
                return null;

            return GetCategory((DebugThingInfo)entry.Tag);
        }

        private static DebugListCategory GetCategory(DebugThingInfo tInfo)
        {
            if (tInfo.Category != null)
                return tInfo.Category;

            //CATEGORY DETECTION

            //TODO: DebugThingBox - toss everything that is defined in duck game's base classes (thing, physobj, ...) into their own categories instead of all into other
            //  Mainly to easier find custom-defined fields/props

            var value = tInfo.GetCurrentValue();
            
            if (value is StateBinding || value is AccessorInfo || value is MemberInfo)
                return new DebugListCategory("Bindings", -2);

            if (tInfo.Info.Name.EqualsToAny(false, DebugThingBox._generalEntries))
                return new DebugListCategory("General", 1);

            if (tInfo.Info.Name.EqualsToAny(false, DebugThingBox._visualEntries))
                return "Visual";

            if (tInfo.Info.Name.EqualsToAny(false, DebugThingBox._gameplayEntries))
                return "Gameplay";

            if (tInfo.Info.Name.EqualsToAny(false, DebugThingBox._physicsEntries))
                return "Physics";

            return new DebugListCategory("Other", -2);
        }

        private static readonly IEnumerable<string> _generalEntries = new []
        {
            "position", "x", "y", "velocity", "hSpeed", "vSpeed", "owner", "duck"
        };

        private static readonly IEnumerable<string> _visualEntries = new[]
        {
            "angle", "angleDegrees", "scale", "scalex", "scaley", "width", "height", "center", "centerx", "centery", "depth", "visible"
        };

        private static readonly IEnumerable<string> _gameplayEntries = new[]
        {
            "canPickUp", "ammo", "active", "ammoType", "collisionCenter", "collisionOffset", "collisionSize", "level", "sleeping", "onFire", "flammable", "_hasTrigger"
        };

        private static readonly IEnumerable<string> _physicsEntries = new[]
{
            "weight", "enablePhysics", "updatePhysics", "bouncy", "friction"
        };

        /// <summary>
        /// Class containing info about a specified MemberInfo of a thing.
        /// </summary>
        public class DebugThingInfo
        {
            /// <summary>
            /// Whether the value of this info is locked.
            /// </summary>
            public bool Locked = false;

            /// <summary>
            /// The locked value of this info. Forced every tick if Locked = true.
            /// </summary>
            public object LockedValue = null;

            /// <summary>
            /// Cached category of this info. Set using GetCategory(info)!
            /// </summary>
            public DebugListCategory Category;

            /// <summary>
            /// The MemberInfo assigned to this ThingInfo.
            /// </summary>
            public readonly MemberInfo Info;

            /// <summary>
            /// The last cached value of this info.
            /// </summary>
            public string CachedValue = "N/A";

            /// <summary>
            /// The object assigned to this info.
            /// </summary>
            private readonly object Object;

            /// <summary>
            /// String representation of the type of this thing, for displaying in the title.
            /// </summary>
            public string Type;

            /// <summary>
            /// Creates a new DebugThingInfo hooking the specified MemberInfo to the specified thing.
            /// </summary>
            /// <param name="info"></param>
            /// <param name="obj"></param>
            public DebugThingInfo(MemberInfo info, object obj)
            {
                this.Info = info;
                this.Object = obj;
            }

            public void UpdateLocked()
            {
                if (Locked)
                    SetValue(this.LockedValue);
            }

            /// <summary>
            /// Sets value of object's field/property using reflection.
            /// <para />This will not work if locked, since the update call does so every tick.
            /// </summary>
            /// <param name="value"></param>
            public void SetValue(object value)
            {
                var pInfo = this.Info as PropertyInfo;
                if (pInfo != null)
                {
                    pInfo.SetValue(this.Object, value, null);
                    return;
                }
                var fInfo = this.Info as FieldInfo;
                if (fInfo != null)
                {
                    fInfo.SetValue(this.Object, value);
                    return;
                }
                if (this.Info is MethodInfo)
                {
                    throw new InvalidOperationException("Can't set value of a method! Use an invoke call instead.");
                }
            }

            /// <summary>
            /// Runs a GetValue on the MemberInfo. Does not return LockedValue if Locked!
            /// </summary>
            /// <returns></returns>
            public object GetCurrentValue()
            {
                try
                {
                    if (Info is PropertyInfo)
                    {
                        var prop = (PropertyInfo)Info;
                        return prop.GetValue(this.Object, null);
                    }
                    if (Info is FieldInfo)
                    {
                        var prop = (FieldInfo)Info;
                        return prop.GetValue(this.Object);
                    }
                }
                catch
                {
                    
                }

                return null;
            }

            /// <summary>
            /// Updates the CachedValue.
            /// </summary>
            public void UpdateValue()
            {
                if (Locked)
                {
                    this.CachedValue = Extensions.ToNeatString(this.LockedValue);
                    return;
                }

                try
                {
                    if (Info is PropertyInfo)
                    {
                        var prop = (PropertyInfo)Info;
                        this.CachedValue = Extensions.ToNeatString(prop.GetValue(this.Object, null));
                    }
                    else if (Info is FieldInfo)
                    {
                        var prop = (FieldInfo)Info;
                        this.CachedValue = Extensions.ToNeatString(prop.GetValue(this.Object));
                    }
                    else if (Info is MethodInfo)
                    {
                        this.CachedValue = "[Execute]";
                    }
                }
                catch
                {
                    this.CachedValue = "N/A";
                }
            }
        }
    }
}
