﻿using System;
using System.Linq;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// A debug box used for printing debug values and updating them dynamically.
    /// <para />You usually only have one of them, created using the static function call, updated with new values on its own.
    /// </summary>
    public class DebugPrintBox : DebugBox
    {
        //TODO: DebugPrintBox - categorization for debug values in form of tabs
        //TODO: DebugPrintBox - button to clear all timer entries, at the bottom

        public DebugListBox _listBox = new DebugListBox(300f, default(Color), 0f, -1);
        private const float DespawnTimer = 5f;
        private const string NoDebug = "No debug messages...";
        private DebugBoxButton _modeButton;
        private DebugPrintBoxMode _mode = DebugPrintBoxMode.OnUpdated;
        private static readonly Color AlertRed = new Color(255, 63, 63);

        private enum DebugPrintBoxMode
        {
            OnChanged,
            OnUpdated,
            NoTimer
        }

        public DebugPrintBox(int index) : base(index)
        {
            this._modeButton = new DebugBoxButton(IconManager.ButtonIcons.CloneMap(), 0, this.CycleMode);
            this._buttons.Add(this._modeButton);
            this.InvisOnClose = true;
            this.MinimumSize = new Vec2(400, 300);
            this.Size = this.MinimumSize;
            this.Title = "Debug Values (0)";
        }

        private void CycleMode()
        {
            var inc = ((int)this._mode + 1);
            if (inc > Enum.GetValues(typeof(DebugPrintBoxMode)).Length - 1)
                inc = 0;
            this._mode = (DebugPrintBoxMode)inc;

            string str;

            switch (this._mode)
            {
                case DebugPrintBoxMode.OnChanged:
                    str = "Timer resets when value is different.";
                    break;
                case DebugPrintBoxMode.OnUpdated:
                    str = "Timer resets when PrintEntry is called.";
                    break;
                case DebugPrintBoxMode.NoTimer:
                    str = "No timer.";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            DebugUI.MouseToolTip(str, 3f);
        }

        /// <summary>
        /// Adds or updates a DebugPrintEntry.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="despawns">Only gets set when added!</param>
        /// <param name="caller"></param>
        public void AddOrUpdateKey(string name, object value, bool despawns, object caller)
        {
            var entry = this._listBox.Entries.FirstOrDefault(e => ((DebugPrintEntry)e.Tag).Name == name);
            if (entry != null)
            {
                //Key exists
                var tag = (DebugPrintEntry)entry.Tag;
                if ((this._mode == DebugPrintBoxMode.OnChanged && !Extensions.EntriesEqual(tag.Value, value)) || this._mode != DebugPrintBoxMode.OnChanged)
                {
                    entry.Icons[0].alpha = 1f;
                    entry.Color = AlertRed;
                    tag.Timer = 0;
                    entry.Visible = true;
                }

                tag.Value = value;
                tag.CalledBy = caller;
            }
            else
            {
                //Add key, doesnt exist yet
                var lbe = new DebugListEntry(null, name, IconManager.ButtonIcons.CloneMap(), 3);
                var tag = new DebugPrintEntry(name, value, despawns, caller);
                lbe.Tag = tag;
                lbe.TextFunction = () =>
                {
                    //Return based on name
                    if (string.IsNullOrWhiteSpace(tag.Name))
                        return Extensions.ToNeatString(tag.Value);
                    if (tag.Value is NoValue)
                        return tag.Name;
                    return string.Format("{0} = {1}", tag.Name, Extensions.ToNeatString(tag.Value));
                };
                lbe.Color = AlertRed;
                lbe.DisplayOrder = "BIT";
                var butt = new DebugBoxButton(IconManager.ButtonIcons.CloneMap(), 15, () => lbe.Visible = false,
                    "Time until the entry disappears.\nChange timer mode in header or click to hide.");
                butt.TooltipPosition = new Vec2(this.Size.x, 0);
                butt.DoDrawAction = () =>
                {
                    butt.IconMap.alpha = this._mode == DebugPrintBoxMode.NoTimer ? 1 : 0;
                    if (this._mode != DebugPrintBoxMode.NoTimer)
                        FontManager.Chat.Draw(((int)(DebugPrintBox.DespawnTimer - tag.Timer)).ToString(), butt.x + 4, butt.y + 4, lbe.Color, 1f);
                };
                lbe.Buttons.Add(butt);
                this._listBox.Entries.Add(lbe);
            }
        }

        /// <summary>
        /// Returns whether any button was pressed
        /// </summary>
        /// <returns></returns>
        public override object DoUpdate()
        {
            //Minusu timer
            foreach (var entry in this._listBox.Entries)
            {
                if (entry.Icons[0].alpha > 0)
                    entry.Icons[0].alpha -= 0.025f;

                if (entry.Color.g < 255)
                {
                    //Fade to white
                    entry.Color.g += 8;
                    entry.Color.b += 8;
                }

                var tag = (DebugPrintEntry)entry.Tag;
                tag.Timer += Maths.IncFrameTimer();

                if (tag.Despawns && this._mode != DebugPrintBoxMode.NoTimer && tag.Timer > DebugPrintBox.DespawnTimer)
                {
                    entry.Visible = false;
                    //TODO: DebugPrintBox - Do a form of garbage collection for DebugPrintEntries, mostly for way too old ones (test when this is really needed)
                }
            }

            //POSSISHIN
            this._listBox.Size = this.Size - new Vec2(0, 24);
            this._listBox.position = this.position + new Vec2(0, 24);
            if (!this.Folded)
            {
                var selected = (int)this._listBox.DoUpdate();
                if (selected > -1)
                {
                    //Add selected entry's caller DebugThingBox if possible
                    var entry = this._listBox.Entries[selected];
                    var tag = (DebugPrintEntry)entry.Tag;
                    var thing = tag.CalledBy as Thing;
                    if (thing != null && thing.level == Level.current)
                    {
                        DebugUI.AddDebugBox(new DebugThingBox(thing, DebugUI._debugBoxes.Count));
                    }
                }
            }

            return base.DoUpdate();
        }

        public override void DoDraw()
        {
            if (!this.Visible)
                return;

            //Update title with count
            this.Title = string.Format("Debug Values ({0})", this._listBox.Entries.Count(e => e.Visible));

            //IM AN ARTIST, DRAWING!
            if (!this.Folded)
            {
                if (this._listBox.Entries.Count(e => e.Visible) == 0)
                {
                    //No entries
                    DebugUI.ShadowedText(NoDebug, this._listBox.x, this._listBox.y, 0.99f, Color.LightGray);
                }
                else
                {
                    //List of entries
                    this._listBox.DoDraw();
                }
            }

            this._modeButton.Frame = (int)this._mode;
            base.DoDraw();
        }

        private class DebugPrintEntry
        {
            public string Name;

            public float Timer;

            public object Value;

            public bool Despawns;

            public object CalledBy;
            
            public DebugPrintEntry(string name, object value, bool despawns, object caller)
            {
                this.Name = name;
                this.Value = value;
                this.Despawns = despawns;
                this.CalledBy = caller;
            }
        }
    }
}
