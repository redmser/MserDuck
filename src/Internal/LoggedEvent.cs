﻿using System;
using System.Reflection;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Reimplemented event class.
    /// </summary>
    public class LoggedEvent
    {
        /// <summary>
        /// Original event instance.
        /// </summary>
        public object BaseEvent;

        /// <summary>
        /// Who was affected by the event. May not always be set.
        /// </summary>
        public Profile Victim;

        /// <summary>
        /// Who caused the event. May not always be set.
        /// </summary>
        public Profile Dealer;

        /// <summary>
        /// When the event occured.
        /// </summary>
        public DateTime Timestamp;

        /// <summary>
        /// The type of the base event.
        /// <para />Equivilent to <code>this.BaseEvent.GetType().Name</code>.
        /// </summary>
        public readonly string Type;

        /// <summary>
        /// Reimplemented event class, using base event instance.
        /// </summary>
        /// <param name="event"></param>
        public LoggedEvent(object @event)
        {
            this.BaseEvent = @event;
            this.Type = @event.GetType().Name;
            this.Victim = (Profile)LoggedEvent._victimField.GetValue(@event);
            this.Dealer = (Profile)LoggedEvent._dealerField.GetValue(@event);
            this.Timestamp = (DateTime)LoggedEvent._timestampField.GetValue(@event);
        }

        private static readonly FieldInfo _victimField = MserGlobals.GetField("Event", "_victim");
        private static readonly FieldInfo _dealerField = MserGlobals.GetField("Event", "_dealer");
        private static readonly FieldInfo _timestampField = MserGlobals.GetField("Event", "_timestamp");
    }
}
