﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Helper for a variety of debugging functions.
    /// </summary>
    public static class MserDebug
    {
        /// <summary>
        /// Adds a new entry to the debug UI print box. If the specified name exists, refreshes the timer and updates the value. Can despawn over time.
        /// </summary>
        /// <param name="name">Name of the entry. Shown before the value, and used for value updating.</param>
        /// <param name="value">Value of the entry. Use an instance of <see cref="NoValue"/> in order to display no value instead.</param>
        /// <param name="despawns">Whether the entry gets removed after not being changed for a longer time.</param>
        /// <param name="sender">If included, allows to debug an object when this debug message's entry is selected in the list. Mostly "this".</param>
        public static void PrintEntry(string name, object value, object sender = null, bool despawns = true)
        {
            if (MserGlobals.GetGameState() == GameState.Loading)
                return;

            if (value is string)
            {
                value = ((string)value).ToLiteral();
            }

            //Add/set the key
            var box = DebugUI._debugBoxes.OfType<DebugPrintBox>().Single();
            box.AddOrUpdateKey(name, value, despawns, sender);
        }

        /// <summary>
        /// Whether profiling is currently enabled. Set using <see cref="ConfigEntries.ProfilingEntry"/> in config.
        /// </summary>
        public static bool Profiling = true;

        /// <summary>
        /// Container for profiling data.
        /// </summary>
        public static readonly SortedDictionary<string, ProfilingRegion> ProfilingData = new SortedDictionary<string, ProfilingRegion>();

        /// <summary>
        /// Starts a profiling region. Any following calls to the profiler will attempt to track time differences.
        /// <para />To help with sorting and categorization, be sure to prefix your region name if multiple in that place exists (such as ChatXXX for <see cref="ChatManager"/> stuff).
        /// <para />Notice that this may return null if profiling is disabled, so be sure to use the other MserDebug calls for debugging instead of the instance methods!
        /// </summary>
        /// <param name="region">Name of the region, for display in the DebugUI for instance.</param>
        public static ProfilingRegion StartProfiling(string region)
        {
            if (string.IsNullOrWhiteSpace(region))
                throw new ArgumentNullException("region", "The specified region name is not valid.");

            if (!Profiling)
                return null;

            ProfilingRegion reg;
            if (!MserDebug.ProfilingData.ContainsKey(region))
            {
                //Add new region
                reg = new ProfilingRegion(region);
                MserDebug.ProfilingData.Add(region, reg);

                if (DebugUI._debugBoxes.Any(d => d is DebugProfilerBox))
                {
                    ((DebugProfilerBox)DebugUI._debugBoxes.First(d => d is DebugProfilerBox)).UpdateList(region);
                }
            }
            else
            {
                //Get existing region
                reg = MserDebug.ProfilingData[region];
            }

            //Begin profiling
            reg.StartProfiling();
            return reg;
        }

        /// <summary>
        /// Takes a step in the profiling region. Only call this after a StartProfiling call!
        /// </summary>
        /// <param name="region">Instance of the region (as returned by StartProfiling).</param>
        /// <param name="name">Name of the function call to be logged.</param>
        public static void StepProfiling(ProfilingRegion region, string name)
        {
            if (!Profiling || region == null)
                return;

            region.StepProfiling(name);
        }

        /// <summary>
        /// Ends the profiling region. Only call this after a StartProfiling call!
        /// </summary>
        /// <param name="region">Instance of the region (as returned by StartProfiling).</param>
        public static void EndProfiling(ProfilingRegion region)
        {
            if (!Profiling || region == null)
                return;

            region.EndProfiling();
        }

        /// <summary>
        /// Container for profiling data, collected by the MserDebug profiling functions.
        /// <para />You should not be doing profiling using this class due to a lack of interaction with the profiling lists.
        /// </summary>
        public class ProfilingRegion
        {
            /// <summary>
            /// Name of the region.
            /// </summary>
            private readonly string _name;

            /// <summary>
            /// Timer for profiling.
            /// </summary>
            private readonly Stopwatch _timer = new Stopwatch();
            //TODO: ProfilingRegion - add a timeout for the timer - if a configurable max time is reached, stop the timer to avoid any potential slowdown from many ticking timers

            /// <summary>
            /// Container for profiling data, collected by the MserDebug profiling functions.
            /// </summary>
            /// <param name="name">Name of the region.</param>
            internal ProfilingRegion(string name)
            {
                this._name = name;
            }

            /// <summary>
            /// The total time, in 100 nanoseconds ( = 0.0001 ms) unit, that this method took to execute.
            /// </summary>
            public long TotalTime
            {
                get { return this._totalTime; }
                private set { this._totalTime = value; }
            }

            private long _totalTime;

            /// <summary>
            /// Logs a new event.
            /// </summary>
            /// <param name="type"></param>
            /// <param name="name"></param>
            public void LogEvent(ProfilingEventType type, string name)
            {
                this.LogEvent(new ProfilingEvent(type, this._timer.Elapsed.Ticks, name));
            }

            /// <summary>
            /// Logs a new event.
            /// </summary>
            /// <param name="event"></param>
            private void LogEvent(ProfilingEvent @event)
            {
                this.Events.Add(@event);
                this.TotalTime += @event.Time;
            }

            /// <summary>
            /// Starts a profiling region. Any following calls to the profiler will attempt to track time differences.
            /// <para />You should probably use <code>MserDebug.StartProfiling</code> instead!
            /// </summary>
            public void StartProfiling()
            {
                if (!this.Updating)
                    return;

                this.TotalTime = 0;
                this.Events.Clear();
                this._timer.Restart();
            }

            /// <summary>
            /// Takes a step in the profiling region. Only call this after a <code>StartProfiling</code> call!
            /// <para />You should probably use <code>MserDebug.StepProfiling</code> instead, since the region may have a null value if profiling is disabled!
            /// </summary>
            /// <param name="function">Name of the function that was called before this step call.</param>
            public void StepProfiling(string function)
            {
                if (!this.Updating)
                    return;

                //Log step event
                this.LogEvent(ProfilingEventType.Step, function);

                //Sum up time
                //this.TotalTime = this.Events.Sum(e => e.Time);

                this._timer.Restart();
            }

            /// <summary>
            /// Ends the profiling region. Only call this after a <code>StartProfiling</code> call!
            /// <para />You should probably use <code>MserDebug.EndProfiling</code> instead, since the region may have a null value if profiling is disabled!
            /// </summary>
            public void EndProfiling()
            {
                //TODO: MserDebug - allow calculation of avg. number of executions per timespan using more profiling
                //  This would allow a calculation of FPS reduction, and calls per second

                if (!this.Updating)
                    return;

                //Log end event
                this.LogEvent(ProfilingEventType.End, "[End Method]");

                //Clean up
                this._timer.Reset();
            }

            /// <summary>
            /// List of events that occurred in this region.
            /// </summary>
            public List<ProfilingEvent> Events = new List<ProfilingEvent>();

            /// <summary>
            /// Whether the values may be updated by redoing profiling on this region.
            /// </summary>
            public bool Updating = true;

            /// <summary>
            /// Returns a string that represents the current object.
            /// </summary>
            /// <returns>
            /// A string that represents the current object.
            /// </returns>
            public override string ToString()
            {
                return this._name;
            }

            /// <summary>
            /// Container for profiling event data.
            /// </summary>
            public struct ProfilingEvent
            {
                /// <summary>
                /// Type of profiling event.
                /// </summary>
                public ProfilingEventType Type;

                /// <summary>
                /// Time delta from profiling data start to this event, measured in 100 nanoseconds ( = 0.0001 ms).
                /// </summary>
                public long Time;

                /// <summary>
                /// Name of the step or event.
                /// </summary>
                public string Name;

                /// <summary>
                /// Creates a new profiling event.
                /// </summary>
                /// <param name="type">Type of profiling event.</param>
                /// <param name="time">Time delta from profiling data start to this event, measured in 100 nanoseconds ( = 0.0001 ms).</param>
                /// <param name="name">Name of the step or event.</param>
                public ProfilingEvent(ProfilingEventType type, long time, string name)
                {
                    this.Type = type;
                    this.Time = time;
                    this.Name = name;
                }
            }
        }

        /// <summary>
        /// Type of profiling event.
        /// </summary>
        public enum ProfilingEventType
        {
            Step, End
        }
    }
}
