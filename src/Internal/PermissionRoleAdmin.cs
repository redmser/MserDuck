﻿using DuckGame.MserDuck.CommandHandlers;

namespace DuckGame.MserDuck
{
    public class PermissionRoleAdmin : PermissionRole
    {
        /// <summary>
        /// Creates a PermissionRoleAdmin.
        /// </summary>
        public PermissionRoleAdmin() : base("Admin", Color.Orange)
        {
            var icon = IconManager.GetIconFromSet(IconManager.TinyIcons, (int)IconManager.TinyIconsFrames.Shield, default(Vec2), new Vec2(2), 0.9f);
            this._icon = icon;
        }

        /// <summary>
        /// An icon to be associated with this role. Size should be 16x16.
        /// <para />This should not change over time or through user input.
        /// </summary>
        public override Sprite Icon
        {
            get { return this._icon; }
        }

        private readonly Sprite _icon;

        /// <summary>
        /// Whether a user of this role has the specified permission (specified by permission name).
        /// </summary>
        /// <param name="permission"></param>
        /// <returns></returns>
        public override bool HasPermission(Permission permission)
        {
            return true;
        }
    }
}
