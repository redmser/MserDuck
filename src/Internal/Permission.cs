﻿using System;
using System.Linq;
using DuckGame.MserDuck.CommandHandlers;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// A permission to a certain action in MserDuck's context, which can be given through roles.
    /// </summary>
    public abstract class Permission
    {
        /// <summary>
        /// The name of this permission, which has to be entered when this permission is given.
        /// </summary>
        /// <returns></returns>
        public abstract string GetName();

        protected Permission()
        {
            
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            return this.GetName();
        }

        public static Permission ParsePermission(string perm)
        {
            //Command Permissions
            if (perm.StartsWith("/"))
            {
                var cmd = ChatManager.GetCommandByName(perm);
                if (cmd != null)
                    return new CommandPermission(cmd);
            }

            //Is a DebugUI Permission?
            DebugUIPermissions duip;
            if (Enum.TryParse(perm, true, out duip))
            {
                return new DebugUIPermission(duip);
            }

            return null;
        }
    }

    /// <summary>
    /// A permission for executing a certain command.
    /// </summary>
    public class CommandPermission : Permission
    {
        /// <summary>
        /// Command's first known name (same as shown in the autocomplete list).
        /// </summary>
        /// <returns></returns>
        public override string GetName()
        {
            return "/" + Command.GetNames().First();
        }

        public CommandHandler Command;

        public CommandPermission(CommandHandler command)
        {
            this.Command = command;
        }
    }

    /// <summary>
    /// A permission for the debug UI.
    /// </summary>
    public class DebugUIPermission : Permission
    {
        private DebugUIPermissions Permissions;

        /// <summary>
        /// The name of this permission, which has to be entered when this permission is given.
        /// </summary>
        /// <returns></returns>
        public override string GetName()
        {
            return this.Permissions.ToString();
        }

        public DebugUIPermission(DebugUIPermissions permits)
        {
            this.Permissions = permits;
        }
    }

    /// <summary>
    /// Permissions for the debug UI.
    /// </summary>
    public enum DebugUIPermissions
    {
        /// <summary>
        /// Pressing F9 to open it.
        /// </summary>
        DebugModeOpen,
        /// <summary>
        /// Extra info at mouse pos about things. -> Can't open DebugThingBox.
        /// </summary>
        DebugModeHoverInfo,
        /// <summary>
        /// Edit any member in a DebugThingBox.
        /// </summary>
        DebugModeEditMembers,
        /// <summary>
        /// Using the DebugMode Level Editor.
        /// </summary>
        DebugModeLevelEditor
    }
}
