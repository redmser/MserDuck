﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DuckGame.MserDuck
{
    public class IntermissionLimmeh : Level
    {
        private readonly ScoreBoardMode _mode;
        private ScoreBoardState _state = ScoreBoardState.Intro;
        private const float FadeSpeed = 0.01f;

        private ContinueCountdown _netCountdown;
        private readonly List<InputObject> _inputs = new List<InputObject>();
        private bool _matchOver;
        private bool _tie;

        private readonly List<LimmehSlot> _slots = new List<LimmehSlot>();

        private Sprite _background;

        public IntermissionLimmeh() : this(ScoreBoardMode.ShowScores)
        {
            
        }

        public IntermissionLimmeh(ScoreBoardMode mode)
        {
            this._mode = mode;
        }

        public override void Initialize()
        {
            //Sync input
            if (Network.isActive && Network.isServer)
            {
                var idx = 0;
                foreach (var p in DuckNetwork.profiles)
                {
                    if (p.connection == null)
                        continue;

                    var o = new InputObject {profileNumber = (sbyte)idx};
                    Level.Add(o);
                    this._inputs.Add(o);
                    idx++;
                }
            }

            //Clamp score of all active teams
            var teams = Teams.all.Where(t => t.activeProfiles.Count > 0).ToList();
            foreach (var tea in teams)
            {
                tea.rockScore = tea.score;
                if (RockScoreboard.wallMode)
                {
                    tea.score = Math.Min(tea.score, GameMode.winsPerSet);
                }
            }

            //Setup the level
            this.lowestPoint = 1000f;
            this.backgroundColor = new Color(0, 0, 0);
            MusicEx.ResetVolume();
            this.camera = new Camera(0, 0, 320, 180);

            //Play the music
            MusicEx.PlaySong("limmusic");

            if (this._mode == ScoreBoardMode.ShowScores)
            {
                //Start black
                Graphics.fade = 1f;
                Layer.Game.fade = 0f;
                Layer.Background.fade = 0f;

                //Create ducks and handle further logic
                var highestScore = 0;
                foreach (var t2 in teams)
                {
                    var teamscore = t2.score;
                    if (RockScoreboard.wallMode && teamscore > GameMode.winsPerSet)
                    {
                        teamscore = GameMode.winsPerSet;
                    }
                    if (teamscore >= GameMode.winsPerSet && teamscore == highestScore)
                    {
                        this._tie = true;
                    }
                    else if (teamscore >= GameMode.winsPerSet && teamscore > highestScore)
                    {
                        this._tie = false;
                        highestScore = teamscore;
                    }

                    var sortedList = new List<Profile>();
                    var nextIsThrower = false;
                    Profile activeThrower = null;
                    foreach (var p2 in t2.activeProfiles)
                    {
                        if (nextIsThrower)
                        {
                            activeThrower = p2;
                            nextIsThrower = false;
                        }
                        if (p2.wasRockThrower)
                        {
                            p2.wasRockThrower = false;
                            nextIsThrower = true;
                        }
                        sortedList.Add(p2);
                    }
                    if (activeThrower == null)
                    {
                        activeThrower = t2.activeProfiles[0];
                    }
                    sortedList.Remove(activeThrower);
                    sortedList.Insert(0, activeThrower);
                    activeThrower.wasRockThrower = true;

                    //Get percentual increments/positions
                    float inc;
                    float start;
                    switch (sortedList.Count)
                    {
                        case 1:
                            inc = 0f;
                            start = 0.5f;
                            break;
                        case 2:
                            inc = 0.5f;
                            start = 0.25f;
                            break;
                        case 3:
                            inc = 0.25f;
                            start = 0.25f;
                            break;
                        case 4:
                            inc = 0.2f;
                            start = 0.2f;
                            break;
                        default:
                            inc = 0f;
                            start = 0.5f;
                            break;
                    }

                    var x = start * 300;
                    var y = 160f;
                    var x2 = start * 150 + 50;
                    var y2 = 140f;
                    foreach (var prof in sortedList)
                    {
                        //Create the ducks and devices!
                        var duck = new Duck(x, y, prof);
                        duck.ignoreGhosting = true;
                        duck.forceMindControl = true;
                        duck.derpMindControl = false;
                        duck.connection = DuckNetwork.localConnection;
                        duck.ai = new DuckAI(prof.inputProfile);
                        if (Network.isActive && prof.connection != DuckNetwork.localConnection)
                        {
                            duck.ai._manualQuack = this.GetNetInput((sbyte)prof.networkIndex);
                        }
                        duck.mindControl = duck.ai;
                        duck.isRockThrowDuck = true;

                        var device = new LimmehDevice(x2, y2, duck);
                        device.ignoreGhosting = true;
                        device.grounded = true;

                        var slot = new LimmehSlot(duck, device);
                        this._slots.Add(slot);
                        slot.Initialize();

                        //Increment positions
                        x += inc * 300;
                        x2 += inc * 150;
                    }
                }
                
                //Collision walls and floor
                Level.Add(new Block(0f, 165f, 320f, 15f));
                Level.Add(new Block(-15f, 0f, 15f, 180f));
                Level.Add(new Block(320f, 0f, 15f, 180f));

                //Game logic
                if (!this._tie && highestScore > 0)
                {
                    this._matchOver = true;
                }
                if (this._tie)
                {
                    //TODO: IntermissionLimmeh - for custom intermissions, modify GameMode.showdown behaviour, since it is hardcoded to
                    //  open the RockScoreboard level. Probably best to inject the netmessage's code/handling (if possible)
                    //  AND/OR handle the setting of Level.current directly (if possible - probably cant inject the method that the showdown is directly handled in)
                    //  Could also simply set showdown to false and handle it myself, if nothing else works out
                    GameMode.showdown = true;
                }
            }
            else if (this._mode == ScoreBoardMode.ShowWinner)
            {
                //NYI: IntermissionLimmeh - show the winner :)
            }

            //BG Sprite
            this._background = new Sprite(Thing.GetPath<MserDuck>("limbg"));
            this._background.depth = 0.5f;

            base.Initialize();
        }

        public override void Draw()
        {
            this._background.Draw();

            base.Draw();
        }

        public override void Update()
        {
            //State handling
            switch (this._state)
            {
                case ScoreBoardState.Intro:
                    //Fade from black
                    Graphics.fade = Maths.LerpTowards(Graphics.fade, 0f, -FadeSpeed);

                    if (Graphics.fade < 0.01f)
                    {
                        //Continue with state
                        this._state = ScoreBoardState.ThrowRocks;

                        //Allow votes from here on
                        _openVoting.Invoke(null, new object[] {"", "", false});
                    }
                    break;
                case ScoreBoardState.Transition:
                    //Fade to black
                    Graphics.fade = Maths.LerpTowards(Graphics.fade, 1f, FadeSpeed);
                    break;
                case ScoreBoardState.ThrowRocks:
                    //NYI: IntermissionLimmeh - Each duck moves to their device and get a "rating" from limmeh when picked up + showing score increasing on TV in BG
                    //  That can maybe also show the coolness of each duck, and some other stupid stats, all around the monitor (make it look like a selling tv show)
                    break;
                case ScoreBoardState.ShowBoard:
                    //NYI: IntermissionLimmeh - slowly move a monitor down (annoying creaky sound too) which shows the scores of all ducks
                    break;
                case ScoreBoardState.MatchOver:
                    break;
                case ScoreBoardState.MatchOverShowDucks:
                    break;
                case ScoreBoardState.ShowTrophies:
                    break;
                case ScoreBoardState.None:
                    break;
            }

            //Timers
            if (Network.isActive && (this._mode == ScoreBoardMode.ShowScores || this._mode == ScoreBoardMode.ShowWinner))
            {
                if (this._netCountdown == null)
                {
                    if (Network.isServer)
                    {
                        this._netCountdown = new ContinueCountdown((this._mode == ScoreBoardMode.ShowScores) ? 5f : 15f);
                        Level.Add(this._netCountdown);
                    }
                    else
                    {
                        var cd = Level.current.things[typeof(ContinueCountdown)].ToList();
                        if (cd.Any())
                        {
                            this._netCountdown = (cd.ElementAt(0) as ContinueCountdown);
                        }
                    }
                }
                /*else if (this._continueHUD != null)
                {
                    if (Network.isServer)
                    {
                        this._continueHUD.text = "@START@CONTINUE(" + (int)System.Math.Ceiling((double)this._netCountdown.timer) + ")";
                        this._netCountdown.UpdateTimer();
                    }
                    else
                    {
                        this._continueHUD.text = "WAITING(" + (int)System.Math.Ceiling((double)this._netCountdown.timer) + ")";
                    }
                }*/
            }

            base.Update();
        }

        public override void Terminate()
        {
            if (this._mode == ScoreBoardMode.ShowWinner)
            {
                foreach (var t in Teams.all)
                    t.prevScoreboardScore = 0;
            }
            else
            {
                foreach (var t2 in Teams.all)
                    t2.prevScoreboardScore = t2.score;
            }

            _closeVoting.Invoke(null, null);
        }

        public InputProfile GetNetInput(sbyte index)
        {
            if (index >= this._inputs.Count || this._inputs[index].duckProfile == null || this._inputs[index].duckProfile.inputProfile == null)
            {
                return new InputProfile();
            }
            return this._inputs[index].duckProfile.inputProfile;
        }

        public void RateScore(Duck who)
        {
            //NYI: IntermissionLimmeh - rate the gameplay of the specified duck, based on increase in score and maybe coolness points?
        }

        private static readonly MethodInfo _closeVoting = MserGlobals.GetMethod("Vote", "CloseVoting");
        private static readonly MethodInfo _openVoting = MserGlobals.GetMethod("Vote", "OpenVoting");
    }
}
