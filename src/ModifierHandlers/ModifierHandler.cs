﻿namespace DuckGame.MserDuck.ModifierHandlers
{
    /// <summary>
    /// Abstract base class for all custom MserDuck modifiers. By inheriting this class, the subclass will be considered a modifier automatically and added to the corresponding list.
    /// </summary>
    public abstract class ModifierHandler
    {
        //HINT: Add any new class inheriting from ModifierHandler to the ModifierHandlers namespace in order for it to be recognized as a custom modifier.

        /// <summary>
        /// Creates a new ModifierHandler instance.
        /// </summary>
        protected ModifierHandler()
        {
            
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public virtual string[] GetDescription()
        {
            return new string[] {};
        }

        /// <summary>
        /// Returns the name of the modifier.
        /// </summary>
        /// <returns></returns>
        public abstract string GetName();

        /// <summary>
        /// Whether this modifier should only be updated for the server or also for all the clients per tick.
        /// <para /> The modifier's enabled state will still update, no matter which setting. False by default.
        /// </summary>
        public virtual bool IsServerOnly()
        {
            return false;
        }

        /// <summary>
        /// The modifier's direct enabled state.
        /// </summary>
        private bool _enabled;

        /// <summary>
        /// What category this modifier should be found inside of.
        /// </summary>
        public virtual string GetCategory()
        {
            return "General";
        }

        /// <summary>
        /// Whether this modifier is currently enabled.
        /// </summary>
        public bool Enabled
        {
            get
            {
                if (!ModifierManager.ModifiersReady)
                    return false;
                return this._enabled;
            }
            set
            {
                if (this._enabled == value)
                    return;

                this._enabled = value;

                if (value)
                {
                    this.OnEnabled();
                }
                else
                {
                    this.OnDisabled();
                }
            }
        }

        /// <summary>
        /// Called every tick if the modifier is enabled and a game is currently in progress. No checks for game state are being done!
        /// </summary>
        /// <returns></returns>
        public virtual void Update()
        {
            
        }

        /// <summary>
        /// Called when the modifier has been enabled, having been disabled before.
        /// <para />Can be called from joining a server too, when the server sends the modifier state over.
        /// </summary>
        public virtual void OnEnabled()
        {
            
        }

        /// <summary>
        /// Called when the modifier has been disabled, having been enabled before.
        /// <para />Can be called from joining a server too, when the server sends the modifier state over.
        /// </summary>
        public virtual void OnDisabled()
        {
            
        }

        /// <summary>
        /// Whether this modifier is not implemented yet, and as such should not be used.
        /// <para />Selecting it with /modifier or in the modifier list will show a warning about the modifier not being implemented.
        /// <para />You may implement certain update behaviour regardless.
        /// </summary>
        /// <returns></returns>
        public virtual bool IsNotImplemented()
        {
            return false;
        }
    }
}
