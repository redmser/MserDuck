﻿namespace DuckGame.MserDuck.ModifierHandlers
{
    public class CustomIntermissionsModifier : ModifierHandler
    {
        /// <summary>
        /// Returns the name of the modifier.
        /// </summary>
        /// <returns></returns>
        public override string GetName()
        {
            return "Custom Intermissions";
        }

        /// <summary>
        /// What category this modifier should be found inside of.
        /// </summary>
        public override string GetCategory()
        {
            return "Intermissions";
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            //Linebreak can be removed once the [NI] is gone
            return new[]
            {
                "Randomly select intermissions",
                "instead of only showing RockThrow."
            };
        }

        /// <summary>
        /// Whether this modifier is not implemented yet, and as such should not be used.
        /// <para />Selecting it with /modifier or in the modifier list will show a warning about the modifier not being implemented.
        /// <para />You may implement certain update behaviour regardless.
        /// </summary>
        /// <returns></returns>
        public override bool IsNotImplemented()
        {
            return true;
        }

        //NYI: CustomIntermissionsModifier -> more random intermissions instead of rockthrow (also conform to ManualControl mode here)
        //  chance for random advertisement such as iLoveShoes (random text and interaction)
        //  or implement the sssssssss ehhhhh poooooooooh as chancy who wants to help
    }
}
