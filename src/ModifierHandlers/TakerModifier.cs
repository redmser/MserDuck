﻿using System.Linq;

namespace DuckGame.MserDuck.ModifierHandlers
{
    public class TakerModifier : ModifierHandler
    {
        /// <summary>
        /// Returns the name of the modifier.
        /// </summary>
        /// <returns></returns>
        public override string GetName()
        {
            return "Taker";
        }

        /// <summary>
        /// What category this modifier should be found inside of.
        /// </summary>
        public override string GetCategory()
        {
            return "Weapons";
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[] { "Steal items right from your enemy's hands." };
        }
    }

    /// <summary>
    /// This class is required to allow a cast to Duck.
    /// </summary>
    public class PseudoDuck : Duck
    {
        public PseudoDuck(float xval, float yval, Profile pro) : base(xval, yval, pro)
        {
            
        }

        private void TryGrab()
        {
            var thisduck = this as Duck;
            var pickups = Level.CheckCircleAll<Holdable>(new Vec2(thisduck.x, thisduck.y + 4f), 18f);
            pickups = pickups.OrderBy(h => h, new CompareHoldablePriorities(thisduck));
            var takerenabled = ModifierManager.GetModifier<TakerModifier>().Enabled;
            var freeenabled = ModifierManager.GetModifier<FreePickupModifier>().Enabled;

            foreach (var pickup in pickups)
            {
                //Either does the regular owner == null check, or if the modifier is enabled and you aren't
                //the owner (to avoid spam pickups). Disabled CarController stealing simply because it
                //is incredibly buggy, may need to do so for more too. The remaining conditions
                //on the next lines are from DuckGame's regular code. canPickUp condition can be overridden by FreePickupModifier,
                //which in turn can be controlled by certain types.
                if (((takerenabled && pickup.owner != thisduck && !(pickup is CarController)) || (pickup.owner == null)) &&
                    ((pickup.canPickUp || (freeenabled && !(pickup is RagdollPart) && !(pickup is BookStand))) &&
                    (pickup != thisduck._lastHoldItem || thisduck._timeSinceThrow >= 30) &&
                    Level.CheckLine<Block>(thisduck.position, pickup.position, null) == null))
                {
                    if (pickup is Equipment && ((Equipment)pickup).equippedDuck != null)
                        break;

                    thisduck.GiveHoldable(pickup);
                    if (Network.isActive)
                    {
                        if (thisduck.isServerForObject)
                        {
                            thisduck._netTinyMotion.Play(1f, 0f);
                        }
                    }
                    else
                    {
                        SFX.Play("tinyMotion", 1f, 0f, 0f, false);
                    }
                    if (thisduck.holdObject.disarmedFrom != thisduck && (System.DateTime.Now - thisduck.holdObject.disarmTime).TotalSeconds < 0.5)
                    {
                        thisduck.AddCoolness(2);
                    }

                    MserGlobals.GetField(typeof(Duck), "tryGrabFrames").SetValue(this, 0);
                    break;
                }
            }
        }
    }
}
