﻿namespace DuckGame.MserDuck.ModifierHandlers
{
    public class GunShotgunModifier : ModifierHandler
    {
        /// <summary>
        /// What category this modifier should be found inside of.
        /// </summary>
        public override string GetCategory()
        {
            return "Weapons";
        }

        public override string GetName()
        {
            return "Gun Shotgun";
        }

        public override void Update()
        {
            SetBullets(15);
        }

        /// <summary>
        /// Called when the modifier has been disabled.
        /// </summary>
        public override void OnDisabled()
        {
            //Reset as best as possible
            //TODO: GunShotgun - reset better by creating instance of each gun, getting the default value of their field
            //  This can be done for similar modifiers
            SetBullets(1);
        }

        private static void SetBullets(int value)
        {
            var thingList = Level.current.things[typeof(Gun)];
            if (thingList == null)
                return;

            foreach (Gun thing in thingList)
            {
                thing.scale = value == 1 ? Vec2.One : new Vec2(2);
                MserGlobals.GetField(typeof(Gun), "_numBulletsPerFire").SetValue(thing, value);
            }
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[] { "BIG GUNS." };
        }
    }
}