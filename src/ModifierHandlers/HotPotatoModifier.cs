﻿namespace DuckGame.MserDuck.ModifierHandlers
{
    public class HotPotatoModifier : ModifierHandler
    {
        /// <summary>
        /// What category this modifier should be found inside of.
        /// </summary>
        public override string GetCategory()
        {
            return "Weapons";
        }

        public override string GetName()
        {
            return "Hot Potato";
        }

        public override void Update()
        {
            if (MserGlobals.GetGameState() != GameState.Playing &&
                MserGlobals.GetGameState() != GameState.Lobby) return;

            var thingList = Level.current.things[typeof(Duck)];
            if (thingList == null) return;

            foreach (Duck duck in thingList)
            {
                if (duck.holdObject != null)
                    duck.holdObject.DoHeatUp(0.004f);
            }
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[] { "Held weapons heat up over time." };
        }
    }
}