﻿namespace DuckGame.MserDuck.ModifierHandlers
{
    public class BouncyDucksModifier : ModifierHandler
    {
        /// <summary>
        /// What category this modifier should be found inside of.
        /// </summary>
        public override string GetCategory()
        {
            return "Physics";
        }

        public override string GetName()
        {
            return "Bouncy Ducks";
        }

        public override void Update()
        {
            SetBouncy(0.75f);
        }

        /// <summary>
        /// Called when the modifier has been disabled.
        /// </summary>
        public override void OnDisabled()
        {
            //Reset bounciness
            SetBouncy(0);
        }

        private static void SetBouncy(float value)
        {
            var thingList = Level.current.things[typeof(Duck)];
            if (thingList == null)
                return;

            foreach (Duck thing in thingList)
            {
                thing.bouncy = value;
            }
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[] { "Makes all ducks bounce around." };
        }
    }
}
