﻿namespace DuckGame.MserDuck.ModifierHandlers
{
    public class InfiniteJetpackModifier : ModifierHandler
    {
        /// <summary>
        /// Returns the name of the modifier.
        /// </summary>
        /// <returns></returns>
        public override string GetName()
        {
            return "Infinite Jetpack";
        }

        /// <summary>
        /// What category this modifier should be found inside of.
        /// </summary>
        public override string GetCategory()
        {
            return "Weapons";
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new [] {"Fly forever!"};
        }

        /// <summary>
        /// Called every tick if the modifier is enabled and a game is currently in progress. No checks for game state are being done!
        /// </summary>
        /// <returns></returns>
        public override void Update()
        {
            foreach (var jp in MserGlobals.GetListOfThings<Jetpack>())
            {
                jp._heat = 0;
            }
        }
    }
}
