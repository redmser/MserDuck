﻿namespace DuckGame.MserDuck.ModifierHandlers
{
    public class FreePickupModifier : ModifierHandler
    {
        /// <summary>
        /// What category this modifier should be found inside of.
        /// </summary>
        public override string GetCategory()
        {
            return "Weapons";
        }

        /// <summary>
        /// Returns the name of the modifier.
        /// </summary>
        /// <returns></returns>
        public override string GetName()
        {
            return "Free Pickup";
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[]
            {
                "Pick up EVERYTHING, the only limit is...",
                "Whatever isn't pickup-able, I guess."
            };
        }
    }
}
