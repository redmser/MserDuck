﻿using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.ModifierHandlers
{
    public class PurpleBlockForWinnersModifier : ModifierHandler
    {
        private readonly List<PendingSave> _savedProfiles = new List<PendingSave>();

        //private int StateCount;

        /// <summary>
        /// Whether this modifier should only be updated for the server or also for all the clients per tick.
        /// <para /> The modifier's enabled state will still update, no matter which setting. False by default.
        /// </summary>
        public override bool IsServerOnly()
        {
            return true;
        }

        public override string GetName()
        {
            return "Save for Winners";
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[] { "Winners get a save block spawned above them." };
        }

        public override void Update()
        {
            /*var started = (bool)MserGlobals.GetField(typeof(DeathmatchLevel), "_started").GetValue(null);

            if (started && this.StateCount == 0)
            {
                //Last level started
                this.StateCount = 1;
                return;
            }
            if (!started && this.StateCount == 1)
            {
                //New level not started
                this.StateCount = 2;
                return;
            }
            if (started && this.StateCount == 2)*/
            {
                //New level started
                //this.StateCount = 0;
                var delList = new List<PendingSave>();

                foreach (var save in this._savedProfiles)
                {
                    /*if (!save.Spawned)
                    {
                        Program.LogLine("created save block");

                        Level.Add(save.PurpleBlock);
                        save.Spawned = true;
                    }
                    else*/ if (save.Profile.duck != null && Vec2.Distance(save.Profile.duck.position, save.PurpleBlock.position) >= 72)
                    {
                        Level.Remove(save.PurpleBlock);
                        delList.Add(save);
                    }
                }

                foreach (var del in delList)
                {
                    this._savedProfiles.Remove(del);
                }
            }
        }

        public void OnLevelChanged()
        {
            this._savedProfiles.Clear();
        }

        public void GrantSaveBlock(Profile prof)
        {
            //No nulls
            if (prof == null || prof.duck == null)
                return;

            //No dupes
            if (this._savedProfiles.Any(sav => sav.Profile == prof))
                return;

            //TODO: PurpleBlockForWinners - Remove blocks around save block

            var pend = new PendingSave
            {
                PurpleBlock = new PurpleBlock(prof.duck.x, prof.duck.y - 32),
                Profile = prof
            };

            this._savedProfiles.Add(pend);

            Level.Add(pend.PurpleBlock);
        }

        public class PendingSave
        {
            public Profile Profile;
            public PurpleBlock PurpleBlock;
        }
    }
}
