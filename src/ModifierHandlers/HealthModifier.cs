﻿using System.Linq;

namespace DuckGame.MserDuck.ModifierHandlers
{
    public class HealthModifier : ModifierHandler
    {
        /// <summary>
        /// Returns the name of the modifier.
        /// </summary>
        /// <returns></returns>
        public override string GetName()
        {
            return "Healthbars";
        }

        /// <summary>
        /// Called every tick if the modifier is enabled and a game is currently in progress. No checks for game state are being done!
        /// </summary>
        /// <returns></returns>
        public override void Update()
        {
            if (!MserGlobals.GetListOfThings<HealthBar>().Any())
            {
                //Add healthbars for everyone!
                foreach (var duck in MserGlobals.GetListOfThings<Duck>())
                {
                    Level.Add(new HealthBar(duck));
                }
            }
        }

        /// <summary>
        /// Called when the modifier has been disabled.
        /// </summary>
        public override void OnDisabled()
        {
            foreach (var bar in MserGlobals.GetListOfThings<HealthBar>())
            {
                Level.Remove(bar);
            }
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[] { "Ducks have health, because screw death." };
        }

        /// <summary>
        /// Whether this modifier is not implemented yet, and as such should not be used.
        /// <para />Selecting it with /modifier or in the modifier list will show a warning about the modifier not being implemented.
        /// <para />You may implement certain update behaviour regardless.
        /// </summary>
        /// <returns></returns>
        public override bool IsNotImplemented()
        {
            return true;
        }
    }
}
