﻿using System.Linq;

namespace DuckGame.MserDuck.ModifierHandlers
{
    public class RocketTablesModifier : ModifierHandler
    {
        /// <summary>
        /// Whether this modifier should only be updated for the server or also for all the clients per tick.
        /// <para /> The modifier's enabled state will still update, no matter which setting. False by default.
        /// </summary>
        public override bool IsServerOnly()
        {
            return true;
        }

        public override string GetName()
        {
            return "Rocket Tables";
        }

        public override void Update()
        {
            ModDesk.UpdateModifier(0);
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[] { "Flipped desks start FLYING." };
        }
    }
}
