﻿namespace DuckGame.MserDuck.ModifierHandlers
{
    public class SuperDisarmModifier : ModifierHandler
    {
        /// <summary>
        /// Returns the name of the modifier.
        /// </summary>
        /// <returns></returns>
        public override string GetName()
        {
            return "Super Disarm";
        }

        /// <summary>
        /// What category this modifier should be found inside of.
        /// </summary>
        public override string GetCategory()
        {
            return "Weapons";
        }

        public static void DoSuperDisarm(Duck disarmed)
        {
            if (disarmed._trapped != null)
                return;

            disarmed.hSpeed *= 3f;
            disarmed.vSpeed = -2f;
            disarmed.GoRagdoll();
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[] { "Disarms are more than effective." };
        }
    }
}
