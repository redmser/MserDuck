﻿namespace DuckGame.MserDuck.ModifierHandlers
{
    public class GlitchGrenadeOnDeathModifier : ModifierHandler
    {
        /// <summary>
        /// Whether this modifier should only be updated for the server or also for all the clients per tick.
        /// <para /> The modifier's enabled state will still update, no matter which setting. False by default.
        /// </summary>
        public override bool IsServerOnly()
        {
            return true;
        }

        public override string GetName()
        {
            return "Glitchnade on Death";
        }

        public void OnKill(Duck d)
        {
            if (this.Enabled)
            {
                //Spawn glitchnade
                GlitchGrenade gnade = new GlitchGrenade(d.x, d.y);
                gnade.hSpeed = d.hSpeed + Rando.Float(-2f, 2f);
                gnade.vSpeed = d.vSpeed - Rando.Float(1f, 2.5f);
                Level.Add(gnade);
                gnade.PressAction();
            }
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[] { "Drop a glitch grenade when dying." };
        }
    }
}
