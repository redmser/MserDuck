﻿namespace DuckGame.MserDuck.ModifierHandlers
{
    public class StartWithCarModifier : ModifierHandler
    {
        /// <summary>
        /// Returns the name of the modifier.
        /// </summary>
        /// <returns></returns>
        public override string GetName()
        {
            return "Start With Car";
        }

        public static void SpawnCars()
        {
            var duck = MserGlobals.GetLocalDuck();
            var car = new CarController(duck.x, duck.y);
            Level.Add(car);
            Thing.Fondle(car, duck.connection);
            duck.GiveHoldable(car);
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[] { "Everyone gets a car. Always." };
        }
    }
}
