﻿namespace DuckGame.MserDuck.ModifierHandlers
{
    public class StayRagdolledModifier : ModifierHandler
    {
        /// <summary>
        /// Returns the name of the modifier.
        /// </summary>
        /// <returns></returns>
        public override string GetName()
        {
            return "Stay Ragdolled";
        }

        /// <summary>
        /// What category this modifier should be found inside of.
        /// </summary>
        public override string GetCategory()
        {
            return "General";
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[]
            {
                "You no longer unragdoll by pressing jump,",
                "only with the ragdoll key.",
                "Perfect for flying around like an idiot."
            };
        }
    }

    public class PseudoRagdoll : Ragdoll
    {
        public PseudoRagdoll(float xpos, float ypos, Duck who, bool slide, float degrees, int off, Vec2 v, DuckPersona p = null) : base(xpos, ypos, who, slide, degrees, off, v, p)
        {

        }

        public void UpdateInputNew()
        {
            var thisrag = this as Ragdoll;
            var modenabled = ModifierManager.GetModifier<StayRagdolledModifier>().Enabled;

            if (!thisrag._duck.dead && thisrag._timeSinceNudge > 1f && !thisrag.jetting)
            {
                if (thisrag.captureDuck.inputProfile.Pressed("LEFT", false))
                {
                    float randy = NetRand.Float(-2f, 2f);
                    thisrag._part1.vSpeed += randy;
                    thisrag._part3.vSpeed += NetRand.Float(-2f, 2f);
                    thisrag._part2.hSpeed += NetRand.Float(-2f, -1.2f);
                    thisrag._part2.vSpeed -= NetRand.Float(1f, 1.5f);
                    thisrag._timeSinceNudge = 0f;
                }
                else if (thisrag.captureDuck.inputProfile.Pressed("RIGHT", false))
                {
                    thisrag._part1.vSpeed += NetRand.Float(-2f, 2f);
                    thisrag._part3.vSpeed += NetRand.Float(-2f, 2f);
                    thisrag._part2.hSpeed += NetRand.Float(1.2f, 2f);
                    thisrag._part2.vSpeed -= NetRand.Float(1f, 1.5f);
                    thisrag._timeSinceNudge = 0f;
                }
                else if (thisrag.captureDuck.inputProfile.Pressed("UP", false))
                {
                    thisrag._part1.vSpeed += NetRand.Float(-2f, 2f);
                    thisrag._part3.vSpeed += NetRand.Float(-2f, 2f);
                    thisrag._part2.vSpeed -= NetRand.Float(1.5f, 2f);
                    thisrag._timeSinceNudge = 0f;
                }
            }
            if (!thisrag._duck.dead && (thisrag.captureDuck.inputProfile.Pressed("RAGDOLL", false) || (!modenabled && thisrag.captureDuck.inputProfile.Pressed("JUMP", false)))
                && thisrag._part2.totalImpactPower < 1f && (thisrag._part1.framesSinceGrounded < 5 || thisrag._part2.framesSinceGrounded < 5
                || thisrag._part3.framesSinceGrounded < 5 || thisrag._part1.doFloat || thisrag.part2.doFloat || thisrag._part3.doFloat) && thisrag._part1.owner == null
                && thisrag._part2.owner == null && thisrag._part3.owner == null)
            {
                thisrag._makeActive = true;
            }
        }
    }
}
