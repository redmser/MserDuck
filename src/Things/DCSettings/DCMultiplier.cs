﻿using System;

namespace DuckGame.MserDuck.DCSettings
{
    public class DCMultiplier : DeathCrateSetting
    {
        //TODO: DeathCrateSettings - MORE
        /*  Ideas:
         *  Birds that, once landing on the ground, follow the nearest duck + spew feathers everywhere
         *  A bunch of netguns that randomly fire on their own, spinning around the place
         *  Lit firecrackers - a lot of them
         *  Really slow OK-like bullets. Spawn in a circle with randomized directions
         *  Once implemented, lightnings from the lightningshotgun in every direction
         */

        public override void Activate(DeathCrate c, bool server = true)
        {
            if (!server)
                return;

            float x = c.x;
            float num = c.y - 2f;
            Level.Add(new ExplosionPart(x, num, true));
            int num2 = 6;
            if (Graphics.effectsLevel < 2)
            {
                num2 = 3;
            }
            for (int i = 0; i < num2; i++)
            {
                float deg = (float)i * 60f + Rando.Float(-10f, 10f);
                float num3 = Rando.Float(12f, 20f);
                ExplosionPart thing = new ExplosionPart(x + (float)(Math.Cos((double)Maths.DegToRad(deg)) * (double)num3), num - (float)(Math.Sin((double)Maths.DegToRad(deg)) * (double)num3), true);
                Level.Add(thing);
            }
            for (int j = 0; j < 3; j++)
            {
                float deg2 = (float)j * 75f + Rando.Float(-8f, 8f);
                float num4 = Rando.Float(8f, 14f);
                var dc = new DeathCrate(c.x, c.y)
                {
                    hSpeed = (float)Math.Cos(Maths.DegToRad(deg2))*num4,
                    vSpeed = -(float)Math.Sin(Maths.DegToRad(deg2))*num4
                };
                Level.Add(dc);
                dc.activated = true;
            }
            Graphics.flashAdd = 1.3f;
            Layer.Game.darken = 1.3f;
            SFX.Play("explode", 1f, 0f, 0f, false);
            Level.Remove(c);
        }
    }
}
