﻿using System;
using System.Linq;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Moving and fun part of the car. Not too functional on its own!
    /// </summary>
    [BaggedProperty("canSpawn", false)]
    public class CarBody : PhysicsObject, IPlatform
    {
        //TODO: Car - Attempt implementing ITakeInput successfully in order to avoid unsuccessful fondles leading to lagging input
        //  To do such, the car needs to take the input from the duck's inputprofile directly, instead of using the controller
        //  Also interesting is the InputObject, which can be added to the Level too (see RockThrow for example implementation)

        //TODO: Car - autodetect skins/destruct values from width/height of each frame and total texture size
        public const int DestructNum = 3;
        public const int SkinNum = 5;

        public StateBinding _destroyedBinding = new StateBinding("_destroyed", -1, false);

        public StateBinding _hitPointsBinding = new StateBinding("_hitPoints", -1, false);

        private float damageMultiplier = 1f;
        public StateBinding _damageMultiplierBinding = new StateBinding("damageMultiplier", -1, false);

        private int _skin = 0;
        private StateBinding _skinBinding = new StateBinding("_skin");

        private CarController _controller;
        private StateBinding _controllerBinding = new StateBinding("_controller");

        private SpriteMap _sprite;
        private SpriteMap _spriteDuck;

        private Sprite _frontWheel;
        private Sprite _backWheel;

        private ConstantSound _honkSound = new ConstantSound(Thing.GetPath<MserDuck>("sounds/honk.wav"));
        private ConstantSound _reverseSound = new ConstantSound(Thing.GetPath<MserDuck>("sounds/reverse.wav"));
        private ConstantSound _carIdleSound = new ConstantSound("chainsawIdle");

        private NetSoundEffect _driftSound = new NetSoundEffect(Thing.GetPath<MserDuck>("sounds/drifter.wav")) {volume = 1};
        private NetSoundBinding _driftSoundBinding = new NetSoundBinding("_driftSound");

        public NetSoundEffect _netSlip = new NetSoundEffect("slip") { volume = 1 };
        public StateBinding _netSlipBinding = new NetSoundBinding("_netSlip");

        public CarBody(float xpos, float ypos, CarController control) : base(xpos, ypos)
        {
            if (control != null)
            {
                this._controller = control;
            }

            this._maxHealth = 28f;
            this._hitPoints = 28f;
            this._sprite = new SpriteMap(this.GetPath("car"), 32, 24);
            this._spriteDuck = new SpriteMap(this.GetPath("carDuck"), 32, 24);
            this._frontWheel = new Sprite(this.GetPath("wheel"));
            this._backWheel = new Sprite(this.GetPath("wheel"));
            this.graphic = this._sprite;
            this.center = new Vec2(15f, 18f);
            this.collisionOffset = new Vec2(-14f, -9f);
            this.collisionSize = new Vec2(27f, 15f);
            this.depth = 0.3f;
            this._editorName = "Car";
            this.thickness = 4f;
            this.weight = 13f;
            this.collideSounds.Clear();
            this.physicsMaterial = PhysicsMaterial.Metal;
            this.flammable = 0.1f;
            this.bouncy = 0.15f;
            this.friction = 0.1f;
            this.buoyancy = 0.1f;
        }

        public override void Initialize()
        {
            //BUG: Car - the color still seems to be broken for other people, despite the controller looking just fine beforehand!
            if (this._controller != null)
                this._skin = this._controller.frame;

            this._frontWheel.depth = 0.4f;
            this._frontWheel.CenterOrigin();
            this._backWheel.depth = 0.4f;
            this._backWheel.CenterOrigin();
            this._spriteDuck.depth = 0.25f;

            this._carIdleSound.lerpSpeed = 0.1f;
            this._honkSound.lerpSpeed = 0.3f;
            this._reverseSound.lerpSpeed = 0.3f;

            if (debugtext == null && _debugMode)
            {
                debugtext = new MserText(Level.current.camera.x + 5, Level.current.camera.y + 5, "", Color.Lime, 0.75f, -1, true);
                Level.Add(debugtext);
            }
        }

        public override void Draw()
        {
            var destr = (int)Math.Floor((1f - this._hitPoints / this._maxHealth) * 3f);
            this.frame = this._skin * CarBody.DestructNum + destr;

            this._spriteDuck.center = this._sprite.center;
            this._spriteDuck.flipH = this._sprite.flipH;
            this._spriteDuck.angle = this.angle;
            this._spriteDuck.position = this.position;
            this._spriteDuck.frame = this._controller.duck == null ? 0 : (this._controller.duck.quack > 0 ? 3 : (this._strafing || this._drifting ? 2 : 1));

            base.Draw();
            this._spriteDuck.Draw();

            this._frontWheel.position = br;
            this._backWheel.position = bl;
            this._frontWheel.angleDegrees += (this.hSpeed * 8);
            this._backWheel.angleDegrees += (this.hSpeed * 8);

            this._frontWheel.Draw();
            this._backWheel.Draw();

            if (_debugMode)
            {
                Graphics.DrawRect(new Vec2(this.x, this.y - 4 * this.height), new Vec2(tr2.x, br2.y), Color.Green * 0.4f,
                    0.999f);
                Graphics.DrawRect(new Vec2(bl2.x, tl2.y), new Vec2(this.x, this.y - 3 * this.height),
                    Color.Orange * 0.4f, 0.999f);
            }
        }

        private Vec2 tl;
        private Vec2 tr;
        private Vec2 bl;
        private Vec2 br;
        private Vec2 tl2;
        private Vec2 tr2;
        private Vec2 bl2;
        private Vec2 br2;

        private float _lastH;
        private bool _anglin;
        private bool _debugMode = false;
        private bool _recolored = false;
        private bool _strafing = false;
        private StateBinding _strafingBinding = new StateBinding("_strafing");
        private bool _drifting = false;
        private StateBinding _driftingBinding = new StateBinding("_drifting");
        private int _smokeTimer = -1;
        private int _soundTimer = 0;
        private int _fireTimer = 0;

        private static MserText debugtext;

        public override void Update()
        {
            DebugLog("u0");

            //slowdown
            var movementdamp = Math.Min(this._hitPoints / this._maxHealth + 0.5f, 1f);

            this.tl  = (this.position + this.collisionOffset)                                                 .Rotate(this.angle, this.position);
            this.tr  = (this.position + this.collisionOffset + new Vec2(this.collisionSize.x, 0))             .Rotate(this.angle, this.position);
            this.bl  = (this.position + this.collisionOffset + new Vec2(0, this.collisionSize.y))             .Rotate(this.angle, this.position);
            this.br  = (this.position + this.collisionOffset + this.collisionSize)                            .Rotate(this.angle, this.position);
            this.tl2 = (this.position + this.collisionOffset - 2 * new Vec2(this.height))                     .Rotate(this.angle, this.position);
            this.tr2 = (this.position + this.collisionOffset + new Vec2(this.collisionSize.x + 4, 0))      ;//.Rotate(this.angle, this.position);
            this.bl2 = (this.position + this.collisionOffset + new Vec2(-4, this.collisionSize.y))         ;//.Rotate(this.angle, this.position);
            this.br2 = (this.position + this.collisionOffset + this.collisionSize - 2 * new Vec2(this.height)).Rotate(this.angle, this.position);

            if (this._soundTimer > 0)
                this._soundTimer--;

            DebugLog("u1");

            //if (this._carIdleSound != null)
            {
                this._carIdleSound.lerpVolume = (this._controller.duck == null) ? 0f : 0.6f;
                this._carIdleSound.pitch = Math.Min(Math.Abs(this.hSpeed / 7 + this.vSpeed / 3) - (1f - movementdamp), 1f);
            }

            //if (this._reverseSound != null)
            {
                this._reverseSound.lerpVolume = this._strafing && Math.Abs(this.velocity.length) > 0.1f ? 1f : 0f;
                this._reverseSound.pitch = Math.Min(Math.Abs(this.vSpeed / 10) - (1f - movementdamp), 0.15f);
            }

            if (this.damageMultiplier > 1f)
            {
                this.damageMultiplier -= 0.2f;
            }
            else
            {
                this.damageMultiplier = 1f;
            }

            if (this._hitPoints <= 0f && !base._destroyed)
            {
                this.Destroy(new DTImpact(this));
            }
            if (this._hitPoints <= 2f)
            {
                //Fire timer
                if (this._fireTimer < 150)
                {
                    this._fireTimer++;
                }
                else
                {
                    //BURN ME
                    this.Burn(Vec2.Zero, this);
                }
            }

            DebugLog("u2");

            if (this.isServerForObject)
            {
                _anglin = false;

                //Angle up right wall
                if (this.wallCollideRight != null)
                {
                    if (Level.CheckRect<IPlatform>(new Vec2(this.x, this.y - 3 * this.height), new Vec2(tr2.x, br2.y)) == null)
                    {
                        //Do angle
                        this.angleDegrees -= 5;
                        this.vSpeed -= 0.4f * movementdamp;
                        this.hSpeed = this._lastH;
                        this.y -= 1.5f * movementdamp;
                        _anglin = true;
                    }
                }

                //Angle up left wall
                if (this.wallCollideLeft != null)
                {
                    if (Level.CheckRect<IPlatform>(new Vec2(bl2.x, tl2.y), new Vec2(this.x, this.y - 3 * this.height)) == null)
                    {
                        //Do angle
                        this.angleDegrees += 5;
                        this.vSpeed -= 0.4f * movementdamp;
                        this.hSpeed = this._lastH;
                        this.y -= 1.5f * movementdamp;
                        _anglin = true;
                    }
                }

                if (this.grounded)
                {
                    //Fixable angl
                    this.angleDegrees = Extensions.Lerp(0, this.angleDegrees, 0.6f);
                }
            }

            DebugLog("u3");

            if (this.hSpeed < 3.5f)
            {
                this._drifting = false;
            }
            if ((this.hSpeed > 3.5f && this._drifting) || this._hitPoints <= 16f)
            {
                //Smokin
                this._smokeTimer++;
                if (this._smokeTimer == 0)
                {
                    Level.Add(SmallSmoke.New(this.br.x + Rando.Float(-3, 3), this.br.y + Rando.Float(-3, 3)));
                }
                else if (this._smokeTimer == 1 && !this._drifting && this._hitPoints <= 6f)
                {
                    Level.Add(SmallSmoke.New(this.x + Rando.Float(-7, 7), this.y + Rando.Float(-6, 3.5f)));
                }
                else if (this._smokeTimer == 2 && (this._drifting || this._hitPoints <= 11f))
                {
                    Level.Add(SmallSmoke.New(this.bl.x + Rando.Float(-3, 3), this.bl.y + Rando.Float(-3, 3)));
                }
                else if (this._smokeTimer >= 4)
                {
                    this._smokeTimer = -1;
                }
            }

            DebugLog("u4");

            if (this._controller.duck != null)
            {
                var duk = this._controller.duck;

                //TODO: Car - Check if duck color has changed while inside car (although very unlikely)
                if (!this._recolored && duk.persona != null && MserGlobals.GetGameState() != GameState.RockThrow)
                {
                    //TODO: Car - This should be done using the new skin system, to allow custom duck visuals, PLUS add the duck's hat, and correctly display armor
                    this._recolored = true;
                    this._spriteDuck = new SpriteMap(Graphics.Recolor(this.GetPath("carDuck"), duk.persona.color), 32, 24);
                }

                this._honkSound.lerpVolume = duk.inputProfile.Down("QUACK") ? 0.8f : 0;
                this._honkSound.pitch = Math.Min(Math.Abs(this.hSpeed / 20 + this.vSpeed / 10) - (1f - movementdamp), 0.2f);

                //Dont do a flingy palingy
                duk.velocity = Vec2.Zero;

                DebugLog("u5");

                if (this.isServerForObject)
                {
                    if (duk == null)
                        return;

                    if (this.grounded && duk.inputProfile.Pressed("JUMP"))
                    {
                        //Jump!
                        this.vSpeed = -4f * movementdamp;
                    }

                    //Friction
                    this.hSpeed = Extensions.Lerp(0, this.hSpeed, 0.95f);

                    this._strafing = duk.inputProfile.Down("STRAFE");

                    if (duk.inputProfile.Down("DOWN"))
                    {
                        this.hSpeed = Extensions.Lerp(0, this.hSpeed, 0.95f);
                    }
                    else if (duk.inputProfile.Down("RIGHT"))
                    {
                        if (!this.grounded)
                        {
                            //Do angle
                            this.angleDegrees += 5;
                        }

                        if (!_strafing)
                        {
                            this.hSpeed += 0.5f * movementdamp;
                            this.offDir = 1;

                            if (duk.inputProfile.Pressed("RIGHT") && this.hSpeed > 3.5f && !this._drifting)
                            {
                                this._drifting = true;
                                _driftSound.Play();
                            }
                        }
                        else
                        {
                            this.hSpeed = 0.7f * movementdamp;
                        }
                    }
                    else if (duk.inputProfile.Down("LEFT"))
                    {
                        if (!this.grounded)
                        {
                            //Do angle
                            this.angleDegrees -= 5;
                        }

                        if (!_strafing)
                        {
                            this.hSpeed -= 0.5f * movementdamp;
                            this.offDir = -1;

                            if (duk.inputProfile.Pressed("LEFT") && this.hSpeed > 3.5f && !this._drifting && this.grounded)
                            {
                                this._drifting = true;
                                _driftSound.Play();
                            }
                        }
                        else
                        {
                            this.hSpeed = -0.7f * movementdamp;
                        }
                    }

                    DebugLog("u6");

                    //TODO: Car - Some functionality for the fire button (and should ragdoll just work the way it is expected?)
                    //TODO: Car - (How) do some certain states - such as conversion, netting, stunning - work?

                    if (this.grounded)
                    {
                        if (this.angleDegrees > 170 && this.angleDegrees < 190)
                        {
                            //Rip you
                            this._controller.duck.GoRagdoll();
                            this.angleDegrees = Extensions.Lerp(0, this.angleDegrees, 0.7f);
                            base.Update();
                            return;
                        }
                    }

                    var b = Level.CheckCircle<Banana>(this.position, 12f);
                    if (b != null && Math.Abs(this.hSpeed) > 0.5f && this.grounded)
                    {
                        //SLIPPIDOO
                        if (Network.isActive)
                        {
                            this._netSlip.Play(1f, Rando.Float(-0.2f, 0.2f));
                        }
                        else
                        {
                            SFX.Play("slip", 1f, Rando.Float(-0.2f, 0.2f), 0f, false);
                        }

                        Level.Remove(b);
                        this._controller.duck.velocity = this.velocity;
                        this._controller.duck.GoRagdoll();
                        base.Update();
                        return;
                    }

                    DebugLog("u7");

                }

                duk.position = this.position - new Vec2(0, 8);

                if (this._debugMode)
                    debugtext.text = string.Format("pos: {0}\r\nvel: {1}\r\ngrounded: {2}\r\nwalls: {3} / {4}\r\anglin: {5}", this.position, this.velocity, this.grounded, this.wallCollideLeft != null, this.wallCollideRight != null, this._anglin);
            }
            else
            {
                this._reverseSound.lerpVolume = 0f;
                this._honkSound.lerpVolume = 0f;
                this._recolored = false;
            }

            DebugLog("u8");

            base.Update();
        }

        public override void Terminate()
        {
            this._carIdleSound.Kill();
            this._honkSound.Kill();
            this._reverseSound.Kill();

            base.Terminate();

            if (this._controller != null && !this.removing)
            {
                this._controller.removing = true;
                Level.Remove(this._controller);
            }
        }

        internal bool removing = false;

        public override void Impact(MaterialThing with, ImpactedFrom from, bool solidImpact)
        {
            if (with == null || with == this || (this._controller != null && with == this._controller) ||
                (this._controller != null && with == this._controller.duck))
                return;

            base.Impact(with, from, solidImpact);

            if (from == ImpactedFrom.Left || from == ImpactedFrom.Right)
            {
                if (Math.Abs(this.hSpeed) > 0.1)
                {
                    this._lastH = this.hSpeed;
                }
            }

            var duk = with as Duck;
            var ragdoll = with as RagdollPart;

            if (duk != null && !(duk.holdObject is CarController))
            {
                if (Math.Abs(this.velocity.length) > 6.5f)
                {
                    var helmet = (Helmet)duk.GetEquipment(typeof(Helmet));
                    if (helmet == null)
                    {
                        duk.Kill(new DTCrush(this));
                    }
                    else
                    {
                        //Mush the helmet
                        helmet.Crush();
                    }
                }
                else
                {
                    if (Math.Abs(this.velocity.length) > 2.5f)
                        duk.GoRagdoll();

                    if (duk.ragdoll != null && duk.ragdoll.part1 != null)
                        duk.ragdoll.part1.velocity += this.velocity;
                }
            }
            else if (ragdoll != null)
            {
                if (Math.Abs(this.velocity.length) > 6f)
                {
                    if (ragdoll.duck != null)
                        ragdoll.duck.Kill(new DTCrush(this));
                }
                else
                {
                    ragdoll.velocity += this.velocity;
                }
            }
            else if (with.GetType().IsSubclassOf(typeof(PhysicsObject)))
            {
                if (this.onFire)
                {
                    with.onFire = true;
                }

                if (with is CarBody)
                {
                    //Check if ontop
                    if (Level.CheckRectAll<CarBody>(this.position - new Vec2(10, 20), this.position + new Vec2(10, 20)).Count() < 2)
                    {
                        var car = with as CarBody;

                        this._hitPoints -= Math.Abs(car.hSpeed) / 15f + Math.Abs(this.hSpeed) / 8f;
                        car._hitPoints -= Math.Abs(this.hSpeed) / 15f + Math.Abs(car.hSpeed) / 8f;

                        //Solid if stopped
                        if (car.hSpeed < 0.1f && (car.wallCollideLeft != null || car.wallCollideRight != null))
                        {
                            this.hSpeed = -(this.hSpeed / 5f);
                            car.vSpeed -= 0.1f;
                        }
                        else
                        {
                            //Car impact, softer
                            this.hSpeed /= 4;
                            car.hSpeed = Extensions.Lerp(this.hSpeed * 2, car.hSpeed, 0.25f);
                        }

                        if (_soundTimer <= 0 && this.velocity.length > 0.1f && car.velocity.length > 0.1f)
                        {
                            this._soundTimer = 20;
                            SFX.Play("lightTurnOn");
                        }
                    }
                }
                else
                {
                    //Phys impact
                    with.velocity += this.velocity;
                }

                if (Math.Abs(this.velocity.length) > 7f && with.GetType().IsSubclassOf(typeof(Gun)))
                {
                    ((Gun)with).PressAction();
                }
            }
            else if (Math.Abs(this.velocity.length) > 1f && (with.GetType() == typeof(Door) || with.GetType() == typeof(Window)))
            {
                with.Destroy(new DTImpact(this));
            }
        }

        protected override bool OnDestroy(DestroyType type = null)
        {
            this._hitPoints = 0f;
            Level.Remove(this);
            SFX.Play(Thing.GetPath<MserDuck>("sounds/carexplode.wav"));
            var flyDir = Vec2.Zero;
            if (type is DTShot)
            {
                flyDir = (type as DTShot).bullet.travelDirNormalized;
            }
            for (var i = 0; i < 20; i++)
            {
                //BUG: Car - effects and sounds are local in this OnDestroy method, instead set a bool with statebinding for showing those FX
                var ins = Spark.New(this.x - 8f + Rando.Float(16f), this.y - 8f + Rando.Float(16f), -flyDir, 0.01f);
                ins.hSpeed = ((Rando.Float(1f) > 0.5f) ? 1f : -1f) * Rando.Float(3f) + Math.Sign(flyDir.x) * 0.5f;
                ins.vSpeed = -Rando.Float(1f);
                Level.Add(ins);
                if (i > 6)
                    continue;

                var deg = i * 60f + Rando.Float(-10f, 10f);
                var num3 = Rando.Float(12f, 20f);
                var thing = new ExplosionPart(this.x + (float)(Math.Cos(Maths.DegToRad(deg)) * num3), this.y - 2f - (float)(Math.Sin(Maths.DegToRad(deg)) * num3), true);
                Level.Add(thing);

                if (i > 3)
                    continue;
                
                var gib = new CarGib(this.x, this.y);
                gib.hSpeed = Rando.Float(-2.5f, 2.5f);
                gib.vSpeed = Rando.Float(-5, 0.5f);
                gib.angleDegrees = Rando.Float(0, 180);
                Level.Add(gib);
            }
            foreach (var obj in Level.CheckCircleAll<PhysicsObject>(this.position, 25f))
            {
                obj.Destroy(new DTRocketExplosion(this));
            }
            return true;
        }

        public override bool Hit(Bullet bullet, Vec2 hitPos)
        {
            if (this._hitPoints <= 0f)
            {
                return base.Hit(bullet, hitPos);
            }
            if (bullet.isLocal && this.owner == null && this._controller != null && this._controller.owner == null)
            {
                Thing.Fondle(this, DuckNetwork.localConnection);
            }
            int i = 0;
            while (i < 1f + this.damageMultiplier / 2f)
            {
                Thing rebound = MetalRebound.New(hitPos.x, hitPos.y, bullet.travelDirNormalized.x > 0 ? 1 : -1);
                Level.Add(rebound);
                var ins = Spark.New(this.x - 8f + Rando.Float(16f), this.y - 8f + Rando.Float(16f), -bullet.travelDirNormalized, 0.007f);
                ins.hSpeed = -bullet.travelDirNormalized.x * 2f * (Rando.Float(1f) + 0.3f);
                ins.vSpeed = -bullet.travelDirNormalized.y * 2f * (Rando.Float(1f) + 0.3f) - Rando.Float(2f);
                Level.Add(ins);
                i++;
            }
            SFX.Play("ting");
            this._hitPoints -= this.damageMultiplier;
            this.damageMultiplier += 2f;
            if (this._hitPoints <= 0f)
            {
                this.Destroy(new DTShot(bullet));
            }
            return base.Hit(bullet, hitPos);
        }

        public override void ExitHit(Bullet bullet, Vec2 exitPos)
        {
            int i = 0;
            while (i < 1f + this.damageMultiplier / 2f)
            {
                Thing rebound = MetalRebound.New(exitPos.x, exitPos.y, bullet.travelDirNormalized.x > 0 ? 1 : -1);
                Level.Add(rebound);
                var ins = Spark.New(this.x - 8f + Rando.Float(16f), this.y - 8f + Rando.Float(16f), -bullet.travelDirNormalized, 0.025f);
                ins.hSpeed = -bullet.travelDirNormalized.x * 2f * (Rando.Float(1f) + 0.3f);
                ins.vSpeed = -bullet.travelDirNormalized.y * 2f * (Rando.Float(1f) + 0.3f) - Rando.Float(2f);
                Level.Add(ins);
                i++;
            }
        }

        public void DebugLog(string str)
        {
            if (_debugMode)
                Program.LogLine("CAR DEBUGGING - " + str);
        }
    }
}
