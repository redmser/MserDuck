﻿using System;

namespace DuckGame.MserDuck
{
    [EditorGroup("stuff|mser")]
    public class SpikesHat : Hat
    {
        public SpikesHat(float xpos, float ypos) : base(xpos, ypos)
        {
            this._pickupSprite = new Sprite(this.GetPath("spikesHatPickup"), 0f, 0f);
            this._sprite = new SpriteMap(this.GetPath("spikesHat"), 32, 32, false);
            base.graphic = this._pickupSprite;
            this.center = new Vec2(8f, 10f);
            this.collisionOffset = new Vec2(-6f, -6f);
            this.collisionSize = new Vec2(12f, 12f);
            this._sprite.CenterOrigin();
            this.thickness = 0.2f;
            this.physicsMaterial = PhysicsMaterial.Metal;
        }

        public override void Update()
        {
            base.Update();

            var offset = new Vec2(0, 2);
            var offset2 = Vec2.Zero;
            if (this.equippedDuck != null)
            {
                //Proximity check above hat if worn, to be neater
                offset = new Vec2(-2, -4);
                offset2 = new Vec2(2, -2);
            }

            //Mush all of the things!
            var things = Level.CheckRectAll<MaterialThing>(this.topLeft + offset, this.bottomRight + offset2);
            foreach (var t in things)
            {
                var d = t as Duck;
                float vs;

                if (this.equippedDuck != null && this.equippedDuck.sliding)
                {
                    //Factor wearer's "sliding speed"
                    //TODO: SpikesHat: sliding speed check has to be directional - only have vs increase if sliding in correct direction compared to offDir
                    vs = Math.Abs(this.equippedDuck.hSpeed) + Math.Abs(t.hSpeed);
                }
                else
                {
                    vs = t.vSpeed;
                    if (this.equippedDuck != null)
                    {
                        //Factor duck's vSpeed
                        vs += Math.Min(0, this.equippedDuck.vSpeed);
                    }
                    else
                    {
                        //Factor own vSpeed
                        vs += Math.Min(0, this.vSpeed);
                    }
                }

                if (vs > 1.5f && !ReferenceEquals(t, this) && (d == null || this.duck == null || !ReferenceEquals(d, this.duck)) && (d == null || !d.HasEquipment(typeof(Boots))))
                {
                    t.Fondle(this);

                    if (this.equippedDuck != null)
                    {
                        if (this.equippedDuck.sliding)
                        {
                            t.hSpeed += 0.5f * this.equippedDuck.offDir;
                        }
                        else
                        {
                            t.vSpeed -= 0.5f;
                        }
                    }

                    //Kill the persono!
                    t.Destroy(new DTImpale(this));
                }
            }
        }
    }
}
