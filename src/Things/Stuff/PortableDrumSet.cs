﻿using System;

namespace DuckGame.MserDuck
{
    [BaggedProperty("isFatal", false), EditorGroup("stuff|mser")]
    public class PortableDrumSet : Gun
    {
        private Sprite _sprite;

        public PortableDrumSet(float xval, float yval) : base(xval, yval)
        {
            this.ammo = 1;
            this._ammoType = new ATShrapnel();
            this._type = "gun";
            this._sprite = new Sprite(this.GetPath("drumsetmini"));
            base.graphic = this._sprite;
            this.center = new Vec2(10f, 7f);
            this.collisionOffset = new Vec2(-9f, -6f);
            this.collisionSize = new Vec2(19f, 12f);
            base.bouncy = 0.2f;
            this.friction = 0.35f;
            this.weight = 2.5f;
        }

        public override void OnPressAction()
        {
            //Only local call to avoid duplicate drumsets
            if (!this.isServerForObject)
                return;

            var pos = this.TryPlacement();

            if (float.IsNaN(pos))
            {
                //FUKIT
                if (this.duck != null)
                {
                    this.duck.Swear();
                    this.duck.ThrowItem(true);
                }
            }
            else
            {
                //DOIT
                if (this.duck != null)
                    this.duck.ThrowItem(false);
                var drumset = (Thing)Activator.CreateInstance(MserGlobals.GetType("DrumSet"), this.x, pos - 8);
                Level.Add(drumset);
                Level.Remove(this);
            }
        }

        public float TryPlacement()
        {
            var yp = this.y;
            for (var i = 0; i < 20; i++)
            {
                if (Level.CheckPoint<IPlatform>(this.x, yp) != null)
                {
                    //DOIT
                    SFX.Play("harp", 0.75f, -0.2f);
                    return yp;
                }
                yp += 4;
            }

            //FUKIT
            return float.NaN;
        }
    }
}
