﻿using System.Collections.Generic;

namespace DuckGame.MserDuck
{
    [BaggedProperty("isFatal", false), EditorGroup("stuff|mser")]
    public class BookStand : PortableDrumSet
    {
        private SpriteMap _sprite;

        private Sprite _specialSprite;
        private SinWave _wave = 0.1f;

        private const float RadiusOfEffect = 92;

        private Vec2 _lockPos;
        private StateBinding _lockPosBinding = new StateBinding("_lockPos");
        private bool _placeddown = false;
        private StateBinding _placedBinding = new StateBinding("_placeddown");
        private List<Duck> _influenced = new List<Duck>();

        public BookStand(float xval, float yval) : base(xval, yval)
        {
            this.ammo = 1;
            this._ammoType = new ATShrapnel();
            this._type = "gun";
            this._sprite = new SpriteMap(this.GetPath("choir"), 17, 25);
            base.graphic = this._sprite;
            this.center = new Vec2(8f, 12f);
            this.collisionOffset = new Vec2(-6f, -10f);
            this.collisionSize = new Vec2(13f, 23f);
            base.bouncy = 0.2f;
            this.friction = 0.4f;
            this.weight = 3f;
            this._specialSprite = new Sprite("hats/senpaiStar", 0f, 0f);
            this._specialSprite.CenterOrigin();
        }

        public override void OnPressAction()
        {
            if (this._placeddown)
                return;

            var pos = this.TryPlacement();

            if (float.IsNaN(pos))
            {
                //FUKIT
                if (this.duck != null)
                {
                    this.duck.Swear();
                    this.duck.ThrowItem(true);
                }
            }
            else
            {
                //DOIT
                if (this.duck != null)
                    this.duck.ThrowItem(false);
                this.y = pos - 12;
                this._lockPos = this.position;
                this._placeddown = true;
            }
        }

        public override void Draw()
        {
            base.Draw();

            if (!this._placeddown)
                return;

            this._specialSprite.angle += 0.02f;
            this._specialSprite.alpha = 0.3f + this._wave.normalized * 0.2f;
            this._specialSprite.depth = this.depth - 10;
            var s = 0.8f + this._wave.normalized * 0.2f;
            this._specialSprite.scale = new Vec2(s);
            var pos = this.Offset(new Vec2(-1f, -5f));
            Graphics.Draw(this._specialSprite, pos.x, pos.y);
        }

        public override void Update()
        {
            base.Update();

            if (this._placeddown)
            {
                this.position = this._lockPos;
                this.updatePhysics = false;
                this.enablePhysics = false;
                this.frame = 1;
                this.canPickUp = false;
                base.depth = 0.5f;
                this.thickness = 4f;
                
                //Get ducks in area
                BookStand._changeTime++;
                var ducks = MserGlobals.GetListOfThings<Duck>();
                foreach (var duk in ducks)
                {
                    if (Vec2.Distance(duk.position, this.position) > BookStand.RadiusOfEffect)
                    {
                        //Undo magic
                        if (_influenced.Contains(duk))
                        {
                            _influenced.Remove(duk);
                            duk.manualQuackPitch = false;
                            duk.quackPitch = 0;
                            duk._netQuack = new NetSoundEffect("quack");
                            duk._netQuack.pitchBinding = new FieldBinding(duk, "quackPitch");

                            if (duk.profile.localPlayer)
                            {
                                DuckBehaviourHandler.SkipPitch = false;
                            }
                        }
                    }
                    else
                    {
                        //Do magic
                        if (!_influenced.Contains(duk))
                        {
                            //TODO: BookStand - Give a shiny bloomy effect to all ducks
                            //TODO: BookStand - Quacking should spawn musicnotes

                            _influenced.Add(duk);
                            duk.manualQuackPitch = true;

                            if (duk.profile.localPlayer)
                            {
                                DuckBehaviourHandler.SkipPitch = true;
                            }
                        }

                        if (_changeTime >= 50)
                        {
                            duk._netQuack = new NetSoundEffect(this.GetPath(string.Format("sounds/holyquack{0}.wav", NetRand.Int(1, 2))));
                            duk._netQuack.pitchBinding = new FieldBinding(duk, "quackPitch");
                        }
                        duk.quackPitch = (byte)Maths.Clamp(Instrument.GetMousePitch(true) * 255, 0, 255);
                    }
                }
            }
        }

        private static int _changeTime = 0;
    }
}
