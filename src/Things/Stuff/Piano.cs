﻿using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck
{
    [BaggedProperty("isFatal", false), EditorGroup("stuff|mser")]
    public class Piano : PortableDrumSet
    {
        private SpriteMap _sprite;

        private Vec2 _lockPos;
        private StateBinding _lockPosBinding = new StateBinding("_lockPos");
        private bool _placeddown = false;
        private StateBinding _placedBinding = new StateBinding("_placeddown");

        private int _selected = 1;
        private const int SFXNum = 4;
        private readonly List<NetSoundEffect> SFXList = new List<NetSoundEffect>();

        public Piano(float xval, float yval) : base(xval, yval)
        {
            this.ammo = 1;
            this._ammoType = new ATShrapnel();
            this._type = "gun";
            this._sprite = new SpriteMap(this.GetPath("piano"), 35, 22);
            base.graphic = this._sprite;
            this.center = new Vec2(17f, 6f);
            this.collisionOffset = new Vec2(-16f, -5f);
            this.collisionSize = new Vec2(33f, 10f);
            base.bouncy = 0.2f;
            this.friction = 0.35f;
            this.weight = 3.5f;
        }

        public override void Initialize()
        {
            base.Initialize();

            for (var i = 1; i <= SFXNum; i++)
            {
                this.SFXList.Add(new NetSoundEffect(this.GetPath(string.Format("sounds/piano{0}.wav", i))));
            }
        }

        public override void OnPressAction()
        {
            if (this._placeddown)
                return;

            var pos = this.TryPlacement();

            if (float.IsNaN(pos))
            {
                //FUKIT
                if (this.duck != null)
                {
                    this.duck.Swear();
                    this.duck.ThrowItem(true);
                }
            }
            else
            {
                //DOIT
                this.y = pos - 15;
                this._lockPos = this.position;
                this._placeddown = true;
            }
        }

        public override void CheckIfHoldObstructed()
        {
            if (this.duck != null)
                this.duck.holdObstructed = false;
        }

        public override void Draw()
        {
            //Force position to avoid blinky blinky
            if (this._placeddown)
                this.position = this._lockPos;

            base.Draw();
        }

        public override void Update()
        {
            base.Update();

            if (this._placeddown)
            {
                this.angle = 0;
                this.offDir = 1;
                this.position = this._lockPos;
                this.updatePhysics = false;
                this.enablePhysics = false;
                this.frame = 1;
                this.handOffset = new Vec2(0f, -9999f);
                base.depth = 0.5f;
                this.thickness = 4f;
                this._holdOffset = new Vec2(1f, 7f);
                this.collisionSize = new Vec2(33f, 22f);

                //Binds state
                BindManager.BindsEnabled = this.duck == null || !this.duck.profile.localPlayer;

                if (this.owner != null)
                {
                    this.owner.vSpeed = 0f;
                    this.owner.hSpeed = 0f;
                    this.owner.position = this.position + new Vec2(0f, -7f);
                    if (!this.isServerForObject)
                    {
                        //Has to be server
                        Thing.Fondle(this, this.owner.connection);
                        return;
                    }

                    //Server-only code

                    //TODO: Piano - pitching notes further using mouse?

                    //Selection of instrument
                    if (this.duck.inputProfile.Pressed("STRAFE"))
                    {
                        //Next or reset
                        this._selected++;
                        if (this._selected > Piano.SFXNum)
                            this._selected = 1;
                        SFX.Play("openClick");
                        //TODO: Piano - small text floating up (NT style, but fade instead of blinky) of the newly selected instrument
                    }

                    var pressed = KeyLayoutManager.GetPressedKeys().KeyMaskToString();
                    foreach (var lch in pressed.Select(char.ToLowerInvariant))
                    {
                        //TODO: Piano - disallow ragdolling by injecting Duck's GoRagdoll
                        //TODO: Piano - include number keys in CharMap too

                        var newch = lch;
                        if (KeyLayoutManager.Layout == "de-DE")
                        {
                            //QWERTZ masterrace
                            if (newch == 'y')
                                newch = 'z';
                            else if (newch == 'z')
                                newch = 'y';
                        }

                        //Try playing a note
                        float pitch;
                        if (!Piano.CharMap.TryGetValue(newch, out pitch))
                            continue;

                        //Play the note
                        //TODO: Piano - allow playing multiple notes at once by instancing new NetSFX whenever playing the note
                        //TODO: Piano - stop playing notes when not holding the key anymore
                        //IMP: Piano - sync played notes
                        Level.Add(new MusicNote(this.x, this.y, new Vec2(this.owner.offDir, 0)));
                        this.SFXList[this._selected - 1].Play(1, pitch);
                    }
                }
            }
        }

        private static readonly Dictionary<char, float> CharMap = new Dictionary<char, float>
        {
            { 'q', -1.00f},
            { 'w', -0.93f},
            { 'e', -0.88f},
            { 'r', -0.80f},
            { 't', -0.72f},
            { 'y', -0.64f},
            { 'u', -0.56f},
            { 'i', -0.48f},
            { 'o', -0.40f},
            { 'p', -0.32f},
            { 'a', -0.24f},
            { 's', -0.16f},
            { 'd', -0.08f},
            { 'f',  0.00f},
            { 'g',  0.08f},
            { 'h',  0.16f},
            { 'j',  0.24f},
            { 'k',  0.32f},
            { 'l',  0.40f},
            { 'z',  0.48f},
            { 'x',  0.56f},
            { 'c',  0.64f},
            { 'v',  0.72f},
            { 'b',  0.80f},
            { 'n',  0.88f},
            { 'm',  1.00f},
        }; 
    }
}
