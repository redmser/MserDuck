﻿using System;
using System.Collections.Generic;

namespace DuckGame.MserDuck
{
    //NYI: DeathCrateHat - frame keeps being reset to 0 and as such the animation never plays out - manual animation coding instead? ugh
    //  else remove timer
    [EditorGroup("stuff|mser"), BaggedProperty("canSpawn", false)]
    public class DeathCrateHat : Hat, IPlatform
    {
        public StateBinding _settingIndexBinding = new StateBinding("settingIndex", -1, false);

        public StateBinding _activatedBinding = new StateBinding("activated", -1, false);

        public StateBinding _beepsBinding = new StateBinding("_beeps", -1, false);

        public byte _beeps;

        private bool _didActivation;

        public bool activated;

        public byte settingIndex;

        public DeathCrateSetting setting
        {
            get
            {
                if (this.settingIndex < Settings.Count)
                {
                    return Settings[this.settingIndex];
                }
                return new DCSwordAdventure();
            }
        }

        public static List<DeathCrateSetting> Settings
        {
            get { return _settings; }
        }

        private static List<DeathCrateSetting> _settings;

        public static void Init()
        {
            DeathCrateHat._settings = (List<DeathCrateSetting>)MserGlobals.GetField(typeof(DeathCrate), "_settings").GetValue(null);
        }

        public DeathCrateHat(float xpos, float ypos) : base(xpos, ypos)
        {
            this._pickupSprite = new Sprite(this.GetPath("dcHatPickup"), 0f, 0f);
            this._sprite = new SpriteMap(this.GetPath("dcHat"), 32, 32, false);
            this._sprite.AddAnimation("idle", 0, true, 0);
            this._sprite.AddAnimation("activate", 0.35f, false,
                1,
				1,
				2,
				2,
				2,
				2,
				2,
				2,
				2,
				3,
				3,
				3,
				3,
				3,
				3,
				3,
				3,
				2,
				4,
				4,
				4,
				4,
				4,
				4,
				4,
				4,
				2,
				5,
				5,
				5,
				5,
				5,
				5,
				5,
				5,
				2,
				6,
                6,
                2);
            this._sprite.SetAnimation("idle");
            base.graphic = this._pickupSprite;
            this.center = new Vec2(8f, 4f);
            this.collisionOffset = new Vec2(-8f, -4f);
            this.collisionSize = new Vec2(16f, 6f);
            this._sprite.CenterOrigin();
            this.thickness = 0.2f;
            this.physicsMaterial = PhysicsMaterial.Metal;
        }

        public override void Update()
        {
            //DeathCrate triggering
            if (this.activated && this._sprite.currentAnimation != "activate")
            {
                this._sprite.SetAnimation("activate");
            }
            if (this._sprite.imageIndex == 3 && this._beeps == 0)
            {
                SFX.Play("singleBeep", 1f, 0f, 0f, false);
                this._beeps += 1;
            }
            if (this._sprite.imageIndex == 4 && this._beeps == 1)
            {
                SFX.Play("singleBeep", 1f, 0f, 0f, false);
                this._beeps += 1;
            }
            if (this._sprite.imageIndex == 5 && this._beeps == 2)
            {
                SFX.Play("singleBeep", 1f, 0f, 0f, false);
                this._beeps += 1;
            }
            if (this._sprite.imageIndex == 2 && this._beeps == 3)
            {
                SFX.Play("doubleBeep", 1f, 0.2f, 0f, false);
                this._beeps += 1;
            }
            if (base.isServerForObject && this._sprite.currentAnimation == "activate" && this._sprite.finished && !this._didActivation)
            {
                this._didActivation = true;
                var fakedc = new DeathCrate(this.x, this.y);
                this.setting.Activate(fakedc, true);
                Send.Message(new NMActivateDeathCrate(this.settingIndex, fakedc));
                this.equippedDuck.Kill(new DTImpact(this));
            }

            base.Update();

            //Manual fixing
            this.solid = true;

            //DebugUI.PrintEntry("SpikesHat equippedDuck", this.equippedDuck);
            //DebugUI.PrintEntry("SpikesHat duck", this.duck);
            //DebugUI.PrintEntry("SpikesHat owner", this.owner);

            if (this._didActivation && this.grounded && this.duck == null)
            {
                //Make disappear
                Level.Add(SmallSmoke.New(base.x, base.y));
                Level.Add(SmallSmoke.New(base.x + 4f, base.y));
                Level.Add(SmallSmoke.New(base.x - 4f, base.y));
                Level.Add(SmallSmoke.New(base.x, base.y + 4f));
                Level.Add(SmallSmoke.New(base.x, base.y - 4f));
                Level.Remove(this);
            }

            //Don't bother with the rest otherwise
            if (this.activated || this.equippedDuck == null)
                return;

            //Trigger the DC!
            var things = Level.CheckRectAll<MaterialThing>(this.topLeft + new Vec2(-2, -4), this.bottomRight + new Vec2(2, -2));
            foreach (var t in things)
            {
                var d = t as Duck;
                float vs;

                if (this.equippedDuck.sliding)
                {
                    //Factor wearer's "sliding speed"
                    vs = Math.Abs(this.equippedDuck.hSpeed) + Math.Abs(t.hSpeed);
                }
                else
                {
                    //Factor own vSpeed
                    vs = t.vSpeed + Math.Min(0, this.vSpeed);
                }

                if (vs > 1f && !ReferenceEquals(t, this) && (d == null || !ReferenceEquals(d, this.duck)))
                {
                    /*
                    DebugUI.PrintEntry("SpikesHat killspeed of " + t, vs);
                    DebugUI.PrintEntry("SpikesHat e1 of " + t, ReferenceEquals(t, this));

                    if (d != null)
                    {
                        DebugUI.PrintEntry("SpikesHat e2 of " + t, d.HasEquipment(typeof(Boots)));
                        DebugUI.PrintEntry("SpikesHat e3 of " + t, ReferenceEquals(d, this.duck));
                    }
                    else
                    {
                        DebugUI.PrintEntry("SpikesHat D IS", null);
                    }
                    */

                    //Trigger the DC
                    t.Fondle(this);
                    this.activated = true;
                    this._sprite.SetAnimation("activate");
                    SFX.Play("click", 1f, 0f, 0f, false);
                }
            }
        }
    }
}
