using System.Collections.Generic;

namespace DuckGame.MserDuck
{

    /// <summary>
    /// SuperSpecialMegaAwesomeBonusDuckinator3000PremiumEdition is a helper class for the perhaps more aptly named "Ducks". This is a simple timer that gets <para/>
    /// attached to a ragdoll created by the perhaps more aptly named "Ducks", allowing the said ragdoll created by the perhaps more aptly named "Ducks" to <para/>
    /// detonate and be removed after a herein specified time period, in order to cease to commonly heard complaint that the perhaps more aptly named "Ducks" <para/>
    /// causes people running the game on potatoes and virtually existant not-so-potatoes to experience a conflict with a bottleneck in either or both their <para/>
    /// central or their graphical processing unit, leading the aforementioned people that run the game on potatoes and virtually existant not-so-potatoes to <para/>
    /// tell the creator of the perhaps more aptly named "Ducks" to "Mend his excrements".
    /// </summary>
    [BaggedProperty("canSpawn", false)]
	public class SuperSpecialMegaAwesomeBonusDuckinator3000PremiumEdition : NetworkRagdoll
	{
     	//Disloodged from Gernoodel with foodle
		private bool _explosionCreated;
		
		// coonts doon until the ragdoll exploodes
        private const int DOOTOONOOTIOON_COONTDOON = 0x003C * 0x0003;

        //Hoodel (a noodel).
		private int ASSPLOODEL_DELUXE_FIREBLAST;
        private StateBinding ASSPLOODEL_DELUXE_FIREBLAST_BINDING_OF_STATESAAC = new StateBinding("ASSPLOODEL_DELUXE_FIREBLAST");
		
		private List<Bullet> firedBullets;
		private byte bulletFireIndex = 0;
		
		public SuperSpecialMegaAwesomeBonusDuckinator3000PremiumEdition(float xpos, float ypos, float angleDeg, int offDir, Vec2 velocity, Vec3 color) : base(xpos, ypos, angleDeg, offDir, velocity, color)
		{
			ASSPLOODEL_DELUXE_FIREBLAST = SuperSpecialMegaAwesomeBonusDuckinator3000PremiumEdition.DOOTOONOOTIOON_COONTDOON;
			firedBullets = new List<Bullet>();
		}
		
		/* ZOOMG TEH POORJOOCT*/
		public override void Update ()
		{
			if (ASSPLOODEL_DELUXE_FIREBLAST-- < 0) {
				//MOONES NOODEL!! (more than one, naturlich)
				DoDoodel();
			}
		}
		
		private void DoDoodel() {
            if (this.part2 != null)
			    OnNoodelDoodled(this.part2.position);
			Level.Remove(this);
		}
		
		//STOOLEN FROM GERNOODEL
		private void OnNoodelDoodled(Vec2 pos)
		{
			if (!this._explosionCreated)
			{
				/*Graphics.flashAdd = 1.3f;
				Layer.Game.darken = 1.3f; EYEOODLE MC NOPE*/
			}
			this.MakeBoodels(pos);
			this.CreateKerbloodel(pos);
		}

		private void MakeBoodels(Vec2 pos) {
			if (base.isServerForObject)
			{
				float x = pos.x;
				float num = pos.y - 2f;
				for (int i = 0; i < 20; i++)
				{
					
					float num2 = (float)i * 18f - 5f + Rando.Float(10f);
					
					ATShrapnel aTShrapnel = new ATShrapnel();
					aTShrapnel.range = 60f + Rando.Float(18f);
					Bullet bullet = new Bullet(x + (float)(System.Math.Cos((double)Maths.DegToRad(num2)) * 6.0), num - (float)(System.Math.Sin((double)Maths.DegToRad(num2)) * 6.0), aTShrapnel, num2, null, false, -1f, false, true);
					bullet.firedFrom = this;
					this.firedBullets.Add(bullet);
					Level.Add(bullet);
				}
				System.Collections.Generic.IEnumerable<Window> enumerable = Level.CheckCircleAll<Window>(this.position, 40f);
				foreach (Window current in enumerable)
				{
					if (Level.CheckLine<Block>(this.position, current.position, current) == null)
					{
						current.Destroy(new DTImpact(this));
					}
				}
				this.bulletFireIndex += 20;
				if (Network.isActive)
				{
					NMFireGun msg = new NMFireGun(null, this.firedBullets, this.bulletFireIndex, false, 4, false);
					Send.Message(msg, NetMessagePriority.ReliableOrdered, null);
					this.firedBullets.Clear();
				}
			}
		}

		private void CreateKerbloodel(Vec2 pos)
		{
			if (!this._explosionCreated)
			{
				float x = pos.x;
				float num = pos.y - 2f;
				Level.Add(new ExplosionPart(x, num, true));
				int num2 = 18;
				if (Graphics.effectsLevel < 2) //scruboodel
				{
					num2 = 3;
				}
				for (int i = 0; i < num2; i++)
				{
					float deg = (float)i * 60f + Rando.Float(-10f, 10f);
					float num3 = Rando.Float(12f, 20f);
					ExplosionPart thing = new ExplosionPart(x + (float)(System.Math.Cos((double)Maths.DegToRad(deg)) * (double)num3), num - (float)(System.Math.Sin((double)Maths.DegToRad(deg)) * (double)num3), true);
					Level.Add(thing);
				}
				this._explosionCreated = true;
				SFX.Play("explode", 1f, 0f, 0f, false);
			}
		}
	}
}
