﻿namespace DuckGame.MserDuck
{
    [BaggedProperty("isFatal", false), EditorGroup("guns|mser")]
    public class Cola : Gun
    {
        private SpriteMap _sprite;

        private readonly NetSoundEffect _netOpen = new NetSoundEffect(Thing.GetPath<MserDuck>("sounds/canopen.wav"));
        private readonly NetSoundBinding _netOpenBinding = new NetSoundBinding("_netOpen");
        private readonly NetSoundEffect _netKick = new NetSoundEffect(Thing.GetPath<MserDuck>("sounds/cankick.wav"));
        private readonly NetSoundBinding _netKickBinding = new NetSoundBinding("_netKick");
        private readonly NetSoundEffect _netDrink = new NetSoundEffect(Thing.GetPath<MserDuck>("sounds/candrink.wav"));
        private readonly NetSoundBinding _netDrinkBinding = new NetSoundBinding("_netDring");

        public bool _pin = true;
        public bool _thrown;

        private int _drinkTime = 5;
        private int _drinkAmount = 15;

        public Cola(float xval, float yval) : base(xval, yval)
		{
            this.ammo = 1;
            this._ammoType = new ATShrapnel();
            this._type = "gun";
            this._sprite = new SpriteMap(this.GetPath("cola"), 16, 16, false);
            base.graphic = this._sprite;
            this.center = new Vec2(8f, 9f);
            this.collisionOffset = new Vec2(-5f, -6f);
            this.collisionSize = new Vec2(9f, 13f);
            this._holdOffset = new Vec2(-1f, 1f);
            base.bouncy = 0.4f;
            this.friction = 0.25f;
            this.collideSounds.Add(Thing.GetPath<MserDuck>("sounds/cankick.wav"));
        }

        public override void Update()
        {
            base.Update();
            if (this._thrown && this.owner == null)
            {
                this._thrown = false;
                if (System.Math.Abs(this.hSpeed) + System.Math.Abs(this.vSpeed) > 0.4f)
                {
                    base.angleDegrees = 180f;
                }
            }
            if (!this._pin && this.owner == null)
            {
                this._sprite.frame = 1;
                this.weight = 0.1f;
                this.canPickUp = false;
            }
            if (!this._pin && this._grounded)
            {
                //Kicking logic
                var col = Level.CheckLineAll<Duck>(new Vec2(base.x - 5f, base.y + 2f), new Vec2(base.x + 5f, base.y + 2f));
                foreach (var duk in col)
                {
                    if (duk.grounded && duk.bottom <= this.bottom + 2f && System.Math.Abs(duk.hSpeed) > 2.5f)
                    {
                        //Kick the can!
                        if (Network.isActive)
                        {
                            if (base.isServerForObject)
                            {
                                this._netKick.Play();
                            }
                        }
                        else
                        {
                            SFX.Play(Thing.GetPath<MserDuck>("sounds/cankick.wav"), 1f, Rando.Float(-0.6f, 0.6f), 0f, false);
                        }

                        if (duk.isServerForObject)
                        {
                            this.hSpeed += duk.hSpeed * 1.5f;
                            this.vSpeed -= 2.5f;
                        }
                        
                        Level.Add(new BananaSlip(base.x, base.y + 2f, duk.offDir > 0));
                    }
                }
            }
            if (this._triggerHeld)
            {
                if (base.duck != null)
                {
                    base.duck.quack = 20;
                    if (this.offDir > 0)
                    {
                        this.handAngle = -1.0995574f;
                    }
                    else
                    {
                        this.handAngle = 1.0995574f;
                    }
                    this.handOffset = new Vec2(8f, -1f);
                    this._holdOffset = new Vec2(1f, 10f);
                }
            }
            else
            {
                this.handAngle = 0f;
                this.handOffset = new Vec2(0f, 0f);
                this._holdOffset = new Vec2(-1f, 2f);
            }
        }

        public override void HeatUp(Vec2 location)
        {
        }

        public void EatBanana()
        {
            this._pin = false;
            this._holdOffset = new Vec2(-2f, 3f);
            this.collisionOffset = new Vec2(-4f, -2f);
            this.collisionSize = new Vec2(8f, 4f);
            if (Network.isActive)
            {
                if (base.isServerForObject)
                {
                    this._netOpen.Play();
                }
            }
            else
            {
                SFX.Play(Thing.GetPath<MserDuck>("sounds/canopen.wav"), 1f, Rando.Float(-0.6f, 0.6f), 0f, false);
            }
            //TODO: Cola: spraying liquid (animation and maybe liquid too?)
            this.friction = 0.1f;
        }

        public override void OnPressAction()
        {
            if (this._pin)
            {
                this.EatBanana();
            }
            else
            {
                Drink();
            }
        }

        public override void OnHoldAction()
        {
        }

        public override void OnReleaseAction()
        {
            if (!this._pin)
            {
                Drink();
            }
        }

        private void Drink()
        {
            this._drinkTime--;

            if (this._drinkTime <= 0 && this._drinkAmount > 0)
            {
                this._drinkAmount--;
                this._drinkTime = Rando.Int(4, 7);

                //TODO: Cola - the more has been drunk, the faster firing rate should be, until autofire + instant reloads

                if (Network.isActive)
                {
                    if (base.isServerForObject)
                    {
                        this._netDrink.Play();
                    }
                }
                else
                {
                    SFX.Play(Thing.GetPath<MserDuck>("sounds/candrink.wav"), 1, Rando.Float(-0.6f, 0.6f));
                }
            }
        }
    }
}
