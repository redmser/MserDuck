using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DuckGame.MserDuck
{
	public class RobotDuck : Duck
	{
		private SpriteMap _spriteBase;
        private SpriteMap _spriteSpawning;
        private Sprite _spriteGlow;

        /// <summary>
        /// Whether the RobotDuck is currently spawning (not able to attack, move, ...).
        /// </summary>
        public bool spawning = true;

        /// <summary>
        /// Whether PathNodes are generated and present for the current level.
        /// </summary>
        public bool pathNodesGenerated;

        /// <summary>
        /// Amount of sparks emitted during spawning.
        /// </summary>
        private int _sparkCount = 0;
        /// <summary>
        /// Current sound to play during spawning.
        /// </summary>
        private int _soundIndex = 0;

        /// <summary>
        /// The current target of the RobotDuck. Is null if none.
        /// </summary>
        public Thing target;

        private StateBinding targetBinding = new StateBinding("target");

        /// <summary>
        /// The current path to travel to the target.
        /// </summary>
        public AIPath path;

        /// <summary>
        /// Whether any valid targets are found on the map.
        /// </summary>
        public bool noTargets = false;

        /// <summary>
        /// Constantly running timer that decides when a new action should be taken (switching targets for example).
        /// </summary>
        public float timer = 0.5f;

        private StateBinding timerBinding = new StateBinding("timer");

        /// <summary>
        /// Timer that should count down while the target cannot be reached. Switch target if it stays like this.
        /// </summary>
        public float cantReachTimer = 10f;

        /// <summary>
        /// The update method, inherited from Duck's base, PhysicsObject.
        /// </summary>
        private readonly MethodInfo _drawMethod = MserGlobals.GetMethod("PhysicsObject", "Draw");

        /// <summary>
        /// The Duck who created this RobotDuck.
        /// </summary>
        public readonly Duck summoner;

        /// <summary>
        /// The sound of the idle motor.
        /// </summary>
        private readonly ConstantSound _soundMotor = new ConstantSound(Thing.GetPath<MserDuck>("sounds/rdMotor.wav"), 0, 0, Thing.GetPath<MserDuck>("sounds/rdMotor.wav"));

        //NYI: RobotDuck - currently not in place due to horrible AI system resulting in idiotic behaviour and slow generation times

        public RobotDuck(float xpos, float ypos, Duck owner, Profile pro) : base(xpos, ypos, pro)
		{
            SetupSprites();
			base.graphic = this._spriteSpawning;
			this._collisionSize = new Vec2(8f, 22f);
			this._collisionOffset = new Vec2(-4f, -7f);
			this.center = new Vec2(16f, 16f);

            this.summoner = owner;
            this.ai = new DuckAI();
            this.ai.locomotion.pathFinder.followObject = this;
            this.derpMindControl = false;
            this.forceMindControl = true;
            this.mindControl = this.ai;
		}
		
		public override void Initialize()
		{
            //Add to follow cam
            FollowCam followCam = Level.current.camera as FollowCam;
			if (followCam != null)
			{
				followCam.Add(this);
			}

            //Generate nodes
            try
            {
                GeneratePathNodes();
            }
            catch (Exception ex)
            {
                Program.LogLine("During PathNode Generation: " + ex.Message + ex.StackTrace);
            }
        }

        public override void Draw()
		{
            if (spawning)
            {
                if (this._spriteSpawning.imageIndex == 8)
                {
                    //Show a glow flashing up when face lights up
                    Graphics.Draw(this._spriteGlow, this.x, this.y - 1);
                }
            }
            else
            {
                //TODO: RoboDuck - Draw weapon held + holding hand (indicator for team?) + anims
                //TODO: RoboDuck - Render _spriteGlow, use alpha to signify different anger states, or when it for example fires/quacks
            }

            //TODO: RoboDuck - Switch animations depending on AI's state!

            //Do not use Duck's renderer, get base method!
            var ptr = this._drawMethod.MethodHandle.GetFunctionPointer();
            var baseDraw = (Action)Activator.CreateInstance(typeof(Action), this, ptr);
            baseDraw();
        }

        public override void Update()
        {
            if (!this.spawning)
            {
                //Use random time to decide next action
                if (this.timer <= 0f)
                {
                    //Think!
                    this.timer = Rando.Float(3, 5) / 3f;
                    this.DoThinkingLogic();
                }
                else if (this.isServerForObject)
                {
                    this.timer -= 0.1f;
                }

                //Update AI if needed
                if (this.pathNodesGenerated && this.target != null)
                {
                    try
                    {
                        this.ai.SetTarget(this.target.position);
                        this.ai.Update(this);
                    }
                    catch (Exception ex)
                    {
                        Program.LogLine("During AI Update: " + ex.Message + ex.StackTrace);
                    }
                }
            }
            else
            {
                //Currently spawning, worry about that
                this.DoSpawningLogic();
            }

            base.Update();
        }

	    /// <summary>
        /// Think of a new action, like finding a new target.
        /// </summary>
        public void DoThinkingLogic()
        {
            //TODO: RoboDuck - What should it be influenced by (other guns kill it, it COULD be converted with the book, ignited, netted, slip on banana to ragdoll, exchanged, ...)

            if (this.pathNodesGenerated)
            {
                //Let it think
                try
                {
                    if (this.noTargets)
                    {
                        //TODO: RoboDuck - No targets in Level, go self-destruct or play reverse spawning animation, dunno
                        //this.Destroy();

                        this.target = null;
                    }
                    else
                    {
                        //If no target is selected, random chance or current target can not be reached, find a new target!
                        if (this.target == null)
                        {
                            try
                            {
                                this.FindNewTarget();
                            }
                            catch (Exception ex)
                            {
                                Program.LogLine("During Setting Target: " + ex.Message + ex.StackTrace);
                            }
                        }
                        else if (!AI.CanReach(AI.NearestNode(this.position), this.target))
                        {
                            //Can't reach target!
                            if (this.cantReachTimer <= 0f)
                            {
                                //Find a new target
                                this.target = null;

                                Program.LogLine("Couldn't reach target in time. Finding new!");
                            }
                            else
                            {
                                //Count down
                                this.cantReachTimer -= 0.5f;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Program.LogLine("During Thinking Logic: " + ex.Message + ex.StackTrace);
                }
            }
            else
            {
                //Has to have path nodes to be able to think
                try
                {
                    this.GeneratePathNodes();
                }
                catch (Exception ex)
                {
                    Program.LogLine("During PathNode Generation: " + ex.Message + ex.StackTrace);
                }
            }
        }

        /// <summary>
        /// Tells the RobotDuck to find a new target.
        /// </summary>
	    private void FindNewTarget()
        {
            if (!this.isServerForObject)
                return;

	        //Get list of valid targets
	        var possibleThings = new List<Thing>();
	        possibleThings.AddRange(MserGlobals.GetListOfThingsInterface<IAmADuck>());
	        possibleThings.AddRange(MserGlobals.GetListOfThings<RCCar>());
	        possibleThings.AddRange(MserGlobals.GetListOfThings<YellowBarrel>());
	        possibleThings.AddRange(MserGlobals.GetListOfThings<BlueBarrel>());
	        possibleThings.AddRange(MserGlobals.GetListOfThings<CarBody>());

	        //Exclude invalid targets
	        possibleThings.Remove(this);
	        possibleThings.Remove(this.summoner);

	        //Select a new possible target
	        while (possibleThings.Count > 0)
	        {
	            var possibleTarget = possibleThings[Rando.Int(possibleThings.Count - 1)];

	            //Check for target validity
	            if (possibleThings.Count == 0 && this.target == null)
	            {
	                //No targets in level, be sad
	                this.noTargets = true;

	                Program.LogLine("No more targets in the level. Sad!");
	            }
	            else if (AI.CanReach(AI.NearestNode(this.position), possibleTarget)) //TODO: RoboDuck - Detect reachability of target
	            {
	                //Set if target is reachable and not my owner
	                this.target = possibleTarget;

	                Program.LogLine("Set target to a " + this.target.GetType().Name + " (distance: " +
	                                Vec2.Distance(this.position, this.target.position) + ")");

	                break;
	            }
	            else
	            {
	                //Not a valid target
	                Program.LogLine("Invalid target " + possibleTarget.GetType().Name + " removed (distance: " +
	                                Vec2.Distance(this.position, possibleTarget.position) + ")");

	                possibleThings.Remove(possibleTarget);
	            }
	        }
	    }

	    /// <summary>
        /// Call every update while spawning. Handles SFX and VFX during spawning.
        /// </summary>
        public void DoSpawningLogic()
        {
            //Currently spawning
            if ((this._spriteSpawning.imageIndex == 4 && this._sparkCount < 2) ||
                (this._spriteSpawning.imageIndex == 6 && this._sparkCount < 4))
            {
                //Create sparks at extreme frames
                this._sparkCount++;
                Level.Add(Spark.New(this.x, this.y - 2, new Vec2(Rando.Float(-1f, 1f), Rando.Float(-1f, 1f)), 0.02f));
            }

            if (this._spriteSpawning.imageIndex == 3 && this._soundIndex == 0)
            {
                //Raise head
                this._soundIndex++;
                SFX.Play(GetPath("sounds/rdRaise.wav"));
            }
            else if ((this._spriteSpawning.imageIndex == 8 && this._soundIndex == 1) || (this._spriteSpawning.imageIndex == 7 && this._soundIndex == 2))
            {
                //Beep noise
                this._soundIndex++;
                SFX.Play(GetPath("sounds/rdBeep.wav"));
            }

            if (this._spriteSpawning.imageIndex == 9)
            {
                //RobotDuck has spawned
                base.graphic = this._spriteBase;
                spawning = false;

                //Graphics.flashAdd = 1.3f;
                //Layer.Game.darken = 1.3f;

                SFX.Play(GetPath("sounds/rdCreated.wav"), 1f, 0f, 0f, false);
                this._soundMotor.lerpVolume = 0.5f;
            }
        }

        public override bool Hit(Bullet bullet, Vec2 hitPos)
        {
            if (this.spawning)
                return false;

            //TODO: RoboDuck - Should use health (have damaged sprite, do metal hit sound, kill if no health left). Should leak oil too.

            //Create sparks where shot
            for (int i = 0; i < 5; i++)
            {
                Level.Add(Spark.New(hitPos.x, hitPos.y, new Vec2(Rando.Float(-1f, 1f), Rando.Float(-1f, 1f)), 0.01f));
            }

            return false;
        }

	    public override void ExitHit(Bullet bullet, Vec2 exitPos)
	    {
            if (this.spawning)
                return;

            //Create sparks where exitted
            for (int i = 0; i < 5; i++)
            {
                Level.Add(Spark.New(exitPos.x, exitPos.y, new Vec2(Rando.Float(-1f, 1f), Rando.Float(-1f, 1f)), 0.01f));
            }
        }

	    protected override bool OnDestroy(DestroyType type = null)
        {
            //Remove from followcam
            FollowCam followCam = Level.current.camera as FollowCam;
            if (followCam != null)
            {
                followCam.Remove(this);
            }

            //TODO: RobotDuck - explosion + sound + gibs + oil

            //Remove it from existence
            Level.Remove(this);

            return true;
        }

        public override void Terminate()
        {
            this._soundMotor.Kill();

            base.Terminate();
        }

        /// <summary>
        /// Generates PathNodes for the current Level.
        /// </summary>
        private void GeneratePathNodes()
        {
            //Check count of PathNodes in Level
            var nodeList = MserGlobals.GetListOfThings<PathNode>();

            if (!nodeList.Any())
            {
                //Generate nodes
                var startPos = Extensions.RoundVec2(this.position / 16) * 16;
                var png = new PathNodeGenerator();
                png.GenerateNodes(startPos);
                var nodes = png.nodes;
                //TODO: RobotDuck - send over newly generated nodes instead of having each client generate them theirselves, UNLESS
                //  this is reliable and goes as fast as it should

                if (nodes != null && nodes.Count > 0)
                {
                    Program.LogLine("Generated " + nodes.Count + " nodes!");
                    this.pathNodesGenerated = true;

                    //TODO: RobotDuck - Find a way to speed InitializeLevelPaths up! Is the current amount of nodes needed?
                    //  Only position ABOVE any blocks rather than also allowing them all around them???
                    //  New thought: can the connections be created as the nodes are being generated? Should the nodes be serialized and cached to a file
                    //  Store node setup in another file, for each level, in order to reduce load times (similar to snippets.dat)?
                    AI.InitializeLevelPaths();
                }
            }
            else
            {
                //Set PathNodes to current ones
                this.pathNodesGenerated = true;
            }
        }

	    /// <summary>
        /// Creates the sprites used by the RoboDuck, including its animations.
        /// </summary>
        private void SetupSprites()
        {
            //Base robot
            this._spriteBase = new SpriteMap("survival/robot", 32, 32, false);
            this._spriteBase.AddAnimation("idle", 0.4f, true, new int[]
            {
                0,
                1,
                2,
                3,
                4,
                5,
                6,
                7
            });
            this._spriteBase.AddAnimation("walk", 0.2f, true, new int[]
            {
                8,
                9,
                10,
                11,
                12,
                13,
                14,
                15
            });
            this._spriteBase.SetAnimation("idle");

            //Spawning
            this._spriteSpawning = new SpriteMap(base.GetPath("robotDuckSpawning"), 32, 32, false);
            this._spriteSpawning.AddAnimation("spawn", 0.35f, false, new int[]
            {
                0,
                1,
                2,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                7,
                7,
                7,
                8,
                7,
                8,
                7,
                10,
                11,
                12,
                13,
                14,
                15,
                9
            });
            this._spriteSpawning.SetAnimation("spawn");

            //Eye Glow
            this._spriteGlow = new Sprite("redHotGlow", 0f, 0f);
            this._spriteGlow.CenterOrigin();
            this._spriteGlow.alpha = 0.2f;
        }
    }
}
