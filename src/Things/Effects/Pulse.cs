﻿namespace DuckGame.MserDuck
{
    public class Pulse : Thing
    {
        private readonly float lifetime;
        private readonly float fadeFrom;
        private readonly float linesize;
        private readonly float maxrad; //How rad can you get!?
        private readonly Color col;

        private float rad = 0.000000001f; //micronanopico
        private float life;
        private float alpu = 1f;
        /// <summary>
        /// A circular pulse effect. Additive, expands over time starting from a raduis of zero, then fades out.
        /// </summary>
        /// <param name="xpos">Center of pulse</param>
        /// <param name="ypos">Center of pulse</param>
        /// <param name="color">The color of the pulse, keep in mind it is additive</param>
        /// <param name="maxRadius">The max radius the pulse will reach</param>
        /// <param name="linesize">The thickness of the pulse (too thick = artifacts/gaps!)</param>
        /// <param name="lifetime">How long the pulse will take to expand to maxRadius, in seconds</param>
        /// <param name="fadeFrom">At which position in its lifetime will the pulse start fading out. 0 = not, 1 = from the start, 0.5 = from half of lifetime.</param>
        public Pulse(float xpos, float ypos, Color color, float maxRadius = 50f, float linesize = 1f, float lifetime = 2f, float fadeFrom = 0.25f) : base(xpos, ypos)
        {
            base.graphic = null;
            this.enablePhysics = false;
            base.depth = 1f;
            //this.material = new Material("Shaders/basicAdd");
            this.layer = Layer.Glow;

            this.lifetime = lifetime;
            this.maxrad = maxRadius;
            this.col = color;
            this.fadeFrom = fadeFrom;
            this.linesize = linesize;

            this.life = lifetime;
        }

        public override void Update()
        {
            float delta = Maths.IncFrameTimer();

            life -= delta;

            //Ded yet?
            if (life < 0f)
            {
                Level.Remove(this);
            }

            //lerp a rad, I hope is not bad...
            rad = maxrad * (1f - (life / lifetime));

            //And I disappear; A ghost amidst the combat; Preparing to strike...
            alpu = Maths.Clamp(life / (lifetime * fadeFrom),0f,1f);
        }

        public override void Draw()
        {
            //base.Draw();
            Graphics.DrawCircle(this.position, rad, col*alpu, linesize, 1f);
            
        }

        public override void Terminate()
        {
            base.Terminate();
        }


       

      

    }
}
