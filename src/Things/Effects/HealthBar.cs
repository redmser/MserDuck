﻿using System.Reflection;

namespace DuckGame.MserDuck
{
    public class HealthBar : Thing
    {
        /// <summary>
        /// The duck.
        /// </summary>
        public Duck Duck;

        /// <summary>
        /// Duck's health from 0 to 100
        /// </summary>
        public byte Health = 100;

        private FieldInfo _killed = MserGlobals.GetField(typeof(Duck), "_killed");

        public StateBinding DuckBinding = new StateBinding("Duck");

        public StateBinding HealthBinding = new StateBinding("Health");

        public HealthBar(Duck duck) : base(0, 0)
        {
            this.Duck = duck;
        }

        public override void Draw()
        {
            if (this.Duck == null) return;

            //Render HP above the duck
            Graphics.DrawRect(this.Duck.position - new Vec2(50, 20), this.Duck.position - new Vec2(-50, 16), Color.Black * 0.6f, 1);
            Graphics.DrawRect(this.Duck.position - new Vec2(49, 19), this.Duck.position - new Vec2(-49 + (100 - this.Health), 17), Color.Red, 1);
        }

        public override void Update()
        {
            //BUG: HealthBar - Ducks currently can not die, even if Health is 0

            if (this.Duck == null) return;

            //This quits out of the Kill-block ASAP.
            _killed.SetValue(this.Duck, true);

            if (this.Health == 0)
            {
                //Kill the duck for real
                _killed.SetValue(this.Duck, false);
                this.Duck.Kill(new DTImpale(this));

                //Remove me
                Level.Remove(this);
            }
            else if (IsDamaged(this.Duck))
            {
                //Reached short kill block, undo!
                this.Duck.dead = false;

                if (this.Health < 7)
                    this.Health = 0;
                else
                    this.Health -= 7;
            }

            base.Update();
        }

        private static bool IsDamaged(Duck duck)
        {
            //TODO: HealthBar - Fill up with conditions that would kill the duck

            var collisionP1 = new Vec2(duck.left, duck.top);
            var collisionP2 = new Vec2(duck.left + duck.collisionSize.x, duck.top + duck.collisionSize.y);

            if (Level.current.CollisionRect<Bullet>(collisionP1, collisionP2) != null)
                return true;

            if (Level.current.CollisionRect<DizzyStar>(collisionP1, collisionP2) != null)
                return true;

            return false;
        }
    }
}
