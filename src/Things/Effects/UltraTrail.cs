﻿namespace DuckGame.MserDuck
{
    internal class UltraTrail : Thing
    {
        private bool created;

        private Sprite sprite;

        private float wait;

        public UltraTrail(float xpos, float ypos) : base(xpos, ypos, null)
        {
            this.sprite = new Sprite(Mod.GetPath<MserDuck>("ntsmoke/sprSmoke_"+Rando.Int(0, 4).ToString()));
            base.graphic = this.sprite;
            base.xscale = 1f;
            base.yscale = base.xscale;
            this.center = new Vec2(12f, 12f);
            base.depth = Rando.Float(0.2f, 1f);
            this.wait = 0f;
            this.rotDir = Rando.ChooseInt(-1, 1);
        }

        public void CreateNew(string path, float xscale, float yscale, Vec2 spriteOffsets)
        {
            this.sprite = new Sprite(Mod.GetPath<MserDuck>(path));
            base.graphic = this.sprite;
            base.xscale = 1f;
            base.yscale = base.xscale;
            this.center = new Vec2(12f, 12f);
            base.depth = Rando.Float(0.2f, 1f);
            this.wait = 0f;
            this.rotDir = Rando.ChooseInt(-1, 1);
        }

        public override void Update()
        {
            this.xscale -= 0.03f;
            this.yscale = this.xscale;
            this.angle += 0.1f * this.rotDir;
            if (!this.created)
            {
                this.created = true;
            }
            if(this.xscale <= 0)
            {
                Level.Remove(this);
            }
        }

        public override void Draw()
        {
            if ((double)this.wait > 0.0)
            {
                this.wait -= 0.1f;
                return;
            }
            base.Draw();
        }

        int rotDir;
    }
}