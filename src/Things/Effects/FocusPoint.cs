﻿namespace DuckGame.MserDuck
{
    /// <summary>
    /// Spawnable thing which gets added to the follow cam of a level, always leaving the point in view somehow.
    /// </summary>
    [EditorGroup("stuff|mser")]
    public class FocusPoint : Thing
    {
        private bool _inited;

        public FocusPoint(float xval, float yval) : base(xval, yval)
        {
            base.graphic = new Sprite("ball");
            this.center = new Vec2(8f, 8f);
            this.collisionOffset = new Vec2(-8f, -8f);
            this.collisionSize = new Vec2(16f, 16f);
            base.scale = new Vec2(0.5f, 0.5f);
            base.editorOffset = new Vec2(0f, -8f);
        }

        public override void Update()
        {
            base.Update();

            if (!this._inited)
            {
                //Add to followcam
                var cam = Level.current.camera as FollowCam;
                if (cam != null)
                {
                    cam.Add(this);
                    this._inited = true;
                }
            }
        }

        public override void Draw()
        {

        }
    }
}
