﻿using System;

namespace DuckGame.MserDuck
{
    public class FirecrackerMP : PhysicsParticle, ITeleport
    {

        public FirecrackerMP(float xpos, float ypos, float ang = 0f) : base(xpos, ypos)
        {
            base.graphic = new Sprite("fireCracker", 0f, 0f);
            this.center = new Vec2(4f, 4f);
            this._bounceSound = "plasticBounce";
            this._airFriction = 0.02f;
            this._bounceEfficiency = 0.65f;
            this._spinAngle = ang;
        }

        public override void Update()
        {
            base.Update();
            this._life = 1f;
            base.angleDegrees = this._spinAngle;
            if (this._sparkTimer)
            {
                Level.Add(Spark.New(base.x, base.y - 2f, new Vec2(Rando.Float(-1f, 1f), -0.5f), 0.1f));
            }
            if (this._explodeTimer)
            {
                SFX.Play("littleGun", Rando.Float(0.8f, 1f), Rando.Float(-0.5f, 0.5f), 0f, false);
                for (int i = 0; i < 8; i++)
                {
                    float num = (float)i * 45f - 5f + Rando.Float(10f);
                    ATShrapnel aTShrapnel = new ATShrapnel();
                    aTShrapnel.range = 8f + Rando.Float(3f);
                    Level.Add(new Bullet(base.x + (float)(Math.Cos((double)Maths.DegToRad(num)) * 6.0), base.y - (float)(Math.Sin((double)Maths.DegToRad(num)) * 6.0), aTShrapnel, num, null, false, -1f, false, true)
                    {
                        firedFrom = this
                    });
                }
                Level.Add(SmallSmoke.New(base.x, base.y));
                if (Rando.Float(1f) < 0.1f)
                {
                    Level.Add(SmallFire.New(base.x, base.y, 0f, 0f, false, null, true, this, false));
                }
                Level.Remove(this);
            }
        }

        private ActionTimer _explodeTimer = Rando.Float(0.01f, 0.012f);

        private ActionTimer _sparkTimer = 0.2f;
    }
}
