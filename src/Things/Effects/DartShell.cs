﻿namespace DuckGame.MserDuck
{
    public class DartShell : EjectedShell
    {
        public DartShell(float xpos, float ypos) : base(xpos, ypos, Thing.GetPath<MserDuck>("sundartshell"), "metalBounce")
        {
        }
    }
}
