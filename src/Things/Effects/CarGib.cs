﻿using System;

namespace DuckGame.MserDuck
{
    public class CarGib : PhysicsParticle
    {
        public CarGib(float xpos, float ypos) : base(xpos, ypos)
        {
            base.graphic = new SpriteMap(this.GetPath("cargibs"), 16, 16);
            this.center = new Vec2(8f, 8f);
            this._bounceSound = "plasticBounce";
            this._airFriction = 0.02f;
            this._bounceEfficiency = 0.3f;
            this.frame = Rando.Int(0, 2);
        }

        public override void Update()
        {
            base.Update();

            if (!this._grounded)
                base.angleDegrees += this.hSpeed >= 0 ? 5 : -5;
        }
    }
}
