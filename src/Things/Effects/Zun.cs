﻿namespace DuckGame.MserDuck
{
    [EditorGroup("stuff|mser"), BaggedProperty("canSpawn", false)]
    public class Zun : Thing
    {
        private float lifespan = 15f;
        private StateBinding lifespanBinding = new StateBinding("lifespan");
        private const float pulsecycletotal = 3f;
        private float pulsecyclecurrent = 2.8f;

        private byte r;
        private byte g;
        private byte b;
        private StateBinding rBinding = new StateBinding("r");
        private StateBinding gBinding = new StateBinding("g");
        private StateBinding bBinding = new StateBinding("b");

        private Color col
        {
            get { return new Color(r, g, b); }
            set
            {
                r = value.r;
                g = value.g;
                b = value.b;
            }
        }
        private Color pulsecol;

        private const float panscalar = 90f;
        private const float volscalar = 21000; //squared for SPEED!

        private Sound snd_zun = null; //ZUN ZUN ZUUUUN



        public Zun(float xpos, float ypos, byte r, byte g, byte b, float life=15f) : base(xpos, ypos)
        {
            this.r = r;
            this.g = g;
            this.b = b;
            base.graphic = new Sprite(GetPath("sun"), 0f, 0f);
            this.center = new Vec2(4f, 4f);
            this.enablePhysics = false;
            base.depth = 1f;
            lifespan = life;
            this.col = Color.White.MergeWith(new Color(r,g,b), 0.35f);
            pulsecol = new Color(r,g,b);
        }

        public override void Update()
        {

            float delta = Maths.IncFrameTimer();
            this.lifespan -= delta;
            this.pulsecyclecurrent += delta;

            if (lifespan < 0f && this.isServerForObject)
            {
                Level.Remove(this);
            }

            if (pulsecyclecurrent > pulsecycletotal)
            {
                pulsecyclecurrent -= pulsecycletotal;
                pulse();
                
            }

            sound_update();
        }

        public override void Draw()
        {
            base.graphic.color = this.col;
            base.Draw();
        }

        public override void Terminate()
        {
            if (snd_zun != null)
            {
                snd_zun.Stop();
            }
            base.Terminate();
        }

        public void pulse()
        {
            Level.Add(new Pulse(this.x, this.y, this.pulsecol, 60f, 1f, 2f, 0.25f));
        }

        public void sound_update()
        {

            //Get pan
            Duck local = MserGlobals.GetLocalDuck();
            float fire_pan = 0f;
            float vol = 1f;

            if (local != null)
            {
                Vec2 diff = (local.position - this.position);
                vol = 0.2f + (0.8f*((volscalar - System.Math.Min(diff.LengthSquared(),volscalar)) / volscalar));
                fire_pan = (System.Math.Min(System.Math.Abs(diff.x),panscalar)/ panscalar) * System.Math.Sign(-diff.x);
            }

            //play sound
            if (this.snd_zun == null)
            {
                this.snd_zun = SFX.Play(GetPath("sounds/sunhumming.wav"), vol, 0f, fire_pan, true);
            } else
            {
                this.snd_zun.Pan = fire_pan;
                this.snd_zun.Volume = vol;
            }

        }

        

    }
}
