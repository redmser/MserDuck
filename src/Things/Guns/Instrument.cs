﻿using System;
using System.Collections.Generic;

namespace DuckGame.MserDuck
{
    [BaggedProperty("isFatal", false), EditorGroup("guns|mser"), BaggedProperty("isSuperWeapon", true)]
    public class Instrument : Gun
    {
        public StateBinding _notePitchBinding = new StateBinding("notePitch", -1, false);

        public StateBinding _handPitchBinding = new StateBinding("handPitch", -1, false);
		
		public StateBinding _instrBinding = new StateBinding("instr", -1, false);

		public int instr = -1;
		
		private int lastinstr = -1;

        private readonly string[] _spritenames = {"acoust", "bass", "guitar", "violin", "flute", "acidtrumpet", "scififlute"};

        private readonly string[] _soundnames = {"guitar", "bass", "overdrive", "strings", "flute", "acidtrumpet", "sflute"};

        private readonly bool[] _soundreps = {false, false, false, false, true, false, true};

        public Sprite tromboneSlide = null;

        private float _slideVal;

        public float notePitch;

        public float handPitch;

        private float prevNotePitch;

        private float hitPitch;

        private Sound noteSound;

        public Instrument(float xval, float yval, int instr) : base(xval, yval)
        {
            this.ammo = 4;
            this._ammoType = new ATLaser();
            this._ammoType.range = 170f;
            this._ammoType.accuracy = 0.8f;
            this._type = "gun";
            base.graphic = new Sprite(base.GetPath("acoust"), 0f, 0f);
            this.center = new Vec2(20f, 18f);
            this.collisionOffset = new Vec2(-4f, -7f);
            this.collisionSize = new Vec2(8f, 16f);
            base.depth = 0.6f;
            this._barrelOffsetTL = new Vec2(24f, 16f);
            this._fireSound = "smg";
            this._fullAuto = true;
            this._fireWait = 1f;
            this._kickForce = 3f;
            this._holdOffset = new Vec2(6f, 2f);
            this._notePitchBinding.skipLerp = true;
			this._instrBinding.skipLerp = true;
            this.instr = instr;
        }

        /// <summary>
        /// Axis to pitch instruments on. Set using <see cref="ConfigEntries.PitchAxisEntry"/> in config.
        /// </summary>
        public static int PitchAxis { get; set; }

        public override void Initialize()
        {
            base.Initialize();
			
			//Set instrument type
            if (this.instr == -1)
            {
                this.instr = Rando.Int(0, this._spritenames.Length - 1);
            }
        }

        public override void Update()
        {
			if (this.lastinstr != this.instr)
			{
                //Update on changed state
                this.lastinstr = this.instr;

                if (this.instr >= 0)
                {
                    if (this.instr == 5)
                    {
                        //TRUMPTAC
                        var map = new SpriteMap(base.GetPath(this._spritenames[this.instr]), 32, 24);
                        map.CenterOrigin();
                        map.AddAnimation("drip", 0.25f, true, 0,1,2,3,4,5,6,7);
                        map.SetAnimation("drip");
                        base.graphic = map;
                        this.center = new Vec2(13,5);
                        this._holdOffset = new Vec2(-2, -5);
                        this.collisionSize = new Vec2(8f, 12f);
                    }
                    else if (this.instr == 6)
                    {
                        //FLUTSCIF
                        var map = new SpriteMap(base.GetPath(this._spritenames[this.instr]), 32, 32);
                        map.CenterOrigin();
                        map.AddAnimation("flash", 0.7f, true, 0, 1, 2, 3, 4, 5, 6, 7);
                        map.SetAnimation("flash");
                        base.graphic = map;
                    }
                    else
                    {
                        base.graphic = new Sprite(base.GetPath(this._spritenames[this.instr]), 0f, 0f);
                    }
                }
				else if (this.instr == -2)
                {
                    base.graphic = new Sprite("tromboneBody", 0f, 0f);
                    this.tromboneSlide = new Sprite("tromboneSlide", 0f, 0f);
                    this.tromboneSlide.CenterOrigin();
                    this.collisionSize = new Vec2(8f, 11f);
                    this.collisionOffset = new Vec2(-4f, -5f);
                    this._barrelOffsetTL = new Vec2(19f, 14f);
                    this.center = new Vec2(10f, 16f);
                }
                else if (this.instr == -3)
                {
                    base.graphic = new Sprite("saxaphone", 0f, 0f);
                }
                else
                {
                    throw new DllNotFoundException("IM SCARED");
                }
            }
			
			if (this.instr != -1)
			{
				Duck duck = this.owner as Duck;
				if (duck != null)
				{
					if (base.isServerForObject)
					{
                        if (duck.inputProfile.leftTrigger > 0)
                        {
                            //Controller? Use LT
                            this.handPitch = duck.inputProfile.leftTrigger;
                        }
                        else
                        {
                            //Keyboard? Use mouse
                            this.handPitch = Instrument.GetMousePitch(false);
                        }
                        if (Mouse.left == InputState.Down)
                        {
                            //Play instrument
                            this._triggerHeld = true;
                        }

						if (duck.inputProfile.Down("SHOOT"))
						{
							this.notePitch = this.handPitch + 0.01f;
						}
						else
						{
							this.notePitch = 0f;
						}
					}

                    var pitch = Maths.Clamp((this.notePitch - this.hitPitch) * 0.1f, -1f, 1f);
                    var num = (int)Math.Round(this.notePitch * 12f);
                    if (num < 0)
                    {
                        num = 0;
                    }
                    if (num > 12)
                    {
                        num = 12;
                    }

                    if (this.notePitch != this.prevNotePitch)
					{
						if (this.notePitch != 0f)
						{
							if (this.noteSound == null)
							{
								this.hitPitch = this.notePitch;

                                Sound sound = null;

                                if (this.instr > -1)
                                {
                                    sound = SFX.Play(base.GetPath("sounds/" + this._soundnames[this.instr] + num + ".wav"), 1f, 0f, 0f, false);
                                }
                                else if (this.instr == -2)
                                {
                                    sound = SFX.Play("trombone" + num, 1f, 0f, 0f, false);
                                }
                                else if (this.instr == -3)
                                {
                                    sound = SFX.Play("sax" + num, 1f, 0f, 0f, false);
                                }
                                else
                                {
                                    throw new DivideByZeroException("FFS");
                                }

                                this.noteSound = sound;
								Level.Add(new MusicNote(base.barrelPosition.x, base.barrelPosition.y, base.barrelVector));
							}
							else
							{
                                this.noteSound.Pitch = pitch;
							}
						}
						else if (this.noteSound != null)
						{
							this.noteSound.Stop();
							this.noteSound = null;
						}
					}

                    if (this.instr >= 0 && _soundreps[this.instr] && this.notePitch != 0f && (pitch > 0.8f || num == 12))
                    {
                        var windows = Level.CheckCircleAll<Window>(this.position, 64);
                        foreach (var wi in windows)
                        {
                            wi.hitPoints -= Rando.Float(0.1f, 0.15f);
                            wi.Shake();
                        }

                        var tvs = Level.CheckCircleAll<TV>(this.position, 64);
                        foreach (var tv in tvs)
                        {
                            tv._hitPoints -= Rando.Float(0.1f, 0.15f);
                            if (tv._hitPoints < -5f)
                                tv.Destroy(new DTImpact(this));
                        }
                    }

                    if (this._raised)
					{
						this.handAngle = 0f;
						this.handOffset = new Vec2(0f, 0f);
						this._holdOffset = new Vec2(0f, 2f);
						this.collisionOffset = new Vec2(-4f, -7f);
						this.collisionSize = new Vec2(8f, 16f);
						this.OnReleaseAction();
					}
					else
					{
                        if (this.instr == -2)
                        {
                            //Trombone
                            this.handOffset = new Vec2(6f + (1f - this.handPitch) * 4f, -4f + (1f - this.handPitch) * 4f);
                            this.handAngle = (1f - this.handPitch) * 0.4f * (float)this.offDir;
                            this._holdOffset = new Vec2(5f + this.handPitch * 2f, -9f + this.handPitch * 2f);
                            this.collisionOffset = new Vec2(-4f, -7f);
                            this.collisionSize = new Vec2(2f, 16f);
                            this._slideVal = 1f - this.handPitch;
                        }
                        else
                        {
                            this.handOffset = new Vec2(5f + (1f - this.handPitch) * 2f, -2f + (1f - this.handPitch) * 4f);
                            this.handAngle = (1f - this.handPitch) * 0.4f * (float)this.offDir;
                            this._holdOffset = new Vec2(4f + this.handPitch * 2f, this.handPitch * 2f);
                            this.collisionOffset = new Vec2(-1f, -7f);
                            this.collisionSize = new Vec2(2f, 16f);
                        }
                    }
				}
				else
				{
                    if (this.instr == -2)
                    {
                        //Trombone
                        this.collisionOffset = new Vec2(-4f, -5f);
                        this.collisionSize = new Vec2(8f, 11f);
                    }
                    else
                    {
                        this.collisionOffset = new Vec2(-4f, -7f);
                        this.collisionSize = new Vec2(8f, 16f);
                    }
				}
				this.prevNotePitch = this.notePitch;
				base.Update();
			}
        }

        /// <summary>
        /// Gets the instrument's pitch value depending on the mouse position and pitch axis.
        /// <para />Values are intended to be between -1 and 1, but must be clamped or modulo'd to be assured.
        /// </summary>
        /// <param name="normalized">True to have a range from 0 to 1, false to use -1 to 1.</param>
        /// <returns></returns>
        public static float GetMousePitch(bool normalized)
        {
            float value;
            switch (Instrument.PitchAxis)
            {
                //TODO: Instrument - handPitch config entry for scale multiplier
                case 1:
                    //X
                    value = 1 - (Mouse.x / Layer.HUD.camera.width * 2);
                    break;
                case 2:
                    //Y
                    value = 1 - (Mouse.y / Layer.HUD.camera.height * 2);
                    break;
                default:
                    //Unknown or case 0? No pitch
                    value = 0;
                    break;
            }

            if (normalized)
                return (value + 1) / 2;
            return value;
        }

        public override void Draw()
        {
            base.Draw();
            if (this.tromboneSlide != null)
            {
                base.Draw(this.tromboneSlide, new Vec2(6f + this._slideVal * 8f, 0f), -1);
            }
        }

        public override void OnPressAction()
        {
        }

        public override void OnReleaseAction()
        {
        }

        public override void Fire()
        {
        }
    }
}
