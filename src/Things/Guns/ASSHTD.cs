﻿using System;

namespace DuckGame.MserDuck
{
    [BaggedProperty("isFatal", false), EditorGroup("guns|mser")]
    public class ASSHTD : Gun
    {
        public ASSHTD(float xval, float yval) : base(xval, yval)
        {
            this.ammo = 99;
            this.graphic = new Sprite(Mod.GetPath<MserDuck>("teleporter"));
            this.center = new Vec2(7f, 11f);
            this.collisionOffset = new Vec2(-3f, -9f);
            this.collisionSize = new Vec2(6f, 13f);
            this._type = "gun";
            this._holdOffset = new Vec2(-2f, 0f);
            this.glitched = false;
        }
        public override void Initialize()
        {
            base.Initialize();

            Rando.generator = new Random(NetRand.currentSeed);
        }
        public override void OnPressAction()
        {
            if (!glitched)
            {
                if (this.duck == null)
                {
                    return;
                }

                this.xTo = this.x;
                this.yTo = this.y;

                // check if player wants to go in a vertical direction instead
                if (this.duck.inputProfile.Down("DOWN"))
                {
                    this.yTo += 64f;
                }
                else if (this.duck.inputProfile.Down("UP"))
                {
                    this.yTo -= 64f;
                }
                else
                {
                    this.xTo += this.offDir * 80f;

                    var VecToTwo = new Vec2(this.xTo, this.yTo);

                    if (Level.current.CollisionRect<Block>(
                    Vec2.Subtract(VecToTwo, this.duck.collisionOffset),
                    Vec2.Add(VecToTwo, this.duck.collisionSize)) != null)
                    {
                        this.xTo = this.x + this.offDir * 40f;
                    }
                }

                var VecTo = new Vec2(this.xTo, this.yTo);

                var hasFoundSpot = false;

                for (int i = 0; i >= -64; i -= 8)
                {
                    var VecToTwo = new Vec2(this.xTo, this.yTo + i);

                    if (Level.current.CollisionRect<Block>(
                    Vec2.Subtract(VecToTwo, this.duck.collisionOffset),
                    Vec2.Add(VecToTwo, this.duck.collisionSize)) == null)
                    {
                        // block doesnt exist, so
                        this.yTo += i;
                        VecTo = new Vec2(this.xTo, this.yTo);
                        hasFoundSpot = true;
                        break;
                    }
                }

                if (!hasFoundSpot)
                {
                    for (int i = 0; i <= 64; i += 8)
                    {
                        var VecToTwo = new Vec2(this.xTo, this.yTo + i);

                        if (Level.current.CollisionRect<Block>(
                        Vec2.Subtract(VecToTwo, this.duck.collisionOffset),
                        Vec2.Add(VecToTwo, this.duck.collisionSize)) == null)
                        {
                            // block doesnt exist, so
                            this.yTo += i;
                            VecTo = new Vec2(this.xTo, this.yTo);
                            break;
                        }
                    }
                }

                SFX.Play(Mod.GetPath<MserDuck>("sounds/teleport"), 1f, Rando.Float(-0.2f, 0.2f));

                Level.Add(new GlitchExplosion(this.position.x, this.position.y - 1f, true));
                Level.Add(new GlitchExplosion(this.xTo, this.yTo - 1f, true));

                // teleport duk
                this.duck.x = this.xTo;
                this.duck.y = this.yTo;
                this.duck.vSpeed = 0f;

                if (Level.current.CollisionRect<Block>(
                    Vec2.Subtract(VecTo, this.duck.collisionOffset),
                    Vec2.Add(VecTo, this.duck.collisionSize)) != null)
                {
                    // kill da duk
                    this.duck.Kill(new DTImpact(this));
                }
                else
                {
                    this.duck.sleeping = false;

                    // fuk da duk
                    if (Rando.Int(2) == 0)
                    {
                        this.duck.scale = new Vec2(Rando.Float(-2f, 2f), Rando.Float(-2f, 2f));
                    }
                    // disarm before teleporting da duk B)
                    if (Rando.Int(26) == 0)
                    {
                        this.duck.Disarm(this);

                        // glitchify the teleporter
                        this.material = new MaterialGlitch(this);
                        this.glitched = true;
                        SFX.Play("breakTV", 1f, Rando.Float(-1f, 1f), 0f, false);
                        Level.Add(new GlitchExplosion(this.x, this.y - 1f, true));
                    }
                }
            }
        }
        public override void Update()
        {
            base.Update();

            if (glitched && Rando.Int(20) == 0)
            {
                if (this.duck != null)
                {
                    this.duck.Disarm(this);
                }

                this.xTo = this.x + 40f * Rando.Float(-1, 1);
                this.yTo = this.y + 40f * Rando.Float(-1, 1);

                SFX.Play(Mod.GetPath<MserDuck>("sounds/teleport"), 0.4f, Rando.Float(-1f, 1f));

                Level.Add(new GlitchExplosion(this.position.x, this.position.y - 1f, true));
                Level.Add(new GlitchExplosion(this.xTo, this.yTo - 1f, true));

                // teleport
                var physObjs = Level.CheckCircleAll<PhysicsObject>(new Vec2(this.x, this.y), 16f);
                foreach (var physObj in physObjs)
                {
                    physObj.x = this.xTo;
                    physObj.y = this.yTo;
                    physObj.material = new MaterialGlitch(physObj);
                    physObj.scale = new Vec2(Rando.Float(-2f, 2f), Rando.Float(-2f, 2f));
                    physObj.sleeping = false;
                    physObj.angleDegrees = Rando.Float(360f);

                    if(physObj is Gun)
                    {
                        ((Gun)physObj).PressAction();
                    }
                }
                this.x = this.xTo;
                this.y = this.yTo;
                this.vSpeed = 0f;
                this.scale = new Vec2(Rando.Float(-2f, 2f), Rando.Float(-2f, 2f));
                this.sleeping = true;
                this.angleDegrees = Rando.Float(360f);
            }
        }

        public float xTo;

        public float yTo;

        public bool glitched;
        public StateBinding glitchedBinding = new StateBinding("glitched");
    }
}
