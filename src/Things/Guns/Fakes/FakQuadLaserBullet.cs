namespace DuckGame.MserDuck.Fakes
{
	public class FakQuadLaserBullet : Thing, ITeleport
	{
		public StateBinding _positionBinding = new CompressedVec2Binding("position", 2147483647);

		public StateBinding _travelBinding = new CompressedVec2Binding("travel", 20);

		private Vec2 _travel;

		private SinWave _wave = 0.5f;

		private SinWave _wave2 = 1f;

		public float timeAlive;

		public Vec2 travel
		{
			get
			{
				return this._travel;
			}
			set
			{
				this._travel = value;
			}
		}

		public FakQuadLaserBullet(float xpos, float ypos, Vec2 travel) : base(xpos, ypos, null)
		{
			this._travel = travel;
			this.collisionOffset = new Vec2(-1f, -1f);
			this._collisionSize = new Vec2(2f, 2f);
		}

		public override void Update()
		{
			this.timeAlive += 0.016f;
			this.position += this._travel * 4f;
			if (base.isServerForObject && (base.x > Level.current.bottomRight.x + 200f || base.x < Level.current.topLeft.x - 200f))
			{
				Level.Remove(this);
			}
			System.Collections.Generic.IEnumerable<MaterialThing> enumerable = Level.CheckRectAll<MaterialThing>(base.topLeft, base.bottomRight);
			foreach (MaterialThing current in enumerable)
			{
				if (current.isServerForObject)
				{
					bool destroyed = current.destroyed;
					current.Destroy(new DTIncinerate(this));
					if (current.destroyed && !destroyed)
					{
						if (Recorder.currentRecording != null)
						{
							Recorder.currentRecording.LogAction(2);
						}
						if (current is Duck && !(current as Duck).dead)
						{
							Recorder.currentRecording.LogBonus();
						}
					}
				}
			}
			base.Update();
		}

        public override void Draw()
		{
			Graphics.DrawRect(this.position + new Vec2(-4f, -4f), this.position + new Vec2(4f, 4f), new Color(255, 0, 0), base.depth, true, 1f);
			Graphics.DrawRect(this.position + new Vec2(-4f, -4f), this.position + new Vec2(4f, 4f), new Color(192, 0, 0), base.depth + 1, false, 1f);
            this.BaseDraw();
        }

        protected void BaseDraw()
        {
            base.Draw();
        }
	}
}
