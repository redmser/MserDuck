﻿namespace DuckGame.MserDuck.Fakes
{
	//Modified copy of OK47 for some custom maps.
    [BaggedProperty("canSpawn", false), EditorGroup("guns|mser|lvl_special")]
    public class OK47NoSlip : Gun
    {
        public OK47NoSlip(float xval, float yval) : base(xval, yval)
        {
            this._numBulletsPerFire = 2;
            this.ammo = 900;
            this._ammoType = new AT9mm();
            this._ammoType.bulletSpeed = 0.28f;
            this._ammoType.bulletLength = 75f;
            this._ammoType.rebound = true;
            this._ammoType.range = 9999f;
            this._ammoType.accuracy = 0.85f;
            this._ammoType.penetration = 2f;
            this._ammoType.speedVariation = 0f;
            this._type = "gun";
            base.graphic = new Sprite("ak47", 0f, 0f);
            this.center = new Vec2(16f, 15f);
            this.collisionOffset = new Vec2(-8f, -3f);
            this.collisionSize = new Vec2(18f, 10f);
            this._barrelOffsetTL = new Vec2(32f, 14f);
            this._fireSound = "deepMachineGun2";
            this._fullAuto = true;
            this._fireWait = 0.48f;
            this._kickForce = 0.5f;
            this.loseAccuracy = 0.2f;
            this.maxAccuracyLost = 0.4f;
        }
        public override void OnReleaseAction()
        {
            // NOT SO NORMAL STUFF
            if (this.duck != null) {
				this.duck.Swear();
			}
        }
    }
}
