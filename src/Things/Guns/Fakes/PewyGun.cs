﻿using System;

namespace DuckGame.MserDuck.Fakes
{
    [EditorGroup("guns|mser|spoiler"), BaggedProperty("fakSubstitute", typeof(PewPewLaser))]
    public class PewyGun : Gun
    {
        public StateBinding _burstingBinding = new StateBinding("_bursting", -1, false);

        public StateBinding _burstNumBinding = new StateBinding("_burstNum", -1, false);

        public float _burstWait;

        public bool _bursting;

        public int _burstNum;

        public PewyGun(float xval, float yval) : base(xval, yval)
        {
            this.ammo = 9000;
            this._ammoType = new ATPewPew();
            this._type = "gun";
            base.graphic = new Sprite("pewpewLaser", 0f, 0f);
            this.center = new Vec2(16f, 16f);
            this.collisionOffset = new Vec2(-8f, -3f);
            this.collisionSize = new Vec2(16f, 7f);
            this._barrelOffsetTL = new Vec2(31f, 15f);
            this._fireSound = "laserRifle";
            this._fullAuto = true;
            this._fireWait = 2f;
            this._kickForce = 1f;
            this._holdOffset = new Vec2(0f, 0f);
            this._flare = new SpriteMap("laserFlare", 16, 16, false);
            this._flare.center = new Vec2(0f, 8f);
        }

        public override void Update()
        {
			this.ammo = 9000;
            if (this._bursting)
            {
                this._burstWait = Maths.CountDown(this._burstWait, 0.16f, 0f);
                if (this._burstWait <= 0f)
                {
                    this._burstWait = 0.85f;
                    if (base.isServerForObject)
                    {
                        this.Fire();
                        NMFireGun msg = new NMFireGun(this, this.firedBullets, this.bulletFireIndex, false, (base.duck != null) ? base.duck.netProfileIndex : Convert.ToByte(4), true);
                        Send.Message(msg, NetMessagePriority.Urgent, null);
                        this.firedBullets.Clear();
                    }
                    this._wait = 0f;
                    this._burstNum++;
                }
            }
            base.Update();
        }

        public override void OnPressAction()
        {
            if (this.receivingPress && this.hasFireEvents && this.onlyFireAction)
            {
                this.Fire();
            }
            if (!this._bursting && this._wait == 0f)
            {
                this._bursting = true;
            }
        }

        public override void OnHoldAction()
        {
        }
    }
}