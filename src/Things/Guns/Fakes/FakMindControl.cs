﻿namespace DuckGame.MserDuck.Fakes
{
    [BaggedProperty("isFatal", false), EditorGroup("guns|mser|spoiler"), BaggedProperty("fakSubstitute", typeof(MindControlRay))]
    public class FakMindControl : MindControlRay
    {
        public FakMindControl(float xval, float yval) : base(xval, yval)
        {
        }

        public override void Update()
        {
            base.Update();

            var ducky = this.owner as Duck;
            if (ducky != null && this._controlledDuck != null)
                ducky.velocity = this._controlledDuck.velocity * 2;
        }
    }
}
