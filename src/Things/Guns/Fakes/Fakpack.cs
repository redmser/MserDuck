using System;

namespace DuckGame.MserDuck.Fakes
{
    [EditorGroup("guns|mser|spoiler"), BaggedProperty("fakSubstitute", typeof(Jetpack))]
    public class Fakpack : Jetpack
    {
        public Fakpack(float xpos, float ypos) : base(xpos, ypos)
		{
            this._sprite = new SpriteMap("jetpack", 16, 16, false);
            base.graphic = this._sprite;
            this.center = new Vec2(8f, 8f);
            this.collisionOffset = new Vec2(-5f, -5f);
            this.collisionSize = new Vec2(11f, 12f);
            this._offset = new Vec2(-3f, 3f);
            this._equippedDepth = -15;
            this._jumpMod = true;
            this.thickness = 0.1f;
            this._wearOffset = new Vec2(-2f, 0f);
        }

        public override void Update()
        {
            this._sprite.frame = (int)(this._heat * 7f);
            if (this._equippedDuck != null)
            {
                float num = 0f;
                PhysicsObject physicsObject = this._equippedDuck;
                if (this._equippedDuck._trapped != null)
                {
                    physicsObject = this._equippedDuck._trapped;
                }
                else if (this._equippedDuck.ragdoll != null && this._equippedDuck.ragdoll.part1 != null)
                {
                    physicsObject = this._equippedDuck.ragdoll.part1;
                }
                if (this._on && this._heat < 1f)
                {
                    if (physicsObject is RagdollPart)
                    {
                        float angle = this.angle;
                        this.angle = physicsObject.angle;
                        Vec2 vec = this.Offset(new Vec2(0f, 8f));
                        Level.Add(new JetpackSmoke(vec.x, vec.y));
                        this.angle = angle;
                        if (physicsObject.velocity.length < 7f)
                        {
                            RagdollPart ragdollPart = physicsObject as RagdollPart;
                            float num2 = -(physicsObject.angle - 1.57079637f);
                            Vec2 zero = Vec2.Zero;
                            if (this._equippedDuck.inputProfile.leftStick.length > 0.1f)
                            {
                                zero = new Vec2(this._equippedDuck.inputProfile.leftStick.x, -this._equippedDuck.inputProfile.leftStick.y);
                            }
                            else
                            {
                                zero = new Vec2(0f, 0f);
                                if (this._equippedDuck.inputProfile.Down("LEFT"))
                                {
                                    zero.x -= 1f;
                                }
                                if (this._equippedDuck.inputProfile.Down("RIGHT"))
                                {
                                    zero.x += 1f;
                                }
                                if (this._equippedDuck.inputProfile.Down("UP"))
                                {
                                    zero.y -= 1f;
                                }
                                if (this._equippedDuck.inputProfile.Down("DOWN"))
                                {
                                    zero.y += 1f;
                                }
                            }
                            if (zero.length < 0.1f)
                            {
                                zero = new Vec2((float)Math.Cos((double)num2), (float)(-(float)Math.Sin((double)num2)));
                            }
                            physicsObject.velocity -= zero * 4f;
                        }
                    }
                    else
                    {
                        Level.Add(new JetpackSmoke(base.x, base.y + 8f + num));
                        if (this.angle > 0f)
                        {
                            //if (physicsObject.hSpeed < 6f)
                            //{
                                physicsObject.hSpeed -= 5.9f;
                            //}
                        }
                        else if (this.angle < 0f)
                        {
                            //if (physicsObject.hSpeed > -6f)
                            //{
                                physicsObject.hSpeed += 5.9f;
                            //}
                        }
                        else //if (physicsObject.vSpeed > -4.5f)
                        {
                            physicsObject.vSpeed += 5.38f;
                        }
                    }
                }
            }
            base.Update();
        }
    }
}
