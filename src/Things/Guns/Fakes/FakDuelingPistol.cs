namespace DuckGame.MserDuck.Fakes
{
	[EditorGroup("guns|mser|spoiler"), BaggedProperty("fakSubstitute", typeof(DuelingPistol))]
	public class FakDuelingPistol : Gun
	{
		public FakDuelingPistol(float xval, float yval) : base(xval, yval)
		{
			this.ammo = 100;
			this._ammoType = new AT9mm();
			this._ammoType.range = 70f;
			this._ammoType.accuracy = 0.05f;
			this._ammoType.penetration = 0.4f;
			this._type = "gun";
			base.graphic = new Sprite("tinyGun", 0f, 0f);
			this.center = new Vec2(16f, 16f);
			this.collisionOffset = new Vec2(-6f, -4f);
			this.collisionSize = new Vec2(12f, 8f);
			this._barrelOffsetTL = new Vec2(20f, 15f);
			this._fireSound = "littleGun";
			this._fullAuto = true;
			this._fireWait = 0.5f;
			this._kickForce = 15f;
			this.loseAccuracy = 0.1f;
			this.maxAccuracyLost = 1f;
		}
	}
}
