﻿namespace DuckGame.MserDuck.Fakes
{
    [BaggedProperty("isFatal", false), EditorGroup("guns|mser|spoiler"), BaggedProperty("fakSubstitute", typeof(DartGun))]
    public class SuperDartGun : Gun
    {
        public StateBinding _burnLifeBinding = new StateBinding("_burnLife", -1, false);

        private SpriteMap _sprite;

        public float _burnLife = 1f;

        public float _burnWait;

        public bool burntOut;
		
		public int timer = 0;

        public StateBinding timerBinding = new StateBinding("timer");

        public SuperDartGun(float xval, float yval) : base(xval, yval)
        {
            this.ammo = 100;
            this._ammoType = new ATLaser();
            this._ammoType.range = 170f;
            this._ammoType.accuracy = 0.8f;
            this._type = "gun";
            this._sprite = new SpriteMap("dartgun", 32, 32, false);
            base.graphic = this._sprite;
            this.center = new Vec2(16f, 16f);
            this.collisionOffset = new Vec2(-8f, -4f);
            this.collisionSize = new Vec2(16f, 9f);
            this._barrelOffsetTL = new Vec2(29f, 14f);
            this._fireSound = "smg";
            this._fullAuto = true;
            this._fireWait = 0.2f;
            this._kickForce = 1f;
            this.flammable = 0.8f;
            this._barrelAngleOffset = 8f;
            this.physicsMaterial = PhysicsMaterial.Plastic;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void UpdateFirePosition(SmallFire f)
        {
            f.position = this.Offset(new Vec2(10f, 0f));
        }

        public override void UpdateOnFire()
        {
            if (base.onFire)
            {
                this._burnWait -= 0.01f;
                if (this._burnWait < 0f)
                {
                    Level.Add(SmallFire.New(10f, 0f, 0f, 0f, false, this, false, this, false));
                    this._burnWait = 1f;
                }
                if (this.burnt < 1f)
                {
                    this.burnt += 0.001f;
                }
            }
        }

        public override void Update()
        {
            if (!this.burntOut && this.burnt >= 1f)
            {
                this._sprite.frame = 1;
                Vec2 vec = this.Offset(new Vec2(10f, 0f));
                Level.Add(SmallSmoke.New(vec.x, vec.y));
                this._onFire = false;
                this.flammable = 0f;
                this.burntOut = true;
            }
            base.Update();
        }

        protected override bool OnBurn(Vec2 firePosition, Thing litBy)
        {
            base.onFire = true;
            return true;
        }

        public override void Draw()
        {
            base.Draw();
        }

        public override void OnReleaseAction()
        {
            this.timer = 0;
        }

        public override void OnHoldAction()
        {
            this.timer -= 1;
            if (this.timer <= 0)
			{
				this.timer = 3;
				if (this.ammo > 0)
				{
					if (this._burnLife <= 0f)
					{
						SFX.Play("dartStick", 0.5f, -0.1f + Rando.Float(0.2f), 0f, false);
						return;
					}
					this.ammo--;
					SFX.Play("dartGunFire", 0.5f, -0.1f + Rando.Float(0.2f), 0f, false);
					this.kick = 1f;
					if (!this.receivingPress && base.isServerForObject)
					{
						Vec2 vec = this.Offset(base.barrelOffset);
						float num = base.barrelAngle + Rando.Float(-0.1f, 0.1f);
						Dart dart = new Dart(vec.x, vec.y, this.owner as Duck, -num);
						base.Fondle(dart);
						if (base.onFire)
						{
							Level.Add(SmallFire.New(0f, 0f, 0f, 0f, false, dart, true, this, false));
							dart.burning = true;
							dart.onFire = true;
						}
						Vec2 vec2 = Maths.AngleToVec(num);
						dart.hSpeed = vec2.x * 10f;
						dart.vSpeed = vec2.y * 10f;
						Level.Add(dart);
					}
				}
				else
				{
					base.DoAmmoClick();
				}
			}
        }

        public override void Fire()
        {
        }
    }
}
