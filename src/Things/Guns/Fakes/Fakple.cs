﻿using System;
using System.Reflection;

namespace DuckGame.MserDuck.Fakes
{
    [EditorGroup("guns|mser|spoiler"), BaggedProperty("fakSubstitute", typeof(Grapple))]
    public class Fakple : Grapple
    {
        public Fakple(float xpos, float ypos) : base(xpos, ypos)
		{
            this._sprite = new SpriteMap("grappleArm", 16, 16, false);
            base.graphic = this._sprite;
            this.center = new Vec2(8f, 8f);
            this.collisionOffset = new Vec2(-5f, -4f);
            this.collisionSize = new Vec2(11f, 7f);
            this._offset = new Vec2(0f, 7f);
            this._equippedDepth = 11;
            this._barrelOffsetTL = new Vec2(10f, 4f);
            this._jumpMod = true;
            this.thickness = 0.1f;
            this._laserTex = Content.Load<Tex2D>("pointerLaser");
        }

        public new void Degrapple()
        {
            this._harpoon.Return();
            if (this._rope != null)
            {
                this._rope.RemoveRope();
            }
            this._rope = null;
        }

        public override void DrawGlow()
        {
            if (this._equippedDuck != null && this._harpoon != null && this._harpoon.inGun && this._canGrab && this._equippedDuck._trapped == null)
            {
                Graphics.DrawTexturedLine(this._laserTex, this.Offset(this.barrelOffset), this._wallPoint, Color.Red, 0.5f, base.depth - 1);
                if (this._sightHit != null)
                {
                    this._sightHit.color = Color.Red;
                    Graphics.Draw(this._sightHit, this._wallPoint.x, this._wallPoint.y);
                }
            }
            base.DrawGlow();
        }

        public new void DeserializeRope(Rope r)
        {
            if (this.ropeData.ReadBool())
            {
                if (r == null)
                {
                    this._rope = new Rope(0f, 0f, r, null, null, false, this._ropeSprite, this);
                    r = this._rope;
                }
                r.attach1 = r;
                r._thing = null;
                Level.Add(r);
                Vec2 uncompressedVec = CompressedVec2Binding.GetUncompressedVec2(this.ropeData.ReadInt(), 2147483647);
                if (r == this._rope)
                {
                    r.attach1 = r;
                    if (base.duck != null)
                    {
                        r.position = base.duck.position;
                    }
                    r._thing = base.duck;
                }
                if (r.attach2 == null || !(r.attach2 is Rope) || r.attach2 == r)
                {
                    Rope attach = new Rope(uncompressedVec.x, uncompressedVec.y, r, null, null, false, this._ropeSprite, this);
                    r.attach2 = attach;
                }
                if (r.attach2 != null)
                {
                    r.attach2.position = uncompressedVec;
                    (r.attach2 as Rope).attach1 = r;
                }
                this.DeserializeRope(r.attach2 as Rope);
                return;
            }
            if (r == this._rope)
            {
                this.Degrapple();
                return;
            }
            if (r != null)
            {
                Rope rope = r.attach1 as Rope;
                rope.TerminateLaterRopes();
                this._harpoon.Latch(r.position);
                rope.attach2 = this._harpoon;
            }
        }

        public new Rope GetRopeParent(Thing child)
        {
            for (Rope rope = this._rope; rope != null; rope = (rope.attach2 as Rope))
            {
                if (rope.attach2 == child)
                {
                    return rope;
                }
            }
            return null;
        }

        public override void Initialize()
        {
            this.hup = MserGlobals.GetMethod(typeof(Equipment), "Update");
            base.Initialize();
        }

        private MethodInfo hup;

        public override void OnPressAction()
        {
        }

        public override void OnReleaseAction()
        {
        }

        public override void OnTeleport()
        {
            this.Degrapple();
        }

        public new void SerializeRope(Rope r)
        {
            if (r != null)
            {
                this.ropeData.Write(true);
                this.ropeData.Write(CompressedVec2Binding.GetCompressedVec2(r.attach2Point, 2147483647));
                this.SerializeRope(r.attach2 as Rope);
                return;
            }
            this.ropeData.Write(false);
        }

        public override void Update()
        {
            if (this._harpoon == null)
            {
                return;
            }
            if (base.isServerForObject)
            {
                this.ropeData.Clear();
                this.SerializeRope(this._rope);
            }
            else
            {
                this.ropeData.SeekToStart();
                this.DeserializeRope(this._rope);
            }
            if (this._rope != null)
            {
                this._rope.SetServer(base.isServerForObject);
            }
            if (base.isServerForObject && this._equippedDuck != null && base.duck != null)
            {
                ATTracer aTTracer = new ATTracer();
                float num = aTTracer.range = this._grappleLength;
                aTTracer.penetration = 1f;
                float ang = 45f;
                if (this.offDir < 0)
                {
                    ang = 135f;
                }
                if (this._harpoon.inGun)
                {
                    Vec2 vec = this.Offset(this.barrelOffset);
                    if (this._lagFrames > 0)
                    {
                        this._lagFrames--;
                        if (this._lagFrames == 0)
                        {
                            this._canGrab = false;
                        }
                        else
                        {
                            ang = Maths.PointDirection(vec, this._lastHit);
                        }
                    }
                    aTTracer.penetration = 9f;
                    Bullet bullet = new Bullet(vec.x, vec.y, aTTracer, ang, this.owner, false, -1f, true, true);
                    this._wallPoint = bullet.end;
                    this._grappleTravel = bullet.travelDirNormalized;
                    num = (vec - this._wallPoint).length;
                }
                if (num < this._grappleLength - 2f && num <= this._grappleDist + 16f)
                {
                    this._lastHit = this._wallPoint;
                    this._canGrab = true;
                }
                else if (this._canGrab && this._lagFrames == 0)
                {
                    this._lagFrames = 6;
                    this._wallPoint = this._lastHit;
                }
                else
                {
                    this._canGrab = false;
                }
                this._grappleDist = num;
                if (base.duck.inputProfile.Pressed("JUMP", false) && base.duck._trapped == null)
                {
                    if (this._harpoon.inGun)
                    {
                        if (!base.duck.grounded && base.duck.framesSinceJump > 6 && this._canGrab)
                        {
                            this._harpoon.Fire(this.wallPoint, this.grappelTravel);
                            this._rope = new Rope(this.barrelPosition.x, this.barrelPosition.y, null, this._harpoon, base.duck, false, this._ropeSprite, this);
                            Level.Add(this._rope);
                        }
                    }
                    else
                    {
                        this.Degrapple();
                        this._lagFrames = 0;
                        this._canGrab = false;
                    }
                }
            }

            //Run holdable's update
            if (this.hup != null)
            {
                var ptr = this.hup.MethodHandle.GetFunctionPointer();
                var baseUpdate = (Action)Activator.CreateInstance(typeof(Action), this, ptr);
                baseUpdate();
            }

            if (this.owner != null)
            {
                this.offDir = this.owner.offDir;
            }
            if (base.duck != null)
            {
                base.duck.grappleMul = false;
            }
            if (base.isServerForObject && this._rope != null)
            {
                if (this.owner != null)
                {
                    this._rope.position = this.owner.position;
                }
                else
                {
                    this._rope.position = this.position;
                    if (base.prevOwner != null)
                    {
                        PhysicsObject physicsObject = base.prevOwner as PhysicsObject;
                        this._prevOwner = null;
                        if (base.prevOwner is Duck)
                        {
                            (base.prevOwner as Duck).grappleMul = false;
                        }
                    }
                }
                if (this._harpoon.stuck)
                {
                    if (base.duck != null)
                    {
                        if (!base.duck.grounded)
                        {
                            base.duck.frictionMult = 0f;
                        }
                        else
                        {
                            base.duck.frictionMult = 1f;
                            base.duck.gravMultiplier = 1f;
                        }
                        if (this._rope.properLength > 0f)
                        {
                            if (base.duck.inputProfile.Down("UP") && this._rope.properLength >= 16f)
                            {
                                this._rope.properLength -= 2f;
                            }
                            if (base.duck.inputProfile.Down("DOWN") && this._rope.properLength <= 256f)
                            {
                                this._rope.properLength += 2f;
                            }
                            this._rope.properLength = Maths.Clamp(this._rope.properLength, 16f, 256f);
                        }
                    }
                    else if (!base.grounded)
                    {
                        this.frictionMult = 0f;
                    }
                    else
                    {
                        this.frictionMult = 1f;
                        this.gravMultiplier = 1f;
                    }
                    Vec2 value = this._rope.attach1.position - this._rope.attach2.position;
                    if (this._rope.properLength < 0f)
                    {
                        this._rope.properLength = value.length;
                    }
                    if (value.length > this._rope.properLength)
                    {
                        value = value.normalized;
                        /*if (base.duck != null)
                        {
                            base.duck.grappleMul = true;
                            PhysicsObject duck = base.duck;
                            if (base.duck.ragdoll != null)
                            {
                                this.Degrapple();
                                return;
                            }
                            Vec2 arg_575_0 = duck.position;
                            duck.position = this._rope.attach2.position + value * this._rope.properLength;
                            Vec2 vec2 = duck.position - duck.lastPosition;
                            duck.hSpeed = vec2.x;
                            duck.vSpeed = vec2.y;
                            return;
                        }
                        else*/
                        {
                            Vec2 arg_5DC_0 = this.position;
                            this.position = this._rope.attach2.position + value * this._rope.properLength;
                            Vec2 vec3 = this.position - base.lastPosition;
                            this.hSpeed = vec3.x;
                            this.vSpeed = vec3.y;
                        }
                    }
                }
            }
        }
        
        private bool _canGrab;

        private float _grappleDist;

        private float _grappleLength = 200f;

        private int _lagFrames;

        private Tex2D _laserTex;

        private Vec2 _lastHit = Vec2.Zero;

    }
}
