﻿namespace DuckGame.MserDuck
{
    [EditorGroup("guns|mser")]
    public class PartyPooper : Gun
    {
        private float _timer = 1.2f;

        public StateBinding _timerBinding = new StateBinding("_timer", -1, false);

        private SpriteMap _sprite;

        private bool _pin = true;

        public PartyPooper(float xval, float yval) : base(xval, yval)
        {
            this.ammo = 1;
            this._type = "gun";
            this._sprite = new SpriteMap(this.GetPath("party"), 16, 16, false);
            this.graphic = this._sprite;
            this.center = new Vec2(8f, 10f);
            this.collisionOffset = new Vec2(-3f, -3f);
            this.collisionSize = new Vec2(6f, 7f);
            this.bouncy = 0.7f;
            this.friction = 0.1f;
            this._editorName = "Party Pooper";
        }

        public override void Update()
        {
            base.Update();

            if (!this._pin)
            {
                this._timer -= 0.01f;
            }

            if (this._timer < 0f)
            {
                Graphics.flashAdd = 0.3f;
                SFX.Play("hatOpen");
                SFX.Play(this.GetPath("sounds/party" + Rando.Int(1, 4) + ".wav"));
                for (var i = 0; i < 50; i++)
                {
                    var num2 = (float)i * 18f - 5f + Rando.Float(10f);
                    var xp = this.x + (float)(System.Math.Cos(Maths.DegToRad(num2)) * 3.0);
                    var xa = (float)(System.Math.Cos(Maths.DegToRad(num2)) * Rando.Float(1, 4));
                    var yp = this.y - (float)(System.Math.Sin(Maths.DegToRad(num2)) * 3.0);
                    var ya = (float)(System.Math.Sin(Maths.DegToRad(num2)) * Rando.Float(1, 4));
                    var fetti = new ConfettiNormalo(xp, yp, default(Color), new Vec2(xa, ya));
                    Level.Add(fetti);
                }
                Level.Remove(this);
                this._destroyed = true;
            }
            this._sprite.frame = (this._pin ? 0 : 1);
        }

        public override void OnPressAction()
        {
            if (this._pin)
            {
                this._pin = false;
                var pin = new GrenadePin(this.x, this.y)
                {
                    hSpeed = (float)(-(float)this.offDir) * (1.5f + Rando.Float(0.5f)),
                    vSpeed = -2f,
                };
                pin.graphic.color = new Color(15, 15, 15);
                SFX.Play("flipPage", 1f, 0f, 0f, false);
                Level.Add(pin);
            }
        }
    }
}
