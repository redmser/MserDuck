using System;
using System.Collections.Generic;
using DuckGame.MserDuck.NetMessages;

namespace DuckGame.MserDuck
{
    [EditorGroup("guns|mser")]
    public class GlitchGrenade : Gun
    {
        private float _timer = 1.2f;

        private bool _pin = true;

        public bool exploded = false;

        private SpriteMap _sprite;

        public GlitchGrenade(float xpos, float ypos) : base(xpos, ypos)
        {
            this._editorName = "Glitch Grenade";
            this.ammo = 1;
            this._ammoType = new ATShrapnel();
            this._ammoType.penetration = 0.4f;
            this._type = "gun";
            this._sprite = new SpriteMap(base.GetPath("glitchnade"), 16, 16, false);
            base.graphic = this._sprite;
            this.center = new Vec2(7f, 8f);
            this.collisionOffset = new Vec2(-4f, -5f);
            this.collisionSize = new Vec2(8f, 10f);
            base.bouncy = 0.1f;
            this.friction = 0.5f;
            base.material = new MaterialGlitch(this);
        }

        public override void Initialize()
        {
            //Sync rando
            if (Network.isActive)
            {
                Rando.generator = new Random(NetRand.currentSeed);
            }

            base.Initialize();
        }

        public override void Update()
        {
            base.Update();
            if (!this._pin && !this.exploded)
            {
                this._timer -= 0.01f;
                if (this._timer < 0f)
                {
                    //grenade explodes
                    Level.Add(new GlitchExplosion(this.position.x, this.position.y - 1f, true));
                    SFX.Play(base.GetPath("sounds/glitchnade.wav"), 1f, 0f, 0f, false);
                    SFX.Play("breakTV", 1f, 0f, 0f, false);
                    this.exploded = true;

                    for (int i = 0; i < 60; i++)
                    {
                        Level.Add(new GlassParticle(base.x + Rando.Float(-8f, 8f), base.y + Rando.Float(-8f, 8f), new Vec2(Rando.Float(-1f, 1f), Rando.Float(-1f, 1f)), -1));
                    }

                    if (base.isServerForObject)
                    {
                        DoNadeGlitching();
                    }

                    if (this.duck != null)
                        this.duck.ThrowItem(true);
                    Level.Remove(this);
                }
            }
        }

        private void DoNadeGlitching()
        {
            //BUG: GlitchGrenade - still does not always glitch the blocks around it. related to RNG? distance check using wrong coords?
            //  News: probably related to too many blocks being a part of the netmessages!

            HashSet<ushort> deletedBlocks = new HashSet<ushort>();
            List<BlockInfo> createdBlocks = new List<BlockInfo>();

            var blockgroups = Level.current.things[typeof(BlockGroup)];
            foreach (BlockGroup gr in blockgroups)
            {
                //Add AutoBlocks from BlockGroups to remove.
                foreach (Block bl in gr.blocks)
                {
                    if (bl is AutoBlock && !(bl is IContainAThing) && Vec2.DistanceSquared(this.position, bl.position) < 2500 /*Collision.Circle(this.position, 50f, bl.rectangle)*/)
                    {
                        bl.skipWreck = true;
                        bl.shouldWreck = true;
                        deletedBlocks.Add((bl as AutoBlock).blockIndex);

                        var blockInfo = new BlockInfo();
                        blockInfo.x = (short)(Math.Round(base.x / 16) + Rando.Int(-4, 4));
                        blockInfo.y = (short)(Math.Round(base.y / 16) + Rando.Int(-4, 4));
                        blockInfo.tileset = (byte)getTilesetForBlock(bl as AutoBlock);
                        createdBlocks.Add(blockInfo);
                    }
                }
                gr.Wreck();
            }

            NMCreateBlocks blockCreationMessage = new NMCreateBlocks(createdBlocks);
            blockCreationMessage.Activate();
            if (Network.isActive)
            {
                if (createdBlocks.Count > 0)
                {
                    Send.Message(blockCreationMessage);
                }
                if (deletedBlocks.Count > 0)
                {
                    Send.Message(new NMDestroyBlocks(deletedBlocks));
                }
            }

            var phys = Level.CheckCircleAll<PhysicsObject>(this.position, 50f);
            foreach (PhysicsObject ph in phys)
            {
                ph.sleeping = false;
            }

            //Swap sprites of nearby things.
            /*var nearbyThings = Level.CheckCircleAll<Thing>(this.position, 50f);
            var nearbyValidThings = (from x in nearbyThings where !(x is Block) && !(x is RagdollPart) && !(x is TeamHat) && !(x is Duck) select x);

            foreach (Thing nearbyThing in nearbyValidThings)
            {
                var otherValidThings = (from x in nearbyValidThings where !ReferenceEquals(x, nearbyThing) select x).ToList();
                if (otherValidThings.Count == 0)
                {
                    continue;
                }
                Thing otherThing = otherValidThings[Rando.Int(0, otherValidThings.Count - 1)];
                NMSwapSprites swapMessage = new NMSwapSprites(otherThing, nearbyThing);
                swapMessage.Apply();
                if (Network.isActive)
                {
                    Send.Message(swapMessage);
                }
            }*/
        }

        public override void OnPressAction()
        {
            if (this._pin)
            {
                this._sprite.frame = 1;
                GrenadePin Pin = new GrenadePin(this.position.x, this.position.y - 1f);
                Level.Add(Pin);
                this._pin = false;
            }
        }

        public enum Tileset
        {
            Blueprint,
            Castle,
            Donut,
            Industrial,
            Nature,
            Office,
            Space,
            Spooky,
            Underground,

            Count
        }

        public static Tileset getTilesetForBlock(AutoBlock block)
        {
            if (block is BlueprintTileset)
                return Tileset.Blueprint;
            else if (block is CastleTileset)
                return Tileset.Castle;
            else if (block is DonutTileset)
                return Tileset.Donut;
            else if (block is IndustrialTileset)
                return Tileset.Industrial;
            else if (block is NatureTileset)
                return Tileset.Nature;
            else if (block is OfficeTileset)
                return Tileset.Office;
            else if (block is SpaceTileset)
                return Tileset.Space;
            else if (block is SpookyTileset)
                return Tileset.Spooky;
            else if (block is UndergroundTileset)
                return Tileset.Underground;
            return Tileset.Nature;
        }

        public static AutoBlock generateTileFromTileset(Tileset tileset, float x, float y)
        {
            switch (tileset)
            {
                case Tileset.Blueprint:
                    return new BlueprintTileset(x, y);
                case Tileset.Castle:
                    return new CastleTileset(x, y);
                case Tileset.Donut:
                    return new DonutTileset(x, y);
                case Tileset.Industrial:
                    return new IndustrialTileset(x, y);
                case Tileset.Nature:
                    return new NatureTileset(x, y);
                case Tileset.Office:
                    return new OfficeTileset(x, y);
                case Tileset.Space:
                    return new SpaceTileset(x, y);
                case Tileset.Spooky:
                    return new SpookyTileset(x, y);
                case Tileset.Underground:
                    return new UndergroundTileset(x, y);
            }
            return new NatureTileset(x, y);
        }
    }
}
