﻿using System;
using System.Collections.Generic;

namespace DuckGame.MserDuck
{
    [EditorGroup("guns|mser")]
    public class Pickaxe : Gun
    {
        public Pickaxe(float xval, float yval) : base(xval, yval)
		{
            this.ammo = 4;
            this._ammoType = new ATLaser();
            this._ammoType.range = 170f;
            this._ammoType.accuracy = 0.8f;
            this._type = "gun";
            this._sprite = new SpriteMap(Mod.GetPath<MserDuck>("pickaxe"), 32, 32, false);
            this._sledgeSwing = new SpriteMap("sledgeSwing", 32, 32, false);
            this._sledgeSwing.AddAnimation("swing", 0.8f, false, new int[]
            {
                0,
                1,
                2,
                3,
                4,
                5
            });
            this._sledgeSwing.currentAnimation = "swing";
            this._sledgeSwing.speed = 0f;
            this._sledgeSwing.center = new Vec2(16f, 16f);
            base.graphic = this._sprite;
            this.center = new Vec2(16f, 14f);
            this.collisionOffset = new Vec2(-2f, 0f);
            this.collisionSize = new Vec2(4f, 18f);
            this._barrelOffsetTL = new Vec2(16f, 28f);
            this._fireSound = "smg";
            this._fullAuto = true;
            this._fireWait = 1f;
            this._kickForce = 3f;
            this.weight = 9f;
            this._dontCrush = false;
            base.collideSounds.Add("rockHitGround2");
        }

        public override void CheckIfHoldObstructed()
        {
            var duk = this.owner as Duck;
            if (duk != null)
            {
                duk.holdObstructed = false;
            }
        }

        public override void Draw()
        {
            if (this.owner != null && this._drawOnce)
            {
                this._offset = new Vec2((float)this.offDir * -6f + this._swing * 5f * (float)this.offDir, -3f + this._swing * 5f);
                Vec2 position = this.position + this._offset;
                base.graphic.position = position;
                base.graphic.depth = base.depth;
                Duck duck = this.owner as Duck;
                this.handOffset = new Vec2(this._swing * 3f, 0f - this._swing * 4f);
                this.handAngle = 1.4f + (this._sprite.angle * 0.5f - 1f);
                if (duck != null && duck.offDir < 0)
                {
                    this._sprite.angle = -this._sprite.angle;
                    this.handAngle = -this.handAngle;
                }
                base.graphic.Draw();
                if (this._sledgeSwing.speed > 0f)
                {
                    if (duck != null)
                    {
                        this._sledgeSwing.flipH = (duck.offDir <= 0);
                    }
                    this._sledgeSwing.position = this.position;
                    this._sledgeSwing.depth = base.depth + 1;
                    this._sledgeSwing.Draw();
                    return;
                }
            }
            else
            {
                base.Draw();
                this._drawOnce = true;
            }
        }

        public override void Fire()
        {
        }

        public override void OnPressAction()
        {
        }

        public override void OnReleaseAction()
        {
        }

        public override void OnSoftImpact(MaterialThing with, ImpactedFrom from)
        {
            if (with is IPlatform)
            {
                for (int i = 0; i < 4; i++)
                {
                    Level.Add(Spark.New(base.barrelPosition.x + Rando.Float(-6f, 6f), base.barrelPosition.y + Rando.Float(-3f, 3f), -MaterialThing.ImpactVector(from), 0.02f));
                }
            }
        }

        public override void ReturnToWorld()
        {
            this.collisionOffset = new Vec2(-2f, 0f);
            this.collisionSize = new Vec2(4f, 18f);
            this._sprite.frame = 0;
            this._swing = 0f;
            this._swingForce = 0f;
            this._pressed = false;
            this._swung = false;
            this._fullSwing = 0f;
            this._swingVelocity = 0f;
        }

        public override void Update()
        {
            if (this._lastOwner != null && this.owner == null)
            {
                this._lastOwner.frictionMod = 0f;
                this._lastOwner = null;
            }
            this.collisionOffset = new Vec2(-2f, 0f);
            this.collisionSize = new Vec2(4f, 18f);
            if (this._swing > 0f)
            {
                this.collisionOffset = new Vec2(-9999f, 0f);
                this.collisionSize = new Vec2(4f, 18f);
            }
            this._swingVelocity = Maths.LerpTowards(this._swingVelocity, this._swingForce, 0.1f);
            this._swing += this._swingVelocity;
            float num = this._swing - this._swingLast;
            this._swingLast = this._swing;
            if (this._swing > 1f)
            {
                this._swing = 1f;
            }
            if (this._swing < 0f)
            {
                this._swing = 0f;
            }
            this._sprite.flipH = false;
            this._sprite.flipV = false;
            if (this._sparkWait > 0f)
            {
                this._sparkWait -= 0.1f;
            }
            else
            {
                this._sparkWait = 0f;
            }
            Duck duck = this.owner as Duck;
            if (duck != null)
            {
                if (this._sparkWait == 0f && this._swing == 0f)
                {
                    if (duck.grounded && duck.offDir > 0 && duck.hSpeed > 1f)
                    {
                        this._sparkWait = 0.25f;
                        Level.Add(Spark.New(base.x - 22f, base.y + 6f, new Vec2(0f, 0.5f), 0.02f));
                    }
                    else if (duck.grounded && duck.offDir < 0 && duck.hSpeed < -1f)
                    {
                        this._sparkWait = 0.25f;
                        Level.Add(Spark.New(base.x + 22f, base.y + 6f, new Vec2(0f, 0.5f), 0.02f));
                    }
                }
                float hSpeed = duck.hSpeed;
                this._hPull = Maths.LerpTowards(this._hPull, duck.hSpeed, 0.15f);
                if (Math.Abs(duck.hSpeed) < 0.1f)
                {
                    this._hPull = 0f;
                }
                float num2 = Math.Abs(this._hPull) / 2.5f;
                if (num2 > 1f)
                {
                    num2 = 1f;
                }
                this.weight = 8f - num2 * 3f;
                if ((double)this.weight <= 5.0)
                {
                    this.weight = 5.1f;
                }
                float num3 = Math.Abs(duck.hSpeed - this._hPull);
                duck.frictionMod = 0f;
                if (duck.hSpeed > 0f && this._hPull > duck.hSpeed)
                {
                    duck.frictionMod = -num3 * 1.8f;
                }
                if (duck.hSpeed < 0f && this._hPull < duck.hSpeed)
                {
                    duck.frictionMod = -num3 * 1.8f;
                }
                this._lastDir = (int)duck.offDir;
                this._lastSpeed = hSpeed;
                if (this._swing != 0f && num > 0f)
                {
                    duck.hSpeed += (float)duck.offDir * (num * 3f) * base.weightMultiplier;
                    duck.vSpeed -= num * 2f * base.weightMultiplier;
                }
            }
            if (this._swing < 0.5f)
            {
                float num4 = this._swing * 2f;
                this._sprite.imageIndex = (int)(num4 * 10f);
                this._sprite.angle = 1.2f - num4 * 1.5f;
                this._sprite.yscale = 1f - num4 * 0.1f;
            }
            else if (this._swing >= 0.5f)
            {
                float num5 = (this._swing - 0.5f) * 2f;
                this._sprite.imageIndex = 10 - (int)(num5 * 10f);
                this._sprite.angle = -0.3f - num5 * 1.5f;
                this._sprite.yscale = 1f - (1f - num5) * 0.1f;
                this._fullSwing += 0.16f;
                if (!this._swung)
                {
                    this._swung = true;
                    if (base.duck != null)
                    {
                        Level.Add(new ForceWave(base.x + (float)this.offDir * 4f + this.owner.hSpeed, base.y + 8f, (int)this.offDir, 0.15f, 4f + Math.Abs(this.owner.hSpeed), this.owner.vSpeed, base.duck));

                        //BREAK STUFF

                        var destroyedstuff = false;

                        if (this.isServerForObject)
                        {
                            IEnumerable<Window> enumerable = Level.CheckCircleAll<Window>(this.position, 16f);
                            foreach (Window current in enumerable)
                            {
                                if (this.isLocal)
                                {
                                    Thing.Fondle(current, DuckNetwork.localConnection);
                                }
                                if (Level.CheckLine<Block>(this.position, current.position, current) == null)
                                {
                                    current.Destroy(new DTImpact(this));
                                    destroyedstuff = true;
                                }
                            }
                            IEnumerable<PhysicsObject> enumerable2 = Level.CheckCircleAll<PhysicsObject>(this.position, 24f);
                            foreach (PhysicsObject current2 in enumerable2)
                            {
                                if (current2 is Duck) continue;

                                if (this.isLocal)
                                {
                                    Thing.Fondle(current2, DuckNetwork.localConnection);
                                }
                                if ((current2.position - this.position).length < 16f)
                                {
                                    current2.Destroy(new DTImpact(this));
                                    destroyedstuff = true;
                                }
                                current2.sleeping = false;
                                current2.vSpeed = -2f;
                            }
                            HashSet<ushort> hashSet = new HashSet<ushort>();
                            IEnumerable<BlockGroup> enumerable3 = Level.CheckCircleAll<BlockGroup>(this.position, 16f);
                            foreach (BlockGroup current3 in enumerable3)
                            {
                                if (current3 != null)
                                {
                                    BlockGroup blockGroup = current3;
                                    foreach (Block current4 in blockGroup.blocks)
                                    {
                                        if (Collision.Circle(this.position, 16f, current4.rectangle))
                                        {
                                            current4.shouldWreck = true;
                                            destroyedstuff = true;

                                            if (current4 is AutoBlock)
                                            {
                                                hashSet.Add((current4 as AutoBlock).blockIndex);
                                            }
                                        }
                                    }
                                    blockGroup.Wreck();
                                }
                            }
                            IEnumerable<Block> enumerable4 = Level.CheckCircleAll<Block>(this.position, 16f);
                            foreach (Block current5 in enumerable4)
                            {
                                destroyedstuff = true;

                                if (current5 is AutoBlock)
                                {
                                    current5.skipWreck = true;
                                    current5.shouldWreck = true;
                                    if (current5 is AutoBlock)
                                    {
                                        hashSet.Add((current5 as AutoBlock).blockIndex);
                                    }
                                }
                                else if (current5 is Door || current5 is VerticalDoor)
                                {
                                    Level.Remove(current5);
                                    current5.Destroy(new DTRocketExplosion(null));
                                }
                            }
                            if (Network.isActive && this.isLocal && hashSet.Count > 0)
                            {
                                Send.Message(new NMDestroyBlocks(hashSet));
                            }
                        }

                        //END BREAK STUFF

                        if (destroyedstuff)
                        {
                            //if (Network.isActive)
                            //{
                            //    _destroyNetSound.Play();
                            //}
                            //else
                            //{
                                SFX.Play("crateDestroy");
                            //}
                        }
                    }
                }
            }
            if (this._swing == 1f)
            {
                this._pressed = false;
            }
            if (this._swing == 1f && !this._pressed && this._fullSwing > 1f)
            {
                this._swingForce = -0.08f;
                this._fullSwing = 0f;
            }
            if (this._sledgeSwing.finished)
            {
                this._sledgeSwing.speed = 0f;
            }
            this._lastOwner = (this.owner as PhysicsObject);
            if (base.duck != null)
            {
                if (base.duck.action && !this._held && this._swing == 0f)
                {
                    this._fullSwing = 0f;
                    duck.crippleTimer = 1f;
                    this._sledgeSwing.speed = 1f;
                    this._sledgeSwing.frame = 0;
                    this._swingForce = 0.6f;
                    this._pressed = true;
                    this._swung = false;
                    this._held = true;
                }
                if (!base.duck.action)
                {
                    this._pressed = false;
                    this._held = false;
                }
            }
            base.Update();
        }

        //private NetSoundEffect _destroyNetSound = new NetSoundEffect("crateDestroy");

        private bool _drawOnce;

        private float _fullSwing;

        private bool _held;

        private float _hPull;

        private int _lastDir;

        private PhysicsObject _lastOwner;

        private float _lastSpeed;

        private Vec2 _offset = default(Vec2);

        private bool _pressed;

        private SpriteMap _sledgeSwing;

        private float _sparkWait;

        private SpriteMap _sprite;

        private float _swing;

        private float _swingForce;

        private float _swingLast;

        private float _swingVelocity;

        private bool _swung;
    }
}
