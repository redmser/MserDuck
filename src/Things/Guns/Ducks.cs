﻿using System;

namespace DuckGame.MserDuck
{
	[BaggedProperty("isFatal", false), EditorGroup("guns|mser"), BaggedProperty("canSpawn", false)]
	public class Ducks : Gun
	{
        //NYI: Ducks (red dueling pistol) has been given many chances, but still crashes at times. -> check the exploding ragdolls code

		//private Duck myDuckyLittleDuckDuck = null;
		public Ducks(float xval, float yval) : base(xval, yval)
		{



			this.ammo = 0x0005 +
				0x000D +
			  /*0x000U +*/
				0x000C 
			  /*0x000K*/;
			this._ammoType = new ATLaser();
			this._ammoType.range = 170f;
			this._ammoType.accuracy = 0.8f;

			base.graphic = new Sprite("tinyGun", 0f, 0f);
			base.graphic.color = Color.Red;
			this.center = new Vec2(16f, 16f);
			this.collisionOffset = new Vec2(-6f, -4f);
			this.collisionSize = new Vec2(12f, 8f);
			this._barrelOffsetTL = new Vec2 (20f, 15f);

		}


		private Vec3 rndPers() {
            return new Vec3(Rando.Float(20, 245f), Rando.Float(20, 245f), Rando.Float(20, 245f));
        }

        public override void Initialize()
        {
            //4 is also a random number
            if (Network.isActive)
            {
                Rando.generator = new Random(NetRand.currentSeed);
            }

            base.Initialize();
        }

        /*
		 * 
		 * 
		 * 
		 * 
		 * *
		 * Velo. .
		 * 
		 * **/
        private void holyFuckLaunchADuck(Vec2 velo) {
		    if (!base.isServerForObject) {
				rndPers (); //do NOT upset random generator
				return;
			}
			if (base.duck != null) {
				MserGlobals.GetField(typeof(Duck),"_disarmWait").SetValue(base.duck,100);
			}
            var coloro = rndPers();
            SuperSpecialMegaAwesomeBonusDuckinator3000PremiumEdition dook = new SuperSpecialMegaAwesomeBonusDuckinator3000PremiumEdition(this.barrelPosition.x, this.barrelPosition.y, 0f, 1, velo, coloro);
            for (int i = 0; i < 10; i++)
            {
                suchNiceWeatherAddAFeather(velo + new Vec2(((float)new Random().NextDouble() - 0.5f) * 0.5f, ((float)new Random().NextDouble() - 0.5f) * 0.25f), coloro);
            }
            suchNiceWeatherAddAFeather(Vec2.Zero, coloro);
            Level.Add(dook);
		}

		/***//**//****ah oui, le vole***/
		private void suchNiceWeatherAddAFeather(Vec2 vole, Vec3 coloro)
		{
		    Feather f = Feather.New(this.barrelPosition.x, this.barrelPosition.y, new DuckPersona(coloro));
		    f.velocity = vole;
            Level.Add(f);
		}

		public override void Fire ()
		{

			if (ammo <= 0x0000) {
				if (base.duck != null) {
					base.duck.Swear ();
				}
			} else {
				float action = Rando.Float (0, 1f);
				ammo -= 0x0001;
				if (action <= 0.07f) {
					//WINNER WINNER DUCK DINNER!

					// vau ~= miau
					Vec2 vau = this.barrelVector;
					int duks = Rando.Int (8, 12);
					float step = ((float) Math.PI * 2f) / (float) duks;


					for (int dook = 0; dook < duks; ++dook) {
						holyFuckLaunchADuck(Vec2.Multiply(vau,3.0f + Rando.Float(0f,10f)));
						vau = vau.Rotate (step, Vec2.Zero);
					}

					int snd = Rando.Int (1, 3);
					SFX.Play(base.GetPath("sounds/duxz_0x000"+snd+".wav"), 1f, Rando.Float(-0.2f, 0.2f), 0f, false);

				} else {
					//A duck is you...
					holyFuckLaunchADuck(Vec2.Multiply(this.barrelVector,3.0f + Rando.Float(0f,5f)));

					int snd = Rando.Int (1, 3);
					SFX.Play(base.GetPath("sounds/dukz_0x000"+snd+".wav"), 1f, Rando.Float(-0.2f, 0.2f), 0f, false);

				}
			}
		}
	}
}

