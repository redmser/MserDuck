﻿namespace DuckGame.MserDuck
{
    /// <summary>
    /// THANK WEKATEKA!!!
    /// </summary>
	[EditorGroup("guns|mser")]
	public class Boomerang : Gun
	{

		public Vec2 home; //That special place where we must one day all return.
		public bool valid_home = false; //Is this home a little sneaky imposter?
		public bool returning = false; //Is the buumareng flying back at your face?
		public bool flying = false; // I'M FLYYYYIIIINGG!

		public Duck last_owner = null;

		//Stolen from Sword
		private System.Collections.Generic.List<float> _lastAngles = new System.Collections.Generic.List<float>();
		private System.Collections.Generic.List<Vec2> _lastPositions = new System.Collections.Generic.List<Vec2>();
		private bool _drawing;
	    public bool willReturn = true;
	    public static int NumToReturn;


	    public Boomerang(float xval, float yval) : base(xval, yval)
		{
			//Important number for important reasons
			this.ammo = 0x279;
			//Not a gun. A boomerang. Nub.
			this._ammoType = new ATLaser();
			this._ammoType.range = 170f;
			this._ammoType.accuracy = 0.8f;
			this._fireSound = "smg";
			this._type = "gun";
			base.graphic = new Sprite(GetPath("boomerang"), 0f, 0f);
			this.center = new Vec2(10f, 3f);
			this.collisionOffset = new Vec2(-9f, -3f);
			this.collisionSize = new Vec2(13f, 13f);
			this._barrelOffsetTL = new Vec2(31f, 15f);
			this._fireSound = "laserRifle";
			this._fullAuto = false;
			this._fireWait = 0.1f;
			this._kickForce = 1f;
			this._holdOffset = new Vec2(0f, 0f);
			//this._flare = new SpriteMap("laserFlare", 16, 16, false);
			//this._flare.center = new Vec2(0f, 8f);
			this.weight = 1f;
			this.physicsMaterial = PhysicsMaterial.Wood;
			this.bouncy = 0.5f;
			this.buoyancy = 1.0f;
			this.friction = 0.1f;
			this.frictionMult = 1.0f;

			this.gravMultiplier = 1f;
		}


		public void launch() {
			/*if (this.owner != null) {
				stop ();
				return;
			}*/
			flying = true;
			returning = false;
			valid_home = true;
			this.gravMultiplier = 0.5f;
			this.frictionMult = 0.5f;
			home = new Vec2(this.position);
		}

		protected void stop() {
			flying = false;
			returning = false;
			valid_home = false;
			this.gravMultiplier = 1f;
			this.frictionMult = 1f;
		}

		protected void goHome() {
			/*if (this.owner != null) {
				stop ();
				return;
			}*/
			flying = true;
			returning = true;
			this.gravMultiplier = 0.5f;
			this.frictionMult = 0.5f;
		}

		public override void Update ()
		{
			base.Update ();

			if (base.duck != null) {
				last_owner = base.duck;
			}

			if (this.owner != null) {
				stop ();
				return;
			}

			if (flying) {
				this.angle += this.velocity.length * 0.06f;
			}

			if (flying && (this.velocity.length < 1.0f || this.velocity.y > 0.5f)) {
				if (!returning) {
					goHome ();
				}
			}

			if (returning) {
				//Are we back yet?
				if ((!valid_home) || Vec2.Subtract (home, this.position).length < (this.velocity.length * 5f)) {
					stop ();
				} else {
					Vec2 home_dir = Vec2.Multiply(Vec2.Subtract(home, this.position).normalized, 0.2f);
					//\\///\\\\/\\/no.
					this.velocity = Vec2.Add(this.velocity, home_dir);

					//Caution: Boomerangs should not appraoch relativistic speeds.
					if (this.velocity.length > 10f) {
						this.velocity = Vec2.Multiply (this.velocity.normalized, 9f);
					}

					if ((this.velocity.y / this.velocity.x) > ((home_dir.y / home_dir.x) * 1.1f)) {
						this.velocity = new Vec2 (this.velocity.x, this.velocity.y * 0.8f);
					}
				}
			}
		}

		public override void Draw ()
		{
			base.graphic.color = Color.White;
			if ((this.owner == null && flying))
			{
				float old_angle = this.angle;
				Vec2 old_position = this.position;
				this._drawing = true;
				float angle2 = this._angle;
				for (int i = 0; i < 15; i++)
				{
					base.Draw();
					if (this._lastAngles.Count > i)
					{
						this._angle = this._lastAngles[i];
					}
					if (this._lastPositions.Count <= i)
					{
						break;
					}
					this.position = this._lastPositions[i];
					if (this.owner != null)
					{
						this.position += this.owner.velocity;
					}
					base.depth -= 2;
					base.alpha -= 0.15f;
					base.graphic.color = Color.SaddleBrown;
				}
				this.position = old_position;
				this.angle = old_angle;
				base.depth = depth;
				base.alpha = 1f;
				this._angle = angle2;
				base.xscale = 1f;
				this._drawing = false;
			}
			else
			{
				base.Draw();
			}

			this._lastAngles.Insert(0, this.angle);
			this._lastPositions.Insert(0, this.position);
			if (this._lastAngles.Count > 2)
			{
				this._lastAngles.Insert(0, (this._lastAngles[0] + this._lastAngles[2]) / 2f);
				this._lastPositions.Insert(0, (this._lastPositions[0] + this._lastPositions[2]) / 2f);
			}
			if (this._lastAngles.Count > 16)
			{
				this._lastAngles.RemoveAt(this._lastAngles.Count - 1);
			}
			if (this._lastPositions.Count > 16)
			{
				this._lastPositions.RemoveAt(this._lastPositions.Count - 1);
			}

		}

		public override void Thrown ()
		{
			launch ();
			base.Thrown ();
			SFX.Play(base.GetPath("sounds/SWOOSH.wav"), 1f, 0f, 0f, false);
		}

		public override void Impact (MaterialThing with, ImpactedFrom from, bool solidImpact)
		{
			base.Impact (with, from, solidImpact);

			if (with.GetType () == typeof(Duck)) {
				Duck arschlog = (Duck)with;
				if (arschlog == last_owner) {
					//nope.avi
					if (returning && this.velocity.length > 1.0f) {
						if (!arschlog.destroyed) {
							//rek
							arschlog.Destroy (new DTImpale (this));
							SFX.Play(base.GetPath("sounds/SWOOSH.wav"), 1f, 0f, 0f, false);
						}
						stop ();
					}
				} else if (flying) {
					//Hitted a duk!
					if (this.onFire) {
						arschlog.onFire = true;
					} else {
						if (!arschlog.destroyed) {
							//rek
							arschlog.Destroy (new DTImpact (this));
							SFX.Play(base.GetPath("sounds/SWOOSH.wav"), 1f, 0f, 0f, false);
						}
					}
				}
			} else if (with.GetType ().IsSubclassOf(typeof(PhysicsObject))) {
				PhysicsObject trash = (PhysicsObject)with;

				if (this.onFire) {
					trash.onFire = true;
				}

				if (flying && this.velocity.length > 0.2f) {
					
					trash.velocity = Vec2.Multiply (this.velocity, 1.1f);
					trash.velocity = Vec2.Add(trash.velocity, new Vec2(0f,-1.0f));

					Gun knarre = with as Gun;
					if (knarre != null)
					{
						knarre.PressAction ();
					}
				}
			} else {
				if (flying && solidImpact && (with.GetType() == typeof(Block))) {
					if (!returning) {
						goHome ();
					} else {
						stop ();
					}
				}
			}

		}


		public override float angle
		{
			get
			{
				if (this._drawing)
				{
					return this._angle;
				}
				return base.angle;
			}
			set
			{
				this._angle = value;
			}
		}

		public override void Fire()
		{
            //NO. BAD DUK! YOU DO NOT FIRE BOOMERENG, BUUMERANGERENG FIRES YOU!
            if (base.duck != null) {
				base.duck.Swear ();
			}
		}

	    public override void Terminate()
	    {
	        if (this.willReturn && this.removedFromFall)
	        {
	            //I WILL RETURN
	            Boomerang.NumToReturn++;
	        }

	        base.Terminate();
	    }
	}
}

