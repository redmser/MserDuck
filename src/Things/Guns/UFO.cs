﻿using System;
using System.Collections.Generic;

namespace DuckGame.MserDuck
{
    [BaggedProperty("canSpawn", false), EditorGroup("guns|mser")]
    internal class UFO : Holdable, IPlatform
    {
        public float curX;
        public float curY;
        public float curXVel;
        public float curYVel;
        public byte lastOwnerIndex = 255;
        public Duck lastOwner
        {
            get
            {
                if (this.lastOwnerIndex == 255) return null;
                return MserGlobals.GetDuckFromIndex(lastOwnerIndex);
            }
        }
        public float charge;
        public int shotDelay;
        public float offset;
        public int occupiedTime;
        public StateBinding lastOwnerBinding = new StateBinding("lastOwnerIndex");

        //NYI: UFO - desynced over the net and "not as fun as the car"

        public UFO(float xpos, float ypos) : base(xpos, ypos)
        {
            this.graphic = new Sprite(Mod.GetPath<MserDuck>("ufo"));
            this.center = new Vec2(12f, 12f);
            this.collisionOffset = new Vec2(-8f, -11f);
            this.collisionSize = new Vec2(16f, 14f);
            base.depth = 0.5f;
            this.thickness = 4f;
            this.weight = 7f;
            this._holdOffset = new Vec2(1f, 7f);
            this.flammable = 0.3f;
            base.collideSounds.Add("rockHitGround2");
            this.handOffset = new Vec2(0f, -9999f);
            //base.hugWalls = WallHug.Floor;
            this.curX = this.x;
            this.curY = this.y;
            this.curXVel = 0f;
            this.curYVel = 0f;
            this.charge = 0f;
            this.shotDelay = 0;
            this.offset = 0f;
            this.occupiedTime = 0;
            this.lastOwnerBinding.skipLerp = true;
        }

        public override void Initialize()
        {
        }

        public override void Terminate()
        {
        }

        public override void Update()
        {
            /*if(this.alpha == 0)
            {
                if (this.shotDelay <= 0)
                {
                    Bullet bullet = new Bullet(this.x, this.y, new ATMissile(), Rando.Float(360f));
                    bullet.firedFrom = this;
                    bullet.owner = this.owner;
                    Level.Add(bullet);

                    if (Network.isActive)
                    {
                        var list = new List<Bullet>();
                        list.Add(bullet);
                        var msg = new NMFireGun(null, list, 0, false, this.duck.profile.networkIndex, false);
                        Send.Message(msg, NetMessagePriority.ReliableOrdered, null);
                    }

                    // time between shots
                    this.shotDelay = 4;
                    if (this.charge > 5f)
                    {
                        this.shotDelay = 2;
                    }
                }

                this.charge -= 1f;
                if (this.charge <= 0)
                {
                    Level.Remove(this);
                }
            }*/
            if (this.owner != this.lastOwner)
            {
                if (this.owner == null) // owner has exited
                {
                    SFX.Play(Mod.GetPath<MserDuck>("sounds/glitch1"), 1f, Rando.Float(-0.2f, 0.2f));
                    this.lastOwner.alpha = 1;
                    this.lastOwnerIndex = 255;
                    this.sleeping = false;
                    this.occupiedTime = 0;
                }
                else // owner has entered
                {
                    SFX.Play(Mod.GetPath<MserDuck>("sounds/glitch2"), 1f, Rando.Float(-0.2f, 0.2f));
                    this.curX = this.owner.x;
                    this.curY = this.owner.y;
                    this.lastOwnerIndex = this.duck.profile.networkIndex;
                    this.lastOwner.alpha = 0;
                    this.sleeping = true;
                }
            }
            if (this.lastOwner != null)
            {
                var VecTo = new Vec2(this.curX, this.curY);
                var VecTwo = new Vec2(this.curX + this.curXVel, this.curY + this.curYVel);

                if (Level.CheckLine<Block>(VecTo, VecTwo) != null)
                {
                    // kill da üfó
                    this.duck.alpha = 1f;
                    this.duck.Kill(new DTImpact(this));
                    ExplosionPart thing = new ExplosionPart(this.curX, this.curY, true);
                    Level.Add(thing);

                    Level.Remove(this);
                    //this.alpha = 0f;
                }
                else
                {
                    this.occupiedTime += 1;

                    this.owner.vSpeed = 0f;
                    this.owner.hSpeed = 0f;
                    if (base.isServerForObject)
                    {
                        if (base.duck.inputProfile.Down("UP"))
                        {
                            this.curYVel -= 0.2f;
                        }
                        if (base.duck.inputProfile.Down("DOWN"))
                        {
                            this.curYVel += 0.2f;
                        }
                        if (base.duck.inputProfile.Down("LEFT"))
                        {
                            this.curXVel -= 0.2f;
                        }
                        if (base.duck.inputProfile.Down("RIGHT"))
                        {
                            this.curXVel += 0.2f;
                        }
                        if (base.duck.inputProfile.Down("SHOOT"))
                        {
                            if (this.charge <= 0f) // starting to charge a shot
                            {
                                SFX.Play(Mod.GetPath<MserDuck>("sounds/boomerangThrow"), 1f, Rando.Float(-0.2f, 0.2f));
                            }
                            this.charge += 0.04f;
                        }
                        else if (base.duck.inputProfile.Released("SHOOT"))
                        {
                            if (this.charge >= 10f) // 10 shots
                            {
                                SFX.Play(Mod.GetPath<MserDuck>("sounds/strong_smash"), 1f, Rando.Float(-0.2f, 0.2f));
                                this.offset = 20f;
                            }
                            else if (this.charge >= 5f) // 5 shots
                            {
                                SFX.Play(Mod.GetPath<MserDuck>("sounds/medium_smash"), 1f, Rando.Float(-0.2f, 0.2f));
                                this.offset = 10f;
                            }
                            else if (this.charge >= 2f) // 2 shots
                            {
                                SFX.Play(Mod.GetPath<MserDuck>("sounds/weak_smash"), 1f, Rando.Float(-0.2f, 0.2f));
                                this.offset = 5f;
                            }
                            else if (this.charge >= 1f) // 1 shot
                            {
                                SFX.Play(Mod.GetPath<MserDuck>("sounds/weak_hit"), 1f, Rando.Float(-0.2f, 0.2f));
                                this.offset = 2f;
                            }

                        }
                        else if (this.charge > 0)
                        {
                            if (this.shotDelay <= 0)
                            {
                                // time between shots
                                this.shotDelay = 4;
                                if (this.charge > 5f)
                                {
                                    this.shotDelay = 2;
                                }

                                this.charge -= 1f;
                                SFX.Play(Mod.GetPath<MserDuck>("sounds/dunk"), 1f, Rando.Float(-0.2f, 0.2f));

                                Bullet bullet = new Bullet(this.x, this.y, new ATMissile(), -90f + Rando.Float(-this.offset, this.offset));
                                bullet.firedFrom = this;
                                bullet.owner = this.owner;
                                Level.Add(bullet);

                                if (Network.isActive)
                                {
                                    var list = new List<Bullet>();
                                    list.Add(bullet);
                                    var msg = new NMFireGun(null, list, 0, false, this.duck.profile.networkIndex, false);
                                    Send.Message(msg, NetMessagePriority.ReliableOrdered, null);
                                }
                            }
                            else
                            {
                                this.shotDelay -= 1;
                            }
                        }

                        this.curXVel += 0.1f * -Math.Sign(this.curXVel);
                        this.curYVel += 0.1f * -Math.Sign(this.curYVel);
                        this.curXVel = Math.Min(Math.Max(this.curXVel, -2), 2);
                        this.curYVel = Math.Min(Math.Max(this.curYVel, -2), 2);

                        this.curX += this.curXVel;
                        this.curY += this.curYVel;
                    }

                    this.owner.position = new Vec2(this.curX, this.curY);
                }
            }
        }
    }
}
