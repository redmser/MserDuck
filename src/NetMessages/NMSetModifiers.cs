﻿using System.Collections.Generic;
using System.Text;

namespace DuckGame.MserDuck.NetMessages
{
    /// <summary>
    /// A net message for setting the state of all modifiers.
    /// </summary>
    public class NMSetModifiers : NMEvent
    {
        public List<bool> handlerList = new List<bool>();

        public override void Activate()
        {
            for (int i = 0; i < this.handlerList.Count; i++)
            {
                ModifierManager.GetModifierByIndex(i).Enabled = this.handlerList[i];
            }
            CacheManager.ClearModifierAutoCompleteEntries();
        }

        public override void OnDeserialize(BitBuffer msg)
        {
            base.OnDeserialize(msg);

            var mask = msg.ReadString();
            foreach (var ch in mask)
            {
                var enabled = ch == '1';
                this.handlerList.Add(enabled);
            }
        }

        protected override void OnSerialize()
        {
            base.OnSerialize();

            this.serializedData.Write(GetBitMask(this.handlerList));
        }

        private static string GetBitMask(IEnumerable<bool> list)
        {
            var sb = new StringBuilder();
            foreach (var bl in list)
            {
                sb.Append(bl ? '1' : '0');
            }
            return sb.ToString();
        }
    }
}
