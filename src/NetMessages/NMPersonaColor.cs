﻿namespace DuckGame.MserDuck.NetMessages
{
    /// <summary>
    /// A net message which, when received, sets the duck with the specified netIndex to the specified color.
    /// </summary>
    public class NMPersonaColor : NMEvent
    {
        public Duck duck;
        public byte r;
        public byte g;
        public byte b;

        public NMPersonaColor()
        {

        }

        public NMPersonaColor(byte netIndex, byte r, byte g, byte b)
        {
            this.duck = MserGlobals.GetDuckFromIndex(netIndex);
            this.r = r;
            this.g = g;
            this.b = b;
        }

        public override void Activate()
        {
            base.Activate();

            if (this.duck != null && this.duck.profile != null && this.duck.profile.persona != null)
            {
                this.duck.profile.persona.color = new Vec3(this.r,this.g,this.b);
                this.duck.profile.persona.Recreate();
            }
        }
    }
}
