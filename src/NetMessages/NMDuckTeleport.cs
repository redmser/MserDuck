﻿namespace DuckGame.MserDuck.NetMessages
{
    /// <summary>
    /// A net message for teleporting two ducks specified using their netIndex, swapping their states as well (such as ragdolled).
    /// </summary>
    public class NMDuckTeleport : NMEvent
    {
        public byte who1;

        public byte who2;

        public NMDuckTeleport(byte _who1, byte _who2)
        {
            this.who1 = _who1;
            this.who2 = _who2;
        }

        public NMDuckTeleport()
        {
        }

        public override void Activate()
        {
            Duck duck = MserGlobals.GetDuckFromIndex(this.who1);
            Duck duck2 = MserGlobals.GetDuckFromIndex(this.who2);
            if (duck != null && duck2 != null)
            {
                SFX.Play(Thing.GetPath<MserDuck>("sounds/swap"), 1f, 0f, 0f, false);
                NMDuckTeleport.CreateExplosion(duck.x, duck.y);
                NMDuckTeleport.CreateExplosion(duck2.x, duck2.y);
                NMDuckTeleport.SwapPositions(duck, duck2);
            }

            base.Activate();
        }

        /// <summary>
        /// The state of a duck.
        /// </summary>
        private enum DuckState
        {
            Duck,
            Ragdoll,
            Netted
        }

        public static void SwapPositions(Duck d1, Duck d2)
        {
            var d1state = DuckState.Duck;
            if (d1.ragdoll != null)
            {
                d1state = DuckState.Ragdoll;
            }
            if (d1._trapped != null)
            {
                d1state = DuckState.Netted;
            }

            var d2state = DuckState.Duck;
            if (d2.ragdoll != null)
            {
                d2state = DuckState.Ragdoll;
            }
            if (d2._trapped != null)
            {
                d2state = DuckState.Netted;
            }

            if (d1state == DuckState.Ragdoll && d2state == DuckState.Ragdoll)
            {
                //Swap all parts
                var r1 = d1.ragdoll;
                var r2 = d2.ragdoll;

                if (!NMDuckTeleport.ValidRagdoll(r1) || !NMDuckTeleport.ValidRagdoll(r2)) return;

                var o1 = r1._part1.position;
                var o2 = r1._part2.position;
                var o3 = r1._part3.position;

                r1._part1.position = r2._part1.position;
                r1._part2.position = r2._part2.position;
                r1._part3.position = r2._part3.position;
                r2._part1.position = o1;
                r2._part2.position = o2;
                r2._part3.position = o3;

                r1._part1.sleeping = false;
                r1._part2.sleeping = false;
                r1._part3.sleeping = false;
                r2._part1.sleeping = false;
                r2._part2.sleeping = false;
                r2._part3.sleeping = false;
            }
            else if (d1state == DuckState.Duck && d2state == DuckState.Duck)
            {
                //Swap positions the normal way
                var oldpos = d1.position;
                d1.position = d2.position;
                d2.position = oldpos;

                d1.sleeping = false;
                d2.sleeping = false;

                //Swap listening state
                var oldtime = d1.listenTime;
                d1.listenTime = d2.listenTime;
                d2.listenTime = oldtime;
                var oldlisten = d1.listening;
                d1.listening = d2.listening;
                d2.listening = oldlisten;

                //Swap holditem (is this a good idea?)
                var d1hold = d1.holdObject;
                var d2hold = d2.holdObject;
                d1.ThrowItem(false);
                d2.ThrowItem(false);
                d1.GiveHoldable(d2hold);
                d2.GiveHoldable(d1hold);

                //TODO: Exchanger - should still swap equipment for ragdolled and netted, since they dont just lose it
                //BUG: Exchanger - Swap equipment (currently falls off the receiving duck)
                /*var d1equip = d1._equipment;
                var d2equip = d2._equipment;
                foreach (var eq in d1._equipment.Reverse<Equipment>())
                {
                    d1.Unequip(eq);
                }
                foreach (var eq in d2._equipment.Reverse<Equipment>())
                {
                    d2.Unequip(eq);
                }
                foreach (var eq in d1equip)
                {
                    d2.Equip(eq, false);
                }
                foreach (var eq in d2equip)
                {
                    d1.Equip(eq, false);
                }*/
            }
            else if (d1state == DuckState.Netted && d2state == DuckState.Netted)
            {
                //Swap positions the normal way, except for the _trapped
                var oldpos = d1._trapped.position;
                d1._trapped.position = d2._trapped.position;
                d2._trapped.position = oldpos;

                d1._trapped.sleeping = false;
                d2._trapped.sleeping = false;
            }
            else if ((d1state == DuckState.Ragdoll && d2state == DuckState.Duck) || (d1state == DuckState.Duck && d2state == DuckState.Ragdoll))
            {
                //Swap by ragdolling/unragdolling
                Ragdoll r;
                Duck rDuck;
                Duck d;

                if (d1state == DuckState.Duck)
                {
                    //Ragdoll of d2
                    d = d1;
                    rDuck = d2;
                    r = d2.ragdoll;
                }
                else
                {
                    //Ragdoll of d1
                    d = d2;
                    rDuck = d1;
                    r = d1.ragdoll;
                }

                if (!NMDuckTeleport.ValidRagdoll(r)) return;

                var o1 = r._part1.position;
                var o2 = r._part2.position;
                var o3 = r._part3.position;

                r.Unragdoll();
                rDuck.position = d.position;

                d.GoRagdoll();
                if (!NMDuckTeleport.ValidRagdoll(d.ragdoll)) return;

                d.ragdoll._part1.position = o1;
                d.ragdoll._part2.position = o2;
                d.ragdoll._part3.position = o3;

                rDuck.sleeping = false;
                d.ragdoll._part1.sleeping = false;
                d.ragdoll._part2.sleeping = false;
                d.ragdoll._part3.sleeping = false;
            }
            else if ((d1state == DuckState.Netted && d2state == DuckState.Duck) || (d1state == DuckState.Duck && d2state == DuckState.Netted))
            {
                //Swap by netting/unnetting
                TrappedDuck t;
                Duck tDuck;
                Duck d;

                if (d1state == DuckState.Duck)
                {
                    //Get trapped of d2
                    t = d2._trapped;
                    tDuck = d2;
                    d = d1;
                }
                else
                {
                    //Get trapped of d1
                    t = d1._trapped;
                    tDuck = d1;
                    d = d2;
                }

                var oldpos = t.position;
                var oldtime = t._trapTime;

                NMDuckTeleport.Unnet(t);
                tDuck.position = d.position;

                var n = new Net(oldpos.x, oldpos.y, tDuck);
                Level.Add(n);
                d.Netted(n);
                d._trapped._trapTime = oldtime;
                d._trapped.position = oldpos;

                tDuck.sleeping = false;
                d._trapped.sleeping = false;
            }
            else if ((d1state == DuckState.Netted && d2state == DuckState.Ragdoll) || (d1state == DuckState.Ragdoll && d2state == DuckState.Netted))
            {
                //Unnet/net AND unragdoll/ragdoll
                TrappedDuck t;
                Duck tDuck;

                Ragdoll r;
                Duck rDuck;

                if (d1state == DuckState.Netted)
                {
                    t = d1._trapped;
                    tDuck = d1;

                    r = d2.ragdoll;
                    rDuck = d2;
                }
                else
                {
                    t = d2._trapped;
                    tDuck = d2;

                    r = d1.ragdoll;
                    rDuck = d1;
                }

                var oldpos = t.position;
                var oldtime = t._trapTime;

                NMDuckTeleport.Unnet(t);
                tDuck.GoRagdoll();

                if (!NMDuckTeleport.ValidRagdoll(tDuck.ragdoll) || !NMDuckTeleport.ValidRagdoll(r)) return;

                tDuck.ragdoll._part1.position = r._part1.position;
                tDuck.ragdoll._part2.position = r._part2.position;
                tDuck.ragdoll._part3.position = r._part3.position;

                r.Unragdoll();
                var n = new Net(oldpos.x, oldpos.y, tDuck);
                Level.Add(n);
                rDuck.Netted(n);
                rDuck._trapped._trapTime = oldtime;
                rDuck._trapped.position = oldpos;

                rDuck._trapped.sleeping = false;
                tDuck.ragdoll._part1.sleeping = false;
                tDuck.ragdoll._part2.sleeping = false;
                tDuck.ragdoll._part3.sleeping = false;
            }
        }

        public static void Unnet(TrappedDuck t)
        {
            MserGlobals.GetMethod("TrappedDuck", "OnDestroy").Invoke(t, new object[] { null });
        }

        public static bool ValidRagdoll(Ragdoll r)
        {
            return r != null && r._part1 != null && r._part2 != null && r._part3 != null;
        }

        public static void CreateExplosion(float x, float y)
        {
            GlitchExplosion explosionPart = new GlitchExplosion(x - 8f + Rando.Float(16f), y - 8f + Rando.Float(16f), true);
            Level.Add(explosionPart);
        }
    }
}
