﻿namespace DuckGame.MserDuck.NetMessages
{
    public class NMSpawnSun : NMEvent
    {
        public Vec2 Position;
        public Duck Owner;

        public NMSpawnSun()
        {
            
        }

        public NMSpawnSun(Vec2 pos, byte index)
        {
            this.Position = pos;
            this.Owner = MserGlobals.GetDuckFromIndex(index);
        }

        public override void Activate()
        {
            base.Activate();

            var sun = ATSunDart.NewSun(this.Position, this.Owner);
            Level.Add(sun);
        }
    }
}
