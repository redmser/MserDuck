﻿namespace DuckGame.MserDuck.NetMessages
{
    /// <summary>
    /// A net message for swapping the sprites of two different Things.
    /// </summary>
    public class NMSwapSprites : NMEvent
    {
        public Thing a;
        public Thing b;

        public NMSwapSprites(Thing a, Thing b)
        {
            this.a = a;
            this.b = b;
        }

        public NMSwapSprites()
        {
            this.a = null;
            this.b = null;
        }

        public override void Activate()
        {
            if (this.a == null || this.b == null || this.a.graphic == null || this.b.graphic == null || this.a.graphic.texture == null ||
                this.b.graphic.texture == null || this.a.graphic.w == 0 || this.b.graphic.w == 0 || this.a.graphic.h == 0 || this.b.graphic.h == 0)
            {
                return;
            }

            //Swap textures
            var temp = this.a.graphic.texture;
            this.a.graphic.texture = this.b.graphic.texture;
            this.b.graphic.texture = temp;
        }
    }
}
