# About MserDuck

MserDuck is a mod which aims to redefine the way Duck Game is played. This has been accomplished through a robust command system, custom modifiers, a debug mode, many new items and much more!

## Release Trailer

[![MserDuck - Release Trailer](http://img.youtube.com/vi/3syGi1AKipk/0.jpg)](https://www.youtube.com/watch?v=3syGi1AKipk)

# Features

* Enter one of the **30+ Commands** in Duck Game's online chat to execute a variety of commands that can change your appearance, the objects in the level, and much more.

![Commands](docs/commandExample.png)

* **25+ new Items**. They're weird, they're random, but they're there!

![Items](docs/itemsExample.png)

* Introducing a whole new **Debug Mode** right inside your game. Inspect the state of the level and all objects inside of it. Easily trace back wrong behaviour of your work-in-progress items.

![Debug Mode](docs/debugmodeExample.png)

* Your usual modifiers starting to get boring? With MserDuck's **25+ new Modifiers**, you'll never be bored again. (By the way, you can toggle modifiers without going back to the lobby now! Simply use the `/modifier` command for that.)

![Modifiers](docs/modifiersExample.png)

* Want to play with complete strangers? With the **Permissions System**, you can decide who can do what! Give users a role, and they can only run certain commands (or none at all).
* Duck Game didn't get everything right. That's why there are **Game Enhancements** in place. Pitch instruments and your quacking on keyboard&mouse. Create lobbies easier. Chat isn't as clunky to use anymore.
* Don't want to constantly setup your lobby the same way? MserDuck includes a **Config System** which allows you to save all your binds, the current lobby settings (including enabled modifiers) and any other MserDuck-specific settings. Check the `config.cfg` file in your Duck Game folder (created after launching the game with the mod enabled).

# History and Current State

The mod has been in development for multiple years, constantly expanding on what it could do.

But as the mod has aged, so have my interests. I am no longer actively developing the mod on my own, due to the other projects I am busy with, and the lacking interest in Duck Game.
That is why I am releasing this mod as an open-source project: to get those who are dedicated modders or Duck Game players to get inspired or simply have a fun time playing the game completely differently.

Because of this, I am also not going to be managing the project anymore. This means that any bugs, glitches, exploits or feature requests won't be handled by me.

Furthermore, the mod currently has a few problems that would need addressing. Among those are:
* Local multiplayer does not work well with the mod. Most features rely on a networked connection.
* Security issues regarding commands and their abuse.
* A lot of stuff has been left as "work in progress", resulting in a broken and buggy master branch.
* The codebase is a mess. Duck Game's decompiled code here and there - updates are very likely to break the mod completely at any point in time.

Especially because of the security issues, it is **not recommended** to play the mod in its current state with strangers on the internet. Only play with those who you trust!

# Installation

1. Enter `C:\Users\<Username>\Documents\DuckGame\Mods\` in Explorer.
2. **If you plan on using git** (allows for easy updating of the mod):

    Open cmd and clone the repository (this may take a while): `git clone https://gitlab.com/redmser/MserDuck.git`
    
    If you don't know what that means, search for it online. You'll need [this](https://git-scm.com/).

    **Else, if you do not want to use git** (not recommended - you will need to manually update every time):

    Download the repository as a zip and extract the containing folder into the mods folder. Rename the newly created folder to `MserDuck`.
    
    **NOTICE:** All clients have to use the same installation method, otherwise you will get an "Incompatible Mod Setup" error.

3. Ensure that the `MserDuck` directory inside of the `Mods` folder contains the required files (mainly the `src` and `content` directories).
4. **You're all set!** Launch Duck Game.
5. If your main menu has a different color scheme and custom music, you know you've installed the mod correctly. Congrats!
6. If nothing has changed, verify that the mod is enabled (in the Options menu). You could also try disabling other mods, since they may be incompatible.
7. Your computer may also be blocking the method injection process. Simply right-click the `MethodInjector.dll` file inside of the `MserDuck` directory, go to Properties and press the Unblock button.
8. If the mod does not appear in the list, check if the build has failed (log file inside the `MserDuck` directory). This should be reported as a bug.
9. If your game crashes on startup, report this as a bug as well. The crash reason should be found inside of the `ducklog.txt` file (see DuckGame's directory for this).
10. If your game crashes without a log entry being made, the mod may be incompatible with the current version of Duck Game.
11. If the `MserDuck_compiled.dll` file is created, yet the mod does not seem to change the main menu in any way, please report this as a bug.
12. Can't start a server? Try pressing your chat key in the lobby, or simply invite friends to start playing!

# Contributing

* **Please do not upload the mod to the public Steam workshop!** While it would allow easier distribution, this is a very alpha-like version which can't easily be put on the workshop as it is.
* Bugs should be reported in the [issues section](https://gitlab.com/redmser/MserDuck/issues), that's the easiest for everyone to manage and keep track of.
* Merge requests are the way to go! They don't require much management from my side and ensure that the project won't be messed up.
* Any dedicated developers that volunteer to manage the mod's well-being on GitLab is free to message me about being a trusted member of the project. You will need a solid understanding of C#, the codebase and a good reputation in the Duck Game community.
* Want to thank me for my work? Donations [\[1\]](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=B7K6KJ3VECKKG&lc=US) [\[2\]](https://blockchain.info/payment_request?address=1JdK6dMdBU19c2jSCSqWcSKMAJk2T8RXqU&message=Donations%20to%20RedMser) are very welcome and will support future projects of mine.

# Credits

* RedMser - Responsible for pretty much the entire development of this mod.
* 9joao6, wekateka, Marukyu and all my other friends - Thank you all for "playtesting", ideas and helping with the mod's development!
* superjoebob - Thanks for creating Duck Game! <3
* rickyah - Created the [INI File Parser](https://github.com/rickyah/ini-parser) library
* RichardD2 - Created the [NTFS Streams](https://github.com/RichardD2/NTFS-Streams) library
